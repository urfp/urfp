/* URFP - UR Function Protocol

Copyright (c) 2015 Kolja Waschk

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#ifndef URFP_TYPES_H
#define URFP_TYPES_H 1

#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

#define URFP_CODE(x,y) ((((unsigned)y)<<8)|(unsigned)x)
#define URFP_ACTCODE(z) (((unsigned)z)&0xff)
#define URFP_ELEMCODE(z) (((unsigned)z>>8)&0xff)

#if URFP_WITH_CHANNELS
#define URFP_NUM_ELEMTYPES 5
#else
#define URFP_NUM_ELEMTYPES 4
#endif
typedef enum {
    ELEMTYPE_VARIABLE=0,
    ELEMTYPE_FUNCTION=1,
    ELEMTYPE_SOURCE=2,
#if URFP_WITH_CHANNELS
    ELEMTYPE_BLOB=3,
    ELEMTYPE_CHANNEL=4
#else
    ELEMTYPE_BLOB=3
#endif
}
urfp_elemtype_t;

/** Return values for all public UrFP functions */
typedef enum
{
    URFP_OK = 0,
    URFP_ERR = 1,
    URFP_AGAIN = 2,
    URFP_NOAUTH = 3,
    URFP_RANGE = 4
}
urfp_retval_t;

typedef enum
{
    /*
    When telling an other end about data type of output
    or expected arguments, historically three schemes exist.

    The newest scheme consists of a prefix byte followed
    by information as defined above. The prefix byte tells

     - input (argument) or output (result) element
     - comes with or without element name
     - ...  TBD

    The older scheme consists of a null-byte-terminated string of chars as follows,
    which are no valid prefix bytes, and describe output elements:

     {,} struct start, end
     [   array start followed by 4 bytes int32 (array length -1 or > 0)
     ]   array end
     s   c-string
     o   bool (one byte zero or nonzero)
     I,H,B uint32, uint16, uint8
     i,h,b int32, int16, int8

    The oldest scheme consists of only one single char, not even null-terminated,
    as above, describing a scalar type soIHBihb.
    */

    /* 0x00 reserved as universal separator/ end marker */

    /* 0x01..0x3F reserved for custom types */

    URFP_STRUCT_START = 0x40,
    URFP_ARRAY_START  = 0x41,
    URFP_ARRAY_END    = 0x42,
    URFP_STRUCT_END   = 0x43,
    URFP_BOOL         = 0x44,
    URFP_STRING       = 0x45,
    URFP_FLOAT        = 0x46,
    URFP_DOUBLE       = 0x47,
    URFP_INT          = 0x48, /* to be completed with ld2(wordlen) in LSB */
        URFP_INT8     = 0x48, /* = URFP_INT | ld2(1) = URFP_INT*/
        URFP_INT16    = 0x49, /* = URFP_INT | ld2(2) */
        URFP_INT32    = 0x4A, /* = URFP_INT | ld2(4) */
        URFP_INT64    = 0x4B, /* = URFP_INT | ld2(8) */
    URFP_UNSIGNED     = 0x4C, /* to be completed with ld2(wordlen) in LSB */
        URFP_UINT8    = 0x4C, /* = URFP_UNSIGNED | ld2(1) = URFP_UNSIGNED */
        URFP_UINT16   = 0x4D, /* = URFP_UNSIGNED | ld2(2) */
        URFP_UINT32   = 0x4E, /* = URFP_UNSIGNED | ld2(4) */
        URFP_UINT64   = 0x4F, /* = URFP_UNSIGNED | ld2(8) */

    /* 0x50..0xFF reserved for custom types */

    URFP_TYPE_MASK    = 0xff, /* used to cut off the following extra information */

    URFP_TYPE_PREFIX  = (0x80u<<8), /* Differentiates this type information from older one-byte method */
    URFP_ARG          = (0x01u<<8), /* Indicates that type information describes an argument not result */
    URFP_NAMED        = (0x02u<<8), /* Not yet in use: Indicates that a name string follows type information */

    URFP_TYPE_MODE    = (0xffu<<8), /* used to cut off the actual type byte */
}
urfp_argtype_t;

typedef enum
{
    /* These 'environment modes' are used to remember the state of an transaction environment */

    URFP_ENV_IDLE     = 0, /* initial value, not used in action */
    URFP_ENV_REQ_IN   = 1, /* growing (incomplete) incoming request (receiving request) */
    URFP_ENV_REPLY    = 2, /* incoming request currently being answered (building and transmitting reply) */
    URFP_ENV_REQ_OUT  = 3  /* outgoing request (no matter whether it's currently being sent or answered) */
}
urfp_envmode_t;

typedef enum
{
    NO = 0,     /* no dry run */
    INPUT = 1,  /* first phase of dry run: describe expected input (function arguments) */
    OUTPUT = 2,  /* second phase of dry run: describe expected output (values, function results) */
    OUT_ANON = 3 /* same as OUTPUT but with no names given for transfer efficiency, assumed to be already known */
}
urfp_dry_run_t;

typedef struct urfp_transport urfp_transport_t;
typedef struct urfp_app urfp_app_t;
typedef struct urfp_channel urfp_channel_t;
typedef struct urfp_source urfp_source_t;
typedef struct urfp_transaction_env urfp_transaction_env_t;
typedef struct urfp_elem_descriptor urfp_elem_descriptor_t;

/* urfp_elem_list_t is an array of pointers to const urfp_elem_descriptor_t instances */
typedef const urfp_elem_descriptor_t** urfp_elem_list_t;

typedef urfp_retval_t (*urfp_put_continue_callback_t)(urfp_transaction_env_t* env);
typedef urfp_retval_t (*urfp_get_continue_callback_t)(urfp_transaction_env_t* env);
typedef urfp_retval_t (*urfp_get_finish_callback_t)(urfp_transaction_env_t* env);
typedef urfp_retval_t (*urfp_next_elem_from_list_getter_t)(urfp_elem_list_t* base, int* index, const urfp_elem_descriptor_t** elem);

#ifdef __cplusplus
}
#endif

#endif
