/* URFP - UR Function Protocol

Copyright (c) 2015 Kolja Waschk

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

/**
@addtogroup urfp_console URFP serial console
*/

#ifndef URFP_CONSOLE_H
#define URFP_CONSOLE_H 1

#include "urfp_server.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifndef URFP_CONSOLE_MAX_CONTENT_LENGTH
#define URFP_CONSOLE_MAX_CONTENT_LENGTH 128
#endif

typedef struct urfp_console_env_data urfp_console_env_data_t;
struct urfp_console_env_data
{
    bool allocated;
    bool keep_allocated;
    bool oob;
    char prefix;
    char content[URFP_CONSOLE_MAX_CONTENT_LENGTH];
    size_t content_length;
    bool content_complete;
    urfp_retval_t get_succeeded;
};

#ifndef URFP_CONSOLE_MAX_REPLY_ENVS
#define URFP_CONSOLE_MAX_REPLY_ENVS 4
#endif

typedef struct urfp_console urfp_console_t;
struct urfp_console
{
    const char* prompt;
    size_t prompt_length;
    bool want_prompt;
    volatile bool echo;
    volatile char last_char_out;
    volatile char last_char_in;
    volatile int inbuf_length;
    char* inbuf;
    int inbuf_size;
    int arg_offset;
    char last_input_tag;
    char last_oob_tag;

    urfp_transaction_env_t transaction_env [URFP_CONSOLE_MAX_REPLY_ENVS];
    urfp_console_env_data_t transaction_env_data[URFP_CONSOLE_MAX_REPLY_ENVS];

    /* Your implementation of char and string transmission to the client */

    /**
     * An arbitrary pointer that will be passed to the callbacks when calling them
     */
    void* arg;

    urfp_retval_t (*output_pretest)(void* arg, size_t len);

    void (*output_char)(void* arg, char c);
    void (*output_string)(void* arg, const char* s, size_t len);

    /* Please provide functions to protect concurrent access to the inbuf */
    void (*lock_inbuf)(void* arg);
    void (*release_inbuf)(void* arg);
};

/** Call if you got another char input from client. If you intend to use the OOB
 * functionality of UrFP, call this function only in sections with access to the
 * inbuf protected using the same mechanism as used by lock/release_inbuf() callback */
extern urfp_retval_t urfp_console_input_char(urfp_console_t* con, char c);

/** Call this once to have the urfp_instance initialized and hand over pointers
 * to your callback functions, prompt and buffer for client input */
extern urfp_retval_t urfp_console_init(urfp_console_t* con, urfp_app_t* app,
    const char* prompt, char* inbuf, size_t inbuf_size, bool echo);

/** Work in progress: OOB output */
extern void urfp_console_set_channel(urfp_console_t* con, urfp_channel_t* ch);

/** Work in progress: continue backgrounded reply processing */
extern urfp_retval_t urfp_console_resume_replies(urfp_console_t* con);

extern const urfp_transport_t urfp_console_transport;

#ifdef __cplusplus
}
#endif

#endif

/**
@}
*/
