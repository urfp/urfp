/** URFP - Universal Remote Function Protocol

@file  urfp_http.c
@brief Stream parser for HTTP requests and responses
@copyright 2015 Kolja Waschk

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

/** @ingroup urfp_http
@{
*/

/* for memmem */
#ifndef _GNU_SOURCE
#define _GNU_SOURCE 1
#endif
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

/* for write/close */
#include <unistd.h>
/* for stat */
#include <sys/stat.h>

#include <ctype.h>

#include "urfp_api.h"
#include "urfp_http.h"
#include "urfp_server.h"

void urfp_http_init(urfp_http_state_t* state, const urfp_http_handlers_t* handlers, void* arg)
{
    memset(state, 0, sizeof(urfp_http_state_t));

    state->respreq_state = RESPREQ_START;
    state->chunk_state = NOT_CHUNKED;
    state->top.state = LINE_CONTENT;
    state->http_1_1 = false;
    state->handlers = handlers;
    state->handlers_arg = arg;
    state->output_format = OUTFMT_JSON;
}

/******************************************** PARSER *************************************/

static void urfp_http_report_state(urfp_http_state_t *state, urfp_line_state_t ls, int i, int n)
{
#if 0
    printf("%d", i);

    switch(state->respreq_state)
    {
    case RESPREQ_BODY:  printf(" BODY"); break;
    case RESPREQ_START: printf(" START"); break;
    case RESPREQ_HEADER:printf(" HEADER"); break;
    default: break;
    };

    switch(ls)
    {
    case LINE_WHITE:   printf(" WHITE"); break;
    case LINE_CONTENT: printf(" CONTENT"); break;
    case LINE_CR:      printf(" CR"); break;
    case LINE_CRLF:    printf(" CRLF"); break;
    default: break;
    };

    printf(" %d", n);

    switch(state->chunk_state)
    {
    case NOT_CHUNKED: printf(" NOCHUNK"); break;
    case CHUNK_START: printf(" CHUNKKSTART"); break;
    case CHUNK_BODY:  printf(" CHUNKBODY"); break;
    default: break;
    };

#if URFP_HTTP_WITH_FORMS
    switch(state->form_type)
    {
    case FORM_NONE: printf(" NOFORM"); break;
    case FORM_URLENCODED: printf(" URIFORM"); break;
    case FORM_MULTIPART: printf(" MULTIPART"); break;
    default: break;
    };

#if URFP_HTTP_WITH_MULTIPART
    switch(state->multi_state)
    {
    case MULTI_START:  printf(" MULTI_NO"); break;
    case MULTI_HEADER: printf(" MULTI_HEADER"); break;
    case MULTI_BODY:   printf(" MULTI_BODY"); break;
    case MULTI_END:    printf(" MULTI_END"); break;
    default: break;
    };
#endif
#endif

    printf("\n");
#endif
}

static unsigned char urfp_http_uri_hex2nibble(char hexdigit)
{
    if (hexdigit >= 'A' && hexdigit <= 'F')
    {
        return hexdigit - 'A' + 10;
    }
    else if (hexdigit >= 'a' && hexdigit <= 'f')
    {
        return hexdigit - 'a' + 10;
    }
    else if (hexdigit >= '0' && hexdigit <= '9')
    {
        return hexdigit - '0';
    }
    else
    {
        return 0;
    }
}

static int urfp_http_uri_decode_inplace(char* string, int orig_len)
{
    int i;
    int decoded = 0;
    char percent = 0;
    unsigned char decchar;
    for (i=0; i<orig_len; i++)
    {
        if (percent == 0)
        {
            if (string[i] == '%')
            {
                percent = 1;
            }
            else
            {
                string[decoded] = (string[i] == '+') ? 0x20 : string[i];
                decoded ++;
            }
        }
        else if (percent == 1)
        {
            decchar = urfp_http_uri_hex2nibble(string[i]);
            percent ++;
        }
        else if (percent == 2)
        {
            decchar = (decchar << 4) | urfp_http_uri_hex2nibble(string[i]);
            string[decoded] = decchar;
            decoded ++;
            percent = 0;
        }
    }
    return decoded;
}

static void urfp_http_parse_name_value(char* query, int query_len, urfp_http_state_t* state)
{
    int i;
    for (i=0; i<query_len; i++)
    {
        if (query[i] == '=')
        {
            int name_len = urfp_http_uri_decode_inplace(query, i);
            int value_len = urfp_http_uri_decode_inplace(query+i+1, query_len-i-1);
            state->handlers->query(state->handlers_arg, query, name_len, query+i+1, value_len);
            break;
        }
    }
    if (i >= query_len) /* no '=' */
    {
        int name_len = urfp_http_uri_decode_inplace(query, query_len);
        state->handlers->query(state->handlers_arg, query, name_len, "", 0);
    }
}

static void urfp_http_parse_uri_query(char* query, int query_len, urfp_http_state_t* state)
{
    int i;
    int prev_amp = -1;

    for (i=0; i<query_len; i++)
    {
        if (query[i] == '&')
        {
            if (i>0)
            {
                urfp_http_parse_name_value(query+prev_amp+1, i-1-prev_amp, state);
            }
            prev_amp = i;
        }
    }
    if (i > prev_amp)
    {
        urfp_http_parse_name_value(query+prev_amp+1, i-1-prev_amp, state);
    }
}

/* Generic method to parse an entity char-by-char that has
 *
 * - lines like a RFC822 message, HTTP request or response etc. with CR/LF at the end,
 * - eventually continued on the next line commencing with whitespace,
 * - ending in an empty line and followed by arbitrary content.
 *
 * While it looks for CR/LF, it copies line content to the buffer in urfp_header_buf_t
 * and updates its parser state. Whenever a header line is complete, it calls the
 * parse_header callback function with the header line (without CR/LF).
 *
 * It returns true as long as it parses headers, and false after end of headers. It
 * must not be called again on the same stream after it returned false.
 */

bool urfp_http_parse_headers(urfp_header_buf_t* hebuf, char c,
    void (*parse_header)(void* arg, char* line, int line_len), void* arg)
{
    if (c == 10)
    {
        if (hebuf->state != LINE_CR)
        {
            //printf("Warning: LF received with no prior CR\n");
        }

        hebuf->state = LINE_CRLF;

        if (hebuf->len == 0)
        {
            (*parse_header)(arg, "", 0);
            return false;
        }
    }
    else
    {
        if (hebuf->state == LINE_CRLF)
        {
            if (c == 9 || c == 32)
            {
                hebuf->line[hebuf->len] = c;
                hebuf->len ++;

                hebuf->state = LINE_WHITE;
            }
            else
            {
                /* Now that a new header part commences non-white, it is high time to process the previous.
                 * It was necessary to wait that long so that continuation is handled correctly.. */

                (*parse_header)(arg, hebuf->line, hebuf->len);

                hebuf->len = 0;

                if (c == 13)
                {
                    hebuf->state = LINE_CR;
                }
                else
                {
                    hebuf->state = LINE_CONTENT;
                }
            }
        }

        if (hebuf->state == LINE_CONTENT)
        {
            if (c == 13)
            {
                hebuf->state = LINE_CR;
            }
            else if (hebuf->len < sizeof(hebuf->line))
            {
                hebuf->line[hebuf->len] = c;
                hebuf->len ++;
            }
        }
    }

    return true;
}

/******************************************** MULTIPART PARSER *************************************/

#if URFP_HTTP_WITH_MULTIPART
/**
 * Used internally by urfp_http_parse_multipart().
 * It returns the number of bytes processed (which can be discarded then)
 */

static int urfp_http_parse_multipart_data(uint8_t* buf, int buflen, urfp_multipart_t* multi)
{
    int i = 0;
    if (multi->state == MULTI_HEADER)
    {
        bool still_header = true;
        while (i<buflen && still_header)
        {
            still_header = urfp_http_parse_headers(&(multi->part), buf[i], multi->callbacks->header, multi->callbacks_arg);
            i++;
        }

        if (!still_header)
        {
            multi->state = MULTI_BODY;

            multi->part.state = LINE_CONTENT;
            multi->part.len = 0;
        }
    }

    if (i < buflen)
    {
        i += (multi->callbacks->body)(multi->callbacks_arg, buf+i, buflen-i);
    }

    return i;
}

/**
 * Initialize the state for urfp_http_parse_multipart()
 *
 * @arg multi        Pointer to the state
 * @arg callbacks    Pointer to functions to be called during parsing
 * @arg arg          Arbitrary argument will be passed to the callbacks
 * @arg boundary     Pointer to boundary string that separates the parts
 * @arg boundary_len Length of the boundary string
 */

void urfp_http_init_multipart(urfp_multipart_t* multi, const urfp_multi_callbacks_t* callbacks, void* arg, char* boundary, int boundary_len)
{
    multi->callbacks = callbacks;
    multi->callbacks_arg = arg;

    memcpy(multi->boundary+2, boundary, boundary_len);

    /* prepend dashes since they're needed in search always */
    multi->boundary[0] = '-';
    multi->boundary[1] = '-';
    multi->boundary_len = boundary_len+2;
    multi->boundary_matched = 0;

    multi->state = MULTI_START;
    multi->part.state = LINE_CONTENT;
    multi->part.len = 0;
}

/**
 * The multipart parser is used internally for dissecting form/multipart,
 * but also can be used from applications (e.g. to process files with multipart
 * content or form data of type multipart/mixed).
 *
 * Be sure to call urfp_http_init_multipart() beforehand and urfp_http_finish_multipart() afterwards.
 */

int urfp_http_parse_multipart(urfp_multipart_t* multipart, char* line, int line_len)
{
    if (multipart->state != MULTI_END)
    {
        void *boufound = memmem(line, line_len, multipart->boundary, multipart->boundary_len);
        while (boufound)
        {
            /* Boundary found. But does it have CR/LF before? And -- and/or CR/LF afterwards? */

            size_t nbefore = (size_t)((char*)boufound - line);

            /* Check space *before* boundary */

            bool crlf_before = false;
            if (multipart->state == MULTI_START)
            {
                /* Anything before it can be ignored. */
                /* Also, at the beginning, no CR/LF is needed */
                crlf_before = true;
            }
            else
            {
                /* Check CR/LF before */
                if ((nbefore >= 2)
                    && (line[nbefore-2] == 13)
                    && (line[nbefore-1] == 10))
                {
                    crlf_before = true;
                }
            }

            /* Check space *after* boundary */

            int boundary_tail = 0;
            if (crlf_before)
            {

                if ((line_len >= nbefore + multipart->boundary_len + 2)
                    && (memcmp((void *)((char *)boufound + multipart->boundary_len), "\r\n", 2) == 0))
                {
                    boundary_tail = 2;
                }
                else if ((line_len >= nbefore + multipart->boundary_len + 4)
                    && (memcmp((void *)((char *)boufound + multipart->boundary_len), "--\r\n", 2) == 0))
                {
                    boundary_tail = 4;
                }
            }

            /* If boundary is completely there, act accordingly */

            if (crlf_before && boundary_tail > 0)
            {
                if (multipart->state != MULTI_START)
                {
                    /* The return value of parse_multipart_data is ignored, because if the callee
                     * couldn't deal with those last bytes, it never will. There'll come no more. */
                    (void)urfp_http_parse_multipart_data((uint8_t*)line, nbefore - 2, multipart);
                    (multipart->callbacks->end)(multipart->callbacks_arg);
                }

                if (boundary_tail == 2)
                {
                    multipart->state = MULTI_HEADER;
                    (multipart->callbacks->start)(multipart->callbacks_arg);

                    multipart->part.len = 0;
                }
                else /* tail len = 4, --\r\n */
                {
                    multipart->state = MULTI_END;
                }

                int nprocessed = nbefore + multipart->boundary_len + boundary_tail;
                line_len -= nprocessed;
                memmove(line, line + nprocessed, line_len);

                /* There might be another boundary already buffered. */
                boufound = memmem(line, line_len, multipart->boundary, multipart->boundary_len);
            }
            else
            {
                /* No more complete boundary yet. */
                boufound = NULL;
            }
        }

        if (boufound == NULL)
        {
            /* No boundary found. How much of the data in buffer can be
             * discarded resp. passed to the part handler yet, without
             * touching a possible partial boundary that will be recognizable
             * next time? */

            if (line_len > 2 + multipart->boundary_len + 1)
            {
                size_t off_view = line_len - 2 - multipart->boundary_len - 1;
                if (multipart->state == MULTI_START)
                {
                    /* discard that */
                }
                else
                {
                    int i = urfp_http_parse_multipart_data((uint8_t*)line, off_view, multipart);
                    if (i > 0)
                    {
                        /* Only discard the bytes that have been effectively processed. */
                        off_view = i;
                    }
                    else
                    {
                        /* Something *has* to be discarded. Ignore the fact that app couldn't handle. */
                    }
                }
                line_len -= off_view;
                memmove(line, line + off_view, line_len);
            }
        }
    }
    else /* multipart->state == MULTI_END */
    {
    	line_len = 0;
    }

    return line_len;
}

/**
 * To be called when there is no more part data.
 *
 * @arg multi        Pointer to the state
 */
void urfp_http_finish_multipart(urfp_multipart_t* multi)
{
    if (multi->state != MULTI_START && multi->state != MULTI_END)
    {
        (multi->callbacks->end)(multi->callbacks_arg);
    }
}


/******************************************** FORM/MULTIPART PARSER *************************************/

void urfp_http_handle_part_start(void* arg)
{
    urfp_http_state_t* state = (urfp_http_state_t*)arg;
   // printf("PART START\n");

    state->mpquery_name = NULL;
    state->mpquery_origname = NULL;
    state->mpquery_file = -1;
}

void urfp_http_handle_part_header(void* arg, char* header_line, int len)
{
    urfp_http_state_t* state = (urfp_http_state_t*)arg;

    if (strncasecmp(header_line, "content-disposition:", 20) == 0)
    {
        char *s;
        if((s = memmem(header_line, len, "; name=\"", 8)) != NULL)
        {
            char *e;
            s += 8;
            for (e = s; *e != '"' && e < header_line + len; e++)
            {
            }

            if (e > s)
            {
                state->mpquery_name = strndup(s, e-s);
            }
        }

        if((s = memmem(header_line, len, "; filename=\"", 12)) != NULL)
        {
            char *e;
            s += 12;
            for (e = s; *e != '"' && e < header_line + len; e++)
            {
            }

            if (e > s)
            {
                state->mpquery_origname = strndup(s, e-s);
            }

            strcpy(state->mpquery_filename, "/tmp/urfpXXXXXX");
            state->mpquery_file = mkstemp(state->mpquery_filename);

           // printf("PART TO FILE %s(%d)\n", state->mpquery_filename, state->mpquery_file);
        }
    }

   //  printf("PART HEADER(%u)\n", len);
}

int urfp_http_handle_part_body(void* arg, uint8_t* data, int len)
{
    urfp_http_state_t* state = (urfp_http_state_t*)arg;

    if (state->mpquery_file >= 0)
    {
        (void)write(state->mpquery_file, data, len);
    }
    else
    {
        if (state->multi.part.len + len > sizeof(state->multi.part.line))
        {
            len = sizeof(state->multi.part.line) - state->multi.part.len;
        }
        memcpy(state->multi.part.line + state->multi.part.len, data, len);
        state->multi.part.len += len;
    }

   // printf("PART BODY(%u)\n", len);
    return len;
}

void urfp_http_handle_part_end(void* arg)
{
    urfp_http_state_t* state = (urfp_http_state_t*)arg;

    if (state->mpquery_name != NULL)
    {
        if (state->mpquery_file >= 0)
        {
            close(state->mpquery_file);

            /* As a special case, browsers might have left the filename empty
             * and not transferred any data, this is interpreted as 'no file uploaded'
             * and no actual query callback is done (TBD) */

            bool real_file = true;
            if (state->mpquery_origname == NULL || state->mpquery_origname[0] == 0)
            {
                struct stat buf;
                if ((stat(state->mpquery_filename, &buf) == 0) && (buf.st_size == 0))
                {
                    real_file = false;
                    unlink(state->mpquery_filename);
                }
            }

            if (real_file)
            {
                state->handlers->query(state->handlers_arg,
                    state->mpquery_name, strlen(state->mpquery_name),
                    state->mpquery_filename, strlen(state->mpquery_filename));
            }

            state->mpquery_file = -1;
        }
        else
        {
            state->handlers->query(state->handlers_arg,
                state->mpquery_name, strlen(state->mpquery_name),
                state->multi.part.line, state->multi.part.len);
        }

        free(state->mpquery_name);
    }

    if (state->mpquery_origname != NULL)
    {
        free(state->mpquery_origname);
    }

   // printf("PART END\n");
}

static const urfp_multi_callbacks_t urfp_http_form_multipart_callbacks =
{
     .start  = urfp_http_handle_part_start,
     .header = urfp_http_handle_part_header,
     .body   = urfp_http_handle_part_body,
     .end    = urfp_http_handle_part_end,
};

#endif /* with MULTIPART */

/******************************************** POST-METHOD PARSER *************************************/

/*
 * The form parser handles all the body data, given to it from the top-level parser.
 * It separates the header lines from the body.
 * In case of application/x-www-form-urlencoded, the body is then parsed as a query string.
 * A form/multipart is further handed over to the multipart parser.
 */

static void urfp_http_parse_form(uint8_t* buf, int buflen, urfp_http_state_t* state)
{
#if URFP_HTTP_WITH_FORMS
    if (state->form_type == FORM_NONE)
#endif
    {
        state->handlers->body(state->handlers_arg, buf, buflen);
    }
#if URFP_HTTP_WITH_FORMS
    else
    {
        int i = 0;
        char *line = state->form.line;
        int line_len = state->form.len;

        while (i < buflen)
        {
            size_t ntocopy = sizeof(state->form.line) - line_len;
            if (buflen - i < ntocopy)
            {
                ntocopy = buflen - i;
            }

            memcpy(line + line_len, buf + i, ntocopy);
            line_len += ntocopy;
            i += ntocopy;

            if (state->form_type == FORM_URLENCODED)
            {
                /* The complete query string will be evaluated at the end */
                /* Nothing will be freed here by any processing */

                /* If not all has been copied, bail out. TBD: error handling? */
                i = buflen;
            }
#if URFP_HTTP_WITH_MULTIPART
            /* must be FORM_MULTIPART */
            else
            {
                line_len = urfp_http_parse_multipart(&(state->multi), line, line_len);
            }
#endif /* with MULTIPART */
        }

        state->form.len = line_len;
    }
#endif /* with FORMS */
}

static void urfp_http_finish_form(urfp_http_state_t* state)
{
#if URFP_HTTP_WITH_FORMS
    if (state->form_type == FORM_URLENCODED && state->handlers->query != NULL)
    {
        urfp_http_parse_uri_query(state->form.line, state->form.len, state);
    }
#if URFP_HTTP_MULTIPART
    else if (state->form_type == FORM_MULTIPART)
    {
        urfp_http_finish_multipart(state->multi);
    }
#endif
    state->form_type = FORM_NONE;
#endif
    state->handlers->finish(state->handlers_arg);
}

/******************************************** BASIC/GET-METHOD HTTP *************************************/

/**
 * Used by urfp_http_parse() to interpret particular header lines.
 */
static void urfp_http_parse_header(void* arg, char* line, int line_len)
{
    urfp_http_state_t* state = (urfp_http_state_t*)arg;

    if (state->respreq_state == RESPREQ_START)
    {
        /* Start line with request GET/PUT/POST etc. or HTTP/1.1 status response */

        state->content_length = 0;
        state->chunk_state = NOT_CHUNKED;
        state->handlers->start(state->handlers_arg, line, line_len);

        /* If this is a GET or POST request including a query string, user
         * MIGHT want us to parse it for him. He tells us by providing a 'query'
         * callback. */

        if ((state->handlers->query != NULL) && (line_len>=10))
        {
            if(((strncasecmp(line, "get ", 4) == 0) || (strncasecmp(line, "post ", 5) == 0))
                && (strncasecmp(line + line_len - 9, " http/1.", 8) == 0))
            {
                int i=4;
                for (i=4; i<line_len - 10; i++)
                {
                    if (line[i] == '?')
                    {
                        int qlen = 0;
                        /* Ignore fragments at end of line */
                        while (qlen < line_len - i - 10 && line[i + 1 + qlen] != '#')
                        {
                            qlen++;
                        }
                        urfp_http_parse_uri_query(line + i + 1, qlen, state);
                        break;
                    }
                }
            }
        }
    }
    else
    {
        /* Header line end. \todo Handle continuation correctly! */

        if (line != NULL && line_len > 0)
        {
			if (strncasecmp(line, "content-length:", 15) == 0)
			{
				line[line_len] = 0;
				state->content_length = strtol(line + 15, NULL, 10);
			}
#if URFP_HTTP_WITH_FORMS
			else if (strncasecmp(line, "content-type:", 13) == 0)
			{
				line[line_len] = 0;

				if ((state->handlers->query != NULL)
					&& (line_len >= 14)
					&& (strcasecmp(line + 14, "application/x-www-form-urlencoded") == 0))
				{
					state->form_type = FORM_URLENCODED;
					state->form.len = 0;
					state->form.state = LINE_CONTENT;
				}
#if URFP_HTTP_WITH_MULTIPART
				else if((line_len >= 14)
						&& (strncasecmp(line + 14, "multipart/form-data", 19) == 0))
				{
					state->form_type = FORM_MULTIPART;

					int i;
					int n = 0;
					for (i=14+19; i<line_len-9 && n==0; i++)
					{
						if (strncasecmp(line + i, "boundary=", 9) == 0)
						{
							n = line_len-9 - i;
							if (n >= sizeof(state->multi.boundary)-2)
							{
								n = sizeof(state->multi.boundary)-2;
							}

							if (state->handlers->parts != NULL)
							{
								urfp_http_init_multipart(&(state->multi), state->handlers->parts, state->handlers_arg, line+9+i, n);
							}
							else
							{
								urfp_http_init_multipart(&(state->multi), &urfp_http_form_multipart_callbacks, state, line+9+i, n);
							}
						}

					}
				}
#endif /* WITH_MULTIPART */
			}
#endif /* WITH_FORMS */
			else if (strncasecmp(line, "transfer-encoding:", 18) == 0)
			{
				int j = 18;
				while(j<line_len && (line[j]==9 || line[j]==32)) j++;
				if (j<line_len+7 && strncasecmp(line+j, "chunked", 7) == 0)
				{
					state->chunk_state = CHUNK_START;
				}
			}
        }
        state->handlers->header(state->handlers_arg, line, line_len);
    }

    state->respreq_state = RESPREQ_HEADER;
}

/**
 * The top-level parser parses header lines,
 * and itself handles some information from the headers
 * such as Transfer-encoding: chunked,
 * Content-Length or or Content-Type *form* and acts accordingly.
 *
 * It can be applied to requests or responses likewise.
 *
 * In contrast to the multipart parser, the body length is determined
 * by either Content-Length header or chunk headers here, whereas it
 * is determined by boundarys around the data for multipart data.
 * (therefore it needs TWO parsers)
 *
 */
void urfp_http_parse(uint8_t* buf, int buflen, urfp_http_state_t *state)
{
    int i = 0;
    char *line = state->top.line;
    urfp_header_buf_t * const hebuf = &(state->top);

    urfp_http_report_state(state, hebuf->state, i, hebuf->len);

    while (i < buflen)
    {
        /* Are we yet receiving a request body, e.g. for a POST request? */
        if (state->respreq_state == RESPREQ_BODY)
        {
            /* Most common case: Content-length was specified in request header */
            if (state->chunk_state == NOT_CHUNKED)
            {
                int new_bytes_received = buflen - i;
                int remaining_body_bytes = state->content_length - state->body_length;
                if (new_bytes_received <= remaining_body_bytes)
                {
                    urfp_http_parse_form(buf + i, new_bytes_received, state);
                    i += new_bytes_received; // := buflen
                    state->body_length += new_bytes_received;
                }
                else
                {
                    urfp_http_parse_form(buf + i, remaining_body_bytes, state);
                    i += remaining_body_bytes;
                    state->body_length += remaining_body_bytes; // := content_length
                }

                if (state->body_length >= state->content_length)
                {
                    urfp_http_finish_form(state);
                    state->respreq_state = RESPREQ_START;
                    hebuf->state = LINE_CONTENT;
                }
            }
            /* Next case handles Transfer-encoding: chunked after a chunk size has already been read */
            else if (state->chunk_state == CHUNK_BODY)
            {
                int new_bytes_received = buflen - i;
                int remaining_chunk_bytes = state->chunk_size - state->body_length;
                if (new_bytes_received <= remaining_chunk_bytes)
                {
                    urfp_http_parse_form(buf + i, new_bytes_received, state);
                    i += new_bytes_received;
                    state->body_length += new_bytes_received;
                }
                else if (remaining_chunk_bytes > 0)
                {
                    urfp_http_parse_form(buf + i, remaining_chunk_bytes, state);
                    i += remaining_chunk_bytes;
                    state->body_length += remaining_chunk_bytes;
                }
                else /* chunk complete, look for next size info or respond if zero */
                {
                    uint8_t c = buf[i++];

                    if (c == 13)
                    {
                        hebuf->state = LINE_CR;
                    }
                    else if (c == 10)
                    {
                        hebuf->state = LINE_CRLF;
                        state->content_length += state->body_length;

                        if (state->chunk_size == 0)
                        {
                            urfp_http_finish_form(state);
                            state->respreq_state = RESPREQ_START;
                            hebuf->state = LINE_CONTENT;
                        }
                        else
                        {
                            state->chunk_state = CHUNK_START;
                        }
                    }
                }
            }
            /* Last case handles Transfer-encoding: chunked, expecting a line with ASCII hex chunk size spec */
            else /* chunk_state is CHUNK_START */
            {
                uint8_t c = buf[i++];

                if (c == 10)
                {
                    if (hebuf->state != LINE_CR)
                    {
                        // fprintf(stderr, "LF after nothing or non-CR\n");
                    }
                    line[hebuf->len] = 0;
                    state->chunk_size = strtol(line, NULL, 16); // hex!
                    state->chunk_state = CHUNK_BODY;
                    state->body_length = 0;
                    hebuf->state = LINE_CRLF;
                    hebuf->len = 0;
                }
                else if (c == 13)
                {
                    if (hebuf->state == LINE_CRLF)
                    {
                        // fprintf(stderr, "Empty chunk start line\n");
                    }
                    hebuf->state = LINE_CR;
                }
                else
                {
                    if (hebuf->len < sizeof(state->top.line))
                    {
                        line[hebuf->len] = c;
                        hebuf->len ++;
                    }
                    hebuf->state = LINE_CONTENT;
                }
            }
        }
        else /* respreq_state == RESPREQ_START || respreq_state == RESPREQ_HEADER */
        {
            /* Not receiving body. Parse headers */

            uint8_t c = buf[i++];

            if (!urfp_http_parse_headers(&state->top, c, urfp_http_parse_header, state))
            {
                if (state->content_length == 0 && state->chunk_state == NOT_CHUNKED)
                {
                    state->handlers->finish(state->handlers_arg);
                    state->respreq_state = RESPREQ_START;
                    hebuf->state = LINE_CONTENT;

#if URFP_HTTP_WITH_FORMS
                    state->form_type = FORM_NONE;
                    state->form.state = 0;
                    state->form.len = 0;
#endif
                }
                else
                {
                    state->respreq_state = RESPREQ_BODY;
                    state->body_length = 0;
                }
            }
        }

        urfp_http_report_state(state, hebuf->state, i, hebuf->len);
    }
}

/******************************************** URFP TRANSPORT *************************************/

void urfp_http_set_output_format(urfp_transaction_env_t* env, urfp_http_output_format_t fmt)
{
    urfp_http_state_t* state = (urfp_http_state_t*)(env->transport_conn);
    state->output_format = fmt;
}

static bool urfp_http_output_on_hold(urfp_transaction_env_t* env)
{
    urfp_http_state_t* state = (urfp_http_state_t*)(env->transport_conn);
    return state->handlers->on_hold(state->handlers_arg);
}

int urfp_http_get_next_arg(urfp_transaction_env_t* env, char* buf, size_t maxsize)
{
    urfp_http_env_data_t* data = (urfp_http_env_data_t*)(env->transport_data);
    data->get_succeeded = URFP_OK;

    urfp_http_state_t* state = (urfp_http_state_t*)(env->transport_conn);
    return state->handlers->next_arg(state->handlers_arg, buf, maxsize);
}

bool urfp_http_more_args_available(urfp_transaction_env_t* env)
{
    urfp_http_state_t* state = (urfp_http_state_t*)(env->transport_conn);
    return state->handlers->more_args(state->handlers_arg);
}

int urfp_http_get_arg_name(urfp_transaction_env_t* env, char* buf, size_t maxlen)
{
    urfp_http_state_t* state = (urfp_http_state_t*)(env->transport_conn);
    return state->handlers->next_name(state->handlers_arg, buf, maxlen);
}

int urfp_http_get_string_arg(urfp_transaction_env_t* env, const char* name, char* buf, size_t maxlen)
{
    return urfp_http_get_next_arg(env, buf, maxlen);
}

int32_t urfp_http_get_int_arg(urfp_transaction_env_t* env, size_t szof, const char* name)
{
    char arg[16];
    int32_t r;
    urfp_http_env_data_t* data = (urfp_http_env_data_t*)(env->transport_data);
    r = urfp_parse_int_arg(arg, sizeof(arg), urfp_http_get_next_arg(env, arg, sizeof(arg)), &(data->get_succeeded));
    if ((szof == 1 && (r < -128 || r >= 128)) || (szof == 2 && (r < -32768 || r >= 32768)))
    {
        data->get_succeeded = URFP_RANGE;
    }
    return r;
}

uint32_t urfp_http_get_unsigned_arg(urfp_transaction_env_t* env, size_t szof, const char* name)
{
    char arg[16];
    uint32_t r;
    urfp_http_env_data_t* data = (urfp_http_env_data_t*)(env->transport_data);
    r = urfp_parse_unsigned_arg(arg, sizeof(arg), urfp_http_get_next_arg(env, arg, sizeof(arg)), &(data->get_succeeded));
    if ((szof == 1 && r >= 256) || (szof == 2 && r >= 65536))
    {
        data->get_succeeded = URFP_RANGE;
    }
    return r;
}

uint64_t urfp_http_get_uint64_arg(urfp_transaction_env_t* env, const char* name)
{
    char arg[22];
    uint64_t r;
    urfp_http_env_data_t* data = (urfp_http_env_data_t*)(env->transport_data);
    r = urfp_parse_uint64_arg(arg, sizeof(arg), urfp_http_get_next_arg(env, arg, sizeof(arg)), &(data->get_succeeded));
    return r;
}

#ifndef URFP_WITHOUT_FLOAT
double urfp_http_get_float_arg(urfp_transaction_env_t* env, size_t szof, const char* name)
{
    char arg[22];
    double r;
    urfp_http_env_data_t* data = (urfp_http_env_data_t*)(env->transport_data);
    r = urfp_parse_float_arg(arg, sizeof(arg), urfp_http_get_next_arg(env, arg, sizeof(arg)), &(data->get_succeeded));
    return r;
}
#endif

bool urfp_http_get_bool_arg(urfp_transaction_env_t* env, const char* name)
{
    char arg[16];
    bool r;
    urfp_http_env_data_t* data = (urfp_http_env_data_t*)(env->transport_data);
    r = urfp_parse_bool_arg(arg, sizeof(arg), urfp_http_get_next_arg(env, arg, sizeof(arg)), &(data->get_succeeded));
    return r;
}

urfp_retval_t urfp_http_get_succeeded(urfp_transaction_env_t* env)
{
    urfp_http_env_data_t* data = (urfp_http_env_data_t*)(env->transport_data);
    return data->get_succeeded;
}

urfp_retval_t urfp_http_get_array_start(urfp_transaction_env_t* env, const char* name, size_t *maxcount)
{
    urfp_retval_t r = URFP_OK;
    urfp_http_state_t* state = (urfp_http_state_t*)(env->transport_conn);
    if (state->handlers->enter_compound != NULL)
    {
        r = state->handlers->enter_compound(state->handlers_arg, name) ? URFP_OK : URFP_ERR;
    }
    return r;
}

urfp_retval_t urfp_http_get_array_end(urfp_transaction_env_t* env)
{
    urfp_retval_t r = URFP_OK;
    urfp_http_state_t* state = (urfp_http_state_t*)(env->transport_conn);
    if (state->handlers->leave_compound != NULL)
    {
        r = state->handlers->leave_compound(state->handlers_arg) ? URFP_OK : URFP_ERR;
    }
    return r;
}

urfp_retval_t urfp_http_get_struct_start(urfp_transaction_env_t* env, const char* name)
{
    return urfp_http_get_array_start(env, name, 0);
}

urfp_retval_t urfp_http_get_struct_end(urfp_transaction_env_t* env)
{
    return urfp_http_get_array_end(env);
}

urfp_retval_t urfp_http_get_blob_start(urfp_transaction_env_t* env, const char* name, size_t* maxlength)
{
    return urfp_http_get_array_start(env, name, 0);
}

urfp_retval_t urfp_http_get_blob_access(urfp_transaction_env_t* env, void** data, size_t* length)
{
    urfp_retval_t r = URFP_OK;
    urfp_http_state_t* state = (urfp_http_state_t*)(env->transport_conn);
    if (state->handlers->access_compound != NULL)
    {
        r = state->handlers->access_compound(state->handlers_arg, data, length) ? URFP_OK : URFP_ERR;
    }
    return r;
}

urfp_retval_t urfp_http_get_blob_pause(urfp_transaction_env_t* env)
{
    urfp_retval_t r = URFP_OK;
    urfp_http_state_t* state = (urfp_http_state_t*)(env->transport_conn);
    if (state->handlers->pause_compound != NULL)
    {
        r = state->handlers->pause_compound(state->handlers_arg) ? URFP_OK : URFP_ERR;
    }
    return r;
}

urfp_retval_t urfp_http_get_blob_end(urfp_transaction_env_t* env)
{
    return urfp_http_get_array_end(env);
}

void urfp_http_get_end(urfp_transaction_env_t* env)
{
}

//#define URFP_HTTP_PAD_WITH_SPACES 1

#ifdef URFP_HTTP_PAD_WITH_SPACES
static const char urfp_http_put_spaces[32] = "                                ";
#endif

static urfp_retval_t urfp_http_put_member_start(urfp_transaction_env_t* env, const char* name)
{
    urfp_http_state_t* state = (urfp_http_state_t*)(env->transport_conn);
    urfp_http_env_data_t* data = (urfp_http_env_data_t*)(env->transport_data);

    if ((data->in_first & (1<<(data->in_depth))) == 0)
    {
        state->handlers->write(state->handlers_arg, ",", 1);
    }
    else
    {
        data->in_first &= ~(1<<(data->in_depth));
    }

    state->handlers->write(state->handlers_arg, "\r\n", 2);
#ifdef URFP_HTTP_PAD_WITH_SPACES
    state->handlers->write(state->handlers_arg, urfp_http_put_spaces, data->in_depth);
#endif

    if (data->in_depth == 0 || ((data->in_array & (1<<(data->in_depth-1))) == 0))
    {
        if (name == NULL)
        {
            /* This should not happen */
            name = "N/A";
        }

        state->handlers->write(state->handlers_arg, "\"", 1);
        state->handlers->write(state->handlers_arg, name, strlen(name));
#ifdef URFP_HTTP_PAD_WITH_SPACES
        state->handlers->write(state->handlers_arg, "\": ", 3);
#else
        state->handlers->write(state->handlers_arg, "\":", 2);
#endif
    }

    return URFP_OK;
}

static urfp_retval_t urfp_http_put_compound_start(urfp_transaction_env_t* env, const char* name, char open)
{
    urfp_http_state_t* state = (urfp_http_state_t*)(env->transport_conn);
    urfp_http_env_data_t* data = (urfp_http_env_data_t*)(env->transport_data);

    urfp_http_put_member_start(env, name);

    if (data->in_depth < 31)
    {
        state->handlers->write(state->handlers_arg, &open, 1);
        if (open == '[')
        {
            data->in_array |= (1<<data->in_depth);
        }
        else
        {
            data->in_array &= ~(1<<data->in_depth);
        }
        data->in_depth ++;
        data->in_first |= (1<<data->in_depth);
    }

    return URFP_OK;
}

static urfp_retval_t urfp_http_put_compound_end(urfp_transaction_env_t* env, char close)
{
    urfp_http_state_t* state = (urfp_http_state_t*)(env->transport_conn);
    urfp_http_env_data_t* data = (urfp_http_env_data_t*)(env->transport_data);

    if (data->in_depth > 0)
    {
        data->in_depth --;
        state->handlers->write(state->handlers_arg, "\r\n", 2);
#ifdef URFP_HTTP_PAD_WITH_SPACES
        state->handlers->write(state->handlers_arg, urfp_http_put_spaces, data->in_depth);
#endif
        state->handlers->write(state->handlers_arg, &close, 1);
    }

    return URFP_OK;
}

urfp_retval_t urfp_http_put_string(urfp_transaction_env_t* env, const char* name, const char* string)
{
    urfp_http_state_t* state = (urfp_http_state_t*)(env->transport_conn);
    if (state->output_format == OUTFMT_BINARY)
    {
        return state->handlers->write(state->handlers_arg, string, strlen(string) + 1);
    }

    urfp_http_put_member_start(env, name);
    state->handlers->write(state->handlers_arg, "\"", 1);

    if (string != NULL)
    {
        int res = 1;
        int quotedlen = 0;
        char quoted[32];

        const char* ustr = string;
        while (*ustr != 0 && res > 0)
        {
            if (sizeof(quoted) - quotedlen < 6) /* max size of codepoint quoted as "\uXXXX" */
            {
                state->handlers->write(state->handlers_arg, quoted, quotedlen);
                quotedlen = 0;
            }
            res = urfp_utf8_quote(quoted+quotedlen, &ustr);
            if (res > 0)
            {
                quotedlen += res;
            }
        }
        if (quotedlen > 0)
        {
            state->handlers->write(state->handlers_arg, quoted, quotedlen);
        }
    }

    state->handlers->write(state->handlers_arg, "\"", 1);

    return URFP_OK;
}

static urfp_retval_t urfp_http_put_bare_string(urfp_transaction_env_t* env, const char* name, const char* string, size_t slen)
{
    if (string == NULL)
    {
        /* This should not happen */
        string = "";
    }

    urfp_http_state_t* state = (urfp_http_state_t*)(env->transport_conn);

    urfp_http_put_member_start(env, name);

    if (slen < 0)
    {
    	slen = strlen(string);
    }
    state->handlers->write(state->handlers_arg, string, slen);

    return URFP_OK;
}

urfp_retval_t urfp_http_put_bool(urfp_transaction_env_t* env, const char* name, bool value)
{
    urfp_http_state_t* state = (urfp_http_state_t*)(env->transport_conn);
    if (state->output_format == OUTFMT_BINARY)
    {
        uint8_t byte = value ? 1:0;
        return state->handlers->write(state->handlers_arg, &byte, 1);
    }
    else if (value)
    {
        return urfp_http_put_bare_string(env, name, "true", 4);
    }
    else
    {
        return urfp_http_put_bare_string(env, name, "false", 5);
    }
}

urfp_retval_t urfp_http_put_int(urfp_transaction_env_t* env, size_t szof, const char* name, int32_t value)
{
    urfp_http_state_t* state = (urfp_http_state_t*)(env->transport_conn);
    if (state->output_format == OUTFMT_BINARY)
    {
        return state->handlers->write(state->handlers_arg, &value, szof);
    }

    char intstring[12];
    bool negative = (value<0);
    if (negative)
    {
        value = -value;
    }
    size_t i = urfp_anyntoa32(intstring, sizeof(intstring), value, negative);
    return urfp_http_put_bare_string(env, name, intstring+i, sizeof(intstring)-i);
}

urfp_retval_t urfp_http_put_unsigned(urfp_transaction_env_t* env, size_t szof, const char* name, uint32_t value)
{
    urfp_http_state_t* state = (urfp_http_state_t*)(env->transport_conn);
    if (state->output_format == OUTFMT_BINARY)
    {
        return state->handlers->write(state->handlers_arg, &value, szof);
    }

    char intstring[12];
    size_t i = urfp_anyntoa32(intstring, sizeof(intstring), value, false);
    return urfp_http_put_bare_string(env, name, intstring+i, sizeof(intstring)-i);
}

urfp_retval_t urfp_http_put_uint64(urfp_transaction_env_t* env, const char* name, uint64_t value)
{
    urfp_http_state_t* state = (urfp_http_state_t*)(env->transport_conn);
    if (state->output_format == OUTFMT_BINARY)
    {
        return state->handlers->write(state->handlers_arg, &value, 8);
    }

    char intstring[22];
    size_t i = urfp_anyntoa64(intstring, sizeof(intstring), value, false);
    return urfp_http_put_bare_string(env, name, intstring+i, sizeof(intstring)-i);
}

#ifndef URFP_WITHOUT_FLOAT
urfp_retval_t urfp_http_put_float(urfp_transaction_env_t* env, size_t szof, const char* name, double value)
{
    urfp_http_state_t* state = (urfp_http_state_t*)(env->transport_conn);
    if (state->output_format == OUTFMT_BINARY)
    {
        if (szof == sizeof(float))
        {
            float v = value;
            return state->handlers->write(state->handlers_arg, &v, sizeof(float));
        }
        return state->handlers->write(state->handlers_arg, &value, sizeof(double));
    }

    char floatstring[44];
    size_t i = urfp_anyftoa(floatstring, sizeof(floatstring), value, 4);
    return urfp_http_put_bare_string(env, name, floatstring+i, sizeof(floatstring)-i);
}
#endif

urfp_retval_t urfp_http_put_array_start(urfp_transaction_env_t* env, const char* name, int maxcount)
{
    urfp_http_state_t* state = (urfp_http_state_t*)(env->transport_conn);
    if (state->output_format == OUTFMT_BINARY)
    {
        return URFP_OK;
    }

    return urfp_http_put_compound_start(env, name, '[');
}

urfp_retval_t urfp_http_put_array_end(urfp_transaction_env_t* env)
{
    urfp_http_state_t* state = (urfp_http_state_t*)(env->transport_conn);
    if (state->output_format == OUTFMT_BINARY)
    {
        return URFP_OK;
    }

    return urfp_http_put_compound_end(env, ']');
}

urfp_retval_t urfp_http_put_struct_start(urfp_transaction_env_t* env, const char* name)
{
    urfp_http_state_t* state = (urfp_http_state_t*)(env->transport_conn);
    if (state->output_format == OUTFMT_BINARY)
    {
        return URFP_OK;
    }

    return urfp_http_put_compound_start(env, name, '{');
}

urfp_retval_t urfp_http_put_struct_end(urfp_transaction_env_t* env)
{
    urfp_http_state_t* state = (urfp_http_state_t*)(env->transport_conn);
    if (state->output_format == OUTFMT_BINARY)
    {
        return URFP_OK;
    }

    return urfp_http_put_compound_end(env, '}');
}

urfp_retval_t urfp_http_put_start(urfp_transaction_env_t* env)
{
    urfp_http_state_t* state = (urfp_http_state_t*)(env->transport_conn);
    if (state->output_format == OUTFMT_BINARY)
    {
        return URFP_OK;
    }

    urfp_http_env_data_t* data = (urfp_http_env_data_t*)(env->transport_data);

    data->in_first = 1;
    data->in_array = 0;
    data->in_depth = 0;

    if (state->output_format == OUTFMT_JSON)
    {
        char intro[13];
#ifdef URFP_HTTP_PAD_WITH_SPACES
        int n = sprintf(intro, "{ \"%c%c%c\": {", (env->code & 0xFF), (env->code >> 8) & 0xFF, env->tag);
#else
        int n = sprintf(intro, "{\r\n\"%c%c%c\":{", (env->code & 0xFF), (env->code >> 8) & 0xFF, env->tag);
#endif
        return state->handlers->write(state->handlers_arg, intro, n);
    }
    else
    {
        /* OUTFMT_JSON_UNTAGGED: Omit surrounding element */
        return state->handlers->write(state->handlers_arg, "{", 1);
    }

    /* Assume already in element */
    return URFP_OK;
}

urfp_retval_t urfp_http_put_end(urfp_transaction_env_t* env, int code, char* error_text)
{
    urfp_http_state_t* state = (urfp_http_state_t*)(env->transport_conn);
    if (state->output_format == OUTFMT_BINARY)
    {
        return URFP_OK;
    }

    if (env->dry_run == INPUT)
    {
        urfp_http_put_struct_end(env);
    }

    urfp_http_put_unsigned(env, sizeof(int32_t), "error_code", code);
    urfp_http_put_string(env, "error_text", error_text);

    if (state->output_format == OUTFMT_JSON)
    {
        state->handlers->write(state->handlers_arg, " } }\r\n", 6);
    }
    else
    {
        /* OUTFMT_JSON_UNTAGGED: Omit surrounding element */
        state->handlers->write(state->handlers_arg, "\r\n}\r\n", 5);
    }

    return URFP_OK;
}

urfp_retval_t urfp_http_describe_arg(urfp_transaction_env_t* env, const char* name, urfp_argtype_t type, int len)
{
    static const char* urfp_put_itype[5] = { "i8", "i16", "i24", "i32", "i64" };
    static const char* urfp_put_utype[5] = { "u8", "u16", "u24", "u32", "u64" };

    urfp_http_state_t* state = (urfp_http_state_t*)(env->transport_conn);
    if (state->output_format == OUTFMT_BINARY)
    {
        char t = 0;
        urfp_retval_t r;

        /* First output the prefix byte. It indicates the "version" of type information,
         * whether the type describes an argument and whether it comes with a name (not yet) */

        env->transport->put_unsigned(env, 1, "arg", ((type | URFP_TYPE_PREFIX) & URFP_TYPE_MODE) >> 8);

        /* Then output one byte describing the actual type. Amend the length in the lower bits. */

        t = type & URFP_TYPE_MASK;
        if ((t == URFP_INT) || (t == URFP_UNSIGNED))
        {
            switch(len)
            {
            case 2: t |= 1; break;
            case 4: t |= 2; break;
            case 8: t |= 3; break;
            default: break;
            }
        }

        r = env->transport->put_unsigned(env, 1, name, t);

        /* Extra for starting arrays: Output the length (which might be -1 at the end of a set) */

        if (t == URFP_ARRAY_START && r == URFP_OK)
        {
            r = env->transport->put_int(env, 4, "length", len);
        }

        return r;
    }

    /* Not binary output mode: Make up JSON fragment for describing an element.
     * Functions start with an "_args" struct that contains a description of arguments.
     * Note: The arguments should be given in requests in the order they appear in the struct,
     * as most other transports have to live without names for the arguments and thus with
     * just positional interpretation of the input. */

    if (env->dry_run == OUTPUT && ((type & URFP_ARG) != 0))
    {
        env->dry_run = INPUT;
        env->transport->put_struct_start(env, "_args");
    }

    if (env->dry_run == INPUT && ((type & URFP_ARG) == 0))
    {
        env->transport->put_struct_end(env);
        env->dry_run = OUTPUT;
    }

    switch(type & URFP_TYPE_MASK)
    {
    case URFP_BOOL:
        return env->transport->put_string(env, name, "bool");
    case URFP_STRING:
        return env->transport->put_string(env, name, "string");
    case URFP_INT:
        if (len > 4)
        {
            return env->transport->put_string(env, name, urfp_put_itype[4]);
        }
        return env->transport->put_string(env, name, urfp_put_itype[len-1]);
    case URFP_UNSIGNED:
        if (len > 4)
        {
            return env->transport->put_string(env, name, urfp_put_utype[4]);
        }
        return env->transport->put_string(env, name, urfp_put_utype[len-1]);
    case URFP_FLOAT:
        return env->transport->put_string(env, name, "float");
    case URFP_DOUBLE:
        return env->transport->put_string(env, name, "double");
    case URFP_ARRAY_START:
        /* When describing array, output JSON struct, not array, so we can
         * add length info and object names for the elements */
        /* The receiver must differentiate between arrays and structs
         * by looking for the "_len" element. If it is present, an
         * array is meant, otherwise just some struct. */
        (void)env->transport->put_struct_start(env, name);
        return env->transport->put_int(env, sizeof(int32_t), "_len", len);
    case URFP_ARRAY_END:
        /* Because arrays are described by structs, end with struct */
        return env->transport->put_struct_end(env);
    case URFP_STRUCT_START:
        return env->transport->put_struct_start(env, name);
    case URFP_STRUCT_END:
        return env->transport->put_struct_end(env);
    default:
        break;
    }

    return URFP_OK;
}

const urfp_transport_t urfp_http_transport =
{
    .get_string = urfp_http_get_string_arg,
    .get_bool = urfp_http_get_bool_arg,
    .get_int = urfp_http_get_int_arg,
    .get_unsigned = urfp_http_get_unsigned_arg,
    .get_uint64 = urfp_http_get_uint64_arg,
#ifndef URFP_WITHOUT_FLOAT
    .get_float = urfp_http_get_float_arg,
#endif
    .get_succeeded = urfp_http_get_succeeded,
    .get_array_start = urfp_http_get_array_start,
    .get_array_end = urfp_http_get_array_end,
    .get_struct_start = urfp_http_get_struct_start,
    .get_struct_end = urfp_http_get_struct_end,
    .get_blob_start = urfp_http_get_blob_start,
    .get_blob_access = urfp_http_get_blob_access,
    .get_blob_pause = urfp_http_get_blob_pause,
    .get_blob_end = urfp_http_get_blob_end,
    .get_end = urfp_http_get_end,

    .more_args_available = urfp_http_more_args_available,
    .get_arg_name = urfp_http_get_arg_name,
    .output_on_hold = urfp_http_output_on_hold,
    .describe_arg = urfp_http_describe_arg,

    .put_start = urfp_http_put_start,
    .put_string = urfp_http_put_string,
    .put_bool = urfp_http_put_bool,
    .put_int = urfp_http_put_int,
    .put_unsigned = urfp_http_put_unsigned,
    .put_uint64 = urfp_http_put_uint64,
#ifndef URFP_WITHOUT_FLOAT
    .put_float = urfp_http_put_float,
#endif
    .put_array_start = urfp_http_put_array_start,
    .put_array_end = urfp_http_put_array_end,
    .put_struct_start = urfp_http_put_struct_start,
    .put_struct_end = urfp_http_put_struct_end,
    .put_end = urfp_http_put_end,
};

/**
@}
*/
