/* URFP - UR Function Protocol

Copyright (c) 2015 Kolja Waschk

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

/**
@ingroup urfp_server
@{
*/

#ifndef URFP_SERVER_H
#define URFP_SERVER_H 1

#include "urfp_types.h"

#ifdef __cplusplus
extern "C" {
#endif

struct urfp_transport
{
    bool     (*more_args_available)(urfp_transaction_env_t* env);

    int      (*get_arg_name)(urfp_transaction_env_t* env, char* buf, size_t maxlen);

    int      (*get_string)(urfp_transaction_env_t* env, const char* name, char* buf, size_t maxlen);
    bool     (*get_bool)(urfp_transaction_env_t* env, const char* name);
    int32_t  (*get_int)(urfp_transaction_env_t* env, size_t szof, const char* name);
    uint32_t (*get_unsigned)(urfp_transaction_env_t* env, size_t szof, const char* name);
    uint64_t (*get_uint64)(urfp_transaction_env_t* env, const char* name);
#ifndef URFP_WITHOUT_FLOAT
    double   (*get_float)(urfp_transaction_env_t* env, size_t szof, const char* name);
#endif

    urfp_retval_t (*get_succeeded)(urfp_transaction_env_t* env);

    urfp_retval_t (*get_array_start)(urfp_transaction_env_t* env, const char* name, size_t* maxcount);
    urfp_retval_t (*get_array_end)(urfp_transaction_env_t* env);
    urfp_retval_t (*get_struct_start)(urfp_transaction_env_t* env, const char* name);
    urfp_retval_t (*get_struct_end)(urfp_transaction_env_t* env);

    urfp_retval_t (*get_blob_start)(urfp_transaction_env_t* env, const char* name, size_t* maxlength);
    urfp_retval_t (*get_blob_access)(urfp_transaction_env_t* env, void** data, size_t* length);
    urfp_retval_t (*get_blob_pause)(urfp_transaction_env_t* env);
    urfp_retval_t (*get_blob_end)(urfp_transaction_env_t* env);

    void     (*get_end)(urfp_transaction_env_t* env);

    bool (*output_on_hold)(urfp_transaction_env_t* env);

    urfp_retval_t (*describe_arg)(urfp_transaction_env_t* env, const char* name, urfp_argtype_t type, int len);

    urfp_retval_t (*put_start)(urfp_transaction_env_t* env);

    urfp_retval_t (*put_string)(urfp_transaction_env_t* env, const char* name, const char* string);
    urfp_retval_t (*put_bool)(urfp_transaction_env_t* env, const char* name, bool value);
    urfp_retval_t (*put_int)(urfp_transaction_env_t* env, size_t szof, const char* name, int32_t value);
    urfp_retval_t (*put_unsigned)(urfp_transaction_env_t* env, size_t szof, const char* name, uint32_t value);
    urfp_retval_t (*put_uint64)(urfp_transaction_env_t* env, const char* name, uint64_t value);
#ifndef URFP_WITHOUT_FLOAT
    urfp_retval_t (*put_float)(urfp_transaction_env_t* env, size_t szof, const char* name, double value);
#endif

    urfp_retval_t (*put_int_array)(urfp_transaction_env_t* env, size_t szof, const char* name, void* value, size_t count);
    urfp_retval_t (*put_unsigned_array)(urfp_transaction_env_t* env, size_t szof, const char* name, void* value, size_t count);

    urfp_retval_t (*put_array_start)(urfp_transaction_env_t* env, const char* name, int count);
    urfp_retval_t (*put_array_end)(urfp_transaction_env_t* env);

    urfp_retval_t (*put_struct_start)(urfp_transaction_env_t* env, const char* name);
    urfp_retval_t (*put_struct_end)(urfp_transaction_env_t* env);

    urfp_retval_t (*put_end)(urfp_transaction_env_t* env, int code, char* error_text);
};

/** All state that has to be maintained for replying correctly
 *
 *  There will be one instance of these structs per active command.
 */

struct urfp_transaction_env
{
    const urfp_transport_t* transport;
    void* transport_conn; /* transport data (per connection) */
    void* transport_data; /* transport data (per env) */

    urfp_dry_run_t dry_run;

    const urfp_app_t* app;
    const urfp_elem_descriptor_t* elem;

    urfp_channel_t* channel; /* channel and sources state (per channel/connection) */

    urfp_envmode_t mode;
    unsigned code;
    char tag;

    void* auth; /* NULL if not authenticated */

    urfp_put_continue_callback_t put_continue;
    urfp_get_continue_callback_t get_continue;
    urfp_get_continue_callback_t get_finish;
    /* content of this struct and/or union is TBD */
    union
    {
        struct
        {
            urfp_put_continue_callback_t output_element;
            int index;
            void* data;
        } array;
    } continuation;
};

/** Call this if you want to let URFP handle an incoming query
 */
extern urfp_retval_t urfp_dispatch_query(urfp_transaction_env_t* env, unsigned code, char tag);

/** Default function to iterate over urfp_elem_list_t */
extern urfp_retval_t urfp_default_next_elem_from_list(urfp_elem_list_t* base, int* index, const urfp_elem_descriptor_t** elem);

/** Default function to iterate over urfp_elem_list_t */
extern urfp_retval_t urfp_default_next_elem_from_list(urfp_elem_list_t* base, int* index, const urfp_elem_descriptor_t** elem);

/** Call this to let backgrounded functions resume */
extern urfp_retval_t urfp_resume_put(urfp_transaction_env_t* env);
extern urfp_retval_t urfp_resume_get(urfp_transaction_env_t* env);
extern urfp_retval_t urfp_finish_get(urfp_transaction_env_t* env);

/** Inform URFP server about authentication state ... work in progress ... */
extern void urfp_set_auth(urfp_transaction_env_t* env, void* auth);

/** Some tools */

/* These convert unsigned to decimal, aligned *at the end* of "out",
 * optionally prefixed with minus sign if "negative" was true,
 * and returns the offset where the output starts in "out"
 * The length of the converted number is (outlen-retval).
 * No trailing zero is appended.
 */
extern size_t urfp_anyntoa32(char* out, size_t outlen, uint32_t value, bool negative);
extern size_t urfp_anyntoa64(char* out, size_t outlen, uint64_t value, bool negative);
#ifndef URFP_WITHOUT_FLOAT
extern size_t urfp_anyftoa(char* out, size_t outlen, double value, int places);
#endif

#ifdef __cplusplus
}
#endif

#endif

/**
@}
*/
