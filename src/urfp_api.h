/* URFP - UR Function Protocol

Copyright (c) 2015 Kolja Waschk

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

/**
@ingroup urfp_api
@{
*/

#ifndef URFP_API_H
#define URFP_API_H 1

#include "urfp_types.h"
#include <stdarg.h>

#ifdef __cplusplus
extern "C" {
#endif

/** Structure for meta info about related data in referenced sources.
 *
 *   A source that outputs e.g. scan metadata (such as scan number, timestamp,
 *   statistics, speed etc) can use it to point to more details in other sources,
 *   e.g. distance+echo measurements in a "sample" source or some other measurements
 *   taken at a rate lower than sample rate during the scan in an "edge" source.
 */
typedef struct urfp_source_ref urfp_source_ref_t;

/**
 * General structure for describing elements like variables, functions, etc.
 */

struct urfp_elem_descriptor
{
    int id; /* unique number > 0. At end of a list this is 0. If < 0, the entry is a link to another list at "op" */
    const char* name; /* name, all lower case, without spaces */
    const char* desc; /* one-line long desc */
    void *op; /* arbitrary argument to be passed to the methods below, minimum 32 bit, may hold integer instead of pointer */
#if URFP_ELEMENTS_WITH_PROPS
    uint32_t props; /* bitset with application-defined meaning */
#endif

    /* Callback functions
     *
     * The callback functions get their arguments by actively using the
     * urfp_get_next_*_arg() methods from the urfp_transaction_env_t, see below.
     */
#if URFP_ELEMENTS_WITH_PROPS
    /* optional callback to retrieve props with update, ie. dynamic props */
    uint32_t (*props_update)(const urfp_elem_descriptor_t *env);
#endif

    union
    {
        struct
        {
            /* Get: Return variable content.
             *      gv V: return whole content
             *      gv V X: all values starting at offset X (zero based)
             *      gv V -N: return max N values
             *      gv V X N: all values from offset X to offset N
             *      gv V X -N: return values (from offset X up to, not including, N)
             *
             * The response shall be a scalar or vector of values of same type.
             *
             */
            urfp_retval_t (*get)(urfp_transaction_env_t* env);

            /* Set: Change variable content.
             *      sv V ...: set variable starting at offset 0
             *      uv V X ...: set variable starting at offset X
             *
             * The arguments ... shall be a vector of values of the
             * same type as the variable.
             */
            urfp_retval_t (*set)(urfp_transaction_env_t* env, int offset);
        }
        varfuncs;

        struct
        {
            /* Exec: Execute function.
             *       cf F ...: args depend on the function.
             */
            urfp_retval_t (*exec)(urfp_transaction_env_t* env);
        }
        funcfuncs;

        struct
        {
            /* Read: Read from data source
             *
             *
             *  read() is used when someone actively queries for data ("inline output") with command
             *    but also to be used for OOB output from "enabled" sources whenever new data is available.
             *
             *  Console API yet TBD, the server shall augment the 'read' output with meta data
             *    such as actual first index, length, and data from other sources as needed in context.
             *
             *  Called inline for data: let read() output desired data, then output data from related sources.
             *        hd D       help about source (data types and references to other sources by ID)
             *        rd D       read one element without waiting (ie. most recent already avalailable)
             *                   read (up to) N elements
             *        rd D X     read starting at given index
             *                   read starting at given index
             *        rd D X D2 D3 D4 ... also read from other sources, at corresponding index
             *        wd D T X     wait milliseconds for new data (TBD a given index?), return its index
             *        ed D       enable/disable OOB output from data source (TBD arguments? handle? destination?)

             *  Called inline for help: let read() output datatypes, then output IDs of related sources
             *  Called for OOB data output: Prefix index+length, let read() output actual data,
             *       afterwards output indices and lengths of related data (written to related_data by read()).
             *
             *  - Output array of data, starting at index first, with exactly 'count' elements
             *  - In dry_run mode, output array with just one element to describe data type.
             *  - TBD: If count is negative, output "up to" -count iterations or less.
             *  - TBD: Write (index, length) about timely related data in related_sources (if not NULL)
             *
             * ATTN: read() must not itself call urfp_put_end() when done !!
             */
            urfp_retval_t (*read)(urfp_transaction_env_t* env, uint32_t first, int *maxcount);
            urfp_retval_t (*index)(const urfp_elem_descriptor_t* elem, uint32_t* begin, uint32_t *end);
        }
        sourcefuncs;

        struct
        {
            /* Read: Return blob content.
             *      rb V: return whole content
             *      rb V X: all bytes starting at offset X (zero based)
             * TBD:
             *      rb V -N: return max N bytes
             *      rb V X N: all bytes from offset X to offset N
             *      rb V X -N: return bytes (from offset X up to, not including, N)
             *
             * The response shall be a scalar or vector of values of same type.
             *
             */

            /* Set: Change blob content.
             *      wb V ...: Write blob starting at byte offset 0
             *      ub V X ...: Update blob starting at byte offset X
             *
             * The arguments ... shall be a vector of bytes.
             */

            urfp_retval_t (*read)(urfp_transaction_env_t* env);
            urfp_retval_t (*write)(urfp_transaction_env_t* env, int offset);
        }
        blobfuncs;

#if URFP_WITH_CHANNELS
        struct
        {
            /* Channel: open/help
             */
            urfp_retval_t (*open)(urfp_transaction_env_t* env);

            /* Channel: activate/deactivate sources
             */
            urfp_retval_t (*activate)(urfp_transaction_env_t* env, bool activate);
        }
        channelfuncs;
#endif
    };
};

struct urfp_source_ref
{
    int ref_max;   /* reference[] array size */
    int ref_next;  /* next ref from reference[] to process */
    int ref_count; /* total number of references in reference[] */

    union
    {
        urfp_elem_descriptor_t* source_ref; /* link to referenced source */
        uint32_t first; /* Index of first referenced data in that source */
        uint32_t count; /* Amount of referenced data in that source */
    }
    reference[];
};

/** Pointers to callback functions for UrFP to adapt it for your application. They
 * all get as first param the void* arg which you passed to urfp_init(). It can be
 * used for any information as you like. */
struct urfp_app
{
    /* Your implementation of functions for the client */

    /**
     * An arbitrary pointer that will be passed to the callbacks when calling them
     */
    void* arg;

    /** Request termination of session
     *
     * Honoring this request is optional. It is called - for example -
     * when user types Ctrl-D (EOT) in terminal. It should be regarded
     * like a wish to "log out" or "exit".
     *
     * @param arg The arg that you passed to urfp_init()
     */
    void (*exit)(void* arg);

    /** Request termination of currently running function
     *
     * Honoring this request is optional. It is called - for example -
     * when user types Ctrl-C (INT) in terminal. If there is any long-running
     * command currently in progress, it should be aborted with error code.
     *
     * @param arg The arg that you passed to urfp_init()
     * @param env The environment for the function
     */
    void (*cancel)(void* arg, urfp_transaction_env_t* env);

    /** React on unexpected session end
     *
     * Whenever a session is aborted unexpectedly, like in case of a disconnect.
     * this function will be called by the URFP server or transport to enable
     * your app to clean up and free any extra allocated ressources. It will be
     * called once per ongoing transaction with an appropriate env pointer,
     * then again once with env == NULL for finalization.
     *
     * @param arg The arg that you passed to urfp_init()
     * @param env An urfp_transaction_env_t* for each active command and finally
     *            NULL for finalization.
     */
    void (*abort)(void* arg, urfp_transaction_env_t* env);

    /** Called when OOB data comes in */
    urfp_retval_t (*handle_oob)(urfp_transaction_env_t* env);

    /** This method is used to enable or disable sources in this app.
        @arg The arg that you passed to urfp_init()
        @param source_elem A pointer to the source to en/disable (NULL means all)
        @param enable Whether to enable(true) or disable(false) the source(s)
    */
    urfp_retval_t (*enable_source)(void* arg, const urfp_elem_descriptor_t* source_elem, bool enable);

    /** True if source is enabled in this app */
    bool (*is_enabled_source)(void* arg, const urfp_elem_descriptor_t* source_elem);

    /** Helper to iterate through urfp_elem_list. If you don't have a custom one,
        set to urfp_default_next_elem_from_list. */
    urfp_retval_t (*next_elem_from_list)(urfp_elem_list_t* base, int* index, const urfp_elem_descriptor_t** elem);

    /** Lists of elements provided by you, containing
     *  further pointers to handler methods for specific
     *  variables, functions etc.
     */
    urfp_elem_list_t variable;
    urfp_elem_list_t function;
    urfp_elem_list_t source;
    urfp_elem_list_t blob;
#if URFP_WITH_CHANNELS
    urfp_elem_list_t channel;
#endif

#if 0
    /** Method provided by you to read urfp_elem_descriptor_t* from urfp_elem_list_t.
     *  The URFP code initializes "list" with a pointer from this urfp_app_t
     *  (e.g. "variable") and "index" with 0. The first call to
     *  next_elem_from_list() with that information is expected to deliver the
     *  first elem from list into *elem and update the list and index
     *  appropriately, then return URFP_OK. In any other case (e.g. end of list
     *  or no list provided) it returns URFP_ERR and *elem=NULL.
     */
    urfp_retval_t (*next_elem_from_list)(urfp_elem_list_t* list, int* index, urfp_elem_descriptor_t** elem);
#endif
};

/* ********************* */

extern bool urfp_more_args_available(urfp_transaction_env_t* env);
extern int urfp_get_next_arg_name(urfp_transaction_env_t* env, char* buf, size_t maxlen);

/* Append trailing zero in-place and parse ASCII as base-10 integer */
extern int32_t urfp_parse_int_arg(char* buf, int bufsize, int arglen, urfp_retval_t* retval);
extern uint32_t urfp_parse_unsigned_arg(char* buf, int bufsize, int arglen, urfp_retval_t* retval);
extern uint64_t urfp_parse_uint64_arg(char* buf, int bufsize, int arglen, urfp_retval_t* retval);
#ifndef URFP_WITHOUT_FLOAT
extern double urfp_parse_float_arg(char* buf, int bufsize, int arglen, urfp_retval_t* retval);
#endif
extern bool urfp_parse_bool_arg(char* buf, int bufsize, int arglen, urfp_retval_t* retval);

/* Validate and count UTF8 codepoints in string (len=-1 if NUL-terminated) */
/* Returns >0 number of codepoints if valid, or negated offset of first invalid input byte */
extern int urfp_utf8_examine(const char *src, int len);

/* Validate and copy UTF8 src to dst with quoting for output in JSON */
/* dst may be NULL if you just want to know how much space is needed for output */
/* Increments *src by the size of the first codepoint in UTF8 input */
/* Returns >0 number of bytes needed for output if valid, or negated offset of first invalid input byte */
extern int urfp_utf8_quote(char* dst, const char **src);;

/* Ask transport for particular typed data. If transport failed to parse the input (e.g. when
 * converting to integer from ASCII representation), they return 0 (or empty string). If you
 * need to know about parsing errors, call urfp_get_succeeded() afterwards for information. */
extern int32_t urfp_get_next_int_arg(urfp_transaction_env_t* env, size_t szof, const char* name);
extern uint32_t urfp_get_next_unsigned_arg(urfp_transaction_env_t* env, size_t szof, const char* name);
extern uint64_t urfp_get_next_uint64_arg(urfp_transaction_env_t* env, const char* name);
#ifndef URFP_WITHOUT_FLOAT
extern double urfp_get_next_float_arg(urfp_transaction_env_t* env, size_t szof, const char* name);
#endif


extern bool urfp_get_next_bool_arg(urfp_transaction_env_t* env, const char* name);

extern int urfp_get_next_string_arg(urfp_transaction_env_t* env, const char* name, char* buf, size_t maxlen);

/* urfp_get_succeeded() tells if decoding a number or string really succeeded
 * in most recent call to one of the urfp_get_next_...() functions by returning
 * URFP_OK, otherwise URFP_RANGE or URFP_ERR */
extern urfp_retval_t urfp_get_succeeded(urfp_transaction_env_t* env);

/* NOT YET IMPLEMENTED:
 * These functions allow to get a bigger chunk of values at once, if available, from
 * inside an array or structure. However you have to *additionally* call
 * get_array_start/end before and after using them.  The *count is modified to represent
 * the number of elements remaining after sending some, the *value pointer is incremented
 * accordingly. */
/*extern urfp_retval_t urfp_get_next_int_array_arg(urfp_transaction_env_t* env, size_t szof, const char* name, void** value, size_t* count);*/
/*extern urfp_retval_t urfp_get_next_unsigned_array_arg(urfp_transaction_env_t* env, size_t szof, const char* name, void** value, size_t* count);*/

/* The array/struct start/end functions allow the transport to re-align to
 * structure frame markers and/or internally switch to other parsing methods.
 * TBD: The *maxcount can be set to an upper limit beforehand to instruct the transport
 * to limit getting array elements up to that count, or may be reduced by the transport
 * if that already knows the number of available elements, or may be omitted (NULL)..
 * To get the actual data, use the scalar getter above */
extern urfp_retval_t urfp_get_array_start(urfp_transaction_env_t* env, const char* name, size_t* maxcount);
extern urfp_retval_t urfp_get_array_end(urfp_transaction_env_t* env);

extern urfp_retval_t urfp_get_struct_start(urfp_transaction_env_t* env, const char* name);
extern urfp_retval_t urfp_get_struct_end(urfp_transaction_env_t* env);

extern void urfp_get_end(urfp_transaction_env_t* env);


/* ********************* */

extern urfp_retval_t urfp_get_bool(urfp_transaction_env_t* env);
extern urfp_retval_t urfp_set_bool(urfp_transaction_env_t* env, int offset);

/* ********************* */

/** When you're done with the answer, call this function. If all went well the
 * error should be set to 0 (success) and the error text will be ignored; this
 * might spare a line of output in some transport variants.
 */
extern urfp_retval_t urfp_put_end(urfp_transaction_env_t* env, int code, char* error_text);

/** string: UTF-8, null-terminated. For "binary strings", use integer array or blob functions instead */
extern urfp_retval_t urfp_put_string(urfp_transaction_env_t* env, const char* name, const char* string);

/** bool */
extern urfp_retval_t urfp_put_bool(urfp_transaction_env_t* env, const char* name, bool value);

/** int/uint: szof determines the word size (in bytes) */
extern urfp_retval_t urfp_put_int(urfp_transaction_env_t* env, size_t szof, const char* name, int32_t value);
extern urfp_retval_t urfp_put_unsigned(urfp_transaction_env_t* env, size_t szof, const char* name, uint32_t value);

/** uint64_t for timestamps etc. */
extern urfp_retval_t urfp_put_uint64(urfp_transaction_env_t* env, const char* name, uint64_t value);

#ifndef URFP_WITHOUT_FLOAT
/** floating point values: szof determines normal (4 byte) or double precision (8 byte) */
extern urfp_retval_t urfp_put_float(urfp_transaction_env_t* env, size_t szof, const char* name, double value);
#endif

/* These functions allow to put a bigger chunk of values at once, if possible, from
 * memory as part of an array or structure. However you have to *additionally* call
 * put_array_start/end before and after using them. The *count is modified to represent
 * the number of elements remaining after sending some, the *value pointer is incremented
 * accordingly. */
extern urfp_retval_t urfp_put_int_array(urfp_transaction_env_t* env, size_t szof, const char* name, void** value, size_t* count);
extern urfp_retval_t urfp_put_unsigned_array(urfp_transaction_env_t* env, size_t szof, const char* name, void** value, size_t* count);

/* The array/struct start/end functions allow the transport to insert
 * structure frame markers and/or internally switch to other formatting methods.
 * To put the actual data, use the scalar put functions above.
 *  int_array: szof determines the word size (in bytes)
 *  count: -(max) or fixed len during dry run if this is a variable-length array/list
 *         desired access length during normal access */
extern urfp_retval_t urfp_put_array_start(urfp_transaction_env_t* env, const char* name, size_t maxcount);
extern urfp_retval_t urfp_put_array_end(urfp_transaction_env_t* env);

extern urfp_retval_t urfp_put_struct_start(urfp_transaction_env_t* env, const char* name);
extern urfp_retval_t urfp_put_struct_end(urfp_transaction_env_t* env);

/** Output multiple elements, described by format string */
extern urfp_retval_t urfp_put_formatted(urfp_transaction_env_t* env, const char* fmt, ...);
extern urfp_retval_t urfp_put_vformatted(urfp_transaction_env_t* env, const char* fmt, va_list ap);

/** Reply beginnen mit Angabe einer Callback-Funktion für spätere Fortsetzung */

extern urfp_retval_t urfp_get_backgrounded(urfp_transaction_env_t* env, urfp_get_continue_callback_t urfp_get_continue, urfp_get_finish_callback_t urfp_get_finish);
extern urfp_retval_t urfp_put_backgrounded(urfp_transaction_env_t* env, urfp_put_continue_callback_t urfp_put_continue);

/** Callbackbeispiel für Array-Ausgabe */
extern urfp_retval_t urfp_put_continue_list(urfp_transaction_env_t* env);
extern urfp_retval_t urfp_put_continue_array(urfp_transaction_env_t* env);
extern urfp_retval_t urfp_put_continue_array_end(urfp_transaction_env_t* env);
extern urfp_retval_t urfp_put_continue_abort(urfp_transaction_env_t* env);
extern urfp_retval_t urfp_put_continue_end(urfp_transaction_env_t* env);

extern urfp_retval_t urfp_get_continue_array(urfp_transaction_env_t* env);
extern urfp_retval_t urfp_get_continue_array_end(urfp_transaction_env_t* env);
extern urfp_retval_t urfp_get_continue_abort(urfp_transaction_env_t* env);
extern urfp_retval_t urfp_get_continue_end(urfp_transaction_env_t* env);

/** Work in progress: BLOB access
 *
 * Example usage:
 *    write_update() gets its input for flashing from the client using the
 *    get_blob_* set of functions.
 *
 *    urfp_get_blob_start(env, env->elem->name, &maxcount);
 *       Maxcount could be preloaded with a default value to indicate typical (or max?) size of blob.
 *
 *    urfp_get_blob_access(env, &data, &len).
 *       Returns URFP_OK if there was input and/or there might be more input.
 *         TBD: URFP_AGAIN there might be more, OK only if the data is completely consumed?
 *         URFP_ERR if there is definitely no more data.
 *       If URPF_OK, then set
 *         data := Pointer to data from client
 *         len  := Count of bytes available, may be zero
 *
 *    Then call
 *       urfp_get_blob_pause(env) and again urfp_get_blob_access()
 *
 *    Or finally
 *       urfp_get_blob_end(env);
 */
extern urfp_retval_t urfp_get_blob_start(urfp_transaction_env_t* env, const char* name, size_t* maxlength);
extern urfp_retval_t urfp_get_blob_access(urfp_transaction_env_t* env, void** data, size_t* length);
extern urfp_retval_t urfp_get_blob_pause(urfp_transaction_env_t* env);
extern urfp_retval_t urfp_get_blob_end(urfp_transaction_env_t* env);


#ifdef __cplusplus
}
#endif

#endif

/**
@}
*/
