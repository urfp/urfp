/* URFP - UR Function Protocol

Copyright (c) 2023 Kolja Waschk

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

/**
@ingroup urfp_client
@{
*/

#ifndef URFP_CLIENT_H
#define URFP_CLIENT_H 1

#include "urfp_types.h"
#include "urfp_server.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* Work in progress */

/* Questions to be discussed:
    - What arguments are expected..
    - How to reference somewhere / connect to somewhere (how to get the
      "environment".. or embed that into the functions?)
    - how to get info about elements and their data type, amount, calling
      conventions
    - pass by reference or value? or both?
    - C or C++?
*/

extern urfp_retval_t urfp_get_variable(urfp_transaction_env_t* env, int id, void* dst);
extern int urfp_get_var_int(urfp_transaction_env_t* env, int id);
extern urfp_retval_t urfp_set_variable(urfp_transaction_env_t* env, int id, void* src);
extern urfp_retval_t urfp_set_var_int(urfp_transaction_env_t* env, int id, int value);

extern urfp_retval_t urfp_call_function(urfp_transaction_env_t* env, int id, void* arg, void* result);

extern urfp_retval_t urfp_write_blob(urfp_transaction_env_t* env, int id, void* blob);
extern urfp_retval_t urfp_read_blob(urfp_transaction_env_t* env, int id, void* blob);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* URFP_CLIENT_H */

/**
@}
*/
