/* URFP - Universal Remote Function Protocol

Copyright (c) 2015 Kolja Waschk

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

/** @ingroup urfp_api_c
@{
*/

#include "urfp_api.h"
#include "urfp_server.h"

#include <ctype.h>
#include <stdio.h>
#include <string.h>

#include <errno.h> /* for strtod() error detection */

/* Parse up to arglen digits,
    allow "-" before numbers only if negative!=NULL (then store *negative=true, else false)
    return URFP_OK in retval (if != NULL), URFP_RANGE if out of range, URFP_ERR otherwise */

uint64_t urfp_parse_integer_internal(char* buf, int bufsize, int arglen, urfp_retval_t* retval, bool* negative)
{
    urfp_retval_t rv = URFP_OK;
    uint64_t r = 0;

    if (arglen > bufsize)
    {
        rv = URFP_ERR;
    }
    else if (arglen <= 0 || bufsize <= 0 || buf == NULL)
    {
        rv = URFP_ERR;
    }
    else
    {
        int pp = 0;
        while ((buf[pp] <= 0x20 || buf[pp] >= 0x7f) && pp < arglen)
        {
            ++ pp;
        }

        if (pp < arglen)
        {
            if (buf[pp] == '+')
            {
                ++ pp;
            }
            else if (buf[pp] == '-') 
            {

                if (negative != NULL)
                {
                    *negative = true;
                }
                else
                {
                    rv = URFP_RANGE;
                }

                ++ pp;
            }
        }

        while (pp < arglen && buf[pp] >= '0' && buf[pp] <= '9')
        {
            r = 10*r + (buf[pp] - '0');
            ++ pp;
        }

        if (pp < arglen && buf[pp] > 0x20 && buf[pp] < 0x7f)
        {
            rv = URFP_ERR;
        }
    }

    if (retval != NULL)
    {
        *retval = rv;
    }

    return r;
}

/* Append trailing zero in-place and parse as base-10 integer */
int32_t urfp_parse_int_arg(char* buf, int bufsize, int arglen, urfp_retval_t* retval)
{
    urfp_retval_t rv = URFP_OK;
    bool negative = false;
    uint64_t raw = urfp_parse_integer_internal(buf, bufsize, arglen, &rv, &negative);
    int32_t i_res = raw;
    if (negative)
    {
        if (raw > (1ull<<31))
        {
            rv = URFP_RANGE;
            i_res = -(1ll<<31);
        }
        else
        {
            i_res = (int32_t)(-1)*(int64_t)raw;
        }
    }
    else if (raw >= (1ull<<31))
    {
        rv = URFP_RANGE;
        i_res = (1ll<<31)-1;
    }

    if (retval != NULL)
    {
        *retval = rv;
    }

    return i_res;
}


/* Append trailing zero in-place and parse as base-10 integer */
uint32_t urfp_parse_unsigned_arg(char* buf, int bufsize, int arglen, urfp_retval_t* retval)
{
    urfp_retval_t rv = URFP_OK;
    uint64_t raw = urfp_parse_integer_internal(buf, bufsize, arglen, &rv, NULL);

    if (raw >= (1ull<<32))
    {
        rv = URFP_RANGE;
        raw = (1ull<<32)-1;
    }

    if (retval != NULL)
    {
        *retval = rv;
    }

    return (uint32_t)raw;
}

/* Append trailing zero in-place and parse as base-10 integer */
uint64_t urfp_parse_uint64_arg(char* buf, int bufsize, int arglen, urfp_retval_t* retval)
{ 
    return urfp_parse_integer_internal(buf, bufsize, arglen, retval, NULL);
}

#ifndef URFP_WITHOUT_FLOAT
/* Append trailing zero in-place and parse as base-10 integer */
double urfp_parse_float_arg(char* buf, int bufsize, int arglen, urfp_retval_t* retval)
{
    if (arglen < 0)
    {
        return 0;
    }
    /* Enforce trailing zero */
    if (arglen >= bufsize)
    {
        arglen = bufsize-1;
    }
    buf[arglen] = 0;

    errno = 0;
    double r = strtod(buf, NULL);
    if (retval != NULL)
    {
        if (errno == ERANGE)
        {
            *retval = URFP_RANGE;
        }
        else if (errno != 0)
        {
            *retval = URFP_ERR;
        }
    }

    return r;
}
#endif

/* (evtl) append trailing zero in-place and parse as boolean value (on/true/off/false/base-10 integer) */
bool urfp_parse_bool_arg(char* buf, int bufsize, int arglen, urfp_retval_t* retval)
{
    static const int boollen[]   = {  2,    3,     4,          2,    3,    5   };
    static const char* boolstr[] = { "on", "yes", "true",     "no", "off", "false" };

    int i;
    for (i=0; i<sizeof(boolstr)/sizeof(boolstr[0]); ++i)
    {
        if (arglen == boollen[i] && strncasecmp(buf, boolstr[i], arglen) == 0)
        {
            /* first three strings are "true", so i<3 means argument is "true" */
            return (i<3);
        }
    }
        
    bool r = false;
    if (arglen > 0 && isdigit((int)buf[0]))
    {
        r = (0 != urfp_parse_int_arg(buf, bufsize, arglen, retval));
    }
    else if (retval != NULL)
    {
        *retval = URFP_ERR;
    }

    return r;
}

/** Return size of codepoint in UTF8 input at str + offset
 *  or negated offset in case of a decoding error (may be zero!).
 *  Return 32 bit UTF char in *codepoint if non-NULL.
 */
static int urfp_utf8_decode_char(const char* str, int offset, uint32_t* cp)
{
    int len;
    const uint8_t* ch = (const uint8_t*)str;
    uint32_t codepoint;

    ch += offset;
    if ((*ch & 0x80) == 0) // 0xxxxxxx
    {
        len = 1;
        codepoint = *ch;
    }
    else if ((*ch & 0xe0) == 0xc0) // 110xxxxx
    {
        len = 2;
        codepoint = *ch & 0x1f;
        if (codepoint < 2)
        {
            /* 0..127 must not be encoded as a two-byte sequence! */
            return -offset;
        }
    }
    else if ((*ch & 0xf0) == 0xe0) // 1110xxxx
    {
        len = 3;
        codepoint = *ch & 0x0f;
    }
    else if ((*ch & 0xf8) == 0xf0) // 11110xxx
    {
        len = 4;
        codepoint = *ch & 0x07;
        if (codepoint >= 5)
        {
            /* invalid >0x140000 */
            return -offset;
        }
    }
    else
    {
        // invalid header byte
        return -offset;
    }

    int i;
    for (i=1; i<len; i++)
    {
        ch++;
        if ((*ch & 0xc0) == 0x80) // 10xxxxxx
        {
            if (cp != NULL) // otherwise no need to compute codepoint
            {
                codepoint = (codepoint << 6) | (*ch & 0x3f);
            }
        }
        else
        {
            // invalid body byte (also NUL is invalid!)
            return -(offset+i);
        }
    }

    /* Check for further invalid codes */
    if (len == 3 && codepoint < 0x800) /* should be encoded as 2 bytes */
    {
        return -(offset+1);
    }
    if (len == 3 && codepoint >= 0xd800 && codepoint < 0xe000) /* CESU-8 */
    {
        return (-offset+1);
    }
    if (len == 4 && codepoint < 0x10000) /* should be encoded as 3 bytes */
    {
        return (-offset+1);
    }
    if (len == 4 && codepoint >= 0x110000) /* too large */
    {
        return (-offset+1);
    }

    if (cp != NULL)
    {
        *cp = codepoint;
    }

    return len;
}

/* Return number of characters/codepoints (not bytes) in UTF8-encoded string.
 *
 * Argument "len" may be -1 to indicate that str is terminated with NUL byte.
 * Otherwise a NUL byte is just considered simply a valid UTF8 codepoint (0).
 *
 * If an invalid byte was detected, its byte position is returned negated, so this
 * function may as well be used to validate the correctness of an encoded string.
 * Since such position may be zero, you should use it only with non-empty strings.
 *
 *   n = utf8_count(str);
 *   if (n <= 0) printf("invalid byte in string at offset %d", n);
 */

int urfp_utf8_examine(const char *src, int len)
{
    int offset = 0;
    int num_chars = 0;

    while(offset < len || (len<0 && src[offset]!=0))
    {
        uint32_t codepoint;
        int result = urfp_utf8_decode_char(src, offset, &codepoint);
        if (result <= 0)
        {
            // result <= 0 indicates (negated) offset of erraneous byte
            return result;
        }

        offset += result;
        num_chars++;
    }

    return num_chars;
}

/* Helper for constructing a string suitable for JSON output. Most of UTF-8 can
 * be simply copied, but control codes, backslashes and quotes have to be
 * escaped. This function copies or quotes one codepoint at a time, increments
 * *src by the size of the UTF8 code and returns the number of bytes written to
 * dst. If dst==NULL, it does not write anything, just computes the sizes.
 */
int urfp_utf8_quote(char* dst, const char **src)
{
    int num_bytes_out = 0;
    uint32_t codepoint;
    int result = urfp_utf8_decode_char(*src, 0, &codepoint);
    if (result <= 0)
    {
        // result <= 0 indicates (negated) offset of erraneous byte
        return result;
    }

    // else result contains the size of the codepoint in UTF8 bytes
    if (codepoint >= 32 && codepoint != '"' && codepoint != '\\'
#if ESCAPING_FORWARD_SLASH_IS_REQUIRED
         && codepoint != '/'
#endif
    )
    {
        if (dst != NULL)
        {
            memcpy(dst, *src, result);
        }
        num_bytes_out = result;
    }
    else
    {
        uint8_t quoted;
        switch(codepoint)
        {
#if ESCAPING_FORWARD_SLASH_IS_REQUIRED
            case '/':
#endif
            case  '"':
            case '\\': quoted = codepoint; break;
            case    8: quoted = 'b'; break;
            case    9: quoted = 't'; break;
            case   10: quoted = 'n'; break;
            case   12: quoted = 'f'; break;
            case   13: quoted = 'r'; break;

        default:
            quoted = 0; break;
        };

        if (quoted == 0)
        {
            if (dst != NULL)
            {
                memcpy(dst, "\\u000", 5);
                if (codepoint >= 0x10)
                {
                    dst[4] = '1';
                    codepoint &= 0x000F;
                }
                dst[5] = codepoint + ((codepoint<10) ? '0' : ('A'-10));
            }
            num_bytes_out = 6;
        }
        else
        {
            if (dst != NULL)
            {
                dst[0] = '\\';
                dst[1] = quoted;
            }
            num_bytes_out = 2;
        }
    }
    *src += result;
    return num_bytes_out;
}


urfp_retval_t urfp_put_end(urfp_transaction_env_t* env, int code, char* error_text)
{
    return env->transport->put_end(env, code, error_text);
}

urfp_retval_t urfp_put_string(urfp_transaction_env_t* env, const char* name, const char* string)
{
    if (env->dry_run)
    {
        return env->transport->describe_arg(env, name, URFP_STRING, 0);
    }
    else
    {
        return env->transport->put_string(env, name, string);
    }
}

/** bool */
urfp_retval_t urfp_put_bool(urfp_transaction_env_t* env, const char* name, bool value)
{
    if (env->dry_run)
    {
        return env->transport->describe_arg(env, name, URFP_BOOL, 1);
    }
    else
    {
        return env->transport->put_bool(env, name, value);
    }
}

/** int/uint: szof determines the word size (in bytes) */

urfp_retval_t urfp_put_int(urfp_transaction_env_t* env, size_t szof, const char* name, int32_t value)
{
    if (env->dry_run)
    {
        return env->transport->describe_arg(env, name, URFP_INT, szof);
    }
    else
    {
        return env->transport->put_int(env, szof, name, value);
    }
}

urfp_retval_t urfp_put_unsigned(urfp_transaction_env_t* env, size_t szof, const char* name, uint32_t value)
{
    if (env->dry_run)
    {
        return env->transport->describe_arg(env, name, URFP_UNSIGNED, szof);
    }
    else
    {
        return env->transport->put_unsigned(env, szof, name, value);
    }
}

/** uint64_t for timestamps etc. */
urfp_retval_t urfp_put_uint64(urfp_transaction_env_t* env, const char* name, uint64_t value)
{
    if (env->dry_run)
    {
        return env->transport->describe_arg(env, name, URFP_UNSIGNED, 8);
    }
    else
    {
        return env->transport->put_uint64(env, name, value);
    }
}

#ifndef URFP_WITHOUT_FLOAT
urfp_retval_t urfp_put_float(urfp_transaction_env_t* env, size_t szof, const char* name, double value)
{
    if (env->dry_run)
    {
        return env->transport->describe_arg(env, name, szof==sizeof(float) ? URFP_FLOAT : URFP_DOUBLE, szof);
    }
    else
    {
        return env->transport->put_float(env, szof, name, value);
    }
}
#endif

/**
 * The following are automatically called if you use int/uint_array functions but
 * otherwise you have to manually surround your data if needed */

urfp_retval_t urfp_put_array_start(urfp_transaction_env_t* env, const char* name, size_t maxcount)
{
    if (env->dry_run)
    {
        return env->transport->describe_arg(env, name, URFP_ARRAY_START, maxcount);
    }
    return env->transport->put_array_start(env, name, maxcount);
}

urfp_retval_t urfp_put_array_end(urfp_transaction_env_t* env)
{
    if (env->dry_run)
    {
        return env->transport->describe_arg(env, "", URFP_ARRAY_END, 1);
    }
    return env->transport->put_array_end(env);
}

urfp_retval_t urfp_put_struct_start(urfp_transaction_env_t* env, const char* name)
{
    if (env->dry_run)
    {
        return env->transport->describe_arg(env, name, URFP_STRUCT_START, 1);
    }
    return env->transport->put_struct_start(env, name);
}

urfp_retval_t urfp_put_struct_end(urfp_transaction_env_t* env)
{
    if (env->dry_run)
    {
        return env->transport->describe_arg(env, "", URFP_STRUCT_END, 1);
    }
    return env->transport->put_struct_end(env);
}

/** int_array: szof determines the word size (in bytes)
 *  count: -(max) or fixed len during dry run if this is a variable-length array/list
 *         desired access length during normal access
 */
urfp_retval_t urfp_put_int_array(urfp_transaction_env_t* env, size_t szof, const char* name, void** value, size_t* count)
{
    urfp_retval_t r = URFP_OK;
    if (env->dry_run)
    {
        env->transport->describe_arg(env, "value", URFP_INT, szof);
    }
    else
    {
        /* TBD: optimize into extra methods */

        int i = 0;
        if (szof == 1)
        {
            int8_t* valueptr = *(int8_t**)value;
            while ((i<*count) && (r == URFP_OK))
            {
                r = env->transport->put_int(env, szof, "value", *valueptr);
                ++ i;
                ++ valueptr;
            }
            *value = valueptr;
        }
        else if (szof == 2)
        {
            int i = 0;
            int16_t* valueptr = *(int16_t**)value;
            while ((i<*count) && (r == URFP_OK))
            {
                r = env->transport->put_int(env, szof, "value", *valueptr);
                ++ i;
                ++ valueptr;
            }
            *value = valueptr;
        }
        else if (szof == 4)
        {
            int i = 0;
            int32_t* valueptr = *(int32_t**)value;
            while ((i<*count) && (r == URFP_OK))
            {
                r = env->transport->put_int(env, szof, "value", *valueptr);
                ++ i;
                ++ valueptr;
            }
            *value = valueptr;
        }
        *count -= i;
    }
    return r;
}

urfp_retval_t urfp_put_unsigned_array(urfp_transaction_env_t* env, size_t szof, const char* name, void** value, size_t* count)
{
    urfp_retval_t r = URFP_OK;
    if (env->dry_run)
    {
        env->transport->describe_arg(env, "value", URFP_UNSIGNED, szof);
    }
    else
    {
        /* TBD: optimize into extra methods */

        int i = 0;
        if (szof == 1)
        {
            uint8_t* valueptr = *(uint8_t**)value;
            while ((i<*count) && (r == URFP_OK))
            {
                r = env->transport->put_unsigned(env, szof, "value", *valueptr);
                ++ i;
                ++ valueptr;
            }
            *value = valueptr;
        }
        else if (szof == 2)
        {
            int i = 0;
            uint16_t* valueptr = *(uint16_t**)value;
            while ((i<*count) && (r == URFP_OK))
            {
                r = env->transport->put_unsigned(env, szof, "value", *valueptr);
                ++ i;
                ++ valueptr;
            }
            *value = valueptr;
        }
        else if (szof == 4)
        {
            int i = 0;
            uint32_t* valueptr = *(uint32_t**)value;
            while ((i<*count) && (r == URFP_OK))
            {
                r = env->transport->put_unsigned(env, szof, "value", *valueptr);
                ++ i;
                ++ valueptr;
            }
            *value = valueptr;
        }
        *count -= i;
    }
    return r;
}

/** Formatierte Ausgabe (work in progress)
 *
 *  Argumente:
 *  @param env Das Reply-Environment
 *  @param fmt Ein Formatstring mit einem Zeichen je Element, siehe Code...
 *  @param ap  Paarweise Name (const char*) und Wert (const char* oder u/int32) jedes Elements,
 *             bzw. Name gefolgt von Arraylänge (oder -1) bei Arraybeginn (zu Zeichen '[')
 *             bzw. nur Name bei Strukturbeginn (zu Zeichen '{')
 *             bzw. nichts bei Strukturende (zu Zeichen '}' oder ']').
 */

urfp_retval_t urfp_put_vformatted(urfp_transaction_env_t* env, const char* fmt, va_list ap)
{
    int i;

    for (i=0; fmt[i] != 0; i++)
    {
        const char* name = NULL;
        if (fmt[i] != '}' && fmt[i] != ']')
        {
            name = va_arg(ap, const char*);
        }

        /* Note: Smaller types are promoted to int/unsigned int/double when passed through the ellipsis parameter,
                 therefore the va_arg() specifies int/unsigned int/double when necessary */
        switch(fmt[i])
        {
            case '{': urfp_put_struct_start(env, name); break;
            case '}': urfp_put_struct_end(env); break;

            case '[': urfp_put_array_start(env, name, va_arg(ap, int)); break;
            case ']': urfp_put_array_end(env); break;

            case 'o': urfp_put_bool(env, name, va_arg(ap, int)); break;

#ifndef URFP_WITHOUT_FLOAT
            case 'f': urfp_put_float(env, sizeof(float), name, va_arg(ap, double)); break;
            case 'F': urfp_put_float(env, sizeof(double), name, va_arg(ap, double)); break;
#endif

            case 'b': urfp_put_int(env, 1, name, sizeof(int8_t) <sizeof(int) ? va_arg(ap, int) : va_arg(ap, int8_t)); break;
            case 'h': urfp_put_int(env, 2, name, sizeof(int16_t)<sizeof(int) ? va_arg(ap, int) : va_arg(ap, int16_t)); break;
            case 'i': urfp_put_int(env, 4, name, sizeof(int32_t)<sizeof(int) ? va_arg(ap, int) : va_arg(ap, int32_t)); break;
            case 'B': urfp_put_unsigned(env, 1, name, sizeof(uint8_t) <sizeof(int) ? va_arg(ap, unsigned int) : va_arg(ap, uint8_t)); break;
            case 'H': urfp_put_unsigned(env, 2, name, sizeof(uint16_t)<sizeof(int) ? va_arg(ap, unsigned int) : va_arg(ap, uint16_t)); break;
            case 'I': urfp_put_unsigned(env, 4, name, sizeof(uint32_t)<sizeof(int) ? va_arg(ap, unsigned int) : va_arg(ap, uint32_t)); break;

            case 's': urfp_put_string(env, name, va_arg(ap, const char*)); break;

            default:
                return URFP_ERR;
        }
    }

    return URFP_OK;
}

urfp_retval_t urfp_put_formatted(urfp_transaction_env_t* env, const char* fmt, ...)
{
    va_list ap;
    urfp_retval_t r;

    va_start(ap, fmt);
    r = urfp_put_vformatted(env, fmt, ap);
    va_end(ap);

    return r;
}

/** Functions for continued put */

urfp_retval_t urfp_put_continue_end(urfp_transaction_env_t* env)
{
    env->put_continue = urfp_put_continue_end;
    if (env->transport->output_on_hold(env))
    {
        return URFP_AGAIN;
    }

    if (env->transport->put_end(env, 0, "success") != URFP_OK)
    {
        return URFP_AGAIN;
    }

    env->put_continue = NULL;
    return URFP_OK;
}

urfp_retval_t urfp_put_continue_abort(urfp_transaction_env_t* env)
{
    env->put_continue = urfp_put_continue_abort;
    if (env->transport->output_on_hold(env))
    {
        return URFP_AGAIN;
    }

    if (env->transport->put_end(env, 1, "aborted") != URFP_OK)
    {
        return URFP_AGAIN;
    }

    env->put_continue = NULL;
    return URFP_OK;
}

/** same as urfp_put_list_array_end but with call to urfp_put_array_end() */
urfp_retval_t urfp_put_continue_array_end(urfp_transaction_env_t* env)
{
    env->put_continue = urfp_put_continue_array_end;
    if (env->transport->output_on_hold(env))
    {
        return URFP_AGAIN;
    }

    if (urfp_put_array_end(env) != URFP_OK)
    {
        return URFP_AGAIN;
    }

    return urfp_put_continue_end(env);
}

/* same as urfp_put_continue_list, but branching to urfp_put_continue_array_end() afterwards */
urfp_retval_t urfp_put_continue_array(urfp_transaction_env_t* env)
{
    urfp_retval_t r = URFP_AGAIN;
    do
    {
        if (env->transport->output_on_hold(env))
        {
            return URFP_AGAIN;
        }
        r = (*(env->continuation.array.output_element))(env);
    }
    while (r == URFP_AGAIN && !env->dry_run);

    /* TODO: Currently URFP_ERR is interpreted as:
        "urfp_put_end() with error code already done in continuation func,
        do not call another urfp_put_end()."... but that's not strictly an
        ERR, just some verify special situation! */

    if (r != URFP_ERR)
    {
        return urfp_put_continue_array_end(env);
    }

    env->put_continue = NULL;
    return URFP_OK;
}

/* same as urfp_put_continue_array, but ending without urfp_put_array_end() */
urfp_retval_t urfp_put_continue_list(urfp_transaction_env_t* env)
{
    urfp_retval_t r = URFP_AGAIN;
    do
    {
        if (env->transport->output_on_hold(env))
        {
            return r;
        }
        r = (*(env->continuation.array.output_element))(env);
    }
    while (r == URFP_AGAIN && !env->dry_run);

    /* TODO: Currently URFP_ERR is interpreted as:
        "urfp_put_end() with error code already done in continuation func,
        do not call another urfp_put_end()."... but that's not strictly an
        ERR, just some verify special situation! */

    if (r != URFP_ERR)
    {
        return urfp_put_continue_end(env);
    }

    env->put_continue = NULL;
    return URFP_OK;
}


urfp_retval_t urfp_put_backgrounded(urfp_transaction_env_t* env, urfp_put_continue_callback_t urfp_put_continue)
{
    urfp_retval_t r;
    r = (*urfp_put_continue)(env);
    if (r == URFP_AGAIN)
    {
        env->put_continue = urfp_put_continue;
    }
    else
    {
        env->put_continue = NULL;
    }
    return r;
}

urfp_retval_t urfp_resume_put(urfp_transaction_env_t* env)
{
    urfp_retval_t r = URFP_OK;
    if (env->put_continue != NULL)
    {
        r = (*(env->put_continue))(env);

        if (r != URFP_AGAIN)
        {
            env->put_continue = NULL;
        }
    }
    return r;
}

/** Functions for continued get */

urfp_retval_t urfp_get_backgrounded(urfp_transaction_env_t* env, urfp_get_continue_callback_t urfp_get_continue, urfp_get_finish_callback_t urfp_get_finish)
{
    urfp_retval_t r;
    r = (*urfp_get_continue)(env);
    if (r == URFP_AGAIN)
    {
        env->get_continue = urfp_get_continue;
        env->get_finish = urfp_get_finish;
    }
    else
    {
        r = (*urfp_get_finish)(env);
        env->get_continue = NULL;
        env->get_finish = NULL;
    }
    return r;
}

urfp_retval_t urfp_resume_get(urfp_transaction_env_t* env)
{
    urfp_retval_t r = URFP_OK;
    if (env->get_continue != NULL)
    {
        r = (*(env->get_continue))(env);
        if (r != URFP_AGAIN)
        {
            env->get_continue = NULL;
        }
    }
    return r;
}

urfp_retval_t urfp_finish_get(urfp_transaction_env_t* env)
{
    urfp_retval_t r = URFP_OK;
    if (env->get_finish != NULL)
    {
        r = (*(env->get_finish))(env);
        env->get_finish = NULL;
    }
    return r;
}


/** Function for getting binary data */

/** Internal helpers for argument access */

bool urfp_more_args_available(urfp_transaction_env_t* env)
{
    if (env->dry_run)
    {
        return true;
    }
    return env->transport->more_args_available(env);
}

int urfp_get_next_arg_name(urfp_transaction_env_t* env, char* buf, size_t maxlen)
{
    return env->transport->get_arg_name(env, buf, maxlen);
}

/** Public API for argument access */

int32_t urfp_get_next_int_arg(urfp_transaction_env_t* env, size_t szof, const char* name)
{
    if (env->dry_run)
    {
        (void)env->transport->describe_arg(env, name, URFP_ARG | URFP_INT, szof);
        return 0;
    }
    else
    {
        return env->transport->get_int(env, szof, name);
    }
}

uint32_t urfp_get_next_unsigned_arg(urfp_transaction_env_t* env, size_t szof, const char* name)
{
    if (env->dry_run)
    {
        (void)env->transport->describe_arg(env, name, URFP_ARG | URFP_UNSIGNED, szof);
        return 0;
    }
    else
    {
        return env->transport->get_unsigned(env, szof, name);
    }
}

uint64_t urfp_get_next_uint64_arg(urfp_transaction_env_t* env, const char* name)
{
    if (env->dry_run)
    {
        (void)env->transport->describe_arg(env, name, URFP_ARG | URFP_UNSIGNED, 8);
        return 0;
    }
    else
    {
        return env->transport->get_uint64(env, name);
    }
}

#ifndef URFP_WITHOUT_FLOAT
double urfp_get_next_float_arg(urfp_transaction_env_t* env, size_t szof, const char* name)
{
    if (env->dry_run)
    {
        if (szof == sizeof(float))
        {
            (void)env->transport->describe_arg(env, name, URFP_ARG | URFP_FLOAT, sizeof(float));
        }
        else
        {
            (void)env->transport->describe_arg(env, name, URFP_ARG | URFP_DOUBLE, sizeof(double));
        }

        return 0;
    }
    else
    {
        return env->transport->get_float(env, szof, name);
    }
}
#endif

bool urfp_get_next_bool_arg(urfp_transaction_env_t* env, const char* name)
{
    if (env->dry_run)
    {
        (void)env->transport->describe_arg(env, name, URFP_ARG | URFP_BOOL, 1);
    }
    else
    {
        return env->transport->get_bool(env, name);
    }

    return false;
}

int urfp_get_next_string_arg(urfp_transaction_env_t* env, const char* name, char* buf, size_t maxlen)
{
    if (env->dry_run)
    {
        (void)env->transport->describe_arg(env, name, URFP_ARG | URFP_STRING, maxlen);
        return -1;
    }
    else
    {
        return env->transport->get_string(env, name, buf, maxlen);
    }
}

urfp_retval_t urfp_get_succeeded(urfp_transaction_env_t* env)
{
    return env->transport->get_succeeded(env);
}

urfp_retval_t urfp_get_array_start(urfp_transaction_env_t* env, const char* name, size_t* maxcount)
{
    return env->transport->get_array_start(env, name, maxcount);
}

urfp_retval_t urfp_get_array_end(urfp_transaction_env_t* env)
{
    return env->transport->get_array_end(env);
}

urfp_retval_t urfp_get_struct_start(urfp_transaction_env_t* env, const char* name)
{
    return env->transport->get_struct_start(env, name);
}

urfp_retval_t urfp_get_struct_end(urfp_transaction_env_t* env)
{
    return env->transport->get_struct_end(env);
}

urfp_retval_t urfp_get_blob_start(urfp_transaction_env_t* env, const char* name, size_t* maxcount)
{
    return env->transport->get_blob_start(env, name, maxcount);
}

urfp_retval_t urfp_get_blob_access(urfp_transaction_env_t* env, void** where, size_t* length)
{
    return env->transport->get_blob_access(env, where, length);
}

urfp_retval_t urfp_get_blob_pause(urfp_transaction_env_t* env)
{
    return env->transport->get_blob_pause(env);
}

urfp_retval_t urfp_get_blob_end(urfp_transaction_env_t* env)
{
    return env->transport->get_blob_end(env);
}


void urfp_get_end(urfp_transaction_env_t* env)
{
    env->transport->get_end(env);
}

/** Generic implementations for variable access in-memory */

urfp_retval_t urfp_get_bool(urfp_transaction_env_t* env)
{
    urfp_put_bool(env, env->elem->name, *(bool*)(env->elem->op));
    return urfp_put_end(env, 0, NULL);
}

urfp_retval_t urfp_set_bool(urfp_transaction_env_t* env, int offset)
{
    /* TODO: offset is ignored */
    bool on = urfp_get_next_bool_arg(env, env->elem->name);
    *(bool*)(env->elem->op) = on;
    return urfp_put_end(env, 0, NULL);
}

/**
@}
*/

