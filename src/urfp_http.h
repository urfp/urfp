/** URFP - Universal Remote Function Protocol

@file  urfp_http.c
@brief Stream parser for HTTP requests and responses
@copyright 2015 Kolja Waschk

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#ifndef URFP_HTTP_H
#define URFP_HTTP_H 1

#include "urfp_server.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Maximum ength of a header: and accumulated value */
#ifndef URFP_HTTP_MAX_LINE_LENGTH
#define URFP_HTTP_MAX_LINE_LENGTH 256
#endif

/* Parse POSTed application/x-www-form-urlencoded? */
#ifndef URFP_HTTP_WITH_FORMS
#define URFP_HTTP_WITH_FORMS 1
#endif

/* Also parse POSTed multipart/form-data? (depends on URFP_HTTP_WITH_FORMS) */
#if URFP_HTTP_WITH_FORMS
#ifndef URFP_HTTP_WITH_MULTIPART
#define URFP_HTTP_WITH_MULTIPART 1
#endif
#endif

typedef enum
{
    RESPREQ_START,
    RESPREQ_HEADER,
    RESPREQ_BODY
}
urfp_respreq_state_t;

typedef enum
{
    NOT_CHUNKED,
    CHUNK_START,
    CHUNK_BODY
}
urfp_chunk_state_t;

typedef enum
{
    LINE_WHITE,
    LINE_CONTENT,
    LINE_CR,
    LINE_CRLF
}
urfp_line_state_t;

typedef struct
{
    urfp_line_state_t state;
    char line[URFP_HTTP_MAX_LINE_LENGTH];
    int len;
}
urfp_header_buf_t;

#if URFP_HTTP_WITH_FORMS
#if URFP_HTTP_WITH_MULTIPART
typedef enum
{
    MULTI_START,
    MULTI_HEADER,
    MULTI_BODY,
    MULTI_END,
}
urfp_multi_state_t;

typedef struct
{
    void (*start)(void* arg);
    void (*header)(void* arg, char* line, int line_len);
    int (*body)(void* arg, uint8_t* buf, int buflen);
    void (*end)(void* arg);
}
urfp_multi_callbacks_t;

typedef struct
{
    urfp_multi_state_t state;

    char boundary[2+70]; /* leading dashes, max 70 for boundary */
    int boundary_len;
    int boundary_matched;

    urfp_header_buf_t part;

    const urfp_multi_callbacks_t* callbacks;
    void* callbacks_arg;
}
urfp_multipart_t;
#endif

typedef enum
{
    FORM_NONE,
    FORM_URLENCODED,
    FORM_MULTIPART
}
urfp_form_type_t;
#endif

typedef struct
{
    void (*start)(void* arg, void* start_line, int len);
    void (*header)(void* arg, void* header_line, int len);
    void (*query)(void* arg, void* name, int name_len, void* value, int value_len);
    int (*body)(void* arg, void* data, int len);
    void (*finish)(void* arg);

#if URFP_HTTP_WITH_MULTIPART
    const urfp_multi_callbacks_t* parts;
#endif

    urfp_retval_t (*write)(void* arg, const void* buf, size_t len);
    bool (*on_hold)(void* arg);
    bool (*more_args)(void* arg);
    int  (*next_arg)(void* arg, char* buf, size_t maxlen);
    int  (*next_name)(void* arg, char* buf, size_t maxlen);
    bool (*enter_compound)(void* arg, const char* name);
    bool (*access_compound)(void* arg, void** where, size_t* maxlen);
    bool (*pause_compound)(void* arg);
    bool (*leave_compound)(void* arg);
}
urfp_http_handlers_t;

typedef struct
{
	size_t in_depth;
	uint32_t in_array;
	uint32_t in_first;
    urfp_retval_t get_succeeded;
}
urfp_http_env_data_t;

typedef enum
{
    OUTFMT_JSON,
    OUTFMT_JSON_UNTAGGED,
    OUTFMT_BINARY
}
urfp_http_output_format_t;

typedef struct
{
    urfp_respreq_state_t respreq_state;
    urfp_chunk_state_t chunk_state;

    /* content_length is either read beforehand from the HTTP header, or, if
     * transfer-encoding: chunked is used, computed by adding the chunk-sizes as
     * they are received. */
    int content_length;
    /* body_length counts all the body bytes already received, or in case of
     * transfer-encoding: chunked, the bytes received of the current chunk. */
    int body_length;
    /* chunk-size is used only with transfer-encoding: chunked and stores the
     * bytes expected to be received during current chunk. */
    int chunk_size;

#if URFP_HTTP_WITH_FORMS
#if URFP_HTTP_WITH_MULTIPART
    urfp_multipart_t multi;

    char* mpquery_name;
    char* mpquery_origname;
    char mpquery_filename[20];
    int mpquery_file;
#endif

    urfp_form_type_t form_type;
    urfp_header_buf_t form;
#endif

    urfp_http_output_format_t output_format;

    urfp_header_buf_t top;

    bool http_1_1;

    const urfp_http_handlers_t *handlers;
    void* handlers_arg;

    urfp_app_t* app;
}
urfp_http_state_t;

extern void urfp_http_init(urfp_http_state_t* state, const urfp_http_handlers_t* handlers, void* arg);
extern void urfp_http_set_output_format(urfp_transaction_env_t* env, urfp_http_output_format_t out_fmt);
extern void urfp_http_parse(uint8_t* buf, int buflen, urfp_http_state_t *state);
extern bool urfp_http_parse_headers(urfp_header_buf_t* hebuf, char c, void (*parse_header)(void* arg, char* line, int line_len), void* arg);

extern void urfp_http_init_multipart(urfp_multipart_t* multi, const urfp_multi_callbacks_t* callbacks, void* arg, char* boundary, int boundary_len);
extern int urfp_http_parse_multipart(urfp_multipart_t* multipart, char* line, int line_len);
extern void urfp_http_finish_multipart(urfp_multipart_t* multi);


extern const urfp_transport_t urfp_http_transport;

#ifdef __cplusplus
}
#endif

#endif
