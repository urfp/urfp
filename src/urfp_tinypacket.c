/* URFP - Universal Remote Function Protocol

Copyright (c) 2015 Kolja Waschk

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

/** @addtogroup urfp_tpkt URFP tinypacket
@{

"urfp_tinypacket": Use URFP on packet-based communication links,
with compact binary structures instead of human-readable ASCII like
urfp_console.c does.

It can be useful with packets carrying as few as 8 byte payload (e.g. CAN) or
up to 35 byte. Like urfp_console, it allows for transmission of data without
size known beforehand and interleaving of multiple parallel responses (with
different tag, that is).

The first packet of a request or response always starts with a four
byte header:

       4 bits prefix (7..4)
         1100 12 * initial and last packet, and all well
         1101 13 ! initial and last packet, error, retrying is useless
         1110 14 ? initial and last packet, error, but retry may succeed
         1111 15 . initial packet, more will follow (then with prefix 0... etc)

       4 bits len    (3..0)
         0000 0
         1111 15 bytes in packet

       1 bit len MSB (=> 15..31), always 0 if no length>15 is supported
       7 bits tag

       7 bit code0 (atm, only lower 5 bits are used, upmost bit should be zero)
       7 bit code1 (atm, only lower 5 bits are used, upmost bit should be zero)


Further packets use a shorter, two-byte header, omitting the code bytes.
Instead, the 'tag' should be used to find the relationship to the former
packets.

       4 bits prefix
         0..7 - incrementing index. more will follow.
          8 * last packet, all well
          9 ! last packet, error, retrying is useless
         10 ? last packet, error, but retry may succeed
         11 . TBD

       4 bits len

       1 bit len MSB => 15..31
       7 bits tag

TBD/Ideas:

  The incrementing prefix may be used to verify order and completeness
    of incoming packets. However, the *last* packet always comes without
    a numbered prefix. To allow verification of completeness, send one packet
    with no data (len==0) but correct prefix after last data packet before
    the final packet.

  If more than one tinypacket are combined into a larger frame and payload
    spans more than one such frame, care should be taken that first
    incrementing prefix actually differs in each *frame* (compared to the
    prefix that appeared first in the previous frame), otherwise lost frames
    could easily go undetected.

  To request a timeout extension, send a packet with no content (length=0).

  Currently the receiver has to know the data types used when constructing
    a transmission. The 5th length bit or one of the unused bits in code may
    indicate that following transmission contains extra data type info.

  If size of transfer is known in advance *and* the underlying hardware can
  assure contiguous transfer of more than one packet (ie. within a larger
  frame: all or none would be lost in case of transmission errors) *and* no
  interleaving with other tagged content is desired, another syntax without
  prefixes might be more efficient for data transfer..?

  More generic, the 5th length bit may indicate some kind of "protocol
  extension", meaning that "length" instead encodes four bits that show
  presence of optional features, such as
    - 32 bit length as payload in first packet
        followed by so many data bytes with no further headers
    - data type info prefixed to data
    - ... ?


*/

#include "urfp_tinypacket.h"
#include "urfp_server.h"
#include "urfp_channel.h"

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

//#define LOG_TINYPACKET_IO 1
#if LOG_TINYPACKET_IO
#include "urfp_sysdep.h"
#endif

static void urfp_tinypacket_determine_tag(urfp_tinypacket_t* tpkt, char* tag)
{
    *tag = tpkt->last_input_tag + 1;
    if (*tag <= '0' || *tag >= '9')
    {
        *tag = '0';
    }
    tpkt->last_input_tag = *tag;
}

static void urfp_tinypacket_determine_oob_tag(urfp_tinypacket_t* tpkt, char* tag)
{
    *tag = tpkt->last_oob_tag + 1;
    if (*tag <= 'A' || *tag >= 'Z')
    {
        *tag = 'A';
    }
    tpkt->last_oob_tag = *tag;
}

/* Reinit only some members that are needed for commencing
   sequence of packets for replying, while there might be still
   some arguments left to get() from the input buffer. */
static void urfp_tinypacket_init_reply(urfp_tinypacket_env_data_t* data)
{
    data->oob = false;
    data->locked = true;
    data->allocated = true;
    data->final_prefix = 0;
    data->curr_prefix = 0xf0;
    data->first_packet = true;

    data->output_length = 0;
    if (data->arg_offset < data->input_length && data->input == data->content && data->input_size == data->content_size)
    {
        /* If not already tried, move remaining input to end of buffer
            to make some room for constructing the output */

        int n_to_move = data->input_length - data->arg_offset;
        char* dst = data->content + data->content_size - n_to_move;
        memmove(dst, data->input + data->arg_offset, n_to_move);
        data->input = dst;
        data->input_length = n_to_move;
        data->input_size = n_to_move;
        data->arg_offset = 0;
        data->output_size = data->content_size - n_to_move;
    }
    else
    {
        data->output_size = data->content_size;
    }
}

static void urfp_tinypacket_init_data(urfp_tinypacket_t* tpkt, urfp_tinypacket_env_data_t* data)
{
    urfp_tinypacket_init_reply(data);

    data->expected_prefix = 0;

    data->content_size = URFP_TINYPACKET_MAX_CONTENT_LENGTH;

    data->output = data->content;
    data->output_size = data->content_size;
    data->output_length = 0;

    data->input = data->content;
    data->input_size = data->content_size;
    data->input_length = 0;
    data->arg_offset = 0;
}

/**
 * @param tpkt   Pointer to URFP tinypacket instance
 * @param data   Pointer to tinypacket environment data
 * @param code   An unsigned value. Its LSB will be output as first command char, second by as second command char
 * @param tag    Pointer to char containing tag. Put 0 in the char to have it set by URFP (for OOB)
 */

/* Try to get rid of as many bytes from string as possible */

static urfp_retval_t urfp_tinypacket_flush_output(urfp_tinypacket_t* tpkt, urfp_tinypacket_env_data_t* data, unsigned code, char* tag)
{
    if (*tag == 0)
    {
        if (data->oob)
        {
            urfp_tinypacket_determine_oob_tag(tpkt, tag);
        }
        else
        {
            urfp_tinypacket_determine_tag(tpkt, tag);
        }
    }

    /* Send as many packets of maximum size as possible */

    int coff = 0; /* Offset for reading from data->output */
    int poff = data->first_packet ? 4 : 2; /* Offset for writing into packet */

    int r = URFP_OK;
    while (r == URFP_OK && (data->output_length - coff) >= (tpkt->outbuf_size - poff))
    {
        int plen = tpkt->outbuf_size - poff;

        tpkt->outbuf[0] = data->curr_prefix | (plen & 0x0f);
        tpkt->outbuf[1] = ((plen & 0x10) << 3) | *tag;

        memcpy(tpkt->outbuf + poff, data->output + coff, plen);

        if (data->first_packet)
        {
            tpkt->outbuf[2] = (code) & 0xff;
            tpkt->outbuf[3] = (code >> 8) & 0xff;
        }

        r = tpkt->output_packet(tpkt->arg, tpkt->outbuf, poff + plen, false);

        if (r == URFP_OK)
        {
            coff += plen;
            data->first_packet = false;
            data->curr_prefix = (data->curr_prefix + 0x10) & 0x70;
        }

        poff = 2;
    }

    /* Send remainder, eventually the final packet */

    int plen = data->output_length - coff;

    if ((r == URFP_OK) && ((data->final_prefix != 0) || (plen > 0)))
    {
        uint8_t header;
        bool urgent = false;
        if (data->final_prefix == 0)
        {
            header = data->curr_prefix;
        }
        else
        {
            urgent = true;
            if (data->final_prefix == '*')
            {
                header = (8u<<4);
            }
            else if (data->final_prefix == '!')
            {
                header = (9u<<4);
            }
            else /* prefix == '?' */
            {
                header = (10u<<4);
            }

            if (data->first_packet)
            {
                header |= (4u<<4);
            }
        }

        if (plen > 0)
        {
            header |= (plen & 0x0f);
            memcpy(tpkt->outbuf + poff, data->output + coff, plen);
        }

        tpkt->outbuf[0] = header;
        tpkt->outbuf[1] = ((plen & 0x10) << 3) | *tag;

        if (data->first_packet)
        {
            tpkt->outbuf[2] = (code) & 0xff;
            tpkt->outbuf[3] = (code >> 8) & 0xff;
        }

        r = tpkt->output_packet(tpkt->arg, tpkt->outbuf, poff + plen, urgent);

        if (r == URFP_OK)
        {
            data->first_packet = false;
            coff += plen;

            if (data->final_prefix == 0)
            {
                data->curr_prefix = (data->curr_prefix + 0x10) & 0x70;
            }
            else
            {
                data->final_prefix = 0;
            }
        }
    }

    if (coff > 0)
    {
        data->output_length -= coff;
        if (data->output_length > 0)
        {
            memmove(data->output, data->output + coff, data->output_length);
        }
    }

    return r;
}

/* Stripped down version of urfp_tinypacket_flush_output for empty error packets without environment_data */
static urfp_retval_t urfp_tinypacket_empty_output(urfp_tinypacket_t* tpkt, bool oob, int prefix, unsigned code, char* tag)
{
    if (*tag == 0)
    {
        if (oob)
        {
            urfp_tinypacket_determine_oob_tag(tpkt, tag);
        }
        else
        {
            urfp_tinypacket_determine_tag(tpkt, tag);
        }
    }

    /* Send as many packets of maximum size as possible */

    uint8_t header;
    if (prefix == '*')
    {
        header = (8u<<4);
    }
    else if (prefix == '!')
    {
        header = (9u<<4);
    }
    else /* prefix == '?' */
    {
        header = (10u<<4);
    }

    tpkt->outbuf[0] = header | (4u<<4);
    tpkt->outbuf[1] = *tag;

    tpkt->outbuf[2] = (code) & 0xff;
    tpkt->outbuf[3] = (code >> 8) & 0xff;

    return tpkt->output_packet(tpkt->arg, tpkt->outbuf, 4, /* urgent= */ true);
}


static inline bool urfp_tinypacket_output_on_hold(urfp_transaction_env_t* env)
{
    urfp_tinypacket_env_data_t* data = (urfp_tinypacket_env_data_t*)(env->transport_data);

    return (data->final_prefix != 0) || (data->output_length > 0);
}

urfp_retval_t urfp_tinypacket_resume_query(urfp_tinypacket_t* tpkt)
{
    int i;

    for (i=0; i<URFP_TINYPACKET_MAX_REPLY_ENVS; i++)
    {
        urfp_tinypacket_env_data_t* data = &(tpkt->transaction_env_data[i]);
        if (data->allocated && !data->locked)
        {
            data->locked = true;

            urfp_transaction_env_t* env = &(tpkt->transaction_env[i]);

            if (env->get_continue != NULL)
            {
                if (urfp_resume_get(env) != URFP_AGAIN)
                {
                    urfp_finish_get(env);
                }
            }

            data->locked = false;
        }
    }

    return URFP_OK;
}

/* Check on all environments if a reply has to be continued */
/*
 * Return:
 *   URFP_OK if there is nothing left to do
 *   URFP_AGAIN if not all replies could be completed at this time (e.g. missing input from A/D)
 *   URFP_ERR if not all replies could be completed due to an error (e.g. lack of output buffer space)
 *
 * Should be called again after some time passed, or, if it didn't return URFP_OK, after
 * actions have been taken that could have generated more input or corrected the errors.
 */
urfp_retval_t urfp_tinypacket_resume_replies(urfp_tinypacket_t* tpkt)
{
    int i;
    urfp_retval_t r = URFP_OK;

#if LOG_TINYPACKET_IO
    urfp_logf(URFP_LOG_INFO, "%s", __func__);
#endif

    for (i=0; i<URFP_TINYPACKET_MAX_REPLY_ENVS && r == URFP_OK; i++)
    {
        urfp_tinypacket_env_data_t* data = &(tpkt->transaction_env_data[i]);
        if (data->allocated && !data->locked)
        {
            data->locked = true;

            urfp_transaction_env_t* env = &(tpkt->transaction_env[i]);

            if (urfp_tinypacket_output_on_hold(env))
            {
                r = urfp_tinypacket_flush_output(tpkt, data, env->code, &(env->tag));
            }

            if (r == URFP_OK && !urfp_tinypacket_output_on_hold(env))
            {
                r = urfp_resume_put(env);
            }

            data->locked = false;

            /* TBD: Check not just output_length but use output_on_hold()? */
            if (r != URFP_OK || data->output_length>0)
            {
                return URFP_AGAIN;
            }
        }
    }

    return r;
}

void urfp_tinypacket_free_env(urfp_tinypacket_t* tpkt, urfp_transaction_env_t* env)
{
#if 1 /* validate pointer */
    int i;
    for (i=0; i<URFP_TINYPACKET_MAX_REPLY_ENVS; i++)
    {
        if (env == &(tpkt->transaction_env[i]))
        {
            urfp_tinypacket_env_data_t* data = &(tpkt->transaction_env_data[i]);
            data->final_prefix = 0;
            data->locked = false;
            if (!data->keep_allocated)
            {
                data->allocated = false;
            }
        }
    }
#else
    ((urfp_tinypacket_env_data_t*)env->transport_data)->allocated = false;
#endif
}

static urfp_transaction_env_t* urfp_tinypacket_alloc_env(urfp_tinypacket_t* tpkt)
{
    int i;
    for (i=0; i<URFP_TINYPACKET_MAX_REPLY_ENVS; i++)
    {
        urfp_tinypacket_env_data_t* data = &(tpkt->transaction_env_data[i]);
        if (!data->allocated)
        {
            urfp_tinypacket_init_data(tpkt, data);

            return &(tpkt->transaction_env[i]);
        }
    }

    return NULL;
}

static urfp_transaction_env_t* urfp_tinypacket_find_env(urfp_tinypacket_t* tpkt, urfp_envmode_t mode, unsigned code, char tag)
{
    int i;
    for (i=0; i<URFP_TINYPACKET_MAX_REPLY_ENVS-1; i++) /* the last one is not considered here, it's for log OUTPUT */
    {
        urfp_tinypacket_env_data_t* data = &(tpkt->transaction_env_data[i]);
        if (data->allocated)
        {
            urfp_transaction_env_t* env = &(tpkt->transaction_env[i]);

            /* For incoming requests, urfp_dispatch_query() already might have turned
             * the environment mode into URFP_ENV_REPLY although there is still more
             * input coming in... (with mode URFP_ENV_REQ_IN) */

            if (((env->mode ==  mode) || (env->mode == URFP_ENV_REPLY && mode == URFP_ENV_REQ_IN)
                    /* Work in progress: Extra handling of OOB */
                    || (env->mode == URFP_ENV_REQ_IN && mode == URFP_ENV_REQ_OUT && code==0 && env->code == URFP_CODE('l','g')))
                    && ((env->tag == tag) || (tag == 0))
                    && ((env->code == code) || (code == 0)))
            {
                urfp_tinypacket_env_data_t* data = &(tpkt->transaction_env_data[i]);
                data->locked = true;
                return env;
            }
        }
    }

    return NULL;
}

void urfp_tinypacket_catch_up(urfp_tinypacket_env_data_t* data)
{
    if (data->arg_offset > 0 && data->input_length > data->arg_offset)
    {
        data->input_length -= data->arg_offset;
        memmove(data->input, data->input + data->arg_offset, data->input_length);
        data->arg_offset = 0;
    }
    else if (data->arg_offset == data->input_length)
    {
        data->arg_offset = 0;
        data->input_length = 0;
    }
}

urfp_retval_t urfp_tinypacket_input(urfp_tinypacket_t* tpkt, urfp_envmode_t mode, const uint8_t* msg, int len, urfp_transaction_env_t** found_env)
{
#if LOG_TINYPACKET_IO
    urfp_logf(URFP_LOG_INFO, "%s", __func__);
#endif

    if (found_env != NULL)
    {
        *found_env = NULL; // default result, just in case nothing else is found
    }

    if (msg == NULL || len < 1)
    {
        /* too short to be interpreted as a proper URFP tinypacket */
        return URFP_ERR;
    }

    uint8_t prefix = (msg[0] & 0xf0) >> 4;
    uint8_t payoff = (prefix >= 12) ? 4 : 2;

    if (len < payoff)
    {
        /* too short to be interpreted as a proper URFP tinypacket */
        return URFP_ERR;
    }

    uint8_t paylen = ((msg[1] >> 3) & 0x10) + (msg[0] & 0x0f);

    urfp_transaction_env_t* env = NULL;
    urfp_tinypacket_env_data_t* data = NULL;

    char new_tag = (msg[1] & 0x7f);
    if (prefix >= 12)
    {
        /* This is an initial packet. */
        unsigned new_code = URFP_CODE(('a'-1 + (msg[2] & 0x1f)), ('a'-1 + (msg[3] & 0x1f)));

        /* Work in progress: Handle OOB data */
        if (mode == URFP_ENV_REQ_OUT && new_code == URFP_CODE('l','g'))
        {
            mode = URFP_ENV_REQ_IN;
        }

        if (mode == URFP_ENV_REQ_IN)
        {
            /* We handle only one REQ_IN at a time. Free others (probably incomplete) */
            while ((env = urfp_tinypacket_find_env(tpkt, mode, 0, 0)) != NULL)
            {
                urfp_tinypacket_free_env(tpkt, env);
            }
        }

        /* Look if an environment is already allocated to handle this (reply or oob) */
        env = urfp_tinypacket_find_env(tpkt, mode, new_code, new_tag);

        /* 3. Otherwise allocate a new environment for the unexpected */
        if (env == NULL && mode == URFP_ENV_REQ_IN)
        {
            env = urfp_tinypacket_alloc_env(tpkt);

            if (env != NULL)
            {
                env->mode = mode;
                env->code = new_code;
                env->tag = new_tag;
            }
        }

        tpkt->last_input_tag = new_tag;

        if (env != NULL)
        {
            data = (urfp_tinypacket_env_data_t*)(env->transport_data);

            data->input_length = 0;
            data->arg_offset = 0;
        }
    }
    else
    {
        /* This is a continuation packet */
        env = urfp_tinypacket_find_env(tpkt, mode, 0, new_tag);
    }

    if (env != NULL)
    {
        data = (urfp_tinypacket_env_data_t*)(env->transport_data);

        if (prefix < 8 && prefix != data->expected_prefix)
        {
            /* We obviously lost a packet */
            /* TODO:
             * 1. Send error reply
             * 2. Mark environment as erraneous but do not free up yet
             * 3. Later then free environment after timeout or when we got final input packet
             */

            /* FOR NOW: Just dealloc environment immediately. This will cause at least one
             * error reply packet. TODO: Prevent more than this first one. */

            urfp_tinypacket_free_env(tpkt, env);

            env = NULL;
        }
    }

    if (env == NULL)
    {
        /* No env could be found or allocated or allocated. */

        if (mode == URFP_ENV_REQ_IN)
        {
            /* Transport data used for immediate error responses (no environment needed) */
            urfp_tinypacket_empty_output(tpkt, /*oob=*/false, '!', URFP_CODE('?', '?'), &new_tag);
        }

        /* Packet was handled OK, although the result could be better */
        return URFP_OK;
    }

    if (paylen > 0)
    {
        /* If input buffer is so full that additional data wouldn't fit,
         * branch immediately to urfp_dispatch_query() (before the request
         * has been received completely) to start handling the request
         * and hopefully eat some of the input bytes.
         */
        if (data->input_length + paylen > data->input_size)
        {
            if (mode == URFP_ENV_REQ_IN && env->mode == URFP_ENV_REQ_IN)
            {
                /* Signal this is not the end (might be checked by blob_access etc.) */
                data->expected_prefix = (prefix + 1) & 7;

                /* env->mode <= URFP_ENV_REPLY */
                urfp_dispatch_query(env, env->code, env->tag);

                urfp_tinypacket_catch_up(data);
            }
        }

        /* If there's still too much additional input, tell caller. */
        if (data->input_length + paylen > data->input_size)
        {
            return URFP_AGAIN;
        }
        else /* Otherwise append the new input to the (shortened) buffer and continue */
        {
            memcpy(data->input + data->input_length, msg + payoff, paylen);
            data->input_length += paylen;
        }
    }

    if (prefix < 8 || prefix == 15)
    {
    	data->expected_prefix = (prefix + 1) & 7;
    }
    else
    {
        data->expected_prefix = prefix;

        if ((prefix == 8 || prefix == 12)
            && (env->mode == URFP_ENV_REQ_IN))
        {
            if (mode == URFP_ENV_REQ_IN)
            {
                /* env->mode <= URFP_ENV_REPLY */
                urfp_dispatch_query(env, env->code, env->tag);
                urfp_tinypacket_catch_up(data);
            }
            else if (env->code == URFP_CODE('l','g'))
            {
                /* Work in progress: Handle OOB data */
                urfp_dispatch_query(env, env->code, env->tag);
                urfp_tinypacket_catch_up(data);
            }
        }
    }

    if (found_env != NULL)
    {
        *found_env = env;
    }

    data->locked = false;

    return URFP_OK;
}

urfp_retval_t urfp_tinypacket_append_output(urfp_transaction_env_t* env, uint8_t* src, size_t len)
{
    urfp_retval_t r = URFP_OK;

    urfp_tinypacket_t* tpkt = (urfp_tinypacket_t*)(env->transport_conn);
    urfp_tinypacket_env_data_t* data = (urfp_tinypacket_env_data_t*)(env->transport_data);

    size_t srcoff = 0;

    if (src == NULL || len > data->output_size)
    {
        r = URFP_ERR;
    }

    /* Try to make some room if new data wouldn't fit as is */
    while (r == URFP_OK && data->output_length + len > data->output_size)
    {
        r = urfp_tinypacket_flush_output(tpkt, data, env->code, &env->tag);
    }

    /* The loop may have been aborted if output_packet returned URFP_AGAIN
     * so we cannot be sure here that there's now enough room. But if there
     * is, that is OK.
     */

    if (data->output_length + len <= data->output_size && len > 0)
    {
        memcpy(data->output + data->output_length, src+srcoff, len);
        data->output_length += len;

        r = URFP_OK;
    }

    return r;
}


urfp_retval_t urfp_tinypacket_put_string(urfp_transaction_env_t* env, const char* name, const char* string)
{
    if (string == NULL)
    {
        return URFP_OK;
    }

    return urfp_tinypacket_append_output(env, (uint8_t*)string, strlen(string) + 1);
}

urfp_retval_t urfp_tinypacket_put_bool(urfp_transaction_env_t* env, const char* name, bool value)
{
    uint8_t byte = value ? 1:0;
    return urfp_tinypacket_append_output(env, &byte, 1);
}

urfp_retval_t urfp_tinypacket_put_int(urfp_transaction_env_t* env, size_t szof, const char* name, int32_t value)
{
    return urfp_tinypacket_append_output(env, (uint8_t*)&value, szof);
}

urfp_retval_t urfp_tinypacket_put_unsigned(urfp_transaction_env_t* env, size_t szof, const char* name, uint32_t value)
{
    return urfp_tinypacket_append_output(env, (uint8_t*)&value, szof);
}

urfp_retval_t urfp_tinypacket_put_uint64(urfp_transaction_env_t* env, const char* name, uint64_t value)
{
    return urfp_tinypacket_append_output(env, (uint8_t*)&value, 8);
}

#ifndef URFP_WITHOUT_FLOAT
urfp_retval_t urfp_tinypacket_put_float(urfp_transaction_env_t* env, size_t szof, const char* name, double value)
{
    if (szof == sizeof(float))
    {
        float v = value;
        return urfp_tinypacket_append_output(env, (uint8_t*)&v, sizeof(float));
    }
    return urfp_tinypacket_append_output(env, (uint8_t*)&value, sizeof(double));
}
#endif

urfp_retval_t urfp_tinypacket_put_array_start(urfp_transaction_env_t* env, const char* name, int maxcount)
{
    return URFP_OK;
}

urfp_retval_t urfp_tinypacket_put_array_end(urfp_transaction_env_t* env)
{
    return URFP_OK;
}

urfp_retval_t urfp_tinypacket_put_struct_start(urfp_transaction_env_t* env, const char* name)
{
    return URFP_OK;
}

urfp_retval_t urfp_tinypacket_put_struct_end(urfp_transaction_env_t* env)
{
    return URFP_OK;
}


urfp_retval_t urfp_tinypacket_put_start(urfp_transaction_env_t* env)
{
    urfp_tinypacket_env_data_t* data = (urfp_tinypacket_env_data_t*)(env->transport_data);

    urfp_tinypacket_init_reply(data);

    return URFP_OK;
}

urfp_retval_t urfp_tinypacket_put_end(urfp_transaction_env_t* env, int error_code, char* error_text)
{
    urfp_tinypacket_t* tpkt = (urfp_tinypacket_t*)(env->transport_conn);
    urfp_tinypacket_env_data_t* data = (urfp_tinypacket_env_data_t*)(env->transport_data);

    urfp_retval_t r;

    if (error_code == 0)
    {
        data->final_prefix = '*';
    }
    else
    {
        data->final_prefix = '!';
    }

    r = urfp_tinypacket_flush_output(tpkt, data, env->code, &env->tag);

    if (env->mode == URFP_ENV_REPLY)
    {
        /* TODO: Ensure that 1. END IS SENT *and* 2. ENV IS RESET */
        /* Zero data in case of reuse of static env, e.g. for logging */
        urfp_tinypacket_init_data(tpkt, (urfp_tinypacket_env_data_t*)(env->transport_data));
        urfp_tinypacket_free_env(tpkt, env);
    }
    else if (r == URFP_AGAIN)
    {
        data->final_prefix = 0;
    }

    return r;
}

urfp_retval_t urfp_tinypacket_describe_arg(urfp_transaction_env_t* env, const char* name, urfp_argtype_t type, int len)
{
    char t = 0;
    urfp_retval_t r;

#define URFP_TINYPACKET_DESCRIBE_WITH_NAME 1

    /* Output type prefix which indicates whether the type describes an argument or result */
#if URFP_TINYPACKET_DESCRIBE_WITH_NAME
    env->transport->put_unsigned(env, 1, "arg", ((type | URFP_NAMED | URFP_TYPE_PREFIX) & URFP_TYPE_MODE) >> 8);
#else
    env->transport->put_unsigned(env, 1, "arg", ((type | URFP_TYPE_PREFIX) & URFP_TYPE_MODE) >> 8);
#endif

    /* Output type byte with length bits amended */
    t = type & URFP_TYPE_MASK;
    if ((t == URFP_INT) || (t == URFP_UNSIGNED))
    {
        switch(len)
        {
        case 2: t |= 1; break;
        case 4: t |= 2; break;
        case 8: t |= 3; break;
        default: break;
        }
    }

    r = env->transport->put_unsigned(env, 1, name, t);

    /* When starting arrays add the length info as an int32 */
    if (t == URFP_ARRAY_START && r == URFP_OK)
    {
        r = env->transport->put_int(env, 4, "length", len);
    }

#if URFP_TINYPACKET_DESCRIBE_WITH_NAME
    /* Output name */
    r = env->transport->put_string(env, "name", name);
#endif

    return r;
}

static bool urfp_tinypacket_more_args_available(urfp_transaction_env_t *env)
{
    urfp_tinypacket_env_data_t* data = (urfp_tinypacket_env_data_t*)(env->transport_data);

    return (data->arg_offset < data->input_length);
}

static int urfp_tinypacket_get_arg_name(urfp_transaction_env_t *env, char* buf, size_t maxlen)
{
    return 0;
}


bool urfp_tinypacket_formatted_available(urfp_transaction_env_t *env, const char* fmt)
{
    urfp_tinypacket_env_data_t* data = (urfp_tinypacket_env_data_t*)(env->transport_data);

    if (fmt == NULL)
    {
        return true;
    }

    int i = 0;
    int p = data->arg_offset;
    for (i=0; fmt[i] != 0; i++)
    {
        switch(fmt[i])
        {
        case 's':
        {
            while (p < data->input_length && data->input[p] != 0)
            {
                p++;
            }
            if (p >= data->input_length || data->input[p] != 0)
            {
                return false;
            }
            p++;
            break;
        }
        case 'b':
        case 'B':
            p += 1;
            if (p >= data->input_length)
            {
                return false;
            }
            break;
        case 'h':
        case 'H':
            p += 2;
            if (p >= data->input_length)
            {
                return false;
            }
            break;
        case 'i':
        case 'I':
            p += 4;
            if (p >= data->input_length)
            {
                return false;
            }
            break;
        default:
            break;
        }
    }

    return true;
}

uint32_t urfp_tinypacket_get_unsigned_arg(urfp_transaction_env_t* env, size_t szof, const char* name)
{
    urfp_tinypacket_env_data_t* data = (urfp_tinypacket_env_data_t*)(env->transport_data);

    uint32_t r = 0;
    if (data->input_length >= data->arg_offset + szof)
    {
        memcpy(&r, data->input + data->arg_offset, szof);
    }

    data->arg_offset += szof;
    return r;
}

int32_t urfp_tinypacket_get_int_arg(urfp_transaction_env_t* env, size_t szof, const char* name)
{
    urfp_tinypacket_env_data_t* data = (urfp_tinypacket_env_data_t*)(env->transport_data);

    int32_t r = 0;
    if (data->input_length >= data->arg_offset + szof)
    {
        memcpy(&r, data->input + data->arg_offset, szof);

        /* sign extension: */
        switch(szof)
        {
        case 1: r = *(int8_t*)&r; break;
        case 2:
        {
            if (r >= 0x00008000)
            {
                r -= (int32_t)0x10000;
            }
            break;
        }
        case 3:
        {
            if (r >= 0x00800000)
            {
                r -= (int32_t)0x01000000;
            }
            break;
        }
        case 4:
        default:
            break;
        }
    }

    data->arg_offset += szof;
    return r;
}

uint64_t urfp_tinypacket_get_uint64_arg(urfp_transaction_env_t* env, const char* name)
{
    urfp_tinypacket_env_data_t* data = (urfp_tinypacket_env_data_t*)(env->transport_data);

    uint64_t r = 0;
    if (data->input_length >= data->arg_offset + 8)
    {
        memcpy(&r, data->input + data->arg_offset, 8);
    }

    data->arg_offset += 8;
    return r;
}

#ifndef URFP_WITHOUT_FLOAT
double urfp_tinypacket_get_float_arg(urfp_transaction_env_t* env, size_t szof, const char* name)
{
    urfp_tinypacket_env_data_t* data = (urfp_tinypacket_env_data_t*)(env->transport_data);

    if (szof == sizeof(float))
    {
        float r = 0.0;
        if (data->input_length >= data->arg_offset + sizeof(float))
        {
            memcpy(&r, data->input + data->arg_offset, sizeof(float));
        }
        data->arg_offset += sizeof(float);
        return (double)r;
    }
    else
    {
        double r = 0.0;
        if (data->input_length >= data->arg_offset + sizeof(double))
        {
            memcpy(&r, data->input + data->arg_offset, sizeof(double));
        }
        data->arg_offset += sizeof(double);
        return r;
    }
}
#endif

bool urfp_tinypacket_get_bool_arg(urfp_transaction_env_t* env, const char* name)
{
    urfp_tinypacket_env_data_t* data = (urfp_tinypacket_env_data_t*)(env->transport_data);

    bool r = false;
    if (data->arg_offset < data->input_length)
    {
        r = (data->input[data->arg_offset] != 0);
        data->arg_offset ++;
    }

    return r;
}

int urfp_tinypacket_get_string_arg(urfp_transaction_env_t* env, const char* name, char* buf, size_t maxlen)
{
    urfp_tinypacket_env_data_t* data = (urfp_tinypacket_env_data_t*)(env->transport_data);

    int n = 0;
    int p = data->arg_offset;
    while (p < data->input_length && data->input[p] != 0)
    {
        p++;
    }
    if (p < data->input_length)
    {
        n = p - data->arg_offset;
        if (n >= maxlen)
        {
            memcpy(buf, data->input + data->arg_offset, maxlen-1);
            buf[maxlen-1] = 0;
        }
        else
        {
            memcpy(buf, data->input + data->arg_offset, n);
            buf[n] = 0;
        }
        data->arg_offset += n+1;
    }
    else
    {
        /* no terminating zero, nothing copied */
    }

    return n;
}

urfp_retval_t urfp_tinypacket_get_succeeded(urfp_transaction_env_t* env)
{
    return URFP_OK;
}

urfp_retval_t urfp_tinypacket_get_blob_start(urfp_transaction_env_t* env, const char* name, size_t* maxlength)
{
    return URFP_OK;
}

urfp_retval_t urfp_tinypacket_get_blob_access(urfp_transaction_env_t* env, void** ptr, size_t* length)
{
    urfp_tinypacket_env_data_t* data = (urfp_tinypacket_env_data_t*)(env->transport_data);

    *length = data->input_length - data->arg_offset;
    *ptr = data->input + data->arg_offset;

    if (*length > 0)
    {
        return URFP_OK;
    }

    if (data->expected_prefix == 8 || data->expected_prefix == 12)
    {
        /* Got final packet, no more data */
        return URFP_OK;
    }

    if (data->expected_prefix < 8 || data->expected_prefix == 15)
    {
        /* Even if there was no data yet, more packets are expected */
        return URFP_AGAIN;
    }

    return URFP_ERR;
}

urfp_retval_t urfp_tinypacket_get_blob_pause(urfp_transaction_env_t* env)
{
    urfp_tinypacket_env_data_t* data = (urfp_tinypacket_env_data_t*)(env->transport_data);

    /* TODO! The inbuf should be protected from growing between get_blob_access
     * and get_blob_pause using a lock(), or we should only free up the space
     * that definitely has been reported by get_blob_access to be available,
     * or ... otherwise this might free up data that was received between
     * the two function calls!
     */

    data->arg_offset = 0;
    data->input_length = 0;

    return URFP_OK;
}

urfp_retval_t urfp_tinypacket_get_blob_end(urfp_transaction_env_t* env)
{
    return URFP_OK;
}

urfp_retval_t urfp_tinypacket_get_array_start(urfp_transaction_env_t* env, const char* name, size_t* maxcount)
{
    return URFP_OK;
}

urfp_retval_t urfp_tinypacket_get_array_end(urfp_transaction_env_t* env)
{
    return URFP_OK;
}

urfp_retval_t urfp_tinypacket_get_struct_start(urfp_transaction_env_t* env, const char* name)
{
    return URFP_OK;
}

urfp_retval_t urfp_tinypacket_get_struct_end(urfp_transaction_env_t* env)
{
    return URFP_OK;
}


void urfp_tinypacket_get_end(urfp_transaction_env_t* env)
{
    if (env->mode == URFP_ENV_REQ_OUT)
    {
        urfp_tinypacket_t* tpkt = (urfp_tinypacket_t*)(env->transport_conn);
        urfp_tinypacket_free_env(tpkt, env);
    }
    else
    {
        /* Discard remaining input, full buffer can be used for output */
        urfp_tinypacket_env_data_t* data = (urfp_tinypacket_env_data_t*)(env->transport_data);
        data->output_size = data->content_size;
    }
}

const urfp_transport_t urfp_tinypacket_transport =
{
    .get_string = urfp_tinypacket_get_string_arg,
    .get_bool = urfp_tinypacket_get_bool_arg,
    .get_int = urfp_tinypacket_get_int_arg,
    .get_unsigned = urfp_tinypacket_get_unsigned_arg,
    .get_uint64 = urfp_tinypacket_get_uint64_arg,
#ifndef URFP_WITHOUT_FLOAT
    .get_float = urfp_tinypacket_get_float_arg,
#endif
    .get_succeeded = urfp_tinypacket_get_succeeded,
    .get_array_start = urfp_tinypacket_get_array_start,
    .get_array_end = urfp_tinypacket_get_array_end,
    .get_struct_start = urfp_tinypacket_get_struct_start,
    .get_struct_end = urfp_tinypacket_get_struct_end,
    .get_blob_start = urfp_tinypacket_get_blob_start,
    .get_blob_access = urfp_tinypacket_get_blob_access,
    .get_blob_pause = urfp_tinypacket_get_blob_pause,
    .get_blob_end = urfp_tinypacket_get_blob_end,

    .get_end = urfp_tinypacket_get_end,

    .more_args_available = urfp_tinypacket_more_args_available,
    .get_arg_name = urfp_tinypacket_get_arg_name,
    .output_on_hold = urfp_tinypacket_output_on_hold,
    .describe_arg = urfp_tinypacket_describe_arg,

    .put_start = urfp_tinypacket_put_start,
    .put_string = urfp_tinypacket_put_string,
    .put_bool = urfp_tinypacket_put_bool,
    .put_int = urfp_tinypacket_put_int,
    .put_unsigned = urfp_tinypacket_put_unsigned,
    .put_uint64 = urfp_tinypacket_put_uint64,
#ifndef URFP_WITHOUT_FLOAT
    .put_float = urfp_tinypacket_put_float,
#endif
    .put_array_start = urfp_tinypacket_put_array_start,
    .put_array_end = urfp_tinypacket_put_array_end,
    .put_struct_start = urfp_tinypacket_put_struct_start,
    .put_struct_end = urfp_tinypacket_put_struct_end,
    .put_end = urfp_tinypacket_put_end,
};

/**
 * @param tpkt   Pointer to urfp_tinypacket to initialize (allocated by you)
 * @param app    Application elements and callbacks (provided by you)
 * @param inbuf  A buffer for input that you have to provide to UrFP
 * @param inbuf_size Number of bytes your inbuf can take
 * @param outbuf A buffer output that you have to provide to UrFP
 * @param outbuf_size Number of bytes your outbuf can take
 *
 */

urfp_retval_t urfp_tinypacket_init(urfp_tinypacket_t* tpkt, urfp_app_t* app,
    uint8_t* inbuf, size_t inbuf_size, uint8_t* outbuf, size_t outbuf_size)
{
    int i;

    tpkt->last_input_tag = 0;
    tpkt->last_oob_tag = 0;

    /* Used for constructing single packets from content for output */
    tpkt->outbuf = outbuf;
    tpkt->outbuf_size = outbuf_size;

    urfp_transaction_env_t* env;
    urfp_tinypacket_env_data_t* data;

    for (i=0; i<URFP_TINYPACKET_MAX_REPLY_ENVS; i++)
    {
        env = &(tpkt->transaction_env[i]);
        data = &(tpkt->transaction_env_data[i]);

        memset(data, 0, sizeof(urfp_tinypacket_env_data_t));
        data->allocated = false;
        data->oob = false;

        memset(env, 0, sizeof(urfp_transaction_env_t));
        env->app = app;
        env->transport = &urfp_tinypacket_transport;
        env->transport_conn = tpkt;
        env->transport_data = data;
    }

    /* Last in list is dedicated for logging. Use env and data pointers that
       are still pointing to tpkt->transaction_*[URFP_TINYPACKET_MAX_REPLY_ENVS-1] */
    env->tag = 'A';
    env->code = URFP_CODE('l','g');
    env->mode = URFP_ENV_REPLY;
    urfp_tinypacket_init_data(tpkt, data);
    data->allocated = true;
    data->oob = true;

    return URFP_OK;
}

static urfp_transaction_env_t* urfp_tinypacket_get_oob_env(void* arg, urfp_channel_t* channel)
{
    urfp_tinypacket_t* tpkt = (urfp_tinypacket_t*) arg;
    urfp_transaction_env_t* env = &(tpkt->transaction_env[URFP_TINYPACKET_MAX_REPLY_ENVS-1]);
    env->channel = channel;
    urfp_tinypacket_determine_oob_tag(tpkt, &(env->tag));
    return env;
}

static void urfp_tinypacket_release_oob_env(void* arg, urfp_transaction_env_t* env)
{
    /* nothing to do */
}

void urfp_tinypacket_set_channel(urfp_tinypacket_t* tpkt, urfp_channel_t* channel)
{
    for (int i=0; i<URFP_TINYPACKET_MAX_REPLY_ENVS; i++)
    {
        tpkt->transaction_env[i].channel = channel;
    }

    channel->get_env = urfp_tinypacket_get_oob_env;
    channel->get_env_arg = (void*)tpkt;
    channel->release_env = urfp_tinypacket_release_oob_env;
}

urfp_transaction_env_t* urfp_tinypacket_request_start(urfp_tinypacket_t* tpkt, unsigned code)
{
    urfp_transaction_env_t* env = urfp_tinypacket_alloc_env(tpkt);
    if (env != NULL)
    {
        /* What to put in request */
        env->mode = URFP_ENV_REQ_OUT;
        env->code = code;

        /* What to expect in reply */
        urfp_tinypacket_determine_tag(tpkt, &(env->tag));
    }

    return env;
}

urfp_retval_t urfp_tinypacket_wait_for_response(urfp_tinypacket_t* tpkt, urfp_transaction_env_t* env, unsigned abs_timeout)
{
    /* TBD: Do not actively look for data. That (and feeding them to urfp_tinypacket_input)
     * could be implemented elsewhere. Instead, urfp_tinypacket_input could do some callback
     * when input with URFP_ENV_REQ_OUT is complete..    ?
     */
    unsigned time_left = 0;
    do
    {
        if (tpkt->wait(tpkt->arg, abs_timeout, &time_left) == URFP_OK)
        {
            char expected_prefix = ((urfp_tinypacket_env_data_t*)(env->transport_data))->expected_prefix;
            if (expected_prefix >= 8 && expected_prefix < 15)
            {
                // complete (albeit maybe with errors)
                if (expected_prefix == 10 || expected_prefix == 14
                    || expected_prefix == 9 || expected_prefix == 13)
                {
                    return URFP_ERR;
                }
                return URFP_OK;
            }
            return URFP_AGAIN;
        }
    }
    while (time_left > 0);

    return URFP_ERR;
}

/**
@}
*/
