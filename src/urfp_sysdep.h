/* URFP - UR Function Protocol

Copyright (c) 2023 Kolja Waschk

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

/**
@ingroup urfp_sysdep
@{
*/

#ifndef URFP_SYSDEP_H
#define URFP_SYSDEP_H 1

#define URFP_LOG_DEBUG_XL 0 /**< z.B. jedes einzelne übertragene Zeichen. Wird normalerweise nicht einkompiliert. */
#define URFP_LOG_DEBUG_L  1 /**< z.B. regelmässige Zustandsmeldungen auch wenn gar nichts zu tun ist. Normalerweise nicht drin. */
#define URFP_LOG_DEBUG    2 /**< z.B. Statistik nach abgeschlossener Übertragung. Wird eincompiliert falls nicht NDEBUG gesetzt */
#define URFP_LOG_DETAIL   3 /**< wird normalerweise eincompiliert, aber nicht angezeigt */
#define URFP_LOG_INFO     4 /**< wird normalerweise eincompiliert und standardmässig angezeigt */
#define URFP_LOG_WARN     5 /**< immer eincompiliert und standardmässig angezeigt */
#define URFP_LOG_ERR      6 /**< immer eincompiliert und immer angezeigt */
#define URFP_LOG_FATAL    7 /**< Logmeldung in solchem Falle hält ausserdem Ausführung an mit Endlosschleife */

extern int urfp_logf(int level, const char* fmt, ...);
extern unsigned long urfp_milliseconds(void);

#endif /* URFP_SYSDEP_H */

