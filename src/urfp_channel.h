/* URFP - UR Function Protocol

Copyright (c) 2024 Kolja Waschk

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

/**
@ingroup urfp_channel
@{
*/

#ifndef URFP_CHANNEL_H
#define URFP_CHANNEL_H 1

#include "urfp_types.h"

#ifdef __cplusplus
extern "C" {
#endif

#define URFP_MAX_SOURCES_PER_CHANNEL 32

struct urfp_channel
{
    urfp_app_t* app;

    void* get_env_arg;
    urfp_transaction_env_t* (*get_env)(void* arg, urfp_channel_t* ch);
    void (*release_env)(void* arg, urfp_transaction_env_t* env);

#if URFP_WITH_CHANNELS
    /* op meaning is dependent on the channel type */
    void* op;

    /* options are like variables, but available only within scope of a channel */
    urfp_elem_list_t  variable;
#endif

    /* sources can be enabled/disabled within scope of a channel */
    urfp_elem_list_t source;

    int num_sources; /* Count of sources in source_state, up to MAX_SOURCES_PER_CHANNEL */
    uint32_t enabled_sources; /* Bit position corresponds to the index of source in source_state */

    struct channel_source_state
    {
        uint32_t read_index; /* Which item# to read next from this source (where last access left off) */
        const urfp_elem_descriptor_t* elem;
    }
    source_state[URFP_MAX_SOURCES_PER_CHANNEL];

};

/* ----------------------- */

extern urfp_retval_t urfp_channel_source_index(urfp_transaction_env_t* env, const urfp_elem_descriptor_t* elem,
    uint32_t* begin, uint32_t* curr, uint32_t* end);
extern urfp_retval_t urfp_channel_source_read(urfp_transaction_env_t* env, const urfp_elem_descriptor_t* elem,
     uint32_t index, int* maxcount);

extern urfp_retval_t urfp_channel_enable_source(urfp_channel_t* channel, const urfp_elem_descriptor_t* elem, bool enable);
extern bool urfp_channel_source_enabled(urfp_channel_t* channel, const urfp_elem_descriptor_t* elem);
extern bool urfp_channel_source_processed(urfp_channel_t* channel, const urfp_elem_descriptor_t* elem, uint32_t up_to_index);
extern urfp_transaction_env_t* urfp_channel_env_for_source(urfp_channel_t* channel, const urfp_elem_descriptor_t* elem);

extern void urfp_channel_init(urfp_channel_t* channel, urfp_elem_list_t list);

extern void urfp_channel_update_source_list(urfp_channel_t* channel, urfp_elem_list_t list);

extern urfp_retval_t urfp_channel_continue_channel(urfp_channel_t* channel);


#ifdef __cplusplus
}
#endif

#endif

/**
@}
*/
