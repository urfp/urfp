/* URFP - Universal Remote Function Protocol

Copyright (c) 2015 Kolja Waschk

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

/**
@ingroup urfp_proxy
@{
*/

#include <ctype.h>
#include <string.h>
#include <stdio.h> /* for sprintf */

#include "urfp_api.h"
#include "urfp_server.h"
#include "urfp_tinypacket.h"

#include "urfp_sysdep.h"
#include "urfp_proxy.h"

//#pragma GCC push_options
//#pragma GCC optimize ("O0") 

/*-------------------------------------------------------------*/

/* To use proxy,

    1. instantiate some transport (currently urfp_tinypacket is assumed) to node

    2. Use requests for help to gather info about elements (or some internal mechanism, e.g. from ini file)

    3. Provide elements to clients when they ask for help/lists

        4. Satisfy client requests by making requests to the node

    5. Typical proxy request processing:

        urfp_transaction_env_t* env = urfp_tinypacket_request_start(tpkt, URFP_CODE(...))
        urfp_put_int(p, 4, "id", env->elem->id);

        pad.final_timeout = urfp_milliseconds() + 100 + opdata->type.length * 2;

        .. get (from client) and put (to node) further arguments as needed

        ... urfp_put_end to node
        ... urfp_get_end to client

        .. get results (from node) and put (to client) as available

        ... urfp_get_end to node
        ... urfp_put_end to client
*/


const size_t urfp_proxy_elems = 200;

const size_t urfp_proxy_max_string_len = 48;

#if URFP_WITH_CHANNELS
const char* elemtype_name[URFP_NUM_ELEMTYPES] = { "variable", "function", "source", "blob", "channel" };
#else
const char* elemtype_name[URFP_NUM_ELEMTYPES] = { "variable", "function", "source", "blob" };
#endif

typedef struct urfp_proxy_array_desc
{
    urfp_transaction_env_t* src;
    urfp_transaction_env_t* dst;
    bool set;
    bool complete;
    uint32_t final_timeout;
}
urfp_proxy_array_desc_t;

static urfp_retval_t urfp_proxy_fail(urfp_transaction_env_t* env)
{
    return urfp_put_end(env, 1, "Internal proxy error");
}

static urfp_retval_t urfp_proxy_fail_offset(urfp_transaction_env_t* env, int offset)
{
    (void)offset;
    return urfp_proxy_fail(env);
}

static urfp_retval_t urfp_proxy_fail_source_index(const urfp_elem_descriptor_t* elem, uint32_t* begin, uint32_t* end)
{
    (void)begin;
    (void)end;
    return URFP_OK;
}

static urfp_retval_t urfp_proxy_fail_source_read(urfp_transaction_env_t* env, uint32_t first, int* count)
{
    (void)first;
    (void)count;
    return urfp_proxy_fail(env);
}

static bool urfp_proxy_append_elem(urfp_proxy_t* proxy, urfp_transaction_env_t* env, urfp_elemtype_t table_idx, const char* name_prefix, urfp_elem_descriptor_t* existing)
{
    char name[urfp_proxy_max_string_len];
    /* TODO: Error handling */

    /* Access input buffer under protection */
    proxy->lock(proxy->lock_arg, true, env);

    int id = urfp_get_next_int_arg(env, 4, "id");
    urfp_get_next_string_arg(env, "name", name, sizeof(name));

    proxy->lock(proxy->lock_arg, false, env);

    int prefix_len = 0;
    if (name_prefix)
    {
        prefix_len = strlen(name_prefix);
    }

    bool added = true;

    urfp_elem_descriptor_t* et = calloc(sizeof(urfp_elem_descriptor_t), 1);
    if (et == NULL)
    {
        urfp_logf(URFP_LOG_INFO, "While adding %s: out of mem for elem_desc('%s')", elemtype_name[(urfp_elemtype_t)table_idx], name);
        added = false;
    }
    else
    {
        if (prefix_len > 0)
        {
            int name_len = strlen(name);
            char* elemname = malloc(prefix_len + name_len + 1);
            if (elemname != NULL)
            {
                memcpy(elemname, name_prefix, prefix_len);
                memcpy(elemname + prefix_len, name, name_len);
                elemname[prefix_len + name_len] = (char)0;
                et->name = elemname;
            }
        }
        else
        {
            et->name = strdup(name);
        }
        if (et->name == NULL)
        {
            urfp_logf(URFP_LOG_INFO, "While adding %s: out of mem for dup_name('%s')", elemtype_name[(urfp_elemtype_t)table_idx], name);
            free(et);
            added = false;
        }
        else
        {
            et->id = id + proxy->id_offset;

            if (existing != NULL)
            {
                /* Just compare to existing entry, then discard */

                if (et->id != existing->id)
                {
                    urfp_logf(URFP_LOG_INFO, "While verifying: id %d => %d", et->id, existing->id);
                }
                if (strcmp(et->name, existing->name) != 0)
                {
                    urfp_logf(URFP_LOG_INFO, "While verifying: name %s => %s", et->name, existing->name);
                }
                free((void*)et->name);
                free(et);
            }
            else
            {
                /* Insert at beginning of list */

                /* For now, "op" is used as a "prev" pointer to another element instance. */
                et->op = (void*)proxy->elem_tables[table_idx];
                proxy->elem_tables[table_idx] = et;
            }
        }
    }

    /* Access input buffer under protection */
    proxy->lock(proxy->lock_arg, true, env);

    /* Make room for more input data */
#warning TBD: Isnt urfp_get_string and _integer above enough to "make room"?
    urfp_tinypacket_catch_up((urfp_tinypacket_env_data_t*) (env->transport_data));

    proxy->lock(proxy->lock_arg, false, env);

    return added;
}

/* Size of one element when transmitted in URFP tinypacket */
static int urfp_proxy_var_size(urfp_argtype_t code)
{
    int sz = 0;
    if (code == URFP_BOOL)
    {
        sz = 1;
    }
    else if ((code >= URFP_INT8 && code <= URFP_INT64) || (code >= URFP_UINT8 && code <= URFP_UINT64))
    {
        sz = 1u<<(code & 3);
    }
    else if (code == URFP_FLOAT)
    {
        sz = 4;
    }
    else if (code == URFP_DOUBLE)
    {
        sz = 8;
    }
    else /* URFP_STRING or other */
    {
        sz = urfp_proxy_max_string_len + 1;
    }

    return sz;
}

/* This function gets data from src and puts data to dst */

static urfp_retval_t urfp_proxy_var_single(urfp_proxy_array_desc_t* desc, urfp_argtype_t code, const char* name)
{
    urfp_transaction_env_t* src = desc->src; /* may be NULL during dry_run */
    urfp_transaction_env_t* dst = desc->dst;

    code &= URFP_TYPE_MASK;

    if (code == URFP_STRING)
    {
        char s[urfp_proxy_max_string_len];
        if (src != NULL)
        {
            urfp_get_next_string_arg(src, name, s, sizeof(s));
        }
        else
        {
            s[0] = 0;
        }
        if (dst != NULL)
        {
            urfp_put_string(dst, name, s);
        }
    }
    else if (code == URFP_BOOL)
    {
        bool b = true;
        if (src != NULL)
        {
            b = urfp_get_next_bool_arg(src, name);
        }
        if (dst != NULL)
        {
           urfp_put_bool(dst, name, b);
        }
    }
    else if (code >= URFP_INT8 && code <= URFP_INT32)
    {
        /* Note: Not supported: URFP_INT64 */
        int sz = 1u<<(code & 3);
        int32_t i = 0;
        if (src != NULL)
        {
            i = urfp_get_next_int_arg(src, sz, name);
        }
        if (dst != NULL)
        {
            urfp_put_int(dst, sz, name, i);
        }
    }
    else if (code >= URFP_UINT8 && code <= URFP_UINT32)
    {
        int sz = 1u<<(code & 3);
        int32_t i = 0;
        if (src != NULL)
        {
            i = urfp_get_next_unsigned_arg(src, sz, name);
        }
        if (dst != NULL)
        {
            urfp_put_unsigned(dst, sz, name, i);
        }
    }
    else if (code == URFP_UINT64)
    {
        uint64_t i = 0;
        if (src != NULL)
        {
            i = urfp_get_next_uint64_arg(src, name);
        }
        if (dst != NULL)
        {
            urfp_put_uint64(dst, name, i);
        }
    }

    return URFP_OK;
}

static urfp_retval_t urfp_proxy_var_array(urfp_transaction_env_t* env)
{
    urfp_proxy_elem_op_data_t* opdata = (urfp_proxy_elem_op_data_t*)  env->elem->op;
    urfp_proxy_array_desc_t* desc = (urfp_proxy_array_desc_t*) env->continuation.array.data;
    urfp_tinypacket_t* tpkt = opdata->proxy->tpkt;

    int a = env->continuation.array.index;
    int max_sz = urfp_proxy_var_size(opdata->type.code & URFP_TYPE_MASK);

    if (!desc->set && !desc->complete)
    {
        /* Wait for response (data) from device */

        urfp_retval_t ur;
        ur = urfp_tinypacket_wait_for_response(tpkt, desc->src, desc->final_timeout);

        urfp_tinypacket_env_data_t* data = (urfp_tinypacket_env_data_t*) (desc->src->transport_data);

        if (ur == URFP_AGAIN)
        {
            opdata->proxy->lock(opdata->proxy->lock_arg, true, desc->src);

            /* Parse while the inbuf for sure has at least one whole element in it */
            while ((data->input_length - data->arg_offset >= max_sz) && ((opdata->type.length < 0) || (a < opdata->type.length)))
            {
                urfp_proxy_var_single(desc, opdata->type.code, "value");
                ++ a;
            }

            /* Make room for more input data */
            urfp_tinypacket_catch_up(data);

            opdata->proxy->lock(opdata->proxy->lock_arg, false, desc->src);

            env->continuation.array.index = a;
            return URFP_AGAIN;
        }
        else if (ur == URFP_OK)
        {
            /* Parse all the remaining */
            while (((data->input_length - data->arg_offset > 0) && (opdata->type.length < 0)) || (a < opdata->type.length))
            {
                urfp_proxy_var_single(desc, opdata->type.code, "value");
                ++ a;
            }

            desc->complete = true;
        }
        else
        {
            urfp_get_end(desc->src);
            (void)urfp_put_end(desc->dst, 1, "Got no or error response to proxy request");

            free(desc);
            opdata->proxy->lock(opdata->proxy->lock_arg, false, NULL);
            return ur;
        }
    }

    /* setting arrays is not supported, no need to check here */

    urfp_get_end(desc->src);

    /* put_end(desc->dst) will be called in array continue end */

    free(desc);
    opdata->proxy->lock(opdata->proxy->lock_arg, false, NULL);
    return URFP_OK;
}

static urfp_retval_t urfp_proxy_var_access(urfp_transaction_env_t* env, bool set, urfp_proxy_elem_op_data_t* opdata)
{
    urfp_transaction_env_t* p;

    opdata->proxy->lock(opdata->proxy->lock_arg, true, NULL);

    urfp_tinypacket_t* tpkt = opdata->proxy->tpkt;

    p = urfp_tinypacket_request_start(tpkt, set ? URFP_CODE('s','v') : URFP_CODE('g','v'));

    if (p == NULL)
    {
        opdata->proxy->lock(opdata->proxy->lock_arg, false, NULL);
        return urfp_put_end(env, 1, "Failed to perform proxy request");
    }

    urfp_put_int(p, 4, "id", env->elem->id - opdata->proxy->id_offset);

    urfp_proxy_array_desc_t pad;

    pad.set = set;
    pad.complete = false;
    pad.final_timeout = urfp_milliseconds() + 1000;
    /* Arrays take longer. Todo: better handle unknown length, might be even more than 5000! */
    pad.final_timeout += ((opdata->type.length<0) ? 5000 : opdata->type.length) * 2/*ms*/;

    if (set)
    {
        pad.src = env; /* get from client, put to device */
        pad.dst = p;
    }
    else
    {
        pad.src = p;   /* get from device, put to client */
        pad.dst = env;
    }

    if (!set)
    {
        urfp_put_end(p, 0, NULL);
        urfp_get_end(env);
    }

    if (opdata->type.length != 0)
    {
        urfp_proxy_array_desc_t* dynpad = malloc(sizeof(urfp_proxy_array_desc_t));
        memcpy(dynpad, &pad, sizeof(pad));

        urfp_put_array_start(pad.dst, env->elem->name, opdata->type.length);

        env->continuation.array.index = 0;
        env->continuation.array.data = dynpad;
        env->continuation.array.output_element = urfp_proxy_var_array;

        /* Leave actual array output to callback function */
        return urfp_put_backgrounded(env, urfp_put_continue_array);
    }
    else
    {
        if (!set)
        {
            /* Wait for response (data) from device */
            urfp_retval_t r;
            do
            {
                r = urfp_tinypacket_wait_for_response(tpkt, p, pad.final_timeout);
            }
            while (r == URFP_AGAIN);

            if (r != URFP_OK)
            {
                urfp_get_end(p);
                opdata->proxy->lock(opdata->proxy->lock_arg, false, NULL);
                return urfp_put_end(env, 1, "Got no or error response to proxy request");
            }
        }

        opdata->proxy->lock(opdata->proxy->lock_arg, true, pad.src);

        urfp_proxy_var_single(&pad, opdata->type.code, env->elem->name);

        opdata->proxy->lock(opdata->proxy->lock_arg, false, pad.src);

        if (!set)
        {
            urfp_get_end(p);
        }
        else
        {
            urfp_get_end(env);
            urfp_put_end(p, 0, NULL);

            /* Wait for response (confirmation) from device */
            urfp_retval_t r;
            do
            {
                r = urfp_tinypacket_wait_for_response(tpkt, p, pad.final_timeout);
            }
            while (r == URFP_AGAIN);

            urfp_get_end(p);

            if (r != URFP_OK)
            {
                opdata->proxy->lock(opdata->proxy->lock_arg, false, NULL);
                return urfp_put_end(env, 1, "Got no or error response to proxy request");
            }
        }

        opdata->proxy->lock(opdata->proxy->lock_arg, false, NULL);
        return urfp_put_end(env, 0, NULL);
    }
}

static urfp_retval_t urfp_proxy_var_get(urfp_transaction_env_t* env)
{
    urfp_proxy_elem_op_data_t* opdata = (urfp_proxy_elem_op_data_t*)env->elem->op;

    if (opdata == NULL)
    {
        return urfp_proxy_fail(env);
    }

    if (env->dry_run)
    {
        urfp_proxy_array_desc_t pad;
        pad.src = NULL;
        pad.dst = env;

        if (opdata->type.length != 0)
        {
            urfp_put_array_start(env, env->elem->name, opdata->type.length);
            urfp_proxy_var_single(&pad, opdata->type.code, "value");
            urfp_put_array_end(env);
        }
        else
        {
            urfp_proxy_var_single(&pad, opdata->type.code, env->elem->name);
        }

        return urfp_put_end(env, 0, NULL);
    }
    else
    {
        return urfp_proxy_var_access(env, /*set=*/false, opdata);
    }
}

static urfp_retval_t urfp_proxy_var_set(urfp_transaction_env_t* env, int offset)
{
    urfp_proxy_elem_op_data_t* opdata = (urfp_proxy_elem_op_data_t*)env->elem->op;

    if (opdata == NULL)
    {
        return urfp_proxy_fail(env);
    }

    if (opdata->type.length != 0)
    {
        return urfp_put_end(env, 1, "Updating array variables is not yet supported");
    }
    return urfp_proxy_var_access(env, /*set=*/true, opdata);
}

static urfp_retval_t urfp_proxy_func_exec(urfp_transaction_env_t* env)
{
    urfp_proxy_elem_op_data_t* opdata = (urfp_proxy_elem_op_data_t*)env->elem->op;

    if (opdata == NULL)
    {
        return urfp_proxy_fail(env);
    }

    if (env->dry_run)
    {
        urfp_proxy_elem_type_data_t* n = &(opdata->type);

        /* Arguments: dry set (from caller via env, dst=NULL) */

        urfp_proxy_array_desc_t pad;
        pad.src = env;
        pad.dst = NULL;

        while (n != NULL && (n->code & URFP_ARG) != 0)
        {
            if (n->length != 0)
            {
                urfp_put_array_start(env, n->name, n->length);
                urfp_proxy_var_single(&pad, n->code, "value");
                urfp_put_array_end(env);
            }
            else
            {
                urfp_proxy_var_single(&pad, n->code, n->name);
            }
            n = n->next;
        }

        /* Results: dry set (to caller via env, src=NULL) */

        pad.src = NULL;
        pad.dst = env;

        while (n != NULL && n->code != 0)
        {
            if (n->length != 0)
            {
                urfp_put_array_start(env, n->name, n->length);
                urfp_proxy_var_single(&pad, n->code, "value");
                urfp_put_array_end(env);
            }
            else
            {
                urfp_proxy_var_single(&pad, n->code, n->name);
            }
            n = n->next;
        }
    }
    else
    {
        opdata->proxy->lock(opdata->proxy->lock_arg, true, NULL);

        urfp_transaction_env_t* p;
        urfp_tinypacket_t* tpkt = opdata->proxy->tpkt;

        p = urfp_tinypacket_request_start(tpkt, URFP_CODE('c','f'));

        if (p == NULL)
        {
            opdata->proxy->lock(opdata->proxy->lock_arg, false, NULL);
            return urfp_put_end(env, 1, "Failed to perform proxy request");
        }

        urfp_put_int(p, 4, "id", env->elem->id - opdata->proxy->id_offset);

        urfp_proxy_array_desc_t pad;

        pad.set = true;
        pad.complete = false;
        pad.final_timeout = urfp_milliseconds() + 1350;
        // was 100. but R2300 takes 260ms to change a calibration var in flash!

        /* First for all arguments: get from client, put to device */

        pad.src = env;
        pad.dst = p;

        urfp_proxy_elem_type_data_t* n = &(opdata->type);

        while (n != NULL && (n->code & URFP_ARG) != 0 && urfp_more_args_available(env))
        {
            /* TODO: Array support */
            urfp_proxy_var_single(&pad, n->code, n->name);
            n = n->next;
        }

        /* No more args available, skip forward to results */
        while (n != NULL && (n->code & URFP_ARG) != 0)
        {
            n = n->next;
        }

        urfp_put_end(p, 0, NULL);
        urfp_get_end(env);

        /* Wait for response (data) from device */
        urfp_retval_t r;
        do
        {
            r = urfp_tinypacket_wait_for_response(tpkt, p, pad.final_timeout);
        }
        while (r == URFP_AGAIN);

        /* Now for all results: get from device, put to client */
        pad.src = p;
        pad.dst = env;

        while (n != NULL)
        {
            /* TODO: Array support */
            urfp_proxy_var_single(&pad, n->code, n->name);
            n = n->next;
        }

        if (r != URFP_OK)
        {
            urfp_get_end(p);
            opdata->proxy->lock(opdata->proxy->lock_arg, false, NULL);
            return urfp_put_end(env, 1, "Got no or error response to proxy request");
        }

        // tunnel_varfunc_proxy_one(&pad, opdata->type.code, env->elem->name);

        urfp_get_end(p);
        opdata->proxy->lock(opdata->proxy->lock_arg, false, NULL);
    }

    return urfp_put_end(env, 0, NULL);
}


static urfp_retval_t urfp_proxy_source_index(const urfp_elem_descriptor_t* elem, uint32_t* begin, uint32_t* end)
{
    urfp_proxy_source_cache_t* cache = NULL;
    urfp_proxy_elem_op_data_t* opdata = (urfp_proxy_elem_op_data_t*)elem->op;

    if (opdata != NULL)
    {
        cache = (urfp_proxy_source_cache_t*) opdata->cache;
    }

    if (cache != NULL)
    {
        *begin  = cache->begin;
        *end   = cache->begin + cache->count;
    }
    else
    {
        *begin  = 0;
        *end   = 0;
    }

    return URFP_OK;
}

static urfp_retval_t urfp_proxy_source_to_cache(urfp_transaction_env_t* env, urfp_argtype_t code, const char* name, void** to_cache)
{
    code &= URFP_TYPE_MASK;

    if (code == URFP_STRING)
    {
#warning TODO: Get string into cache, check len
        urfp_get_next_string_arg(env, name, (char*)*to_cache, 60);
        *to_cache += strlen(*to_cache);
    }
    else if (code == URFP_BOOL)
    {
        *(uint8_t*)*to_cache = urfp_get_next_bool_arg(env, name) ? 1:0;
        *to_cache += sizeof(uint8_t);
    }
    else if (code >= URFP_INT8 && code <= URFP_INT32)
    {
        int sz = 1u<<(code & 3);
        int32_t i = urfp_get_next_int_arg(env, sz, name);
        if (sz == 1)
        {
            *(int8_t*)*to_cache = i;
        }
        else if (sz == 2)
        {
            *(int16_t*)*to_cache = i;
        }
        else if (sz == 4)
        {
            *(int32_t*)*to_cache = i;
        }
        /* Note: not supported: sz==8 */

        *to_cache += sz;
    }
    else if (code >= URFP_UINT8 && code <= URFP_UINT32)
    {
        int sz = 1u<<(code & 3);
        uint32_t i = urfp_get_next_unsigned_arg(env, sz, name);
        if (sz == 1)
        {
            *(uint8_t*)*to_cache = i;
        }
        else if (sz == 2)
        {
            *(uint16_t*)*to_cache = i;
        }
        else if (sz == 4)
        {
            *(uint32_t*)*to_cache = i;
        }
        *to_cache += sz;
    }
    else if (code == URFP_UINT64)
    {
        *(uint64_t*)*to_cache = urfp_get_next_uint64_arg(env, name);
        to_cache += 8;
    }

    /* TODO: Float, Double? */

    return URFP_OK;
}

static urfp_retval_t urfp_proxy_source_from_cache(urfp_transaction_env_t* env, urfp_argtype_t code, const char* name, void** from_cache)
{
    code &= URFP_TYPE_MASK;

    if (code == URFP_STRUCT_START)
    {
        urfp_put_struct_start(env, name);
    }
    else if (code == URFP_STRUCT_END)
    {
        urfp_put_struct_end(env);
    }
    else if (code == URFP_STRING)
    {
        if (env->dry_run)
        {
            return urfp_put_string(env, name, "");
        }
        else
        {
            char* s = (char *)*from_cache;
            *from_cache += strlen(*from_cache);
            return urfp_put_string(env, name, s);
        }
    }
    else if (code == URFP_BOOL)
    {
        bool b = ((*(uint8_t*)*from_cache) != 0);
        *from_cache += sizeof(uint8_t);
        return urfp_put_bool(env, name, b);
    }
    else if (code >= URFP_INT8 && code <= URFP_INT32)
    {
        int sz = 1u<<(code & 3);
        int32_t i = 0;
        if (sz == 1)
        {
            i = *(int8_t*)*from_cache;
        }
        else if (sz == 2)
        {
            i = *(int16_t*)*from_cache;
        }
        else if (sz == 4)
        {
            i = *(int32_t*)*from_cache;
        }
        /* Note: not supported: sz==8 */

        *from_cache += sz;
        return urfp_put_int(env, sz, name, i);
    }
    else if (code >= URFP_UINT8 && code <= URFP_UINT32)
    {
        int sz = 1u<<(code & 3);
        uint32_t i = 0;
        if (sz == 1)
        {
            i = *(uint8_t*)*from_cache;
        }
        else if (sz == 2)
        {
            i = *(uint16_t*)*from_cache;
        }
        else if (sz == 4)
        {
            i = *(uint32_t*)*from_cache;
        }
        *from_cache += sz;
        return urfp_put_unsigned(env, sz, name, i);
    }
    else if (code == URFP_UINT64)
    {
        uint64_t i = *(uint64_t*)*from_cache;
        from_cache += 8;
        return urfp_put_uint64(env, name, i);
    }

    /* TODO: Float, Double? */

    return URFP_OK;
}

urfp_retval_t urfp_proxy_source_read(urfp_transaction_env_t* env, uint32_t first, int* count)
{
    (void)first;
    (void)count;

    urfp_proxy_elem_op_data_t* opdata = (urfp_proxy_elem_op_data_t*)env->elem->op;

    if (opdata == NULL)
    {
        return urfp_proxy_fail(env);
    }

    opdata->proxy->lock(opdata->proxy->lock_arg, true, NULL);

    urfp_proxy_source_cache_t* cache = (urfp_proxy_source_cache_t*) opdata->cache;

    void* from_cache = cache->base;

    int efflen = cache->count;
    if (env->dry_run)
    {
        efflen = 1;
    }
    for (int i=0; i<efflen; ++i)
    {
        urfp_proxy_elem_type_data_t* n = &(opdata->type);

        while (n != NULL && n->code != 0)
        {
            if (n->length != 0)
            {
                urfp_put_array_start(env, n->name, n->length);
                for (int i=0; i<n->length; i++)
                {
                    urfp_proxy_source_from_cache(env, n->code, "value", &from_cache);
                }
                urfp_put_array_end(env);
            }
            else if (n->code == URFP_STRUCT_START)
            {
                urfp_put_struct_start(env, n->name);
            }
            else if (n->code == URFP_STRUCT_END)
            {
                urfp_put_struct_end(env);
            }
            else
            {
                urfp_proxy_source_from_cache(env, n->code, n->name, &from_cache);
            }
            n = n->next;
        }
    }

    opdata->proxy->lock(opdata->proxy->lock_arg, false, NULL);

    return URFP_OK;
}

urfp_retval_t urfp_proxy_source_enable(const urfp_elem_descriptor_t* source_elem, bool enable)
{
    urfp_retval_t r = URFP_ERR;
    urfp_proxy_elem_op_data_t* opdata = (urfp_proxy_elem_op_data_t*)source_elem->op;

    if (opdata != NULL)
    {
        opdata->proxy->lock(opdata->proxy->lock_arg, true, NULL);

        urfp_transaction_env_t* p;
        urfp_tinypacket_t* tpkt = opdata->proxy->tpkt;

        uint16_t code = enable ? URFP_CODE('e','s') : URFP_CODE('d','s');
        p = urfp_tinypacket_request_start(tpkt, code);

        if (p != NULL)
        {
            urfp_put_int(p, sizeof(int32_t), "id", source_elem->id - opdata->proxy->id_offset);

            urfp_put_end(p, 0, NULL);

            /* Wait for response (data) from device */
            unsigned long final_timeout = urfp_milliseconds() + 350;
            do
            {
                r = urfp_tinypacket_wait_for_response(tpkt, p, final_timeout);
            }
            while (r == URFP_AGAIN);

            /* Ignore list of enabled sources returned by the device */
            urfp_get_end(p);
        }

        opdata->proxy->lock(opdata->proxy->lock_arg, false, NULL);
    }

    return r;
}

urfp_retval_t urfp_proxy_source_feed(urfp_transaction_env_t* env, const urfp_elem_list_t base)
{
    urfp_retval_t r = URFP_OK;

    int id = urfp_get_next_int_arg(env, 4, "id");
    urfp_proxy_elem_op_data_t* opdata = NULL;

    int i = 0;
    urfp_elem_list_t list = base;
    const urfp_elem_descriptor_t* elem;
    urfp_next_elem_from_list_getter_t next_elem = env->app->next_elem_from_list;
    urfp_retval_t re = next_elem(&list, &i, &elem);
    while (re == URFP_OK)
    {
        if(elem->sourcefuncs.read == urfp_proxy_source_read)
        {
            opdata = (urfp_proxy_elem_op_data_t*)(elem->op);
            if ((elem->id - opdata->proxy->id_offset) == id)
            {
                env->elem = elem;
                break;
            }
        }
        re = next_elem(&list, &i, &elem);
    }

    if (opdata != NULL && env->elem != NULL)
    {
        /* read into cache */


        urfp_proxy_source_cache_t* cache = (urfp_proxy_source_cache_t*) opdata->cache;
        int first = urfp_get_next_unsigned_arg(env, 4, "first");

        cache->begin = first;

        unsigned total = 0;
        while (urfp_more_args_available(env))
        {
            void* to_cache = cache->base;

            cache->count = 0;
            /* TODO: Read more than one, as many as fit in cache */
            {
                urfp_proxy_elem_type_data_t* n = &(opdata->type);

                while (n != NULL && n->code != 0)
                {
                    if (n->length != 0)
                    {
                        for (int i=0; i<n->length; i++)
                        {
                            urfp_proxy_source_to_cache(env, n->code, n->name, &to_cache);
                        }
                    }
                    else
                    {
                        urfp_proxy_source_to_cache(env, n->code, n->name, &to_cache);
                    }
                    n = n->next;
                }

                total += (unsigned)(to_cache - cache->base);
                ++ cache->count;
            }

            opdata->proxy->update_source(env->elem, cache->begin + cache->count);

            /* TBD: Assuming that update_source lead to context switch and complete output of all data, so: we can clear the cache now. */
            cache->begin += cache->count;
            cache->count = 0;
        }

        //urfp_logf(URFP_LOG_INFO, "read %s(%d, cached %d @ %p)", env->elem->name, id, total, cache->base);
    }
    else
    {
        //urfp_logf(URFP_LOG_INFO, "ignore %d", id);
    }

    return r;

}

#if URFP_WITH_CHANNELS
static urfp_retval_t urfp_proxy_channel_open(urfp_transaction_env_t* env)
{
    return urfp_proxy_fail(env);
}

static urfp_retval_t urfp_proxy_fail_channel_activate(urfp_transaction_env_t* env, bool activate)
{
    (void)activate;
    return urfp_proxy_fail(env);
}

static urfp_retval_t urfp_proxy_channel_activate(urfp_transaction_env_t* env, bool activate)
{
    (void)activate;
    return urfp_proxy_fail(env);
}
#endif

static urfp_retval_t urfp_proxy_blob_read(urfp_transaction_env_t* env)
{
    return urfp_put_end(env, 0, NULL);
}

static urfp_retval_t urfp_proxy_blob_write(urfp_transaction_env_t* env, int offset)
{
    return urfp_put_end(env, 0, NULL);
}

const struct
{
    char old;
    urfp_argtype_t code;
}
datatype_arg_map[] =
{
    { '{', URFP_STRUCT_START },
    { '[', URFP_ARRAY_START },
    { ']', URFP_ARRAY_END },
    { '}', URFP_STRUCT_END },
    { 'o', URFP_BOOL },
    { 's', URFP_STRING },
    { 'b', URFP_INT8 },
    { 'h', URFP_INT16 },
    { 'i', URFP_INT32 },
    { 'B', URFP_UINT8 },
    { 'H', URFP_UINT16 },
    { 'I', URFP_UINT32 },
    { 0, 0 }
};

static urfp_retval_t get_next_datatype_arg(urfp_tinypacket_env_data_t* data, urfp_argtype_t* type, int* length)
{
    *type = 0;
    *length = 0;

    if ((data->input_length - data->arg_offset) >= 1)
    {
        uint8_t prefix = data->input[data->arg_offset];
        data->arg_offset ++;

        if (prefix < (URFP_TYPE_PREFIX>>8))
        {
            /* Support for now obsolete old types */

            if ((data->input_length - data->arg_offset) >= 4 && prefix == '[')
            {
                *length = *(int32_t*)(&(data->input[data->arg_offset]));
                data->arg_offset += 4;
            }

            int i;
            for (i=0; datatype_arg_map[i].old != 0; i++)
            {
                if (prefix == datatype_arg_map[i].old)
                {
                    *type = datatype_arg_map[i].code;
                }
            }
        }
        else if ((data->input_length - data->arg_offset) >= 1)
        {
            *type = data->input[data->arg_offset];
            data->arg_offset ++;

            if ((data->input_length - data->arg_offset) >= 4 && *type == URFP_ARRAY_START)
            {
                *length = *(int32_t*)(&(data->input[data->arg_offset]));
                data->arg_offset += 4;
            }

            *type |= (prefix) << 8;
        }
    }

    return (*type != 0) ? URFP_OK : URFP_ERR;
}

void urfp_proxy_cleanup_one_type(urfp_proxy_t* proxy, int elemtype)
{
    if (proxy->elem_refs[elemtype] != NULL)
    {
        for (int i=0; i<proxy->elem_index[elemtype]; ++i)
        {
            urfp_elem_descriptor_t* et = proxy->elem_refs[elemtype][i];

            if (et != NULL)
            {
                if (et->id > 0)
                {
                    /* TODO: If space for descriptions and names
                     * has been dynamically allocated, free it now! */
                    if (et->name != NULL)
                    {
                        free((void*)et->name);
                    }
                    if (et->desc != NULL)
                    {
                        free((void*)et->desc);
                    }

                    urfp_proxy_elem_op_data_t* opdata = et->op;
                    if (opdata != NULL)
                    {
                        urfp_proxy_elem_type_data_t* type = opdata->type.next;
                        while (type != NULL)
                        {
                            urfp_proxy_elem_type_data_t* next = type->next;
                            if (type->name != NULL)
                            {
                                free(type->name);
                            }
                            free(type);
                            type = next;
                        }
                        free(opdata);
                    }
                }
            }
            free(et);
        }
        free(proxy->elem_refs[elemtype]);
        proxy->elem_refs[elemtype] = NULL;
        proxy->elem_index[elemtype] = 0;
    }
    else
    {
        /* Else: deallocate linked element_desc one by one. */

        for (int elemtype=0; elemtype<URFP_NUM_ELEMTYPES; elemtype++)
        {
            urfp_elem_descriptor_t* et = proxy->elem_tables[elemtype];
            while (et != NULL)
            {
                urfp_elem_descriptor_t* prev = (urfp_elem_descriptor_t*) (et->op);
                if (et->id > 0)
                {
                    free((void*)(et->name));
                }
                /* No need to care about opdata because that hasn't been filled yet. */

                free(et);
                et = prev;
            }
            proxy->elem_tables[elemtype] = NULL;
            proxy->elem_index[elemtype] = 0;
        }
    }
}

void urfp_proxy_cleanup(urfp_proxy_t* proxy)
{
    for (int elemtype = 0; elemtype<URFP_NUM_ELEMTYPES; ++ elemtype)
    {
        urfp_proxy_cleanup_one_type(proxy, elemtype);
    }
}

void urfp_proxy_fetch_lists(urfp_proxy_t* proxy, const char* name_prefix)
{
    /* Fetch elements from server.

       Start by allocating urfp_elem_descriptor_t for all elements of all four types.

       A pointer to the first one is stored in proxy->elem_tables[0..3] (0..3: var, func, source, blob)

       During first run, further elements are linked in *op member of urfp_element_desc_t.

       Later, when the number of elements is known, a list is allocated to store pointers to them
       and *op is reused to point to urfp_proxy_elem_op_data per element.
    */

    urfp_tinypacket_t* tpkt = proxy->tpkt;

    for (int elemtype=0; elemtype<URFP_NUM_ELEMTYPES; elemtype++)
    {
        int tries;
        int existing_index = 0;
        if (proxy->elem_refs[elemtype] == NULL)
        {
            /* Start from scratch (otherwise keep elem_tables, just verify) */
            proxy->elem_index[elemtype] = 0;
            proxy->elem_tables[elemtype] = NULL;
            tries = 3;
        }
        else
        {
            tries = 1;
        }

        urfp_retval_t ur;
        do
        {
            ur = URFP_AGAIN;
            urfp_transaction_env_t* env = urfp_tinypacket_request_start(tpkt, URFP_CODE('l',elemtype_name[elemtype][0]));
            if (env == NULL)
            {
                urfp_logf(URFP_LOG_INFO, "Failed to start request for list of %ss", elemtype_name[elemtype]);
            }
            else
            {
                urfp_put_end(env, 0, NULL);

                urfp_tinypacket_env_data_t* data = (urfp_tinypacket_env_data_t*) (env->transport_data);

                uint32_t time_left;
                uint32_t abs_timeout = urfp_milliseconds() + 3000;

                urfp_logf(URFP_LOG_INFO, "Retrieving list of %ss", elemtype_name[elemtype]);

                do
                {
                    ur = urfp_tinypacket_wait_for_response(tpkt, env, abs_timeout);

                    if (ur == URFP_AGAIN)
                    {
                        /* Parse while the inbuf for sure has at least one element in it */
                        while (data->input_length >= urfp_proxy_max_string_len + sizeof(uint32_t) + 1)
                        {
                            if (proxy->elem_refs[elemtype] != NULL)
                            {
                                /* Just compare to existing entry */
                                urfp_proxy_append_elem(proxy, env, (urfp_elemtype_t)elemtype, name_prefix, proxy->elem_refs[elemtype][existing_index]);
                                existing_index ++;
                            }
                            else if (urfp_proxy_append_elem(proxy, env, (urfp_elemtype_t)elemtype, name_prefix, NULL))
                            {
                                proxy->elem_index[elemtype] ++;
                            }
                        }

                        /* Make room for more input data */
                        urfp_tinypacket_catch_up(data);
                    }
                    else if (ur == URFP_OK)
                    {
                        /* Parse all the remaining */
                        while (data->input_length > data->arg_offset)
                        {
                            if (proxy->elem_refs[elemtype] != NULL)
                            {
                                /* Just compare to existing entry */
                                urfp_proxy_append_elem(proxy, env, (urfp_elemtype_t)elemtype, name_prefix, proxy->elem_refs[elemtype][existing_index]);
                                existing_index ++;
                            }
                            else if (urfp_proxy_append_elem(proxy, env, (urfp_elemtype_t)elemtype, name_prefix, NULL))
                            {
                                proxy->elem_index[elemtype] ++;
                            }
                        }
                    }
                    else
                    {
                        urfp_logf(URFP_LOG_WARN, "Got no or error response to proxy request");
                    }

                    time_left = (uint32_t)abs_timeout - (uint32_t)urfp_milliseconds();
                    if ((int32_t)time_left < 0)
                    {
                        time_left = 0;
                    }
                }
                while (((int32_t)time_left > 0) && (ur == URFP_AGAIN));

                urfp_get_end(env);

                urfp_logf(URFP_LOG_INFO, "%s list %s", elemtype_name[elemtype], (ur==URFP_OK) ? "OK" : "incomplete");
            }

            -- tries;

            if (ur != URFP_OK && tries>0)
            {
                urfp_proxy_cleanup_one_type(proxy, elemtype);
            }

        } while (ur != URFP_OK && tries>0);
    }
}

void urfp_proxy_verify(urfp_proxy_t* proxy, const char* name_prefix)
{
    urfp_proxy_fetch_lists(proxy, name_prefix);
}

bool urfp_proxy_connect(urfp_proxy_t* proxy, urfp_tinypacket_t* tpkt, int id_offset, const char* name_prefix)
{
    proxy->tpkt = tpkt;
    proxy->id_offset = id_offset;

    urfp_proxy_fetch_lists(proxy, name_prefix);

    /* Now, proxy->elem_index[elem_type] contains the number of elements for each type and
       we can allocate an array to hold just pointers to all elements. */

    for (int elemtype=0; elemtype<URFP_NUM_ELEMTYPES; elemtype++)
    {
        int num_elems = proxy->elem_index[elemtype];
        proxy->elem_refs[elemtype] = (urfp_elem_descriptor_t**) calloc(sizeof(urfp_elem_descriptor_t*), num_elems + 1);
        if (proxy->elem_refs[elemtype] == NULL)
        {
            urfp_proxy_cleanup(proxy);
            memset(proxy, 0, sizeof(urfp_proxy_t));
            return false;
        }

        /* Store beginning at the array end because the first elem became last in allocated chain */
        urfp_elem_descriptor_t* et = proxy->elem_tables[elemtype];
        for (int i=0; i<num_elems; i++)
        {
            urfp_elem_descriptor_t* prev = (urfp_elem_descriptor_t*) (et->op);
            proxy->elem_refs[elemtype][num_elems-1 - i] = et;
            et->op = NULL;
            et = prev;
        }
        /* No linked list anymore */
        proxy->elem_tables[elemtype] = NULL;

        /* Explictly terminate list (To Do: optionally append link to another list) */
        proxy->elem_refs[elemtype][num_elems] = NULL;
    }

    for (int elemtype=0; elemtype<URFP_NUM_ELEMTYPES; elemtype++)
    {
        int num_elems = proxy->elem_index[elemtype];
        for (int edi=0; edi<num_elems; edi++)
        {
            urfp_elem_descriptor_t* et = proxy->elem_refs[elemtype][edi];

            int tries = 3;
            bool got_details = false;
            do
            {
                urfp_transaction_env_t* env = urfp_tinypacket_request_start(tpkt, URFP_CODE('h',elemtype_name[elemtype][0]));
                if (env == NULL)
                {
                    urfp_logf(URFP_LOG_INFO, "Failed to request details about %s %d (%s)", elemtype_name[elemtype], et->id, et->name);
                }
                else
                {
                    urfp_logf(URFP_LOG_INFO, "Getting details about %s %d (%s)", elemtype_name[elemtype], et->id, et->name);

                    urfp_put_int(env, sizeof(int32_t), "id", et->id - proxy->id_offset);
                    urfp_put_end(env, 0, NULL);

                    urfp_tinypacket_env_data_t* data = (urfp_tinypacket_env_data_t*) (env->transport_data);

                    uint32_t final_timeout = urfp_milliseconds() + 500;
                    urfp_retval_t ur;
                    do
                    {
                        ur = urfp_tinypacket_wait_for_response(tpkt, env, final_timeout);
                    }
                    while (ur == URFP_AGAIN);

                    urfp_proxy_elem_op_data_t* opdata = NULL;
                    if (ur != URFP_OK)
                    {
                        urfp_logf(URFP_LOG_INFO, "Got no answer, dismissing %s %d (%s)", elemtype_name[elemtype], et->id, et->name);
                    }
                    else
                    {
                        opdata = (urfp_proxy_elem_op_data_t*)calloc(1, sizeof(urfp_proxy_elem_op_data_t));
                        if (opdata == NULL)
                        {
                            urfp_logf(URFP_LOG_WARN, "Out of memory for remembering more elements");
                            ur = URFP_ERR;
                        }
                    }

#if URFP_ELEMENTS_WITH_PROPS
                    /* TODO: Query each element for properties. */
                    et->props = 0;
                    et->props_update = NULL;
#endif

                    if (ur != URFP_OK)
                    {
                        switch((urfp_elemtype_t)elemtype)
                        {
                        case ELEMTYPE_VARIABLE:
                        {
                            et->varfuncs.get = urfp_proxy_fail;
                            et->varfuncs.set = urfp_proxy_fail_offset;
                            break;
                        }
                        case ELEMTYPE_FUNCTION:
                        {
                            et->funcfuncs.exec = urfp_proxy_fail;
                            break;
                        }
                        case ELEMTYPE_SOURCE:
                        {
                            et->sourcefuncs.read = urfp_proxy_fail_source_read;
                            et->sourcefuncs.index = urfp_proxy_fail_source_index;
                            break;
                        }
                        case ELEMTYPE_BLOB:
                        {
                            et->blobfuncs.read = urfp_proxy_fail;
                            et->blobfuncs.write = urfp_proxy_fail_offset;
                            break;
                        }
#if URFP_WITH_CHANNELS
                        case ELEMTYPE_CHANNEL:
                        {
                            et->channelfuncs.open = urfp_proxy_fail;
                            et->channelfuncs.activate = urfp_proxy_fail_channel_activate;
                            break;
                        }
#endif
                        }
                    }
                    else
                    {
                        got_details = true;

                        opdata->proxy = proxy;
                        et->op = opdata;

                        switch((urfp_elemtype_t)elemtype)
                        {
                        case ELEMTYPE_VARIABLE:
                        {
                            if ((data->input_length - data->arg_offset) == 1)
                            {
                                opdata->type.code = data->input[data->arg_offset];
                            }
                            else
                            {
                                char desc[128];
                                urfp_get_next_string_arg(env, "desc", desc, sizeof(desc));
                                et->desc = strdup(desc);

                                (void)get_next_datatype_arg(data, &(opdata->type.code), (&opdata->type.length));

                                if ((opdata->type.code & URFP_TYPE_MASK) == URFP_ARRAY_START)
                                {
                                    char name_null[16];
                                    if (opdata->type.code & URFP_NAMED)
                                    {
                                        /* Already know the variable name but have to skip it here to get to the element data type */
                                        urfp_get_next_string_arg(env, "name", name_null, sizeof(name_null));
                                    }

                                    int dev_null;
                                    (void)get_next_datatype_arg(data, &(opdata->type.code), &dev_null);
                                }

                                urfp_logf(URFP_LOG_INFO, "Variable desclen: %d type: '%c' len: %d",
                                    strlen(desc), opdata->type.code, opdata->type.length);
                            }

                            et->varfuncs.get = urfp_proxy_var_get;
                            et->varfuncs.set = urfp_proxy_var_set;
                            break;
                        }
                        /* The ELEMTYPE_FUNCTION code also handles ELEMTYPE_SOURCE info retrieval */
                        case ELEMTYPE_SOURCE:
                        case ELEMTYPE_FUNCTION:
                        {
                            char desc[128];
                            urfp_get_next_string_arg(env, "desc", desc, sizeof(desc));
                            et->desc = strdup(desc);
                            if ((get_next_datatype_arg(data, &(opdata->type.code), &(opdata->type.length)) == URFP_OK) && (opdata->type.code != 0))
                            {
                                urfp_argtype_t code;
                                int length;
                                int argc = 0;
                                int outc = 0;
                                char argv[32];

                                if (opdata->type.code & URFP_NAMED)
                                {
                                    urfp_get_next_string_arg(env, "argv", argv, sizeof(argv));
                                }
                                else
                                {
                                    if ((urfp_elemtype_t)elemtype == ELEMTYPE_FUNCTION && ((code & URFP_ARG) != 0))
                                    {
                                        sprintf(argv, "a%d", argc++);
                                    }
                                    else
                                    {
                                        sprintf(argv, "r%d", outc++);
                                    }
                                }
                                opdata->type.name = strdup(argv);

                                urfp_proxy_elem_type_data_t** n = &(opdata->type.next);

                                while (get_next_datatype_arg(data, &code, &length) == URFP_OK)
                                {
                                    if (code != 0)
                                    {
                                        (*n) = (urfp_proxy_elem_type_data_t*) calloc(1, sizeof(urfp_proxy_elem_type_data_t));
                                        if (*n == NULL)
                                        {
                                            break;
                                        }

                                        if (code & URFP_NAMED)
                                        {
                                            urfp_get_next_string_arg(env, "argv", argv, sizeof(argv));
                                        }
                                        else
                                        {
                                            if ((urfp_elemtype_t)elemtype == ELEMTYPE_FUNCTION && ((code & URFP_ARG) != 0))
                                            {
                                                sprintf(argv, "a%d", argc++);
                                            }
                                            else
                                            {
                                                sprintf(argv, "o%d", outc++);
                                            }
                                        }
                                        (*n)->code = code;
                                        (*n)->length = length;
                                        (*n)->name = strdup(argv);
                                        n = &((*n)->next);
                                    }
                                }
                            }

                            if ((urfp_elemtype_t)elemtype == ELEMTYPE_SOURCE)
                            {
                                et->sourcefuncs.read = urfp_proxy_source_read;
                                et->sourcefuncs.index = urfp_proxy_source_index;
#warning allocate appropriate amount of memory
                                urfp_proxy_source_cache_t* cache = (urfp_proxy_source_cache_t*) calloc(1, sizeof(urfp_proxy_source_cache_t) + 200);
                                cache->base = ((void*)cache) + sizeof(urfp_proxy_source_cache_t);
                                opdata->cache = cache;
                            }
                            else
                            {
                                et->funcfuncs.exec = urfp_proxy_func_exec;
                            }
                            break;
                        }
                        case ELEMTYPE_BLOB:
                        {
                            char desc[128];
                            urfp_get_next_string_arg(env, "desc", desc, sizeof(desc));
                            et->desc = strdup(desc);
                            et->blobfuncs.read = urfp_proxy_blob_read;
                            et->blobfuncs.write = urfp_proxy_blob_write;
                            break;
                        }
#if URFP_WITH_CHANNELS
                        case ELEMTYPE_CHANNEL:
                        {
                            char desc[128];
                            urfp_get_next_string_arg(env, "desc", desc, sizeof(desc));
                            et->desc = strdup(desc);
                            et->channelfuncs.open = urfp_proxy_channel_open;
                            et->channelfuncs.activate = urfp_proxy_channel_activate;
                            break;
                        }
#endif
                        }
                    }

                    urfp_get_end(env);
                }

                if (!got_details)
                {
                    unsigned a = urfp_milliseconds();
                    while (urfp_milliseconds() - a < 100) { };
                }

                -- tries;

            } while (!got_details && tries > 0);
        }
    }

    return true;
}

void urfp_proxy_disconnect(urfp_proxy_t* proxy)
{
    urfp_proxy_cleanup(proxy);
    memset(proxy, 0, sizeof(urfp_proxy_t));
}

static void dummy_proxy_lock(void* lock_arg, bool do_acquire, urfp_transaction_env_t* env)
{
}

void urfp_proxy_init(urfp_proxy_t* proxy, void (*update_source)(const urfp_elem_descriptor_t* source_elem, uint32_t up_to))
{
    memset(proxy, 0, sizeof(urfp_proxy_t));
    proxy->lock = dummy_proxy_lock;
    proxy->update_source = update_source;
}

//#pragma GCC pop_options
/**
@{
}*/

