/* URFP - Universal Remote Function Protocol

Copyright (c) 2015 Kolja Waschk

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

/** @ingroup urfp_server
@{
*/

#include "urfp_api.h"
#include "urfp_server.h"
#if ENABLE_URFP_PROXY
#include "urfp_proxy.h"
#endif
#include "urfp_channel.h"

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

static const char* urfp_help[] =
{
    "he", "General help",
    "pi", "Protocol information",
#if URFP_ELEMENTS_WITH_PROPS
    "hp", "Help about properties",
#endif

#if 0
    "lt", "List data types",
    "ht", "Help about particular data type",
#endif

    "lv", "List variables",
    "hv", "Help about particular variable",
#if URFP_ELEMENTS_WITH_PROPS
    "pv", "Properties of variable",
#endif
    "gv", "Get variable",
    "sv", "Set variable (starting at offset 0)",
    "uv", "Update variable (starting at given offset)",

    "lf", "List functions",
    "hf", "Help about particular function",
#if URFP_ELEMENTS_WITH_PROPS
    "pf", "Properties of function",
#endif
    "cf", "Call function",

    "ls", "List sources",
    "es", "Enable sources",
    "ds", "Disable sources",
    "hs", "Help about particular source",
#if URFP_ELEMENTS_WITH_PROPS
    "ps", "Properties of source(s)",
#endif
    "rs", "Read source once",
    "is", "Index source (current, end, #valid up to end)",

    "lb", "List blobs",
    "hb", "Help about particular blob",
#if URFP_ELEMENTS_WITH_PROPS
    "pd", "Properties of blob",
#endif
    "wb", "Write blob (starting at offset 0)",
    "ub", "Update blob (starting at given offset)",

#if URFP_WITH_CHANNELS
    "lc", "List channels", // list channel templates or all instances??
    "hc", "Help about particular channel",
#if URFP_ELEMENTS_WITH_PROPS
    "pc", "Properties of channel",
#endif
    "oc", "Setup channel",
    "rc", "Release (close) channel",
#endif

    NULL
};

urfp_retval_t urfp_put_continue_help(urfp_transaction_env_t* env)
{
    int i = env->continuation.array.index;
    if (urfp_help[i] != NULL)
    {
        urfp_put_formatted(env, "{ss}",
            "command",
                "mnem", urfp_help[i],
                "desc", urfp_help[i+1]);

        env->continuation.array.index = i+2;
        return URFP_AGAIN;
    }

    return URFP_OK;
}

urfp_retval_t urfp_put_continue_features(urfp_transaction_env_t* env)
{
#if URFP_ELEMENTS_WITH_PROPS
#define CHANNELS_FEATURE_INDEX 1
    int i = env->continuation.array.index;
    if (i == 0)
    {
        urfp_put_formatted(env, "{sib}",
            "feature",
                "mnem", "props",
                "shift", i,
                "avail", true);

        env->continuation.array.index = i+1;
        return URFP_AGAIN;
    }
#else
#define CHANNELS_FEATURE_INDEX 0
#endif
#if URFP_WITH_CHANNELS
    if (i == CHANNELS_FEATURE_INDEX)
    {
        urfp_put_formatted(env, "{sib}",
            "feature",
                "mnem", "channels",
                "shift", i,
                "avail", true);

        env->continuation.array.index = i+1;
        return URFP_AGAIN;
    }
#endif

    return URFP_OK;
}

static urfp_retval_t urfp_elem_help(urfp_transaction_env_t* env)
{
    urfp_retval_t r;
    /* Use env->transport->... instead of urfp_put_string because
       urfp_put_string currently would output a "string" type, not the string itself */
    r = env->transport->put_string(env, "desc", env->elem->desc);
    if (r != URFP_OK)
    {
        r = urfp_put_backgrounded(env, urfp_put_continue_abort);
    }
    return r;
}

/* Default iteration over element lists, assuming that list points to urfp_elem_descriptor_t* */
urfp_retval_t urfp_default_next_elem_from_list(urfp_elem_list_t* base, int* index, const urfp_elem_descriptor_t** elem)
{
    urfp_retval_t r = URFP_ERR;
    if (base != NULL && index != NULL)
    {
        urfp_elem_list_t list = *base;
        int i = *index;

        /* While current pointer just is a link (id<0), follow that */
        while (list != NULL && list[i] != NULL && list[i]->id < 0)
        {
            list = (urfp_elem_list_t)(list[i]->op);
            i = 0;

            *base = list;
            *index = i;
        }

        if (list != NULL && list[i] != NULL)
        {
            if (list[i]->id > 0)
            {
                /* Store *current* element from list into *elem* */
                if (elem != NULL)
                {
                    r = URFP_OK;
                    *elem = list[i];
                }

                /* Then advance index and/or list pointer */
                *index = i + 1;
            }
        }
    }

    return r;
}

urfp_retval_t urfp_put_continue_elem_list(urfp_transaction_env_t* env)
{
    int i = env->continuation.array.index;
    urfp_elem_list_t list = (urfp_elem_list_t)(env->continuation.array.data);
    const urfp_elem_descriptor_t* elem;

    urfp_next_elem_from_list_getter_t next_elem = env->app->next_elem_from_list;
    urfp_retval_t re = next_elem(&list, &i, &elem);
    if (re == URFP_OK)
    {
        urfp_put_formatted(env, "{is}",
            "elem",
                "id", elem->id,
                "name", elem->name);

        env->continuation.array.index = i;
        env->continuation.array.data = list;
        return URFP_AGAIN;
    }

    return URFP_OK;
}

static urfp_retval_t urfp_elem_list(urfp_transaction_env_t* env, const char* title, const urfp_elem_list_t base)
{
    char pattern[8];

    int pattern_length = -1;
    if (urfp_more_args_available(env))
    {
        pattern_length = urfp_get_next_string_arg(env, "pattern", pattern, sizeof(pattern));
        if (pattern_length > sizeof(pattern)-1)
        {
            pattern_length = sizeof(pattern)-1;
        }
    }

    env->transport->put_array_start(env, title, -1);

    if (pattern_length < 0)
    {
        env->continuation.array.index = 0;
        env->continuation.array.data = (void *)base;
        env->continuation.array.output_element = urfp_put_continue_elem_list;
        return urfp_put_backgrounded(env, urfp_put_continue_array);
    }

    /* else search for all elements with given pattern in their name */
    uint32_t skip = 0;
    if (urfp_more_args_available(env))
    {
        skip = urfp_get_next_unsigned_arg(env, 4, "skip");
    }
    uint32_t count = __UINT32_MAX__;
    if (urfp_more_args_available(env))
    {
        count = urfp_get_next_int_arg(env, 4, "maxcount");
    }
#if URFP_ELEMENTS_WITH_PROPS
    uint32_t props_value = 0;
    if (urfp_more_args_available(env))
    {
        props_value = urfp_get_next_unsigned_arg(env, 4, "props");
    }
    uint32_t props_mask = 0;
    if (urfp_more_args_available(env))
    {
        props_mask = urfp_get_next_unsigned_arg(env, 4, "mask");
    }
#endif

    int match_length = pattern_length;
    if (pattern[0] == '*')
    {
        /* Ensure trailing zero for strstr() */
        if (pattern_length >= sizeof(pattern))
        {
            pattern[sizeof(pattern)-1] = 0;
        }
    }
    else
    {
        /* Ensure valid length for strncmp() */
        if (pattern_length > sizeof(pattern))
        {
            match_length = sizeof(pattern);
        }
    }

    int i = 0;
    urfp_elem_list_t list = base;
    const urfp_elem_descriptor_t* elem;

    urfp_next_elem_from_list_getter_t next_elem = env->app->next_elem_from_list;
    urfp_retval_t re = next_elem(&list, &i, &elem);
    while (re == URFP_OK)
    {
        if (count > 0)
        {
            bool matched = false;
            if (pattern[0] == '*')
            {
                /* Match pattern anywhere in name */
                matched = (strstr(elem->name, pattern+1) != NULL);
            }
            else
            {
                /* Match pattern aligned with name */
                if (pattern_length > sizeof(pattern))
                {
                    pattern_length = sizeof(pattern);
                }
                matched = (strncmp(elem->name, pattern, match_length) == 0);
            }

#if URFP_ELEMENTS_WITH_PROPS
            if (matched)
            {
                uint32_t current_props;
                if (elem->props_update != NULL)
                {
                    current_props = elem->props_update(elem);
                }
                else
                {
                    current_props = elem->props;
                }
                if (props_mask != 0)
                {
                    /* No match if masked elem->props don't exactly match props_value */
                    matched = ((current_props & props_mask) == props_value);
                }
                else if (props_value != 0)
                {
                    /* Match if any bits set in props_value are also set in elem->props */
                    matched = ((current_props & props_value) != 0);
                }
            }
#endif

            if (matched)
            {
                if (skip > 0)
                {
                    -- skip;
                }
                else
                {
                    -- count;

                    urfp_put_formatted(env, "{is}",
                        "elem",
                            "id", elem->id,
                            "name", elem->name);
                }
            }
        }

        re = next_elem(&list, &i, &elem);
    }

    env->transport->put_array_end(env);

    return env->transport->put_end(env, 0, NULL);
}

static const urfp_elem_descriptor_t* urfp_elem_find(urfp_transaction_env_t* env, const urfp_elem_list_t base)
{
    int id = urfp_get_next_int_arg(env, 4, "id");

    int i = 0;
    urfp_elem_list_t list = base;
    const urfp_elem_descriptor_t* elem;

    urfp_next_elem_from_list_getter_t next_elem = env->app->next_elem_from_list;
    urfp_retval_t re = next_elem(&list, &i, &elem);
    while (re == URFP_OK)
    {
        if (elem->id == id)
        {
            return elem;
        }
        re = next_elem(&list, &i, &elem);
    }

    return NULL;
}

#if URFP_ELEMENTS_WITH_PROPS
/**
 * This function shows the properties bitsets of the elements whose IDs were passed as arguments to it.
 * If you want to list elements with certain properties, use the filter functions of 'list' commands.
 */
static urfp_retval_t urfp_show_props_of(urfp_transaction_env_t* env, const urfp_elem_descriptor_t** list)
{
    env->transport->put_array_start(env, "properties", -1);
    while (urfp_more_args_available(env))
    {
        const urfp_elem_descriptor_t* elem = urfp_elem_find(env, list);

        if (elem == NULL)
        {
            env->transport->put_end(env, 1, "Element unknown");
            break;
        }
        else
        {
            uint32_t current_props;
            if (elem->props_update != NULL)
            {
                current_props = elem->props_update(elem);
            }
            else
            {
                current_props = elem->props;
            }
            env->transport->put_unsigned(env, sizeof(uint32_t), elem->name, current_props);
        }
    }
    env->transport->put_array_end(env);
    return env->transport->put_end(env, 0, NULL);
}
#endif

/**
 * Allgemeiner Kommandohandler.
 *
 * Dieser ist aufzurufen, um einen eingehenden Request zu bearbeiten.
 *
 * Environment, Code und Tag werden vorausgesetzt.  Weitere Argumente holt sich
 * der Kommandohandler durch Aufruf von \c urfp_get_ Funktionen, Resultate
 * werden mittels \c urfp_put_ Funktionen geliefert.
 *
 * Die Funktion endet im Normalfall mit urfp_put_end() enden liefert URFP_OK.
 *
 * @param env Pointer to transaction environment
 * @param app Pointer to application elements and callbacks
 * @param code Code of the incoming query (e.g. 'gv')
 * @param tag Tag for identifying the incoming query
 */
urfp_retval_t urfp_dispatch_query(urfp_transaction_env_t* env, unsigned code, char tag)
{
    urfp_retval_t fret = URFP_OK;

    if (!env->app || !env->app->next_elem_from_list)
    {
        return URFP_ERR; /* assert? */
    }

    env->mode = URFP_ENV_REPLY;
    env->dry_run = NO;
    env->transport->put_start(env);

    const urfp_app_t* app = env->app;
    urfp_next_elem_from_list_getter_t next_elem = app->next_elem_from_list;

#if URFP_WITH_CHANNELS
    urfp_channel_t* channel = NULL;

    uint8_t elemcode = URFP_ELEMCODE(code);
    if (elemcode == 'V' || elemcode == 'S')
    {
        /* If elemcode is upper case, the query is related to a particular channel. */
        const urfp_elem_descriptor_t* channel_elem = urfp_elem_find(env, app->channel);
        if (channel_elem == NULL)
        {
            return env->transport->put_end(env, 1, "Channel unknown or no ID specified");
        }

        channel = (urfp_channel_t*) (channel_elem->op);

        /* Further code doesn't differentiate between normal or channel access. */
        elemcode = (elemcode == 'V') ? 'v' : 's'; // tolower
        code = URFP_CODE(URFP_ACTCODE(code), elemcode);
    }
#endif

    switch(code)
    {
    case URFP_CODE('h','e'):
    {
        env->transport->put_array_start(env, "help", -1);

        env->continuation.array.index = 0;
        env->continuation.array.output_element = urfp_put_continue_help;

        fret = urfp_put_backgrounded(env, urfp_put_continue_array);

        break;
    }

    case URFP_CODE('p','i'):
    {
        env->transport->put_unsigned(env, 2, "version_major", 1);
        env->transport->put_unsigned(env, 2, "version_minor", 0);
        unsigned feature_bits = 0;
#if URFP_ELEMENTS_WITH_PROPS
        feature_bits |= (1u<<0);
#endif
        env->transport->put_unsigned(env, 4, "feature_bits", feature_bits);
        env->transport->put_array_start(env, "features", -1);

        env->continuation.array.index = 0;
        env->continuation.array.output_element = urfp_put_continue_features;

        fret = urfp_put_backgrounded(env, urfp_put_continue_array);

        break;
    }

#if URFP_ELEMENTS_WITH_PROPS
    case URFP_CODE('h','p'): /* All properties */
    {
        const char* proparrays[4] =
        {
            "variable_props",
            "function_props",
            "datasource_props",
            "blob_props"
        };
        env->transport->put_array_start(env, "properties", 4);
        for (int i=0; i<4; i++)
        {
            env->transport->put_array_start(env, proparrays[i], 32);
            for (int j=0; j<32; j++)
            {
                env->transport->put_string(env, "propname", "");
            }
        }
        fret = env->transport->put_end(env, 0, NULL);
        break;
    }
#endif

    /** Variables */

    case URFP_CODE('l','v'):
    {
#if URFP_WITH_CHANNELS
        fret = urfp_elem_list(env, "variables", (channel!=NULL) ? channel->variable : app->variable);
#else
        fret = urfp_elem_list(env, "variables", app->variable);
#endif
        break;
    }

    case URFP_CODE('h','v'):
    case URFP_CODE('g','v'):
    case URFP_CODE('s','v'):
    case URFP_CODE('u','v'):
    {
#if URFP_WITH_CHANNELS
        env->elem = urfp_elem_find(env, (channel!=NULL) ? channel->variable : app->variable);
#else
        env->elem = urfp_elem_find(env, app->variable);
#endif

        if (env->elem == NULL)
        {
            env->transport->put_end(env, 1, "Variable unknown or no ID specified");
        }
        else
        {
            if (code == URFP_CODE('h','v'))
            {
                env->dry_run = OUT_ANON;
                fret = urfp_elem_help(env);

                if (fret == URFP_OK)
                {
                    fret = env->elem->varfuncs.get(env);
                }
            }
            else if (code == URFP_CODE('g','v'))
            {
                fret = env->elem->varfuncs.get(env);
            }
            else /* must be 'sv' or 'uv', i.e. an attempt to set or update */
            {
                if (env->elem->varfuncs.set == NULL)
                {
                    env->transport->put_end(env, 1, "Variable is read-only");
                }
                else if (code == URFP_CODE('s','v'))
                {
                    fret = env->elem->varfuncs.set(env, 0);
                }
                else /* must be 'uv' (with offset) */
                {
                    if (urfp_more_args_available(env))
                    {
                        fret = env->elem->varfuncs.set(env, urfp_get_next_int_arg(env, 4, "offset"));
                    }
                    else
                    {
                        env->transport->put_end(env, 1, "Too few arguments for update");
                    }
                }
            }
        }
        break;
    }

    /** Functions */

    case URFP_CODE('l','f'):
    {
        fret = urfp_elem_list(env, "functions", app->function);
        break;
    }

    case URFP_CODE('c','f'):
    case URFP_CODE('h','f'):
    {
        env->elem = urfp_elem_find(env, app->function);

        if (env->elem == NULL)
        {
            env->transport->put_end(env, 1, "Function unknown or no ID specified");
        }
        else
        {
            if (code == URFP_CODE('h','f'))
            {
                env->dry_run = OUTPUT;
                fret = urfp_elem_help(env);
            }

            if (fret == URFP_OK)
            {
                fret = env->elem->funcfuncs.exec(env);
            }
        }
        break;
    }

#if URFP_WITH_CHANNELS
    /** Channels */

    case URFP_CODE('l','c'):
    {
        fret = urfp_elem_list(env, "channels", app->channel);
        break;
    }

    case URFP_CODE('h','c'):
    case URFP_CODE('o','c'):
    case URFP_CODE('r','c'):
    {
        env->elem = urfp_elem_find(env, app->channel);

        if (env->elem == NULL)
        {
            env->transport->put_end(env, 1, "Channel unknown or no ID specified");
        }
        else
        {
            if (code == URFP_CODE('h','c'))
            {
                env->dry_run = OUTPUT;
                fret = urfp_elem_help(env);

                if (fret == URFP_OK)
                {
                    fret = env->elem->channelfuncs.open(env);
                }
            }
            else if (code == URFP_CODE('o','c'))
            {
                fret = env->elem->channelfuncs.open(env);
            }
            else if (code == URFP_CODE('r','c'))
            {
                /* Todo: Close that channel if possible */
            }
        }
        break;
    }
#endif

    /** Sources */

    case URFP_CODE('l','s'):
    {
#if URFP_WITH_CHANNELS
        fret = urfp_elem_list(env, "sources", (channel!=NULL) ? channel->source : app->source);
#else
        fret = urfp_elem_list(env, "sources", app->source);
#endif
        break;
    }

    case URFP_CODE('h','s'):
    case URFP_CODE('r','s'):
    case URFP_CODE('i','s'):
    {
#if URFP_WITH_CHANNELS
        env->elem = urfp_elem_find(env, (channel!=NULL) ? channel->source : app->source);
#else
        env->elem = urfp_elem_find(env, app->source);
#endif

        if (env->elem == NULL)
        {
            env->transport->put_end(env, 1, "Source unknown or no ID specified");
        }
        else
        {
            uint32_t begin = 0; /* the first available sample */
            uint32_t curr  = 0; /* the next one to output if you want contiguous output */
            uint32_t end   = 0; /* the sample beyond the last available one */

            urfp_channel_source_index(env, env->elem, &begin, &curr, &end);

            if (code == URFP_CODE('i','s'))
            {
                urfp_put_unsigned(env, sizeof(uint32_t), "begin", begin);
                urfp_put_unsigned(env, sizeof(uint32_t), "curr", curr);
                urfp_put_unsigned(env, sizeof(uint32_t), "end", end);

                fret = urfp_put_end(env, 0, NULL);
            }
            else
            {
                int desired_count = 1;
                uint32_t desired_first = end - desired_count;

                if (code == URFP_CODE('h','s'))
                {
                    env->dry_run = OUTPUT;
                    fret = urfp_elem_help(env);

                    if (fret == URFP_OK)
                    {
                        fret = urfp_channel_source_read(env, env->elem, desired_first, &desired_count);
                    }
                }
                else
                {
                    if (urfp_more_args_available(env))
                    {
                        desired_count = urfp_get_next_int_arg(env, sizeof(int32_t), "count");
                        if (urfp_more_args_available(env))
                        {
                            desired_first = urfp_get_next_unsigned_arg(env, sizeof(uint32_t), "first");
                        }
                        else
                        {
                            if (desired_count < 0)
                            {
                                desired_count = -desired_count;
                                desired_first = end - desired_count;
                            }
                        }
                    }

                    if (fret == URFP_OK)
                    {
                        fret = urfp_channel_source_read(env, env->elem, desired_first, &desired_count);
                    }
                }

                /* In contrast to the other elem funcs, read() does not call urfp_put_end by itself */
                if (fret == URFP_OK)
                {
                    urfp_put_end(env, 0, NULL);
                }
                else if (fret == URFP_AGAIN)
                {
                    urfp_put_end(env, 1, "Desired index/count not available");
                }
                else
                {
                    urfp_put_end(env, 1, "Internal error while accessing data");
                }
            }
        }
        break;
    }

    case URFP_CODE('e','s'):
    case URFP_CODE('d','s'):
    {
        if (env->channel == NULL)
        {
            fret = env->transport->put_end(env, 1, "This connection doesn't support sources");
        }
        else
        {
            bool didnt_find_all = false;
            bool enable = (code == (URFP_CODE('e','s')));

#if URFP_WITH_CHANNELS
            urfp_elem_list_t list = (channel!=NULL) ? channel->source : app->source;
#else
            urfp_elem_list_t list = app->source;
#endif

            /* es without any arguments: just list all enabled sources */
            /* ds without any arguments: disable all */
            if (!urfp_more_args_available(env) && !enable)
            {
                urfp_channel_enable_source(env->channel, NULL, enable);

                if (app->enable_source != NULL)
                {
                    app->enable_source(app->arg, NULL, enable);
                }
            }

            while (urfp_more_args_available(env))
            {
                const urfp_elem_descriptor_t* elem = urfp_elem_find(env, list);

                if (elem == NULL)
                {
                    didnt_find_all = true;
                }
                else
                {
                    urfp_channel_enable_source(env->channel, elem, enable);

                    if (app->enable_source != NULL)
                    {
                        app->enable_source(app->arg, elem, enable);
                    }
                }
            }

            if (didnt_find_all)
            {
                /* TBD: Some others might have been enabled but not all.. Disable those? */
                fret = env->transport->put_end(env, 1, "At least one source(s) is unknown");
            }
            else
            {
                /* Finally output a list of all *enabled* sources
                   (regardless of whether this was called to enable, or to disable) */

                urfp_put_array_start(env, "sources", -1);

                int i = 0;
                urfp_elem_list_t srclist = list;
                const urfp_elem_descriptor_t* elem;

                urfp_retval_t re = next_elem(&srclist, &i, &elem);
                while (re == URFP_OK)
                {
                    if (urfp_channel_source_enabled(env->channel, elem))
                    {
                        urfp_put_unsigned(env, sizeof(uint32_t), "id", elem->id);
                    }
                    re = next_elem(&srclist, &i, &elem);
                }
                urfp_put_array_end(env);

                fret = env->transport->put_end(env, 0, NULL);
            }
        }
        break;
    }

    case URFP_CODE('l','b'):
    {
        fret = urfp_elem_list(env, "blobs", app->blob);
        break;
    }

    case URFP_CODE('h','b'):
    case URFP_CODE('r','b'):
    case URFP_CODE('w','b'):
    case URFP_CODE('u','b'):
    {
        env->elem = urfp_elem_find(env, app->blob);

        if (env->elem == NULL)
        {
            env->transport->put_end(env, 1, "Blob unknown or no ID specified");
        }
        else
        {
            if (code == URFP_CODE('h','b'))
            {
                env->dry_run = OUT_ANON;
                fret = urfp_elem_help(env);

                if (fret == URFP_OK)
                {
                    fret = env->elem->blobfuncs.read(env);
                }
            }
            else if (code == URFP_CODE('r','b'))
            {
                fret = env->elem->blobfuncs.read(env);
            }
            else /* must be 'wb' or 'ub', i.e. an attempt to change a blob */
            {
                if (env->elem->blobfuncs.write == NULL)
                {
                    env->transport->put_end(env, 1, "Blob is read-only");
                }
                else if (code == URFP_CODE('w','b'))
                {
                    fret = env->elem->blobfuncs.write(env, 0);
                }
                else /* must be 'ub' (with offset) */
                {
                    if (urfp_more_args_available(env))
                    {
                        fret = env->elem->blobfuncs.write(env, urfp_get_next_int_arg(env, 4, "offset"));
                    }
                    else
                    {
                        env->transport->put_end(env, 1, "Too few arguments for update");
                    }
                }
            }
        }
        break;
    }

#if URFP_ELEMENTS_WITH_PROPS
    case URFP_CODE('p','v'):
    {
        fret = urfp_show_props_of(env, app->variable);
        break;
    }

    case URFP_CODE('p','f'):
    {
        fret = urfp_show_props_of(env, app->function);
        break;
    }

    case URFP_CODE('p','s'):
    {
#if URFP_WITH_CHANNELS
        fret = urfp_show_props_of(env, (channel!=NULL) ? channel->source : app->source);
#else
        fret = urfp_show_props_of(env, app->source);
#endif
        break;
    }

    case URFP_CODE('p','b'):
    {
        fret = urfp_show_props_of(env, app->blob);
        break;
    }

#if URFP_WITH_CHANNELS
    case URFP_CODE('p','c'):
    {
        fret = urfp_show_props_of(env, app->channel);
        break;
    }
#endif
#endif

    case URFP_CODE('l','g'):
    {
        if (app->handle_oob != NULL)
        {
            fret = app->handle_oob(env);
        }
        break;
    }

    default:
    {
        env->transport->put_end(env, 1, "Unknown code, try 'he' for help");
        break;
    }
    } // switch

    return fret;
}

void urfp_set_auth(urfp_transaction_env_t* env, void* auth)
{
    env->auth = auth;
}

/* Write integer with ASCII decimal digits into memory. Writing
   begins with least significant digit at out+outlen-1 and continues
   until the number is written. The offset of the most significant
   digit or sign to out is returned.

   Example:

    out = 0
    outlen = 10
    value = 123
    negative = true

   Effect:

    out+6 = -123

   Returns:

    6

   No null byte is put at the end!
*/
size_t urfp_anyntoa32(char* out, size_t outlen, uint32_t value, bool negative)
{
    size_t i = outlen;
    do
    {
        out[--i] = '0' + (value % 10);
        value /= 10;
    }
    while (value > 0);

    if (negative)
    {
        out[--i] = '-';
    }

    return i;
}

size_t urfp_anyntoa64(char* out, size_t outlen, uint64_t value, bool negative)
{
    size_t i = outlen;
    do
    {
        out[--i] = '0' + (value % 10);
        value /= 10;
    }
    while (value > 0);

    if (negative)
    {
        out[--i] = '-';
    }

    return i;
}

#ifndef URFP_WITHOUT_FLOAT
size_t urfp_anyftoa(char* out, size_t outlen, double value, int places)
{
    bool negative = (value < 0.0);
    if (negative)
    {
        value = -value;
    }
    uint64_t uv = (uint64_t)value;
    double fract = value - (double)uv;

    size_t i = outlen;
    if (places > 0)
    {
        i = outlen-places-1;
        out[i] = '.';
        for (int p=1; p<places; p++)
        {
            fract = fract * 10;
            unsigned f = (unsigned)fract;
            out[i+p] = '0' + f;
            fract -= (double)f;
        }

        /* Because casting double to unsigned always rounds down, we
         * make an extra round up after last digit to ensure that most
         * 753/100 doesn't come out as 72.999 but 73.000 */

        fract = (fract * 100 + 5) / 10;

        /* However, the rounding up might now propagate up to uv */
        int p = outlen - 1;
        out[p] = '0' + (unsigned)fract;
        while (p > i && out[p] >= ':')
        {
            out[p] = '0';
            -- p;
            ++ out[p];
        }

        if (p == i && out[p] >= ':')
        {
            out[p] = '0';
            ++ uv;
        }
    }

    return urfp_anyntoa64(out, i, uv, negative);
}
#endif

/**
@}
*/
