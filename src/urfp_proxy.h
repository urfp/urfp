/* URFP - Universal Remote Function Protocol

Copyright (c) 2015 Kolja Waschk

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#ifndef URFP_PROXY_H
#define URFP_PROXY_H

#include "urfp_api.h"
#include "urfp_tinypacket.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct urfp_proxy
{
    urfp_tinypacket_t* tpkt;
    int elem_index[URFP_NUM_ELEMTYPES]; // number of elements added up to now
    urfp_elem_descriptor_t* elem_tables[URFP_NUM_ELEMTYPES]; // linked list of elems
    urfp_elem_descriptor_t** elem_refs[URFP_NUM_ELEMTYPES]; // lists with pointers to elems
    uint32_t enabled_source;
    int id_offset;
    void* lock_arg; /* arbitrary pointer to be passed to lock */
    void (*lock)(void* lock_arg, bool do_acquire, urfp_transaction_env_t* env); /* env=NULL: protect urfp_proxy_t. env!=NULL: protect env */
    void (*update_source)(const urfp_elem_descriptor_t* source_elem, uint32_t up_to); /* called by proxy to forward source data to */
}
urfp_proxy_t;

typedef struct urfp_proxy_elem_type_data
{
    urfp_argtype_t code;   // type char see urfp_tinypacket_describe_*_arg
    int length;  // -1 unknown, >0 fixed length
    char* name;  // malloc'ed! for storing argument names if known.
    struct urfp_proxy_elem_type_data* next;
}
urfp_proxy_elem_type_data_t;

typedef struct urfp_proxy_elem_op_data
{
    urfp_proxy_t* proxy;
    urfp_proxy_elem_type_data_t type;
    uint32_t props;
    void* cache;
}
urfp_proxy_elem_op_data_t;

typedef struct urfp_proxy_source_cache
{
    void* base;
    uint32_t begin;
    uint32_t end;
    int count;
}
urfp_proxy_source_cache_t;

extern void urfp_proxy_init(urfp_proxy_t* proxy, void (*update_source)(const urfp_elem_descriptor_t* source_elem, uint32_t up_to));
extern bool urfp_proxy_connect(urfp_proxy_t* proxy, urfp_tinypacket_t* tpkt, int id_offset, const char* name_prefix);
extern void urfp_proxy_verify(urfp_proxy_t* proxy, const char* name_prefix);
extern void urfp_proxy_disconnect(urfp_proxy_t* proxy);

extern urfp_retval_t urfp_proxy_source_read(urfp_transaction_env_t* env, uint32_t first, int* count);
extern urfp_retval_t urfp_proxy_source_enable(const urfp_elem_descriptor_t* source_elem, bool enable);
extern urfp_retval_t urfp_proxy_source_feed(urfp_transaction_env_t* env, const urfp_elem_list_t list);

#ifdef __cplusplus
}
#endif

#endif
