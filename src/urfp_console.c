/* URFP - Universal Remote Function Protocol

Copyright (c) 2015 Kolja Waschk

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

/** @addtogroup urfp_console URFP serial console
@{
*/

#include "urfp_console.h"
#include "urfp_server.h"
#include "urfp_api.h"
#include "urfp_channel.h"

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

static void urfp_console_determine_tag(urfp_console_t* con, char* tag)
{
    *tag = con->last_input_tag + 1;
    if (*tag <= '0' || *tag >= '9')
    {
        *tag = '0';
    }
    con->last_input_tag = *tag;
}

static void urfp_console_determine_oob_tag(urfp_console_t* con, char* tag)
{
    *tag = con->last_oob_tag + 1;
    if (*tag <= 'A' || *tag >= 'Z')
    {
        *tag = 'A';
    }
    con->last_oob_tag = *tag;
}

void urfp_console_output_string(urfp_console_t* con, const char* str, size_t str_length)
{
    if (str_length > 0)
    {
        con->last_char_out = str[str_length-1];
        con->output_string(con->arg, str, str_length);
    }
}

void urfp_console_output_char(urfp_console_t* con, char c)
{
    con->last_char_out = c;
    con->output_char(con->arg, c);
}

static void urfp_console_output_prompt(urfp_console_t* con)
{
    size_t needed;

    con->lock_inbuf(con->arg);

    needed = con->prompt_length;

    /* Need to get back to column 1? */
    bool precrlf = (con->last_char_out != 10)
        && (con->echo || !(con->last_char_in == 10));

    if (precrlf)
    {
        needed += 2;
    }
    if (con->inbuf_length > 0)
    {
        needed += con->inbuf_length;
    }

    if (con->output_pretest(con, needed) == URFP_OK)
    {
    	if (precrlf)
        {
            urfp_console_output_char(con, 13);
            urfp_console_output_char(con, 10);
        }

        urfp_console_output_string(con, con->prompt, con->prompt_length);
        con->want_prompt = false;

        /* Reconstruct line with prompt and eventually some input */
        if (con->inbuf_length > 0)
        {
            /* Rewrite all input received up to now */
            urfp_console_output_string(con, con->inbuf, con->inbuf_length);
        }

        con->want_prompt = false;
    }

    con->release_inbuf(con->arg);
}

/**
 * @param con    Pointer to UrFP console instance
 * @param oob    Set to true if this is not part of a reply to a query.
 * @param prefix Should be an incrementing char (e.g. 0..9 with wrap) or, in final line of output, one of
 *               - '*' for final line of a reply to a query or OOB output
 *               - '!' for final line of a reply indicating that a query failed and should not be retried
 *               - '?' for final line of a reply indicating that a query failed but might succeed when retried
 *               -  '~' (TBD) for timeout extension request in reply to a query
 * @param code   An unsigned value. Its LSB will be output as first command char, second by as second command char
 * @param tag    Pointer to char containing tag. Put 0 in the char to have it set by UrFP (for OOB)
 * @param string Pointer to the content. Does not have to have a trailing zero and must not contain CR/LF.
 * @param string_length Length of the content (excluding and trailing zero or CR/LF).
 */

urfp_retval_t urfp_console_output_line(urfp_console_t* con, bool oob, char prefix, unsigned code, char* tag, const char* string, size_t string_length)
{
    int i;

    /* Compute minimum needed space for output */
    i = 5 /* prefix */
        + string_length
        + 5 /* crc & crlf */;

    bool input_to_overwrite = (oob && con->last_char_out != 10);

    if (input_to_overwrite)
    {
    	i++;
    }

    urfp_retval_t r = con->output_pretest(con, i);
    if (r != URFP_OK)
    {
        return r;
    }

    if (input_to_overwrite)
    {
        urfp_console_output_char(con, 13);
    }

    char side[5];
    side[0] = prefix;
    side[1] = code & 0xff;
    side[2] = (code >> 8) & 0xff;
    if (*tag == 0)
    {
        if (oob)
        {
            urfp_console_determine_oob_tag(con, tag);
        }
        else
        {
            urfp_console_determine_tag(con, tag);
        }
    }
    side[3] = *tag;
    side[4] = ' ';

    urfp_console_output_string(con, side, 5);

    urfp_console_output_string(con, string, string_length);

    side[1] = '#'; /* TODO: Compute and output CRC16 */
    side[2] = '#';
    side[3] = 13;
    side[4] = 10;

    if (string_length > 0)
    {
        side[0] = 32;
        urfp_console_output_string(con, side, 5);
        string_length ++;
    }
    else
    {
        urfp_console_output_string(con, side+1, 4);
    }

    if (input_to_overwrite)
    {
        /* Clear to end of line */
        for (i = 5 + string_length + 4; i<con->prompt_length + con->inbuf_length; ++i)
        {
            urfp_console_output_char(con, 32);
        }
    }

    if (prefix == '*' || prefix == '!' || prefix == '?')
    {
        con->want_prompt = true;
    }

    return URFP_OK;
}

static void urfp_console_free_env(urfp_console_t* con, urfp_transaction_env_t* env)
{
#if 1 /* validate pointer */
    int i;
    for (i=0; i<URFP_CONSOLE_MAX_REPLY_ENVS; i++)
    {
        if (env == &(con->transaction_env[i]))
        {
            urfp_console_env_data_t* data = &(con->transaction_env_data[i]);
            if (!data->keep_allocated)
            {
                data->allocated = false;
            }
        }
    }
#else
    ((urfp_console_env_data_t*)env->transport_data)->allocated = false;
#endif
}

static urfp_transaction_env_t* urfp_console_alloc_env(urfp_console_t* con)
{
    int i;
    for (i=0; i<URFP_CONSOLE_MAX_REPLY_ENVS; i++)
    {
        urfp_console_env_data_t* data = &(con->transaction_env_data[i]);
        if (!data->allocated)
        {
            data->allocated = true;
            data->content_length = 0;
            data->content_complete = false;
            return &(con->transaction_env[i]);
        }
    }

    return NULL;
}

static void urfp_console_input_line(urfp_console_t* con)
{
    char tag = '0';
    unsigned code = 0;
    int offset = 0;

    char* line = con->inbuf;
    int line_length = con->inbuf_length;

    /* Special case as in SMTP: A dot on a line by itself ends session */
    if (line_length == 1 && line[0] == '.')
    {
        //urfp_server_exit(&con->urfp_server);
        return;
    }

    /* Skip whitespace at beginning */
    while (offset < line_length && isspace((int)line[offset]))
    {
        offset++;
    }

    if (line_length - offset == 0)
    {
        /* No input at all */
        return;
    }

    /* Determine code (if there's enough input) */
    if (line_length - offset < 2 || isspace((int)line[offset+1]))
    {
        const char tooshort[] = "Need at least two CODE chars!";
        urfp_console_output_line(con, /*oob=*/false, '!', URFP_CODE('?','?'), &tag, tooshort, sizeof(tooshort));
        return;
    }

    code = URFP_CODE(line[offset], line[offset+1]);
    offset += 2;

    /* Determine tag, if given */
    if (line_length - offset >= 1 && !isspace((int)line[offset]))
    {
        if (line_length - offset >= 2 && !isspace((int)line[offset+1]))
        {
            const char toolong[] = "Unrecognized code!";
            urfp_console_output_line(con, /*oob=*/false, '!', code, &tag, toolong, sizeof(toolong));
            return;
        }

        tag = line[offset];
        ++ offset;
    }
    else
    {
        urfp_console_determine_tag(con, &tag);
    }

    con->arg_offset = offset;

    urfp_transaction_env_t* env = urfp_console_alloc_env(con);
    if (env == NULL)
    {
        const char toolong[] = "Out of memory for constructing reply!";
        urfp_console_output_line(con, /*oob=*/false, '!', code, &tag, toolong, sizeof(toolong));
        return;
    }

    env->code = code;
    env->tag  = tag;
    ((urfp_console_env_data_t*)env->transport_data)->oob = false;

    urfp_dispatch_query(env, code, tag);
    if (urfp_resume_get(env) != URFP_OK)
    {
        urfp_finish_get(env);
    }
}

static bool urfp_console_more_args_available(urfp_transaction_env_t *env)
{
    urfp_console_t* con = (urfp_console_t*)(env->transport_conn);

    char* line = con->inbuf;
    int line_length = con->inbuf_length;

    while (con->arg_offset < line_length && isspace((int)line[con->arg_offset]))
    {
        ++ con->arg_offset;
    }

    return (con->arg_offset < line_length);
}

static int urfp_console_get_arg_name(urfp_transaction_env_t *env, char* buf, size_t maxlen)
{
    return 0;
}

static int urfp_console_get_next_arg(urfp_transaction_env_t* env, char* buf, size_t maxsize)
{
    if (!urfp_console_more_args_available(env))
    {
        return -1;
    }

    urfp_console_t* con = (urfp_console_t*)(env->transport_conn);
    char* line = con->inbuf;
    int line_length = con->inbuf_length;
    int offset = con->arg_offset;

    int arglen = 0;
    bool quoted = false;
    bool escaped = false;

    while (offset < line_length && (!isspace((int)line[offset]) || quoted || escaped))
    {
        if (escaped)
        {
            escaped = false;
            if (arglen < maxsize)
            {
                buf[arglen++] = line[offset];
            }
        }
        else if (line[offset] == '\\')
        {
            escaped = true;
        }
        else if (line[offset] == '"' || line[offset] == '\'')
        {
            quoted = !quoted;
        }
        else if (arglen < maxsize)
        {
            buf[arglen++] = line[offset];
        }

        ++ offset;
    }

    con->arg_offset = offset;

    urfp_console_env_data_t* data = (urfp_console_env_data_t*)(env->transport_data);

    /* Enforce trailing zero */
    if (arglen >= maxsize)
    {
        urfp_console_env_data_t* data = (urfp_console_env_data_t*)(env->transport_data);

        data->get_succeeded = URFP_ERR;
        arglen = maxsize-1;
    }
    else
    {
        data->get_succeeded = URFP_OK;
    }

    buf[arglen] = 0;

    return arglen;
}

int32_t urfp_console_get_int_arg(urfp_transaction_env_t* env, size_t szof, const char* name)
{
    char arg[16];
    urfp_console_env_data_t* data = (urfp_console_env_data_t*)(env->transport_data);
    int32_t r = urfp_parse_int_arg(arg, sizeof(arg), urfp_console_get_next_arg(env, arg, sizeof(arg)), &(data->get_succeeded));
    if ((szof == 1 && (r < -128 || r >= 128)) || (szof == 2 && (r < -32768 || r >= 32768)))
    {
        data->get_succeeded = URFP_RANGE;
    }
    return r;
}

uint32_t urfp_console_get_unsigned_arg(urfp_transaction_env_t* env, size_t szof, const char* name)
{
    char arg[16];
    urfp_console_env_data_t* data = (urfp_console_env_data_t*)(env->transport_data);
    uint32_t r = urfp_parse_unsigned_arg(arg, sizeof(arg), urfp_console_get_next_arg(env, arg, sizeof(arg)), &(data->get_succeeded));
    if ((szof == 1 && r >= 256) || (szof == 2 && r >= 65536))
    {
        data->get_succeeded = URFP_RANGE;
    }
    return r;
}

uint64_t urfp_console_get_uint64_arg(urfp_transaction_env_t* env, const char* name)
{
    char arg[22];
    urfp_console_env_data_t* data = (urfp_console_env_data_t*)(env->transport_data);
    uint64_t r = urfp_parse_uint64_arg(arg, sizeof(arg), urfp_console_get_next_arg(env, arg, sizeof(arg)), &(data->get_succeeded));
    return r;

}

#ifndef URFP_WITHOUT_FLOAT
double urfp_console_get_float_arg(urfp_transaction_env_t* env, size_t szof, const char* name)
{
    char arg[22];
    urfp_console_env_data_t* data = (urfp_console_env_data_t*)(env->transport_data);
    double r = urfp_parse_float_arg(arg, sizeof(arg), urfp_console_get_next_arg(env, arg, sizeof(arg)), &(data->get_succeeded));
    return r;
}
#endif

bool urfp_console_get_bool_arg(urfp_transaction_env_t* env, const char* name)
{
    char arg[16];
    urfp_console_env_data_t* data = (urfp_console_env_data_t*)(env->transport_data);
    bool r = urfp_parse_bool_arg(arg, sizeof(arg), urfp_console_get_next_arg(env, arg, sizeof(arg)), &(data->get_succeeded));
    return r;

}

int urfp_console_get_string_arg(urfp_transaction_env_t* env, const char* name, char* buf, size_t maxlen)
{
    return urfp_console_get_next_arg(env, buf, maxlen);
}

urfp_retval_t urfp_console_get_succeeded(urfp_transaction_env_t* env)
{
    urfp_console_env_data_t* data = (urfp_console_env_data_t*)(env->transport_data);
    return data->get_succeeded;
}

urfp_retval_t urfp_console_get_blob_start(urfp_transaction_env_t* env, const char* name, size_t* maxlength)
{
    return URFP_OK;
}

urfp_retval_t urfp_console_get_blob_access(urfp_transaction_env_t* env, void** ptr, size_t* length)
{
    return URFP_ERR;
}

urfp_retval_t urfp_console_get_blob_pause(urfp_transaction_env_t* env)
{
    return URFP_OK;
}

urfp_retval_t urfp_console_get_blob_end(urfp_transaction_env_t* env)
{
    return URFP_OK;
}

urfp_retval_t urfp_console_get_array_start(urfp_transaction_env_t* env, const char* name, size_t* maxcount)
{
    return URFP_OK;
}

urfp_retval_t urfp_console_get_array_end(urfp_transaction_env_t* env)
{
    return URFP_OK;
}

urfp_retval_t urfp_console_get_struct_start(urfp_transaction_env_t* env, const char* name)
{
    return URFP_OK;
}

urfp_retval_t urfp_console_get_struct_end(urfp_transaction_env_t* env)
{
    return URFP_OK;
}

void urfp_console_get_end(urfp_transaction_env_t* env)
{
}

urfp_retval_t urfp_console_input_char(urfp_console_t* con, char c)
{
    switch(c)
    {
    case 10:
    case 13:
    {
        if (c == 10 && con->last_char_in == 13)
        {
            /* ignore LF after CR */
        }
        else
        {
            if (con->echo)
            {
                urfp_console_output_char(con, 13);
                urfp_console_output_char(con, 10);
            }

            urfp_console_input_line(con);
            con->inbuf_length = 0;
            con->want_prompt = true;
        }
        con->last_char_in = c;
        break;
    }
    case 0x03: /* Ctrl-C: EOT */
    {
        //urfp_server_cancel(&(con->urfp_server), con->last_input_tag);
        break;
    }
    case 0x04: /* Ctrl-D: EOT */
    case 0x1A: /* Ctrl-Z: SUB */
    {
        //urfp_server_exit(&con->urfp_server);
        break;
    }
    case 127: /* DEL */
    {
        if (con->inbuf_length > 0)
        {
            -- con->inbuf_length;

            if (con->echo)
            {
                urfp_console_output_char(con, 0x08);
                urfp_console_output_char(con, 0x20);
                urfp_console_output_char(con, 0x08);
            }
        }
        break;
    }
    default:
    {
        if (c >= 32 && c < 127 && con->inbuf_length < con->inbuf_size)
        {
            con->inbuf[con->inbuf_length] = c;
            ++ con->inbuf_length;

            if (con->echo)
            {
                urfp_console_output_char(con, c);
            }
        }
        break;
    }
    }

    return URFP_OK;
}

static inline bool urfp_console_output_on_hold(urfp_transaction_env_t* env)
{
    urfp_console_env_data_t* data = (urfp_console_env_data_t*)(env->transport_data);

    return data->content_complete;
}

static urfp_retval_t urfp_console_advance_line(urfp_transaction_env_t* env)
{
    urfp_console_t* con = (urfp_console_t*)(env->transport_conn);
    urfp_console_env_data_t* data = (urfp_console_env_data_t*)(env->transport_data);

    if (data->content_length > 0)
    {
        data->content_complete = true;

        urfp_retval_t r = urfp_console_output_line(con, data->oob, data->prefix, env->code, &env->tag, data->content, data->content_length);
        if (r != URFP_OK)
        {
            return r;
        }

        data->prefix++;
        if (data->prefix == '8')
        {
            data->prefix = '0';
        };
        data->content_length = 0;
        data->content_complete = false;
    }

    return URFP_OK;
}

/* Output string with line break or space before as appropriate, in double quotes, optionally with :name added */
urfp_retval_t urfp_console_put_string_with_name(urfp_transaction_env_t* env, const char* name, const char* string, bool with_name)
{
    urfp_console_env_data_t* data = (urfp_console_env_data_t*)(env->transport_data);

    if (string == NULL)
    {
        return URFP_OK;
    }

    size_t slen = strlen(string);
    size_t nlen = 0;
    size_t total = slen;

    if (with_name)
    {
        nlen = strlen(name);
        total += /* ':' */ 1 + nlen;
    }

    if (data->content_length > 0 && data->content_length + 2 + total + 1 > URFP_CONSOLE_MAX_CONTENT_LENGTH)
    {
        urfp_retval_t r = urfp_console_advance_line(env);
        if (r != URFP_OK)
        {
            return r;
        }
    }

    if (data->content_length + 2 + total + 1 > URFP_CONSOLE_MAX_CONTENT_LENGTH)
    {
        return URFP_ERR;
    }

    if (data->content_length > 0)
    {
        data->content[data->content_length++] = 0x20;
    }

    data->content[data->content_length++] = '"';

    /* TODO: Escape quotes within string */

    if (with_name)
    {
        memcpy(data->content + data->content_length, name, nlen);
        data->content_length += nlen;

        data->content[data->content_length++] = ':';
    }

    memcpy(data->content + data->content_length, string, slen);
    data->content_length += slen;

    data->content[data->content_length++] = '"';

    return URFP_OK;
}

/* Output string with line break or space before as appropriate, in double quotes, without name (argument is ignored) */
urfp_retval_t urfp_console_put_string(urfp_transaction_env_t* env, const char* name, const char* string)
{
    return urfp_console_put_string_with_name(env, name, string, false);
}

/* Output string with line break or space before as appropriate, without quoting, without name */
static urfp_retval_t urfp_console_put_bare_string(urfp_transaction_env_t* env, const char* name, const char* string, size_t slen)
{
    urfp_console_env_data_t* data = (urfp_console_env_data_t*)(env->transport_data);

    if (data->content_length > 0 && data->content_length + 1 + slen > URFP_CONSOLE_MAX_CONTENT_LENGTH)
    {
        urfp_retval_t r = urfp_console_advance_line(env);
        if (r != URFP_OK)
        {
            return r;
        }
    }

    if (data->content_length + 1 + slen > URFP_CONSOLE_MAX_CONTENT_LENGTH)
    {
        return URFP_ERR;
    }

    if (data->content_length > 0)
    {
        data->content[data->content_length++] = 0x20;
    }

    memcpy(data->content + data->content_length, string, slen);
    data->content_length += slen;

    return URFP_OK;
}


urfp_retval_t urfp_console_put_bool(urfp_transaction_env_t* env, const char* name, bool value)
{
    if (value)
    {
        return urfp_console_put_bare_string(env, name, "true", 4);
    }
    else
    {
        return urfp_console_put_bare_string(env, name, "false", 5);
    }
}

urfp_retval_t urfp_console_put_int(urfp_transaction_env_t* env, size_t szof, const char* name, int32_t value)
{
    char intstring[12];
    bool negative = (value<0);
    if (negative)
    {
        value = -value;
    }
    size_t i = urfp_anyntoa32(intstring, sizeof(intstring), value, negative);

    return urfp_console_put_bare_string(env, name, intstring+i, sizeof(intstring)-i);
}

urfp_retval_t urfp_console_put_unsigned(urfp_transaction_env_t* env, size_t szof, const char* name, uint32_t value)
{
    char intstring[12];
    size_t i = urfp_anyntoa32(intstring, sizeof(intstring), value, false);

    return urfp_console_put_bare_string(env, name, intstring+i, sizeof(intstring)-i);
}

urfp_retval_t urfp_console_put_uint64(urfp_transaction_env_t* env, const char* name, uint64_t value)
{
    char intstring[22];
    size_t i = urfp_anyntoa64(intstring, sizeof(intstring), value, false);

    return urfp_console_put_bare_string(env, name, intstring+i, sizeof(intstring)-i);
}

#ifndef URFP_WITHOUT_FLOAT
urfp_retval_t urfp_console_put_float(urfp_transaction_env_t* env, size_t szof, const char* name, double value)
{
    char floatstring[44];
    size_t i = urfp_anyftoa(floatstring, sizeof(floatstring), value, 3/*decimal places*/);
    return urfp_console_put_bare_string(env, name, floatstring+i, sizeof(floatstring)-i);
}
#endif

urfp_retval_t urfp_console_put_array_start(urfp_transaction_env_t* env, const char* name, int maxcount)
{
    /* TBD: noop on console */
    return urfp_console_advance_line(env);
}

urfp_retval_t urfp_console_put_array_end(urfp_transaction_env_t* env)
{
    /* TBD: noop on console */
    return urfp_console_advance_line(env);
}

urfp_retval_t urfp_console_put_struct_start(urfp_transaction_env_t* env, const char* name)
{
    /* TBD: noop on console */
    return urfp_console_advance_line(env);
}

urfp_retval_t urfp_console_put_struct_end(urfp_transaction_env_t* env)
{
    /* TBD: noop on console */
    return urfp_console_advance_line(env);
}


urfp_retval_t urfp_console_put_start(urfp_transaction_env_t* env)
{
    urfp_console_env_data_t* data = (urfp_console_env_data_t*)(env->transport_data);
    data->prefix = '0';
    return URFP_OK;
}

urfp_retval_t urfp_console_put_end(urfp_transaction_env_t* env, int code, char* error_text)
{
    urfp_console_t* con = (urfp_console_t*)(env->transport_conn);
    urfp_console_env_data_t* data = (urfp_console_env_data_t*)(env->transport_data);

    urfp_retval_t r = URFP_OK;

    if (env->dry_run == INPUT)
    {
        env->dry_run = OUTPUT;
        env->transport->put_string(env, "_args", ")");
    }

    if (code == 0)
    {
        if (data->content_length > 0)
        {
            r = urfp_console_output_line(con, data->oob, '*', env->code, &env->tag, data->content, data->content_length);
        }
        else
        {
            r = urfp_console_output_line(con, data->oob, '*', env->code, &env->tag, "OK", 2);
        }
    }
    else
    {
        if (data->content_length > 0)
        {
            r = urfp_console_output_line(con, data->oob, data->prefix, env->code, &env->tag, data->content, data->content_length);
        }
        if (r == URFP_OK)
        {
        	if (error_text == NULL)
			{
				/* This should not happen */
				error_text = "N/A";
			}
            urfp_console_output_line(con, data->oob, '!', env->code, &env->tag, error_text, strlen(error_text));
        }
    }

    if (r != URFP_AGAIN)
    {
        /* Zero data in case of reuse of static env, e.g. for logging */
        data->content_length = 0;
        data->content_complete = false;

        urfp_console_free_env(con, env);
    }

    return r;
}

urfp_retval_t urfp_console_resume_replies(urfp_console_t* con)
{
    int i;
    urfp_retval_t r = URFP_OK;

    for (i=0; i<URFP_CONSOLE_MAX_REPLY_ENVS && r == URFP_OK; i++)
    {
        urfp_console_env_data_t* data = &(con->transaction_env_data[i]);
        if (data->allocated)
        {
            urfp_transaction_env_t* env = &(con->transaction_env[i]);

            if (data->content_complete)
            {
                r = urfp_console_output_line(con, data->oob, data->prefix, env->code, &env->tag, data->content, data->content_length);

                if (r == URFP_OK)
                {
                    data->content_length = 0;
                    data->content_complete = false;
                }
            }

            if (!urfp_console_output_on_hold(env))
            {
               r = urfp_resume_put(env);
            }

            if (r != URFP_OK || urfp_console_output_on_hold(env))
            {
                return URFP_AGAIN;
            }
        }
    }

    if (r == URFP_OK && con->want_prompt && con->prompt)
    {
        urfp_console_output_prompt(con);
    }

    return r;
}

urfp_retval_t urfp_console_describe_arg(urfp_transaction_env_t* env, const char* name, urfp_argtype_t type, int len)
{
    const char* urfp_put_itype[5] = { "i8", "i16", "i24", "i32", "i64" };
    const char* urfp_put_utype[5] = { "u8", "u16", "u24", "u32", "u64" };

    /* Arguments to functions are shown beforehand inside a struct named _args */
    if (env->dry_run == OUTPUT && ((type & URFP_ARG) != 0))
    {
        env->dry_run = INPUT;
        urfp_console_put_string_with_name(env, "_args", "{", true);
    }

    if (env->dry_run == INPUT && ((type & URFP_ARG) == 0))
    {
        urfp_console_put_string_with_name(env, NULL, "}", false);
        env->dry_run = OUTPUT;
    }

    /* Name shall be shown only when describing functions and data, not with variables or blobs */
    bool show_name = (env->dry_run != OUT_ANON);

    switch(type & URFP_TYPE_MASK)
    {
    case URFP_BOOL:
        return urfp_console_put_string_with_name(env, name, "bool", show_name);
    case URFP_STRING:
        return urfp_console_put_string_with_name(env, name, "string", show_name);
    case URFP_INT:
        if (len > 4)
        {
            return urfp_console_put_string_with_name(env, name, urfp_put_itype[4], show_name);
        }
        else
        {
            return urfp_console_put_string_with_name(env, name, urfp_put_itype[len-1], show_name);
        }
    case URFP_UNSIGNED:
        if (len > 4)
        {
            return urfp_console_put_string_with_name(env, name, urfp_put_utype[4], show_name);
        }
        else
        {
            return urfp_console_put_string_with_name(env, name, urfp_put_utype[len-1], show_name);
        }
    case URFP_FLOAT:
        return urfp_console_put_string_with_name(env, name, "float", show_name);
    case URFP_DOUBLE:
        return urfp_console_put_string_with_name(env, name, "double", show_name);
    case URFP_ARRAY_START:
        if (len != -1)
        {
            char arrstring[14];
            arrstring[13] = 0;
            size_t i = urfp_anyntoa32(arrstring, 13, len, false);
            arrstring[--i] = '[';
            return urfp_console_put_string_with_name(env, name, arrstring + i, show_name);
        }
        return urfp_console_put_string_with_name(env, name, "[", show_name);

    case URFP_ARRAY_END:
        return urfp_console_put_string_with_name(env, NULL, "]", false);
    case URFP_STRUCT_START:
        return urfp_console_put_string_with_name(env, name, "{", show_name);
    case URFP_STRUCT_END:
        return urfp_console_put_string_with_name(env, NULL, "}", false);
    default:
        break;
    }

    return URFP_OK;
}

const urfp_transport_t urfp_console_transport =
{
    .get_string = urfp_console_get_string_arg,
    .get_bool = urfp_console_get_bool_arg,
    .get_int = urfp_console_get_int_arg,
    .get_unsigned = urfp_console_get_unsigned_arg,
    .get_uint64 = urfp_console_get_uint64_arg,
#ifndef URFP_WITHOUT_FLOAT
    .get_float = urfp_console_get_float_arg,
#endif
    .get_succeeded = urfp_console_get_succeeded,
    .get_array_start = urfp_console_get_array_start,
    .get_array_end = urfp_console_get_array_end,
    .get_struct_start = urfp_console_get_struct_start,
    .get_struct_end = urfp_console_get_struct_end,
    .get_blob_start = urfp_console_get_blob_start,
    .get_blob_access = urfp_console_get_blob_access,
    .get_blob_pause = urfp_console_get_blob_pause,
    .get_blob_end = urfp_console_get_blob_end,

    .get_end = urfp_console_get_end,

    .more_args_available = urfp_console_more_args_available,
    .get_arg_name = urfp_console_get_arg_name,
    .output_on_hold = urfp_console_output_on_hold,
    .describe_arg = urfp_console_describe_arg,

    .put_start = urfp_console_put_start,
    .put_string = urfp_console_put_string,
    .put_bool = urfp_console_put_bool,
    .put_int = urfp_console_put_int,
    .put_unsigned = urfp_console_put_unsigned,
    .put_uint64 = urfp_console_put_uint64,
#ifndef URFP_WITHOUT_FLOAT
    .put_float = urfp_console_put_float,
#endif
    .put_array_start = urfp_console_put_array_start,
    .put_array_end = urfp_console_put_array_end,
    .put_struct_start = urfp_console_put_struct_start,
    .put_struct_end = urfp_console_put_struct_end,
    .put_end = urfp_console_put_end,
};

/**
 * @param con    Pointer to urfp_console to initialize (allocated by you)
 * @param app    Pointer to application element lists and callbacks (provided by you)
 * @param prompt Prompt to be sent before expecting input. If NULL, UrFP will never
 *               emit a prompt nor rewrite/echo anything of your input.
 * @param inbuf  A buffer for input that you have to provide to UrFP
 * @param inbuf_size Number of bytes your inbuf can take
 * @param echo   Whether to echo input chars or rely on local echo on terminal side
 *
 */

urfp_retval_t urfp_console_init(urfp_console_t* con, urfp_app_t* app, const char* prompt, char* inbuf, size_t inbuf_size, bool echo)
{
    int i;

    con->last_char_in = 0;
    con->last_char_out = 0;
    con->last_input_tag = 0;
    con->last_oob_tag = 0;

    con->inbuf = inbuf;
    con->inbuf_size = inbuf_size;
    con->inbuf_length = 0;

    urfp_transaction_env_t* env;
    urfp_console_env_data_t* data;

    for (i=0; i<URFP_CONSOLE_MAX_REPLY_ENVS; i++)
    {
        env = &(con->transaction_env[i]);
        data = &(con->transaction_env_data[i]);

        memset(data, 0, sizeof(urfp_console_env_data_t));
        data->allocated = false;

        memset(env, 0, sizeof(urfp_transaction_env_t));
        env->app = app;
        env->transport = &urfp_console_transport;
        env->transport_conn = con;
        env->transport_data = data;
    }

    /* Last in list is dedicated for logging. Use env and data pointers that
       are still pointing to tpkt->transaction_*[URFP_TINYPACKET_MAX_REPLY_ENVS-1] */
    env->tag = 'A';
    env->code = URFP_CODE('l','g');
    data->allocated = true;
    data->keep_allocated = true;
    data->oob = true;
    data->prefix = '0';
    data->content_length  = 0;

    con->prompt = prompt;
    if (prompt)
    {
        con->prompt_length = strlen(prompt);
        con->want_prompt = true;
    }
    else
    {
        con->prompt_length = 0;
        con->want_prompt = false;
    }

    con->echo = echo;

    return URFP_OK;
}

static urfp_transaction_env_t* urfp_console_get_oob_env(void* arg, urfp_channel_t* channel)
{
    urfp_console_t* tpkt = (urfp_console_t*) arg;
    urfp_transaction_env_t* env = &(tpkt->transaction_env[URFP_CONSOLE_MAX_REPLY_ENVS-1]);
    env->channel = channel;
    urfp_console_determine_oob_tag(tpkt, &(env->tag));
    return env;
}

static void urfp_console_release_oob_env(void* arg, urfp_transaction_env_t* env)
{
    /* nothing to do */
}

void urfp_console_set_channel(urfp_console_t* con, urfp_channel_t* channel)
{
    for (int i=0; i<URFP_CONSOLE_MAX_REPLY_ENVS; i++)
    {
        con->transaction_env[i].channel = channel;
    }

    channel->get_env = urfp_console_get_oob_env;
    channel->get_env_arg = (void*)con;
    channel->release_env = urfp_console_release_oob_env;
}


/**
@}
*/
