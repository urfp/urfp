/* URFP - UR Function Protocol

Copyright (c) 2024 Kolja Waschk

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

/**
@ingroup urfp_channel
@{
*/

#include "urfp_channel.h"
#include "urfp_server.h"
#include "urfp_api.h"

/** Helper functions to be used by urfp_app instances that maintain a uint32_t bitset about enabled sources */

void urfp_channel_update_source_list(urfp_channel_t* channel, const urfp_elem_list_t base)
{
    int source_index = 0;

    int i = 0;
    urfp_elem_list_t list = (urfp_elem_list_t) base;
    const urfp_elem_descriptor_t* elem;

    if (!channel->app || !channel->app->next_elem_from_list)
    {
        return; /* assert? */
    }

    urfp_next_elem_from_list_getter_t next_elem = channel->app->next_elem_from_list;
    urfp_retval_t re = next_elem(&list, &i, &elem);
    while (re == URFP_OK)
    {
        channel->source_state[source_index].elem = elem;
        channel->source_state[source_index].read_index = 0;
        ++ source_index;

        re = next_elem(&list, &i, &elem);
    }
    channel->source = list;
    channel->num_sources = source_index;
}


void urfp_channel_init(urfp_channel_t* channel, const urfp_elem_list_t list)
{
    urfp_channel_update_source_list(channel, list);
    channel->enabled_sources = 0;
}

static int urfp_channel_find_elem(urfp_channel_t* channel, const urfp_elem_descriptor_t* source_elem)
{
    for (int i=0; i<channel->num_sources; ++i)
    {
        if (channel->source_state[i].elem == source_elem)
        {
            return i;
        }
    }

    return -1;
}

urfp_retval_t urfp_channel_source_index(urfp_transaction_env_t* env, const urfp_elem_descriptor_t* elem,
    uint32_t* begin, uint32_t* curr, uint32_t* end)
{
    urfp_retval_t r = elem->sourcefuncs.index(elem, begin, end);
    if (curr != NULL)
    {
        int source_index = urfp_channel_find_elem(env->channel, elem);
        if (source_index >= 0)
        {
            *curr = env->channel->source_state[source_index].read_index;
        }
    }

    return r;
}

urfp_retval_t urfp_channel_source_read(urfp_transaction_env_t* env, const urfp_elem_descriptor_t* elem, uint32_t index, int* maxcount)
{
    env->elem = elem;
    return elem->sourcefuncs.read(env, index, maxcount);
}

urfp_retval_t urfp_channel_enable_all_sources(urfp_channel_t* channel, bool enable)
{
    return URFP_OK;
}

urfp_retval_t urfp_channel_enable_source(urfp_channel_t* channel, const urfp_elem_descriptor_t* elem, bool enable)
{
    uint32_t prev_enabled = channel->enabled_sources;

    if (elem == NULL) /* all */
    {
        if (enable)
        {
            channel->enabled_sources = (1u<<(channel->num_sources))-1;
        }
        else
        {
            channel->enabled_sources = 0;
        }
    }
    else
    {
        int source_index = urfp_channel_find_elem(channel, elem);
        if (source_index >= 0)
        {
            if (enable)
            {
                channel->enabled_sources |= (1u<<source_index);
            }
            else
            {
                channel->enabled_sources &= ~(1u<<source_index);
            }
        }
    }

    if (enable)
    {
        uint32_t just_enabled = channel->enabled_sources & ~prev_enabled;
        if (just_enabled != 0)
        {
            for (int i=0; i<channel->num_sources; i++)
            {
                if ((just_enabled & (1u<<i)) != 0)
                {
                    uint32_t dummy;
                    /* Let read_index just behind sample that was recent before channel was enabled */
                    const urfp_elem_descriptor_t* elem = channel->source_state[i].elem;
                    elem->sourcefuncs.index(elem, &dummy, &(channel->source_state[i].read_index));
                }
            }
        }
    }

    return URFP_OK;
}

bool urfp_channel_source_enabled(urfp_channel_t* channel, const urfp_elem_descriptor_t* elem)
{
    int source_index = urfp_channel_find_elem(channel, elem);
    if (source_index >= 0)
    {
        return ((channel->enabled_sources & (1u<<source_index)) != 0);
    }
    return false;
}

urfp_retval_t urfp_channel_continue_channel(urfp_channel_t* channel)
{
    /* Iterate over all sources, starting where we left off at continuation.array.index */
    int source_index = 0;
    while (source_index < channel->num_sources)
    {
        if ((channel->enabled_sources & (1u<<source_index)) != 0)
        {
            uint32_t currindex;
            uint32_t endindex;
       
            uint32_t read_index = channel->source_state[source_index].read_index;
            const urfp_elem_descriptor_t* source_elem = channel->source_state[source_index].elem;


            urfp_retval_t r = URFP_OK;
            do
            {
                urfp_transaction_env_t* env = channel->get_env(channel->get_env_arg, channel);
                source_elem->sourcefuncs.index(source_elem, &currindex, &endindex);

                /* currindex: First available sample in buffer */
                /* endindex: Index after last available sample in buffer */
                /* read_index: Index after last sample that has been read */

                /* Maybe currindex is already ahead of read_index? */
                uint32_t delta = currindex - read_index;
                if (delta > 0 && delta < (1u<<30))
                {
                    /* not zero but also not "negative": currindex > read_index */
                    read_index = currindex;
                }

                /* Do not repeat data from indexes that has already been output */
                delta = endindex - read_index;
                if (delta > 0 && delta < (1u<<30))
                {
                    int maxcount = (int)delta;
                    urfp_put_int(env, sizeof(int32_t), "id", source_elem->id);
                    urfp_put_unsigned(env, sizeof(uint32_t), "first", read_index);
                    urfp_put_array_start(env, source_elem->name, -1);
               
                    /* maxcount will be set to the number of samples actually output. */
                    urfp_channel_source_read(env, source_elem, read_index, &maxcount);
            
                    read_index += maxcount;
            
                    urfp_put_array_end(env);
                    r = urfp_put_end(env, 0, NULL);
                }
                else
                {
                    read_index = endindex;
                }

                channel->release_env(channel->get_env_arg, env);
            }
            while (read_index < endindex && r == URFP_OK);

            channel->source_state[source_index].read_index = read_index;
        }

        ++ source_index;
    }


    if (source_index < channel->num_sources)
    {
        return URFP_AGAIN;
    }

    return URFP_OK;
}



/**
@}
*/
