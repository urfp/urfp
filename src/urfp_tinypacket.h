/* URFP - UR Function Protocol

Copyright (c) 2015 Kolja Waschk

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

/**
@addtogroup urfp_tpkt URFP tinypacket
@{
*/

#ifndef URFP_TINYPACKET_H
#define URFP_TINYPACKET_H 1

#include "urfp_server.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifndef URFP_TINYPACKET_MAX_CONTENT_LENGTH
#define URFP_TINYPACKET_MAX_CONTENT_LENGTH 128
#endif

typedef struct urfp_tinypacket_env_data urfp_tinypacket_env_data_t;
struct urfp_tinypacket_env_data
{
    bool locked;
    bool allocated;
    bool keep_allocated;

    /* related to input */
    uint8_t expected_prefix; // if set to some final prefix, input is complete

    /* related to output */
    bool oob;

    char content[URFP_TINYPACKET_MAX_CONTENT_LENGTH];
    size_t content_size; // sizeof(content)

    char *output;
    size_t output_length; /* Number of valid bytes at input */
    size_t output_size;   /* Maximum number of bytes at input */

    char *input;
    size_t input_length; /* Number of valid bytes at input */
    size_t input_size;   /* Maximum number of bytes at input */

    int arg_offset;

    bool first_packet; // true only until first packet has been successfully sent.

    uint8_t curr_prefix; // 0xf0, 0x00, 0x10, ... 0x70, 0x00, 0x10, ...
    uint8_t final_prefix; // if not 0, the output of content has been completed
};

#ifndef URFP_TINYPACKET_MAX_REPLY_ENVS
#define URFP_TINYPACKET_MAX_REPLY_ENVS 4
#endif

typedef struct urfp_tinypacket urfp_tinypacket_t;
struct urfp_tinypacket
{
    uint8_t* outbuf;
    int outbuf_size;

    char last_input_tag;
    char last_oob_tag;

    urfp_transaction_env_t transaction_env [URFP_TINYPACKET_MAX_REPLY_ENVS];
    urfp_tinypacket_env_data_t transaction_env_data[URFP_TINYPACKET_MAX_REPLY_ENVS];

    /* Your implementation of message transmission to the client */

    /**
     * An arbitrary pointer that will be passed to the callbacks when calling them
     */
    void* arg;

    urfp_retval_t (*output_packet)(void* arg, const uint8_t* msg, size_t len, bool urgent);
    urfp_retval_t (*wait)(void* arg, unsigned abs_timeout, unsigned* left_ms);
};

/** Feed new input packet into URFP machinery.  Should be called with 
    mode==URFP_ENV_REQ_OUT when typically replies and oob data are expected, or
    mode==URFP_ENV_REQ_IN if the other end may want request anything from URFP server.
    Returns URFP_OK if the packet was handled, URFP_AGAIN if it didn't fit into buffers and should be re-input later, URFP_ERR else.
    If it was handled and an environment exists for handling it, a pointer to it will be stored in *found_env (if not NULL) */
extern urfp_retval_t urfp_tinypacket_input(urfp_tinypacket_t* tpkt, urfp_envmode_t mode, const uint8_t* msg, int len, urfp_transaction_env_t** found_env);

/** Call this once to have the urfp_instance initialized and hand over pointers
 * to your callback functions, prompt and buffer for client input */
extern urfp_retval_t urfp_tinypacket_init(urfp_tinypacket_t* tpkt, urfp_app_t* app,
    uint8_t* inbuf, size_t inbuf_size, uint8_t* outbuf, size_t outbuf_size);

/** Work in progress: OOB output */
extern void urfp_tinypacket_set_channel(urfp_tinypacket_t* tpkt, urfp_channel_t* ch);

/** Work in progress: Make requests */
extern urfp_transaction_env_t* urfp_tinypacket_request_start(urfp_tinypacket_t* tpkt, unsigned code);
extern urfp_retval_t urfp_tinypacket_wait_for_response(urfp_tinypacket_t* tpkt, urfp_transaction_env_t* env, unsigned timeout_ms);
extern bool urfp_tinypacket_formatted_available(urfp_transaction_env_t *env, const char* fmt);
extern void urfp_tinypacket_get_catch_up(urfp_transaction_env_t* env);
extern void urfp_tinypacket_get_end(urfp_transaction_env_t* env);

extern void urfp_tinypacket_catch_up(urfp_tinypacket_env_data_t* data);
extern void urfp_tinypacket_free_env(urfp_tinypacket_t* tpkt, urfp_transaction_env_t* env);


/** Low level method for writing data to output without any packetizing */
extern urfp_retval_t urfp_tinypacket_append_output(urfp_transaction_env_t* env, uint8_t* src, size_t len);

/** Work in progress: continue backgrounded reply processing */
extern urfp_retval_t urfp_tinypacket_resume_query(urfp_tinypacket_t* tpkt);
extern urfp_retval_t urfp_tinypacket_resume_replies(urfp_tinypacket_t* tpkt);

extern const urfp_transport_t urfp_tinypacket_transport;

#ifdef __cplusplus
}
#endif

#endif

/**
@}
*/
