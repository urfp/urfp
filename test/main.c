/* URFP - UR Function Protocol

main.c: Prototypical I/O implementation and usage example

Copyright (c) 2015 Kolja Waschk

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <string.h>

#include <errno.h>
#include <pthread.h>
#include <semaphore.h>

#include "urfp_console.h"
#include "urfp_server.h"
#include "urfp_api.h"


volatile bool shutdown = false;
pthread_cond_t shutdown_cond;
pthread_mutex_t shutdown_access;

#define PROMPT "ml2d> "
#define PROMPT_SIZE 6
#define INBUF_SIZE 16
#define MAX_OUT_LINE_LEN 256
#define OUTPUT_SIZE (6*MAX_OUT_LINE_LEN+2+PROMPT_SIZE+2+INBUF_SIZE)

static enum { IDLE, BUSY } output_state;
static char output_buffer[2][OUTPUT_SIZE];
static int output_current;
static size_t output_pointer;

pthread_mutex_t inbuf_access;
pthread_mutex_t outbuf_access;

void request_exit(void* arg)
{
    pthread_mutex_lock(&shutdown_access);
    shutdown = true;
    pthread_cond_broadcast(&shutdown_cond);
    pthread_mutex_unlock(&shutdown_access);
}

void request_cancel(void *arg, char tag)
{
}

void flush_output(void)
{
    pthread_mutex_lock(&outbuf_access);

	if (output_state == BUSY)
	{
	    output_state = IDLE;
	}

	if (output_state == IDLE && output_pointer > 0)
	{
        write(STDOUT_FILENO, output_buffer[output_current], output_pointer);
        output_current = 1 - output_current;
        output_pointer = 0;
        output_state = BUSY;
	}

    pthread_mutex_unlock(&outbuf_access);
}

urfp_retval_t output_pretest(void* arg, size_t len)
{
    if (len > sizeof(output_buffer[0]))
    {
        return URFP_ERR;
    }
    if (len > sizeof(output_buffer[0]) - output_pointer)
    {
        return URFP_AGAIN;
    }
    return URFP_OK;
}

void output_char(void* arg, char c)
{
    pthread_mutex_lock(&outbuf_access);

	if (output_pointer < sizeof(output_buffer[0]))
	{
	    output_buffer[output_current][output_pointer] = c;
	    output_pointer ++;
	}

    pthread_mutex_unlock(&outbuf_access);
}

void output_string(void* arg, const char* str, size_t str_length)
{
    int i;
    pthread_mutex_lock(&outbuf_access);

    for (i=0; i<str_length && output_pointer<sizeof(output_buffer[0]); i++)
    {
	    output_buffer[output_current][output_pointer] = str[i];
	    output_pointer ++;
	}

    pthread_mutex_unlock(&outbuf_access);
}

void lock_inbuf(void* arg)
{
    pthread_mutex_lock(&inbuf_access);
}

void release_inbuf(void* arg)
{
    pthread_mutex_unlock(&inbuf_access);
}


urfp_retval_t show_args(urfp_reply_env_t* env)
{
    int r;
    char arg[16];

    urfp_reply_array_start(env, "argv", -1);
    while ((r = urfp_get_next_string_arg(env, "anyarg", arg, sizeof(arg))) >= 0)
    {
        const char name[] = "argv";
        urfp_reply_string(env, name, arg);
    }
    urfp_reply_array_conclude(env);

    urfp_reply_conclude(env, 0, NULL);

    return URFP_OK;
}

const urfp_elem_descriptor_t oob_demo_funcs[] =
{
    {
        .id=1,
        .name="show_args",
        .desc="Show all arguments passed to the function",
        .op=NULL,
        .funcfuncs = { show_args }
    },
    { 0 }
};

urfp_app_t oob_demo_urfp_app_t =
{
    .arg                  = NULL,
    .exit                 = request_exit,
    .cancel               = request_cancel,

    .variable             = NULL,
    .function             = oob_demo_funcs,
    .source               = NULL,
    .blob                 = NULL,
};

urfp_console_t oob_demo_urfp_console =
{
    .arg                  = NULL,
    .output_pretest       = output_pretest,
    .output_char          = output_char,
    .output_string        = output_string,
    .lock_inbuf           = lock_inbuf,
    .release_inbuf        = release_inbuf,
};

pthread_t oob_demo_thread;

void* oob_demo(void* arg)
{
    int retval = ETIMEDOUT;
    struct timespec timeout;
    urfp_console_t* con = (urfp_console_t*)arg;

    pthread_mutex_lock(&shutdown_access);
    while (!shutdown)
    {
        struct timeval now;
        pthread_mutex_unlock(&shutdown_access);

        if (retval == ETIMEDOUT)
        {
            urfp_reply_env_t* env = urfp_console_get_log_env(con);

            if (env != NULL && env->reply_continue == NULL)
            {
                env->transport->reply_start(env);
                urfp_reply_formatted(env, "s", "log", "oob message");
                urfp_reply_backgrounded(env, urfp_reply_continue_end);

                gettimeofday(&now, NULL);
                timeout.tv_sec = now.tv_sec + 2;
                timeout.tv_nsec = now.tv_usec * 1000;
            }
        }

        pthread_mutex_lock(&shutdown_access);
        retval = pthread_cond_timedwait(&shutdown_cond, &shutdown_access, &timeout);
    }
    pthread_mutex_unlock(&shutdown_access);
    pthread_exit(0);
}

int main(int argc, char* argv[])
{
    fd_set readset;
    bool echo = true;
    struct timeval tv;
    struct termios ttystate, otermiosin;

    int i;
    for (i=1; i<argc; ++i)
    {
        if (!strcmp(argv[i],"noecho"))
        {
            echo = false;
            puts("no local echo\n");
        }
    }

    tcgetattr(STDIN_FILENO, &ttystate);
    otermiosin = ttystate;
    ttystate.c_lflag &= ~ICANON;
    if (echo) {
        /* UrFP does echo, terminal does not */
        ttystate.c_lflag &= ~ECHO;
    } else {
        /* Terminal shall echo */
    }
    ttystate.c_cc[VMIN] = 1;
    tcsetattr(STDIN_FILENO, TCSANOW, &ttystate);

    setbuf(stdout, NULL);

    pthread_mutex_init(&inbuf_access, NULL);
    pthread_mutex_init(&outbuf_access, NULL);
    pthread_cond_init(&shutdown_cond, NULL);
    pthread_mutex_init(&shutdown_access, NULL);

    char inbuf[INBUF_SIZE];
    output_current = 0;
    output_pointer = 0;
    urfp_console_init(&oob_demo_urfp_console, &oob_demo_urfp_app_t, "ml2d> ", inbuf, sizeof(inbuf), echo);

    pthread_create(&oob_demo_thread, NULL, oob_demo, &oob_demo_urfp_console);

    while (!shutdown)
    {
        tv.tv_sec = 0;
        tv.tv_usec = 500u*1000u;

        FD_ZERO(&readset);
        FD_SET(STDIN_FILENO, &readset);

        select(STDIN_FILENO+1, &readset, NULL, NULL, &tv);
        if(FD_ISSET(STDIN_FILENO, &readset))
        {
            char c;
            if (read(STDIN_FILENO, &c, 1) == 1)
            {
#if 0
                char msg[20];
                sprintf(msg, "got 0x%02x", c);
                output_log(msg);
#endif
                urfp_console_input_char(&oob_demo_urfp_console, c);
            }
            else
            {
                perror("stdin");
            }
        }

        urfp_console_resume_replies(&oob_demo_urfp_console);
        flush_output();
    }

    pthread_join(oob_demo_thread, NULL);

    tcsetattr(STDIN_FILENO, TCSANOW, &otermiosin);

    return 0;
}
