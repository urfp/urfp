/** URFP - Universal Remote Function Protocol

@file  cmd.c
@brief Elements to be tested for URFP server in testbench
@copyright 2018 Kolja Waschk

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#ifndef CMD_H
#define CMD_H 1

#include "urfp_api.h"

extern const urfp_elem_descriptor_t* cmd_variable[];
extern const urfp_elem_descriptor_t* cmd_function[];
extern const urfp_elem_descriptor_t* cmd_source[];
extern const urfp_elem_descriptor_t* cmd_channel[];
extern const urfp_elem_descriptor_t* cmd_blob[];

extern urfp_app_t cmd_app;

#endif

/**@}*/
