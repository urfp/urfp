/** URFP - Universal Remote Function Protocol

@file  cmd.c
@brief Elements to be tested for URFP server in testbench
@copyright 2018 Kolja Waschk

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

/* Includes ------------------------------------------------------------------*/

#include "cmd.h"

urfp_retval_t show_args(urfp_transaction_env_t* env)
{
    int r;
    char arg[16];

    urfp_put_array_start(env, "argv", -1);
    while ((r = urfp_get_next_string_arg(env, "anyarg", arg, sizeof(arg))) >= 0)
    {
        const char name[] = "argv";
        urfp_put_string(env, name, arg);
    }
    urfp_put_array_end(env);

    urfp_put_end(env, 0, NULL);

    return URFP_OK;
}

/* Variables */

const urfp_elem_descriptor_t* cmd_variable[] =
{
    NULL
};

/* Functions */

static const urfp_elem_descriptor_t cmd_function_show_args =
{
    .id=1,
    .name="show_args",
    .desc="Show all arguments passed to the function",
    .op=NULL,
    .funcfuncs = { show_args }
};

const urfp_elem_descriptor_t* cmd_function[] =
{
    &cmd_function_show_args,
    NULL
};

/* Data sources */

urfp_retval_t cmd_source_measure_index(const urfp_elem_descriptor_t* elem, uint32_t* begin, uint32_t* end)
{
    return URFP_OK;
}

urfp_retval_t cmd_source_measure_read(urfp_transaction_env_t* env, uint32_t currindex, int* maxcount)
{
    return URFP_OK;
}

static const urfp_elem_descriptor_t cmd_source_adc =
{
    .id = 322,
    .name = "measure",
    .desc = "test source",
    .sourcefuncs = { cmd_source_measure_read, cmd_source_measure_index }
};

const urfp_elem_descriptor_t* cmd_source[] =
{
    &cmd_source_adc,
    NULL
};

/* Channels */

const urfp_elem_descriptor_t* cmd_channel[] =
{
    NULL
};

/* Blobs */

const urfp_elem_descriptor_t* cmd_blob[] =
{
    NULL
};

urfp_app_t cmd_app =
{
    .arg = NULL,
    .exit = NULL,
    .cancel = NULL,

    .variable = &(cmd_variable[0]),
    .function = &(cmd_function[0]),
    .source = &(cmd_source[0]),
#if URFP_WITH_CHANNELS
    .channel = &(cmd_channel[0]),
#endif
    .blob = &(cmd_blob[0]),
};

/**@}*/
