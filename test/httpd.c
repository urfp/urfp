/** URFP - Universal Remote Function Protocol

@file  httpd.c
@brief Usage demo for URFP HTTP implementation (server)
@copyright 2015 Kolja Waschk

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

/** @ingroup urfp_httpd
@{
*/

#include <sys/socket.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <string.h>
#include <stdbool.h>

#include "urfp_http.h"

typedef struct
{
    int socket;
    struct sockaddr_in addr;
    int addrlen;
    bool http_1_1;
    int content_length;
}
conninfo_t;

void print_escaped(const char* lenfmt, void* data, int len)
{
    const int middle=50;
    printf(lenfmt, len);
    printf(" ");
    int i;
    for (i=0; i<len; i++)
    {
        if (i<middle || (i+middle>len))
        {
            unsigned char c = ((unsigned char*)data)[i];
            switch(c)
            {
            case 9: printf("\\t");   break;
            case 10: printf("\n  "); break;
            case 13: printf("\\r");  break;
            default:
                putchar((c<0x20 || c >=0x7f) ? '.':c);
                break;
            }
        }
        else if (i==middle && !(i+middle>len))
        {
            printf(" (...) ");
        }
    }
    printf("\n");
}

void handle_respreq_start(void* arg, void* start_line, int len)
{
    conninfo_t* conninfo = (conninfo_t*)arg;
    // method = ...
    // uri = ...
    conninfo->http_1_1 = (len >= 8 && strncasecmp(start_line + len - 8, "http/1.1", 8) == 0);
    conninfo->content_length = 0;
    print_escaped("START(%u)", start_line, len);
}

void handle_respreq_header(void* arg, void* header_line, int len)
{
    conninfo_t* conninfo = (conninfo_t*)arg;
    print_escaped("HEADER(%u)", header_line, len);
}

void handle_respreq_query(void* arg, void* name, int name_len, void *value, int value_len)
{
    conninfo_t* conninfo = (conninfo_t*)arg;
    print_escaped("QUERY name(%u)", name, name_len);
    print_escaped("QUERY value(%u)", value, value_len);
}

int handle_respreq_body(void* arg, void* data, int len)
{
    conninfo_t* conninfo = (conninfo_t*)arg;
    conninfo->content_length += len;
    print_escaped("BODY(%u)", data, len);

    return len;
}

void handle_respreq_end(void* arg)
{
    conninfo_t* conninfo = (conninfo_t*)arg;

    printf("Read %u body bytes\n", conninfo->content_length);

    const char http_1_0_response[] =
        "HTTP/1.0 200 OK\r\n"
        "Content-type: text/plain\r\n"
        "Content-length: 6\r\n"
        "Connection: close\r\n"
        "\r\n"
        "Body\r\n"
        ;

    const char http_1_1_response[] =
        "HTTP/1.1 200 OK\r\n"
        "Content-type: text/plain\r\n"
        "Transfer-encoding: chunked\r\n"
        "Connection: keep-alive\r\n"
        "\r\n"
        "4\r\n"
        "Body\r\n"
        "0\r\n"
        "\r\n"
        ;

    if (conninfo->http_1_1)
    {
        sendto(conninfo->socket, http_1_1_response, sizeof(http_1_1_response)-1, 0, (struct sockaddr*)&(conninfo->addr), conninfo->addrlen);
    }
    else
    {
        sendto(conninfo->socket, http_1_0_response, sizeof(http_1_0_response)-1, 0, (struct sockaddr*)&(conninfo->addr), conninfo->addrlen);
    }
}

static const urfp_http_handlers_t respreq_handlers =
{
    .start  = handle_respreq_start,
    .header = handle_respreq_header,
    .query  = handle_respreq_query,
    .body   = handle_respreq_body,
    .finish = handle_respreq_end,

#if URFP_HTTP_WITH_MULTIPART
    .parts  = NULL,
#endif
};

int main(int argc, char**argv)
{
   int listenfd;
   struct sockaddr_in servaddr;
   socklen_t clilen;
   pid_t     childpid;
   char mesg[1000];
    conninfo_t conninfo;


   listenfd=socket(AF_INET,SOCK_STREAM,0);

   int enable = 1;
   if (setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0)
       perror("setsockopt(SO_REUSEADDR) failed");

   bzero(&servaddr,sizeof(servaddr));
   servaddr.sin_family = AF_INET;
   servaddr.sin_addr.s_addr=htonl(INADDR_ANY);
   servaddr.sin_port=htons(8080);
   bind(listenfd,(struct sockaddr *)&servaddr,sizeof(servaddr));

   listen(listenfd,1024);

   for(;;)
   {
      conninfo.addrlen=sizeof(conninfo.addr);
      conninfo.socket = accept(listenfd,(struct sockaddr *)&conninfo.addr,&conninfo.addrlen);

      if ((childpid = fork()) == 0)
      {
         close (listenfd);

         urfp_http_state_t urfp_state;
         urfp_http_init(&urfp_state, &respreq_handlers, &conninfo);

         int n;
         do
         {
            n = recvfrom(conninfo.socket, mesg, sizeof(mesg), 0, (struct sockaddr *)&conninfo.addr, &conninfo.addrlen);
            if (n > 0)
            {
                urfp_http_parse(mesg, n, &urfp_state);
            }
         }
         while (n > 0);

         close(conninfo.socket);
         return 0;
      }

      close(conninfo.socket);
      waitpid(-1, NULL, WNOHANG);
   }
}

/**
@}
*/
