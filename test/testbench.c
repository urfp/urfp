/** URFP - Universal Remote Function Protocol

@file  testbench.c
@brief Initial testbench for URFP using embUnit
@copyright 2018 Kolja Waschk

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

/** @ingroup urfp_test
@{
*/

#include "testbench.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>

#include <textui/Outputter.h>
#include <textui/TextOutputter.h>
#include <textui/XMLOutputter.h>
#include <textui/CompilerOutputter.h>
#include <textui/TextUIRunner.h>

#include "urfp_api.h"
#include "urfp_server.h"
#include "cmd.h"
#include "local.h"
#include "test_frame.h"

/** Dummy suite for error handling in case of unusual command line arguments */ 

static void no_setup(void) {};
static void no_teardown(void) {};
static void no_valid_arg(void)
{
    TEST_ASSERT_MESSAGE(false, "Unknown suite");
}

TestRef no_tests(void)
{
    EMB_UNIT_TESTFIXTURES(fixtures)
    {
        new_TestFixture("no_valid_arg", no_valid_arg),
    };
    EMB_UNIT_TESTCALLER(NO, "NO", no_setup, no_teardown, fixtures);
    return (TestRef)&NO;
}

/* ----------------- */

static void setUp(void)
{
    /* Nothing to set up before tests with embUnit */
}

static void tearDown(void)
{
    /* Nothing to tear down after tests with embUnit */
}

static void test_proxy_initialization(void)
{
    local_t loc;
    urfp_retval_t r;

    r = local_init(&loc, &cmd_app, NULL, 0, NULL, 0);
    TEST_ASSERT_MESSAGE(r == URFP_OK, "Initialization of local transport failed");

    urfp_transaction_env_t* up = local_request_start(&loc, URFP_CODE('c','f'));
    TEST_ASSERT_MESSAGE(up != NULL, "Starting a local request failed");

    r = urfp_put_unsigned(up, /*szof=*/ 4, "id", /*id=*/ 1);
    TEST_ASSERT_MESSAGE(r == URFP_OK, "Putting the ID in the request failed");

    r = urfp_put_string(up, "arg1", "argument");
    TEST_ASSERT_MESSAGE(r == URFP_OK, "Putting the first arg in the request failed");

    r =  urfp_put_end(up, 0, NULL);
    TEST_ASSERT_MESSAGE(r == URFP_OK, "Finalizing the request failed");

    /* TODO: We, the requester, and the other side, the server, must NOT 
     *       act on the same FIFO in the env (or even better: NOT on the same
     *       env at all).
     */
}

TestRef proxy_tests(void)
{
    EMB_UNIT_TESTFIXTURES(fixtures)
    {
        new_TestFixture("proxy_initialization", test_proxy_initialization),
        /* ... */
    };

    EMB_UNIT_TESTCALLER(proxy, "proxy", setUp, tearDown, fixtures);
    return (TestRef)&proxy;
}

/* ----------------- */

static void test_parse_uint64(void)
{
    urfp_retval_t retval;
    uint64_t value;

    value = urfp_parse_uint64_arg("123", 100, 3, &retval);
    TEST_ASSERT(retval == URFP_OK);
    TEST_ASSERT_EQUAL_INT(123UL, value);

    value = urfp_parse_uint64_arg("18446744073709551615", 100, 20, &retval);
    TEST_ASSERT(retval == URFP_OK);
    TEST_ASSERT(value == 0xffffffffffffffffUL);
}

static void test_parse_unsigned(void)
{
    urfp_retval_t retval;
    uint32_t value;

    value = urfp_parse_unsigned_arg("0", 100, 3, &retval);
    TEST_ASSERT(retval == URFP_OK);
    TEST_ASSERT_EQUAL_INT(0, value);

    value = urfp_parse_unsigned_arg("123", 100, 3, &retval);
    TEST_ASSERT(retval == URFP_OK);
    TEST_ASSERT_EQUAL_INT(123UL, value);

    value = urfp_parse_unsigned_arg("4294967295", 100, 10, &retval);
    TEST_ASSERT(retval == URFP_OK);
    TEST_ASSERT(value == 0xfffffffful);

    value = urfp_parse_unsigned_arg("-2147483648", 100, 11, &retval);
    TEST_ASSERT(retval == URFP_RANGE);
}

static void test_parse_integer(void)
{
    urfp_retval_t retval;
    int32_t value;

    value = urfp_parse_int_arg("123", 100, 3, &retval);
    TEST_ASSERT(retval == URFP_OK);
    TEST_ASSERT_EQUAL_INT(123UL, value);

    // max positive (ok)
    value = urfp_parse_int_arg("2147483647", 100, 10, &retval);
    TEST_ASSERT(retval == URFP_OK);
    TEST_ASSERT(value == 2147483647L);

    // max negative (ok)
    value = urfp_parse_int_arg("-2147483648", 100, 11, &retval);
    TEST_ASSERT(retval == URFP_OK);
    TEST_ASSERT_EQUAL_INT(value, -2147483648L);

    // too large:
    value = urfp_parse_int_arg("2147483648", 100, 10, &retval);
    TEST_ASSERT(retval == URFP_RANGE);

    // too small:
    value = urfp_parse_int_arg("-2147483649", 100, 11, &retval);
    TEST_ASSERT(retval == URFP_RANGE);
}
   
TestRef arg_parser_tests(void)
{
    EMB_UNIT_TESTFIXTURES(fixtures)
    {
        new_TestFixture("parse_uint64", test_parse_uint64),
        new_TestFixture("parse_unsigned", test_parse_unsigned),
        new_TestFixture("parse_integer", test_parse_integer),
        /* ... */
    };

    EMB_UNIT_TESTCALLER(proxy, "arg_parser", setUp, tearDown, fixtures);
    return (TestRef)&proxy;
}

/* ----------------- */



/** main program for running the tests */

int main (int argc, char* argv[])
{
    int opt;

    const struct
    {
        const char* name;
        TestRef ref;
    }
    suites[] =
    {
        { "arg_parser", arg_parser_tests() },
        { "proxy", proxy_tests() },
        { "frame", test_frame_tests() },
    };

    TextUIRunner_setOutputter(TextOutputter_outputter());
    /* TextUIRunner_setOutputter(CompilerOutputter_outputter()); */

    while ((opt = getopt(argc, argv, "x")) != -1)
    {
        switch(opt)
        {
        case 'x':
        {
            TextUIRunner_setOutputter(XMLOutputter_outputter());
            break;
        }
        default:
        {
            fprintf(stderr, "Usage: %s [-h | [-r] [suites..] ]\n", argv[0]);
            fprintf(stderr, "Suites:\n");
            size_t j;
            for (j=0; j<sizeof(suites)/sizeof(suites[0]); j++)
            {
                fprintf(stderr, "  %s\n", suites[j].name);
            }
            exit(-1);
        }
        }
    }

    TextUIRunner_start();

    if (optind >= argc)
    {
        /* No suites named ... run all */
        size_t j;
        for (j=0; j<sizeof(suites)/sizeof(suites[0]); j++)
        {
            TextUIRunner_runTest(suites[j].ref);
        }
    }
    else for (; optind < argc; optind++)
    {
        bool known = false;
        size_t j;
        for (j=0; j<sizeof(suites)/sizeof(suites[0]); j++)
        {
            if (strcmp(argv[optind], suites[j].name) == 0)
            {
                TextUIRunner_runTest(suites[j].ref);
                known = true;
            }
        }
        if (!known)
        {
            TextUIRunner_runTest(no_tests());
        }
    }

    TextUIRunner_end();

    return 0;
}

/**
@}
*/
