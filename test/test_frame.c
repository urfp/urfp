/**
@file test_tpkt.c

@brief Code for testing tinypacket output

*/

#include "test_frame.h"
#include "urfp_api.h"
#include "urfp_channel.h"
#include "urfp_tinypacket.h"
#include <string.h>

#include "cmd.h"

#include <stdint.h>
#include <stdio.h>
#include "urfp_channel.h"
#include "urfp_tinypacket.h"

/* max payload per tinypacket. 8 can fit in CAN message */
#define UT_MAX_SIZE (2*sizeof(uint32_t))

/* converted into words, that is: */
#define UT_WORDS_PER_PACKET (1+(UT_MAX_SIZE-1)/sizeof(uint32_t))

typedef enum
{
    IDLE=0,  /* empty, unconfigured */
    WORK,    /* already has some words in it and header set up */
    READY,   /* should be sent ASAP but has space for more packets */
    FULL,    /* should be sent ASAP and there is no space for more */
    CHECKED, /* CRC has been filled in */
    RECEIVE, /* Prepared for receiving data, not available for output yet */
    TRANSFER,/* in transmission */
    FILLED   /* with received data */
}
framebuf_state_t;

typedef struct framebuf
{
    struct
    {
        void* hd;
        volatile uint32_t* data;
    }
    frame;
    unsigned numwords;
    framebuf_state_t state;
    uint32_t last_updated;
}
framebuf_t;

/* Double buffer for output frames */
/* Note: "WORDS" mean uint32 here */
#define TX_MAX_WORDS (1400/4)

/* The code is made for single- or double buffers. It is not sufficient
 * to increase this TX_NUM_BUFS if you need more...! */
#define TX_NUM_BUFS 2

#define FRAME_DATA_GAP (1u<<31)
#define FRAME_DATA_IDENT 0xBF

typedef struct frame_state
{
    int framebuf_no; // index of the buffer to be filled yet
    framebuf_t buf[TX_NUM_BUFS]; // buffer state details

    urfp_tinypacket_t urfp_tinypacket;
    urfp_channel_t channel;

    void* frame_ll_state; // low level transport state

    volatile uint8_t outbuf[UT_MAX_SIZE];

    uint8_t packetindex;
}
frame_state_t;

typedef struct
{
    /* frame_ll only uses one buffer for constructing the UDP payload, no double-buffering */
    volatile uint32_t frame[TX_MAX_WORDS] __attribute__((aligned(4)));
}
frame_ll_state_t;

static frame_ll_state_t frame_ll_state;
static frame_state_t frame_state;

/*
 * frame_space() tells you how much space [number of 32 bit words] there is
 * left in the frame currently being filled or -1 if there is nothing to fill
 */

int frame_space(framebuf_t* curr)
{
    framebuf_state_t state = curr->state;
    if (state == IDLE)
    {
        return TX_MAX_WORDS;
    }
    else if (state == WORK || state == READY)
    {
        return TX_MAX_WORDS - curr->numwords;
    }
    return -1;
}

/*
 * frame_output_words()
 */
urfp_retval_t frame_output_words(framebuf_t* curr, const uint8_t* msg, size_t n1, const uint8_t* data, size_t n2, bool urgent)
{
    framebuf_state_t state = curr->state;
    if ((state != IDLE && state != WORK && state != READY) || (frame_space(curr) < n1 + n2))
    {
        return URFP_AGAIN;
    }
    
    unsigned nw = curr->numwords;

    if (state == IDLE)
    {
        nw = 0;
        curr->state = WORK;
    }

    if (n1 != 0)
    {
        memcpy((uint8_t*)&(curr->frame.data[nw]), msg, n1*sizeof(uint32_t));
        nw += n1;
    }

    if (n2 != 0)
    {
        memcpy((uint8_t*)(&curr->frame.data[nw]), data, n2*sizeof(uint32_t));
        nw += n2;
    }

    curr->numwords = nw;

    if (nw + UT_WORDS_PER_PACKET > TX_MAX_WORDS)
    {
        curr->state = FULL;
    }

    if (urgent)
    {
        curr->state = READY;
    }
    
    return URFP_OK;
}

/* Output frame if it's full or already lying around for too long
 */
void frame_output_if_appropriate(frame_state_t* frame_state)
{
    framebuf_t* curr;
    curr = (framebuf_t*) &(frame_state->buf[0]);

    if (curr->state == FULL)
    {
        curr->state = CHECKED;
    }

#if 0
    if (curr->state == READY)
    {
#if 0
        uint32_t age = sysdep_milliseconds() - curr->last_updated;
        if (age > 500)
#endif
        {
            curr->state = CHECKED;
        }
    }
#endif

    frame_ll_state_t* frame_ll_state = (frame_ll_state_t*)(frame_state->frame_ll_state);

    if (curr->state == CHECKED)
    {
        for (int i=0; i<curr->numwords; i+=2)
        {
#if 0
            uint8_t* ptr = (uint8_t*)&(curr->frame.data[i]);
            fprintf(stderr, "%04x: %02x %02x %02x %02x %02x %02x %02x %02x\n",
                (unsigned) (i*sizeof(uint32_t)), ptr[0], ptr[1], ptr[2], ptr[3], ptr[4], ptr[5], ptr[6], ptr[7]);
#endif
        }
        // fprintf(stderr, "Output frame: %d words (%lu byte)\n", curr->numwords, curr->numwords * sizeof(uint32_t));
    }
}

/*
   Main routine for doing output
 */
void frame_process(frame_state_t* frame_state)
{
    framebuf_t* curr;
    curr = (framebuf_t*) &(frame_state->buf[0]);

    urfp_retval_t r = URFP_AGAIN;

    int nout = frame_space(curr);
    int nout_before = 0;

    while (nout > 64 && nout_before != nout && r != URFP_OK)
    {
        nout_before = nout;
        r = urfp_tinypacket_resume_replies(&(frame_state->urfp_tinypacket));
        nout = frame_space(curr);

        if (nout <= 64 && curr->state == WORK)
        {
            curr->state = FULL;
        }

        frame_output_if_appropriate(frame_state);
        curr = (framebuf_t*) &(frame_state->buf[0]);
        nout = frame_space(curr);
    }

    frame_output_if_appropriate(frame_state);
}

static urfp_retval_t frame_ll_output(void* arg, const uint8_t* msg, size_t len, bool urgent)
{
    if (len > UT_MAX_SIZE)
    {
        return URFP_ERR;
    }

    frame_state_t* frame_state = (frame_state_t*)arg;
    framebuf_t* curr = &(frame_state->buf[0]);
    framebuf_state_t state = curr->state;

    urfp_retval_t r = URFP_AGAIN;

    if (state == CHECKED)
    {
        // this should not happen - obviously a previous
        // attempt to start its transmission failed

        frame_process(frame_state);
    }
    else
    {
        r = frame_output_words(curr, msg, UT_WORDS_PER_PACKET, NULL, 0, urgent);

        if (r == URFP_OK)
        {
            frame_process(frame_state);
        }
    }

    return r;
}

/********************* Allgemeinere Funktionen **************/

void test_frame_setup(void)
{
    memset(&frame_ll_state, 0, sizeof(frame_ll_state_t));
    memset(&frame_state, 0, sizeof(frame_state_t));

    frame_state.frame_ll_state = &frame_ll_state;
    frame_ll_state_t* ll_state = (frame_ll_state_t*)frame_state.frame_ll_state;

    /* frame_ll only uses one buffer for constructing the UDP payload, no double-buffering */
    memset((void*)(ll_state->frame), 0, TX_MAX_WORDS * sizeof(uint32_t));

    frame_state.buf[0].frame.hd =  NULL;
    frame_state.buf[0].frame.data = ll_state->frame;

    frame_state.urfp_tinypacket.output_packet = frame_ll_output;
    frame_state.urfp_tinypacket.arg = &frame_state;

    (void)urfp_tinypacket_init(&(frame_state.urfp_tinypacket), &cmd_app,
            NULL, 0, (uint8_t*)frame_state.outbuf, UT_MAX_SIZE);

    urfp_channel_init(&(frame_state.channel), cmd_source);
    //cmd_source_add_channel(&(frame_state.channel));
    urfp_tinypacket_set_channel(&(frame_state.urfp_tinypacket), &(frame_state.channel));
}

void test_frame_teardown(void)
{
}

static void frame_ll_receive(frame_state_t* frame_state, const void* buf, int len)
{
    /* Reply and all further output from now on go to the address of the requester */
    frame_ll_state_t* frame_ll_state = (frame_ll_state_t*)(frame_state->frame_ll_state);

    // Discard remainder bytes that can't be full tinypackets
    int plen = len - (len&7);

    int tries = 0;
    for (int i=0; i<plen; i += UT_MAX_SIZE)
    {
        const uint8_t* msg = (const uint8_t*)buf + i;
        urfp_transaction_env_t* env;
        urfp_retval_t r = urfp_tinypacket_input(&(frame_state->urfp_tinypacket), URFP_ENV_REQ_IN, msg, UT_MAX_SIZE, &env);
        if (r == URFP_AGAIN)
        {
            ++ tries;
            if (tries > 3)
            {
                break;
            }
            else
            {
                /* TODO: *ensure* that any data consumer takes a look at the new data. */
            }
        }
    }
    frame_process(frame_state);
}

static const uint8_t enable_source[] =
{
    0xf4, 0x31, 0x65, 0x73, 0x42, 0x01, 0x00, 0x00,  // es1 0x00000142 (datalen=4)
    0x80, 0x31, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00   //   1 (datalen=0)
};

static void request_enable_source(frame_state_t* frame_state)
{
    frame_ll_receive(frame_state, enable_source, sizeof(enable_source));
}

static void test_frame(void)
{
    const int elems_per_iteration = 10;
    urfp_channel_t* channel = &(frame_state.channel);

    request_enable_source(&frame_state);

    for (int i=0; i<1000; i++)
    {
        urfp_transaction_env_t* env = channel->get_env(channel->get_env_arg, channel);

        urfp_put_int(env, sizeof(int32_t), "id", 322);
        urfp_put_unsigned(env, sizeof(uint32_t), "first", i * elems_per_iteration);
        urfp_put_array_start(env, "measure", -1);

        for (int j=0; j<elems_per_iteration; j++)
        {
            urfp_put_array_start(env, "ch", 2);
            urfp_put_struct_start(env, "measure");
            urfp_put_unsigned(env, sizeof(uint16_t), "a", 0);
            urfp_put_unsigned(env, sizeof(uint16_t), "b", 0);
            urfp_put_unsigned(env, sizeof(uint16_t), "c", 0);
            urfp_put_struct_end(env);
            urfp_put_array_end(env);
        }
    
        urfp_put_array_end(env);
        urfp_put_end(env, 0, NULL);

        channel->release_env(channel->get_env_arg, env);
    }
}

TestRef test_frame_tests(void)
{
    EMB_UNIT_TESTFIXTURES(fixtures)
    {
        new_TestFixture("test_frame", test_frame),
        /* ... */
    };

    EMB_UNIT_TESTCALLER(frame, "frame", test_frame_setup, test_frame_teardown, fixtures);
    return (TestRef)&frame;
}

/**@}*/
