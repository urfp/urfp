#!/usr/bin/python3

import struct
import serial
import sys

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

class URFP_Transport:

    def logstr(self, s: str):
        raise NotImplementedError

    def set_var(self, vartype, varid, value):
        raise NotImplementedError

    def get_var(self, vartype, varid):
        raise NotImplementedError

class URFP_Tinypacket_Framer:

    minlen = 8
    maxlen = 8

    def receive_packet(self):
        raise NotImplementedError

    def send_packet(self, data: bytearray):
        raise NotImplementedError

class URFP_Ring(URFP_Tinypacket_Framer):

    crc16_ccitt_table = [ 
        0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50A5, 0x60C6, 0x70E7,
        0x8108, 0x9129, 0xA14A, 0xB16B, 0xC18C, 0xD1AD, 0xE1CE, 0xF1EF,
        0x1231, 0x0210, 0x3273, 0x2252, 0x52B5, 0x4294, 0x72F7, 0x62D6,
        0x9339, 0x8318, 0xB37B, 0xA35A, 0xD3BD, 0xC39C, 0xF3FF, 0xE3DE,
        0x2462, 0x3443, 0x0420, 0x1401, 0x64E6, 0x74C7, 0x44A4, 0x5485,
        0xA56A, 0xB54B, 0x8528, 0x9509, 0xE5EE, 0xF5CF, 0xC5AC, 0xD58D,
        0x3653, 0x2672, 0x1611, 0x0630, 0x76D7, 0x66F6, 0x5695, 0x46B4,
        0xB75B, 0xA77A, 0x9719, 0x8738, 0xF7DF, 0xE7FE, 0xD79D, 0xC7BC,
        0x48C4, 0x58E5, 0x6886, 0x78A7, 0x0840, 0x1861, 0x2802, 0x3823,
        0xC9CC, 0xD9ED, 0xE98E, 0xF9AF, 0x8948, 0x9969, 0xA90A, 0xB92B,
        0x5AF5, 0x4AD4, 0x7AB7, 0x6A96, 0x1A71, 0x0A50, 0x3A33, 0x2A12,
        0xDBFD, 0xCBDC, 0xFBBF, 0xEB9E, 0x9B79, 0x8B58, 0xBB3B, 0xAB1A,
        0x6CA6, 0x7C87, 0x4CE4, 0x5CC5, 0x2C22, 0x3C03, 0x0C60, 0x1C41,
        0xEDAE, 0xFD8F, 0xCDEC, 0xDDCD, 0xAD2A, 0xBD0B, 0x8D68, 0x9D49,
        0x7E97, 0x6EB6, 0x5ED5, 0x4EF4, 0x3E13, 0x2E32, 0x1E51, 0x0E70,
        0xFF9F, 0xEFBE, 0xDFDD, 0xCFFC, 0xBF1B, 0xAF3A, 0x9F59, 0x8F78,
        0x9188, 0x81A9, 0xB1CA, 0xA1EB, 0xD10C, 0xC12D, 0xF14E, 0xE16F,
        0x1080, 0x00A1, 0x30C2, 0x20E3, 0x5004, 0x4025, 0x7046, 0x6067,
        0x83B9, 0x9398, 0xA3FB, 0xB3DA, 0xC33D, 0xD31C, 0xE37F, 0xF35E,
        0x02B1, 0x1290, 0x22F3, 0x32D2, 0x4235, 0x5214, 0x6277, 0x7256,
        0xB5EA, 0xA5CB, 0x95A8, 0x8589, 0xF56E, 0xE54F, 0xD52C, 0xC50D,
        0x34E2, 0x24C3, 0x14A0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
        0xA7DB, 0xB7FA, 0x8799, 0x97B8, 0xE75F, 0xF77E, 0xC71D, 0xD73C,
        0x26D3, 0x36F2, 0x0691, 0x16B0, 0x6657, 0x7676, 0x4615, 0x5634,
        0xD94C, 0xC96D, 0xF90E, 0xE92F, 0x99C8, 0x89E9, 0xB98A, 0xA9AB,
        0x5844, 0x4865, 0x7806, 0x6827, 0x18C0, 0x08E1, 0x3882, 0x28A3,
        0xCB7D, 0xDB5C, 0xEB3F, 0xFB1E, 0x8BF9, 0x9BD8, 0xABBB, 0xBB9A,
        0x4A75, 0x5A54, 0x6A37, 0x7A16, 0x0AF1, 0x1AD0, 0x2AB3, 0x3A92,
        0xFD2E, 0xED0F, 0xDD6C, 0xCD4D, 0xBDAA, 0xAD8B, 0x9DE8, 0x8DC9, 
        0x7C26, 0x6C07, 0x5C64, 0x4C45, 0x3CA2, 0x2C83, 0x1CE0, 0x0CC1,
        0xEF1F, 0xFF3E, 0xCF5D, 0xDF7C, 0xAF9B, 0xBFBA, 0x8FD9, 0x9FF8,
        0x6E17, 0x7E36, 0x4E55, 0x5E74, 0x2E93, 0x3EB2, 0x0ED1, 0x1EF0
    ]

    def crc16_ccitt(self, data: bytes):
        '''
        CRC-16 (CCITT) implemented with a precomputed lookup table
        '''
        
        crc = 0 # 0xFFFF
        for byte in data:
            crc = (crc << 8) ^ self.crc16_ccitt_table[(crc >> 8) ^ byte]
            crc &= 0xFFFF                                   # important, crc must stay 16bits all the way through
        return crc

    def __init__(self, ser: serial.Serial, address=0):

        self.ser = ser
        self.address = address
        self.seq = 0
 
    def next_seq(self):

        self.seq = (self.seq + 1) & 7
        return self.seq

    def set_address(self, new_address):
        self.address = new_address

    def receive_packet(self):

        # In theory, other lengths than 12 are possible
        # In theory, there might be garbage or a few bytes sent long ago that we miss here

        s = self.ser.read(4+self.minlen) # or timeout.

        if len(s) < 4+self.minlen:
            eprint('Timeout, got only %d byte' % len(s))

        elif s[0] != self.address:
            eprint('Ignore packet to address %d' % s[0])
        
        elif self.crc16_ccitt(s) != 0:
            eprint('CRC error')

        else:
            # Ring framing
            sequence = (s[1] >> 5) & 7  # Sequence No, TODO: check?
            length   = s[1] & 31 # payload length
            data     = s[2:-2]   # payload

            if len(data) >= length:
                return data[:length]
            else:
                eprint('Actual length %d less than specified length %d' % (len(data), length))

    def send_packet(self, data: bytearray):

        if len(data) != 8:
            # TBD, unexpected
            return

        s = bytearray()

        # Ring Packet header (2 byte)
        # 0x80 in address to indicate response to master
        s.append(self.address + 0x80)
        s.append((self.next_seq()<<5) + len(data))

        # URFP Tinypacket
        s += data

        # CRC16 (final 2 byte, MSB first!)
        crc = self.crc16_ccitt(s)
        s.append((crc >> 8) & 0xff)
        s.append(crc & 0xff)

        self.ser.write(s)

class URFP_Tinypacket(URFP_Transport):

    def __init__(self, framer: URFP_Tinypacket_Framer):

        self.framer = framer

    def next_tag(self):

        # TODO: Increment etc.
        return '0'

    def receive(self, want_tag=None):

        tag = None
        code = None

        content = bytearray()
        expected_prefix = 0

        data = self.framer.receive_packet() # Timeout after 1s

        while data:
            new_prefix = (data[0] >> 4) & 15
            add_len = ((data[1] >> 3) & 16) + (data[0] & 15)
            new_tag = (data[1] & 127)

            if want_tag and new_tag != want_tag:
                eprint('Got packet with unwanted tag, ignored')

            elif not tag:
                if new_prefix >= 12: # Some first packet
                    tag = new_tag
                    code = data[2:4]
                    if add_len > 0:
                        content.extend(data[4:(4+add_len)])
                else:
                    eprint('Got some non-initial packet first, ignored')

            if tag:
                if new_tag != tag:
                    eprint('Got packet with wrong tag, ignored')
                else:
                    if new_prefix < 12: # Some further packet
                        content.extend(data[2:(2+add_len)])

                    if new_prefix == 15: # A first packet, to be continued
                        expected_prefix = 0

                    elif new_prefix == 8 or new_prefix == 12: # A final packet unless 15
                        return { 'tag':tag, 'code':code, 'content':content }

                    elif new_prefix > 8: # A final packet with error
                        eprint('Server reported error %d while processing' % (new_prefix & 3))
                        return None

                    elif new_prefix != expected_prefix:
                        eprint('Detected prefix gap, ignored all')
                        return None

            data = self.framer.receive_packet() # Timeout after 1s

    def send(self, code, fmt, *args, **kwargs):

        if 'tag' in kwargs.keys():
            tag = int(kwargs['tag'])
        else:
            tag = int(self.next_tag()) & 127

        if 'err' in kwargs.keys():
            err = int(kwargs['err'])
            if err < 0 or err>2:
                eprint('Invalid errcode %d to send' % err)
            return tag
        else:
            err = 0

        # "fmt" should use URFP type codes, not python pack codes
        encoded = bytearray()
        for i in range(len(fmt)):
            if fmt[i] == 's': # string, zero terminated
                encoded.extend(args[i])
                encoded.append(0)
            elif fmt[i] in 'bBiIhH':
                encoded.extend(struct.Struct('<'+str(fmt[i])).pack(int(args[i])))

        # First packet, maybe final packet

        if 4 + len(encoded) <= self.framer.maxlen:
            # encoded does fit in one single tinypacket, first is also final
            prefix = 12 + err
            thislen = len(encoded)
        else:
            # encoded doesn't fit in one single tinypacket, more will follow
            prefix = 15
            thislen = self.framer.maxlen - 4

        tpkt = bytearray()
        tpkt.append((prefix << 4) + (thislen & 15))
        tpkt.append(((thislen & 16)<<3) | tag)
        tpkt.append(code[0] & 127)
        tpkt.append(code[1] & 127)

        tpkt += encoded[:thislen]
        self.framer.send_packet(tpkt)

        # Further packets if necessary to send all

        prefix = 7 # start at 0 after increment
        while thislen < len(encoded):

            encoded = encoded[thislen:]

            if 2 + len(encoded) <= self.framer.maxlen:
                # final packet
                prefix = 8 + err
                thislen = len(encoded)
            else:
                prefix = (prefix + 1) & 7
                thislen = self.framer.maxlen - 2

            pkt = bytearray()
            tpkt.append((prefix << 4) + (thislen & 15))
            tpkt.append(((thislen & 16)<<3) | tag)

            tpkt += encoded[:thislen]
            self.framer.send_packet(tpkt)

        return tag

    def send_single_uint32(self, tag, code, value):

        self.send(code, 'I', value, tag=tag)


    def logstr(self, s: str):

        self.send(b'lg', 's', s)


    def set_var(self, vartype, varid, value):

        # I=uint32 i=int32 H=uint16 h=int16 B=uint8 b=int8 s=Cstring
        # Todo: Replace with put+put+put+end
        tag = self.send(b'sv', 'I'+vartype, varid, value)
        data = self.receive(tag)

    def get_var(self, vartype, varid):

        # I=uint32 i=int32 H=uint16 h=int16 B=uint8 b=int8 s=Cstring
        # Todo: Replace with put+put+end
        tag = self.send(b'gv', 'I', varid)
        data = self.receive(tag)
        # Todo: Replace with get+end
        if data:
            return struct.Struct('<'+vartype).unpack(data['content'])

class URFP_Client:
    def __init__(self, transport: URFP_Transport):
        self.transport = transport
        self._var = { }

    def add_var(self, varname: str, vartype: str, varid: int):
        self._var[varname] = { 'vartype':vartype, 'varid':varid }

    def get_var(self, varname):
        if varname in self._var:
            return self.transport.get_var(self._var[varname]['vartype'], self._var[varname]['varid'])

    def set_var(self, varname, value):
        if varname in self._var:
            return self.transport.set_var(self._var[varname]['vartype'], self._var[varname]['varid'], value)

class URFP_Server:
    # TODO: Generalize for arbitrary URFP_Transport
    def __init__(self, tpkt: URFP_Tinypacket):
        self.transport = transport

    def serve(self):
       while True:
           print ('---')
           transaction = transport.receive() # Timeout after 1s
           if transaction:
               # print(transaction)
               if transaction['code'] == b'gv':
                    # Todo: Implement and use transaction.get_xyz Methods instead of unpacking raw content
                   varid = struct.Struct('<I').unpack(transaction['content'])
                   tpkt.send_single_uint32(transaction['tag'], transaction['code'], 1234)

if __name__ == "__main__":

    with serial.Serial('/dev/ttyS0', 115200, timeout=1) as ser:

        if True:
            # Two clients for similar servers: Make requests, expect responses
            framer = []
            tpkt = []
            device = []

            for a in range(2):
                e = URFP_Ring(ser, address=a)
                framer.append(e)
                e = URFP_Tinypacket(e)
                tpkt.append(e)
                e = URFP_Client(e)
                device.append(e)
                e.add_var("example", "I", 482)

            # Copy a setting from device1 to device0
            value = device[1].get_var("example")
            device[0].set_var("example", 1234)

        else:
            # Ring Tinypacket Server: Wait for requests, act upon and make responses
            framer = URFP_Ring(ser)
            tpkt = URFP_Tinypacket(framer)
            server = URFP_Server(tpkt)
            server.serve()


