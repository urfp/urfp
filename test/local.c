/* URFP - Universal Remote Function Protocol

Copyright (c) 2015 Kolja Waschk

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#define NEED_NO_URFP_ALIASES
#include "local.h"
#include "urfp_server.h"

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

static void local_determine_tag(local_t* foo_bar, char* tag)
{
    *tag = foo_bar->last_input_tag + 1;
    if (*tag <= '0' || *tag >= '9')
    {
        *tag = '0';
    }
    foo_bar->last_input_tag = *tag;
}

#if 0
static void local_determine_oob_tag(local_t* foo_bar, char* tag)
{
    *tag = foo_bar->last_oob_tag + 1;
    if (*tag <= 'A' || *tag >= 'Z')
    {
        *tag = 'A';
    }
    foo_bar->last_oob_tag = *tag;
}
#endif

void local_init_data(local_t* foo_bar, local_transport_data_t* data)
{
    data->first = NULL;
    data->oob = false;
    data->complete =false;
    data->allocated = true;
}

urfp_retval_t local_resume_replies(local_t* foo_bar)
{
    int i;
    urfp_retval_t r = URFP_OK;

    for (i=0; i<LOCAL_MAX_ENVS && r == URFP_OK; i++)
    {
        local_transport_data_t* data = &(foo_bar->transaction_data[i]);
        if (data->allocated)
        {
            urfp_transaction_env_t* env = &(foo_bar->transaction_env[i]);

            if (env->put_continue != NULL)
            {
                r = (*(env->put_continue))(env);
            }

            if (r != URFP_OK)
            {
                return URFP_AGAIN;
            }
        }
    }

    return r;
}

void local_free_env(local_t* foo_bar, urfp_transaction_env_t* env)
{
#if 1 /* validate pointer */
    int i;
    for (i=0; i<LOCAL_MAX_ENVS; i++)
    {
        if (env == &(foo_bar->transaction_env[i]))
        {
            local_transport_data_t* data = &(foo_bar->transaction_data[i]);
            if (!data->keep_allocated)
            {
                data->allocated = false;
            }
        }
    }
#else
    ((local_transport_data_t*)env->transport_data)->allocated = false;
#endif
}

static urfp_transaction_env_t* local_alloc_env(local_t* foo_bar)
{
    int i;
    for (i=0; i<LOCAL_MAX_ENVS; i++)
    {
        local_transport_data_t* data = &(foo_bar->transaction_data[i]);
        if (!data->allocated)
        {
            local_init_data(foo_bar, data);
            return &(foo_bar->transaction_env[i]);
        }
    }

    return NULL;
}

#if 0
static urfp_transaction_env_t* local_find_env(local_t* foo_bar, urfp_envmode_t mode, unsigned code, char tag)
{
    int i;
    for (i=0; i<LOCAL_MAX_ENVS; i++)
    {
        local_transport_data_t* data = &(foo_bar->transaction_data[i]);
        if (data->allocated)
        {
            urfp_transaction_env_t* env = &(foo_bar->transaction_env[i]);

            if ((env->mode ==  mode)
                    && ((env->tag == tag) || (tag == 0))
                    && ((env->code == code) || (code == 0)))
            {
                return env;
            }
        }
    }

    return NULL;
}
#endif

static bool local_output_on_hold(urfp_transaction_env_t* env)
{
    local_transport_data_t* data = (local_transport_data_t*)(env->transport_data);

    return (data->first != NULL);
}

static urfp_retval_t local_append_elem(urfp_transaction_env_t* env, local_transport_elem_t* ne)
{
    local_transport_data_t* data = (local_transport_data_t*)(env->transport_data);

    ne->next = NULL;

    local_transport_elem_t** eptr = &(data->first);
    while (*eptr != NULL)
    {
        eptr = &((*eptr)->next);
    }
    *eptr = ne;

    return URFP_OK;
}

urfp_retval_t local_put_string(urfp_transaction_env_t* env, const char* name, const char* string)
{
    local_transport_elem_t* ne = malloc(sizeof(local_transport_elem_t));

    ne->name = strdup(name);
    ne->type = URFP_STRING;
    ne->value.str = strdup(string);

    return local_append_elem(env, ne);
}

urfp_retval_t local_put_bool(urfp_transaction_env_t* env, const char* name, bool value)
{
    local_transport_elem_t* ne = malloc(sizeof(local_transport_elem_t));

    ne->name = strdup(name);
    ne->type = URFP_BOOL;
    ne->value.yes = value;

    return local_append_elem(env, ne);
}

urfp_retval_t local_put_int(urfp_transaction_env_t* env, size_t szof, const char* name, int32_t value)
{
    local_transport_elem_t* ne = malloc(sizeof(local_transport_elem_t));

    ne->name = strdup(name);
    ne->type = URFP_INT;
    ne->szof = szof;
    switch(szof)
    {
    case 1: ne->value.i8 = value; break;
    case 2: ne->value.i16 = value; break;
    case 4: ne->value.i32 = value; break;
    default:
        break;
    }

    return local_append_elem(env, ne);
}

urfp_retval_t local_put_unsigned(urfp_transaction_env_t* env, size_t szof, const char* name, uint32_t value)
{
    local_transport_elem_t* ne = malloc(sizeof(local_transport_elem_t));

    ne->name = strdup(name);
    ne->type = URFP_UNSIGNED;
    ne->szof = szof;
    switch(szof)
    {
    case 1: ne->value.u8 = value; break;
    case 2: ne->value.u16 = value; break;
    case 4: ne->value.u32 = value; break;
    default:
        break;
    }

    return local_append_elem(env, ne);
}

urfp_retval_t local_put_uint64(urfp_transaction_env_t* env, const char* name, uint64_t value)
{
    local_transport_elem_t* ne = malloc(sizeof(local_transport_elem_t));

    ne->name = strdup(name);
    ne->type = URFP_UNSIGNED;
    ne->szof = 8;
    ne->value.u64 = value;

    return local_append_elem(env, ne);
}

urfp_retval_t local_put_float(urfp_transaction_env_t* env, size_t szof, const char* name, double value)
{
#if 0
    local_transport_elem_t* ne = malloc(sizeof(local_transport_elem_t));

    ne->name = strdup(name);
    ne->szof = 8;
    ne->type = URFP_DOUBLE;
    ne->value.dbl = value;

    return local_append_elem(env, ne);
#else
    return URFP_ERR;
#endif
}

urfp_retval_t local_put_array_start(urfp_transaction_env_t* env, const char* name, int maxcount)
{
    local_transport_elem_t* ne = malloc(sizeof(local_transport_elem_t));

    ne->name = strdup(name);
    ne->type = URFP_ARRAY_START;

    return local_append_elem(env, ne);
}

urfp_retval_t local_put_array_end(urfp_transaction_env_t* env)
{
    local_transport_elem_t* ne = malloc(sizeof(local_transport_elem_t));

    ne->type = URFP_ARRAY_END;

    return local_append_elem(env, ne);
}

urfp_retval_t local_put_struct_start(urfp_transaction_env_t* env, const char* name)
{
    local_transport_elem_t* ne = malloc(sizeof(local_transport_elem_t));

    ne->name = strdup(name);
    ne->type = URFP_STRUCT_START;

    return local_append_elem(env, ne);
}

urfp_retval_t local_put_struct_end(urfp_transaction_env_t* env)
{
    local_transport_elem_t* ne = malloc(sizeof(local_transport_elem_t));

    ne->type = URFP_STRUCT_END;

    return local_append_elem(env, ne);
}


urfp_retval_t local_put_start(urfp_transaction_env_t* env)
{
    local_t* foo_bar = (local_t*)(env->transport_conn);
    local_transport_data_t* data = (local_transport_data_t*)(env->transport_data);

    local_init_data(foo_bar, data);

    return URFP_OK;
}

urfp_retval_t local_put_end(urfp_transaction_env_t* env, int error_code, char* error_text)
{
    local_t* foo_bar = (local_t*)(env->transport_conn);
#if 0
    local_transport_data_t* data = (local_transport_data_t*)(env->transport_data);
#endif

    if (env->mode == URFP_ENV_REQ_OUT)
    {
        env->mode = URFP_ENV_REQ_IN;
        urfp_dispatch_query(env, foo_bar->app, env->code, env->tag);
        env->mode = URFP_ENV_REQ_OUT;
    }

    return URFP_OK;
}

urfp_retval_t local_describe_arg(urfp_transaction_env_t* env, const char* name, urfp_argtype_t type, int len)
{
    char t = 0;

    /* work in progress! see urfp_put_formatted for types.. */
    switch(type)
    {

    case URFP_BOOL:   t = 'o'; break;
    case URFP_STRING: t = 's'; break; // zero-terminated string
    case URFP_INT:
        switch(len)
        {
        case 1: t = 'b'; break;
        case 2: t = 'h'; break;
        case 4: t = 'i'; break;
        }
        break;
    case URFP_UNSIGNED:
        switch(len)
        {
        case 1: t = 'B'; break;
        case 2: t = 'H'; break;
        case 4: t = 'I'; break;
        }
        break;
    default:
        break;
    }

    return local_put_unsigned(env, 1, name, t);
}

static bool local_more_args_available(urfp_transaction_env_t *env)
{
    local_transport_data_t* data = (local_transport_data_t*)(env->transport_data);

    return (data->first != NULL);
}

bool local_formatted_available(urfp_transaction_env_t *env, const char* fmt)
{
    /*TODO!*/
    return local_more_args_available(env);
}

int local_get_arg_name(urfp_transaction_env_t* env, char* buf, size_t maxlen)
{
    local_transport_data_t* data = (local_transport_data_t*)(env->transport_data);

    int copied = 0;
    if ((data->first != NULL) && (buf != NULL))
    {
        const char* src = data->first->name;
        while (copied < maxlen && *src != 0)
        {
            buf[copied++] = *(src++);
        }
    }

    return copied;
}

uint32_t local_get_unsigned_arg(urfp_transaction_env_t* env, size_t szof, const char* name)
{
    local_transport_data_t* data = (local_transport_data_t*)(env->transport_data);

    uint32_t r = 0;
    local_transport_elem_t* e = data->first;
    if (e != NULL)
    {
        switch(szof)
        {
        case 1: r = e->value.u8; break;
        case 2: r = e->value.u16; break;
        case 4: r = e->value.u32; break;
        default:
            break;
        }
       
        data->first = e->next;
        free((void*)e->name);
        free(e);
    }

    return r;
}

int32_t local_get_int_arg(urfp_transaction_env_t* env, size_t szof, const char* name)
{
    local_transport_data_t* data = (local_transport_data_t*)(env->transport_data);

    uint32_t r = 0;
    local_transport_elem_t* e = data->first;
    if (e != NULL)
    {
        switch(szof)
        {
        case 1: r = e->value.i8; break;
        case 2: r = e->value.i16; break;
        case 4: r = e->value.i32; break;
        default:
            break;
        }
       
        data->first = e->next;
        free((void*)e->name);
        free(e);
    }

    return r;
}

uint64_t local_get_uint64_arg(urfp_transaction_env_t* env, const char* name)
{
    local_transport_data_t* data = (local_transport_data_t*)(env->transport_data);

    uint64_t r = 0;
    local_transport_elem_t* e = data->first;
    if (e != NULL)
    {
        r = e->value.u64;
       
        data->first = e->next;
        free((void*)e->name);
        free(e);
    }

    return r;
}

#ifndef URFP_WITHOUT_FLOAT
double local_get_float_arg(urfp_transaction_env_t* env, size_t szof, const char* name)
{
    return 0.0;
}
#endif

urfp_retval_t local_get_succeeded(urfp_transaction_env_t* env)
{
    return URFP_OK;
}

urfp_retval_t local_get_array_start(urfp_transaction_env_t* env, const char* name, size_t *maxcount)
{
    // todo: enter compound?
    return URFP_OK;
}

urfp_retval_t local_get_array_end(urfp_transaction_env_t* env)
{
    // todo: leave compound?
    return URFP_OK;
}

urfp_retval_t local_get_struct_start(urfp_transaction_env_t* env, const char* name)
{
    return local_get_array_start(env, name, 0);
}

urfp_retval_t local_get_struct_end(urfp_transaction_env_t* env)
{
    return local_get_array_end(env);
}


urfp_retval_t local_get_blob_start(urfp_transaction_env_t* env, const char* name, size_t* maxlength)
{
    return local_get_array_start(env, name, 0);
}

urfp_retval_t local_get_blob_access(urfp_transaction_env_t* env, void** data, size_t* length)
{
    // todo: access compound?
    return URFP_OK;
}

urfp_retval_t local_get_blob_pause(urfp_transaction_env_t* env)
{
    // todo: pause compound?
    return URFP_OK;
}

urfp_retval_t local_get_blob_end(urfp_transaction_env_t* env)
{
    return local_get_array_end(env);
}

#if 0
int local_get_int_array_arg(urfp_transaction_env_t* env, size_t szof, const char* name, void* buf, int nelems)
{
    local_transport_data_t* data = (local_transport_data_t*)(env->transport_data);

    int max_n = nelems;
    if (nelems < 0)
    {
        max_n = -nelems;
    }

    int n = 0;
    void *out = (void*)buf;
    while (n < max_n && data->first != NULL)
    {
        int32_t value = local_get_int_arg(env, szof, name);
        switch(szof)
        {
            case 1: *(int8_t*)out = value; break;
            case 2: *(int16_t*)out = value; break;
            case 4: *(int32_t*)out = value; break;
            default:
                break;
        }
        out += szof;
    }

    return n;
}

int local_get_unsigned_array_arg(urfp_transaction_env_t* env, size_t szof, const char* name, void* buf, int nelems)
{
    local_transport_data_t* data = (local_transport_data_t*)(env->transport_data);

    int max_n = nelems;
    if (nelems < 0)
    {
        max_n = -nelems;
    }

    int n = 0;
    void *out = (void*)buf;
    while (n < max_n && data->first != NULL)
    {
        int32_t value = local_get_unsigned_arg(env, szof, name);
        switch(szof)
        {
            case 1: *(uint8_t*)out = value; break;
            case 2: *(uint16_t*)out = value; break;
            case 4: *(uint32_t*)out = value; break;
            default:
                break;
        }
        out += szof;
    }

    return n;
}
#endif

bool local_get_bool_arg(urfp_transaction_env_t* env, const char* name)
{
    local_transport_data_t* data = (local_transport_data_t*)(env->transport_data);

    bool r = false;
    local_transport_elem_t* e = data->first;
    if (e != NULL)
    {
        r = e->value.yes;
        data->first = e->next;
        free((void*)e->name);
        free(e);
    }

    return r;
}

int local_get_string_arg(urfp_transaction_env_t* env, const char* name, char* buf, size_t maxlen)
{
    local_transport_data_t* data = (local_transport_data_t*)(env->transport_data);

    size_t n = 0;
    local_transport_elem_t* e = data->first;
    if (e != NULL)
    {
        while(n < maxlen && e->value.str && e->value.str[n] != 0)
        {
            buf[n] = e->value.str[n];
            n++;
        }
        buf[n] = 0;
        data->first = e->next;
        free((void*)e->name);
        free((void*)e->value.str);
        free(e);
    }

    return n;
}

void local_get_end(urfp_transaction_env_t* env)
{
    if (env->mode == URFP_ENV_REQ_OUT)
    {
        local_t* foo_bar = (local_t*)(env->transport_conn);
        local_free_env(foo_bar, env);
    }
}

const urfp_transport_t local_transport =
{
    .get_string = local_get_string_arg,
    .get_bool = local_get_bool_arg,
    .get_int = local_get_int_arg,
    .get_unsigned = local_get_unsigned_arg,
    .get_uint64 = local_get_uint64_arg,
    .get_float = local_get_float_arg,
    .get_succeeded = local_get_succeeded,
    .get_array_start = local_get_array_start,
    .get_array_end = local_get_array_end,
    .get_struct_start = local_get_struct_start,
    .get_struct_end = local_get_struct_end,
    .get_blob_start = local_get_blob_start,
    .get_blob_access = local_get_blob_access,
    .get_blob_pause = local_get_blob_pause,
    .get_blob_end = local_get_blob_end,
    .get_end = local_get_end,

    .more_args_available = local_more_args_available,
    .get_arg_name = local_get_arg_name,
    .output_on_hold = local_output_on_hold,
    .describe_arg = local_describe_arg,

    .put_start = local_put_start,
    .put_string = local_put_string,
    .put_bool = local_put_bool,
    .put_int = local_put_int,
    .put_unsigned = local_put_unsigned,
    .put_uint64 = local_put_uint64,
    .put_float = local_put_float,
    .put_array_start = local_put_array_start,
    .put_array_end = local_put_array_end,
    .put_struct_start = local_put_struct_start,
    .put_struct_end = local_put_struct_end,
    .put_end = local_put_end,
};

/**
 * @param con    Pointer to local_ to initialize (allocated by you)
 * @param hal    A local_ structure containing all the pointers to the functions
 *               you provide for communication etc.
 * @param arg    This pointer is passed as is to the callback functions in local_
 * @param prompt Prompt to be sent before expecting input. If NULL, UrFP will never
 *               emit a prompt nor rewrite/echo anything of your input.
 * @param inbuf  A buffer for input that you have to provide to UrFP
 * @param inbuf_size Number of bytes your inbuf can take
 * @param echo   Whether to echo input chars or rely on local echo on terminal side
 *
 */

urfp_retval_t local_init(local_t* foo_bar, urfp_app_t* app,
    uint8_t* inbuf, size_t inbuf_size, uint8_t* outbuf, size_t outbuf_size)
{
    int i;

    foo_bar->app = app;

    foo_bar->last_input_tag = 0;
    foo_bar->last_oob_tag = 0;

    /* Used for constructing content from single input packets */
    foo_bar->inbuf = inbuf;
    foo_bar->inbuf_size = inbuf_size;
    foo_bar->inbuf_length = 0;

    /* Used for constructing single packets from content for output */
    foo_bar->outbuf = outbuf;
    foo_bar->outbuf_size = outbuf_size;

    urfp_transaction_env_t* env;
    local_transport_data_t* data;

    for (i=0; i<LOCAL_MAX_ENVS; i++)
    {
        env = &(foo_bar->transaction_env[i]);
        data = &(foo_bar->transaction_data[i]);

        memset(data, 0, sizeof(local_transport_data_t));
        data->allocated = false;
        data->oob = false;

        memset(env, 0, sizeof(urfp_transaction_env_t));
        env->transport = &local_transport;
        env->transport_conn = foo_bar;
        env->transport_data = data;
    }

    /* Last in list is dedicated for logging */
    env->tag = 'A';
    env->code = URFP_CODE('l','g');
    local_init_data(foo_bar, data);
    data->allocated = true;
    data->oob = true;

    return URFP_OK;
}

urfp_transaction_env_t* local_get_log_env(local_t* foo_bar)
{
    return &(foo_bar->transaction_env[LOCAL_MAX_ENVS-1]);
}

urfp_transaction_env_t* local_request_start(local_t* foo_bar, unsigned code)
{
    urfp_transaction_env_t* env = local_alloc_env(foo_bar);
    if (env != NULL)
    {
        env->mode = URFP_ENV_REQ_OUT;
        env->code = code;
        local_determine_tag(foo_bar, &(env->tag));
    }

    return env;
}

