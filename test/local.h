/* URFP - UR Function Protocol

Copyright (c) 2015 Kolja Waschk

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#ifndef URFP_LOCAL_H
#define URFP_LOCAL_H 1

#include "urfp_types.h"
#include "urfp_server.h"

#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdarg.h>

#define LOCAL_MAX_CONTENT_LENGTH 128
typedef struct local_transport_elem
{
    urfp_argtype_t type;
    uint8_t szof;

    union
    {
        uint8_t u8;
        uint16_t u16;
        uint32_t u32;
        uint64_t u64;

        int8_t i8;
        int16_t i16;
        int32_t i32;

        const char* str;

        bool yes;
    }
    value;

    const char* name;

    struct local_transport_elem* next;
}
local_transport_elem_t;

typedef struct local_transport_data
{
    local_transport_elem_t* first;

    bool allocated;
    bool keep_allocated;

    bool oob;
    bool complete;
}
local_transport_data_t;

#define LOCAL_MAX_ENVS 4

typedef struct local_transport
{
    volatile int inbuf_length;
    uint8_t* inbuf;
    int inbuf_size;

    uint8_t* outbuf;
    int outbuf_size;

    unsigned code;
    uint8_t tag;

    int arg_offset;

    char last_input_tag;
    char last_oob_tag;

    urfp_transaction_env_t transaction_env [LOCAL_MAX_ENVS];
    local_transport_data_t transaction_data[LOCAL_MAX_ENVS];

    urfp_app_t* app;

    /* Your implementation of message transmission to the client */

    /**
     * An arbitrary pointer that will be passed to the callbacks when calling them
     */
    void* arg;

    urfp_retval_t (*output_packet)(void* arg, const uint8_t* msg, size_t len, bool urgent);
    urfp_retval_t (*receive_packet)(void* arg, uint8_t* msg, size_t len, unsigned timeout_ms);
}
local_t;

/** Call with new input packet */
extern urfp_transaction_env_t* local_input(local_t* tpkt, urfp_envmode_t mode, const uint8_t* msg, int len);

/** Call this once to have the urfp_instance initialized and hand over pointers
 * to your callback functions, prompt and buffer for client input */
extern urfp_retval_t local_init(local_t* tpkt, urfp_app_t* app,
    uint8_t* inbuf, size_t inbuf_size, uint8_t* outbuf, size_t outbuf_size);

/** Work in progress: debug output */
extern urfp_transaction_env_t* local_get_log_env(local_t* tpkt);

/** Work in progress: Make requests */
extern urfp_transaction_env_t* local_request_start(local_t* tpkt, unsigned code);
extern urfp_transaction_env_t* local_wait_for_response(local_t* tpkt, unsigned timeout_ms);
extern bool local_formatted_available(urfp_transaction_env_t *env, const char* fmt);
extern void local_get_catch_up(urfp_transaction_env_t* env);
extern void local_get_end(urfp_transaction_env_t* env);

extern void local_init_data(local_t* tpkt, local_transport_data_t* data);
extern void local_free_env(local_t* tpkt, urfp_transaction_env_t* env);


/** Work in progress: continue backgrounded reply processing */
extern urfp_retval_t local_resume_replies(local_t* tpkt);

extern const urfp_transport_t local_transport;


#endif
