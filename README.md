# Universal Remote Function Protocol

## Introduction {#urfp_intro}

Copyright (c) 2007..2024 Kolja Waschk

The up-to-date documentation can be accessed at https://urfp.gitlab.io/urfp/ - if you read this
rendered online from ``README.md`` inside repository, links may not work as expected..

For an in-depth explanation of all URFP, [continue reading about the project below](#urfp_intro_aim).

For the impatient, here are some links to important practical aspects:

 - How to provide variables, functions, blobs, data
   - with URFP server using the URFP API: [URFP_API](#urfp_api_intro)
 - How to access variables, functions, blobs
   - using [common methods of URFP Standard Server](#urfp_server_basics) (like ``gv``) 
   - via [HTTP](#urfp_proto_http) (best for commands and parameters)
   - via [UDP "tinypacket"](#urfp_proto_tinypacket) (best for getting realtime measurement data)
   - via [UART](#urfp_proto_console) console (for human control)
   - via [UART or CAN "tinypacket"](#urfp_proto_tinypacket) (for machine control)
   - utilizing [ready-made libraries](#urfp_client)
 - And more specifically, how to enable and process measurement data output:
   - [enable data source in server](#urfp_server_sources), to
   - receive data as ["tinypackets"](#urfp_proto_tinypacket)
   - utilizing [ready-made libraries](#urfp_client)

### Project aim {#urfp_intro_aim}

URFP defines communication between a server (a device such as an industrial
sensor) and a client (the user of the sensor, e.g. processing unit or
commissioning interface).

Simple low-bandwidth interfaces (UART, CAN) as well as fast network interfaces 
(HTTP, TCP, UDP on Ethernet) for larger amount of data can be used for the data
connection.

The communication mechanisms are designed for robust data exchange with error
detection from machine to machine using dedicated software. Some of the mechanisms
also can be easily controlled manually or with standard software, such as a terminal
programm or HTTP client.

Thanks to availability of an implementation in C (for server, thus devices, for
various interfaces types, hardware independent) and other languages (for clients
and gateways) and general client tools, using URFP can help to quickly bring up
a new device and communicate with it.

The implementation of device functionality on the one hand and access to it on
the other hand is cleany separated by URFP, so later transition to another
interface, adding a second interface or even gatewaying to another type of
interface becomes an easy task.

### Specification scope {#urfp_intro_spec}

URFP specifies a [communication protocol](#urfp_comm) for use with various
[various transport mechanisms](#urfp_comm_transports) with devices. It describes encoding
of basic data types and packaging them into messages.

Building on that, [URFP Standard Server](#urfp_comm_server) specifies a
concept for export of device functionality and data in a structured manner and
allows to develop generalized client software, for communication with any
device that exports its functionality based on that concept.

The [URFP API](#urfp_api_intro) implementation gives an idea how to use the different
transport mechanisms with generalized programming interface, especially when
developing device software.

### URFP devices {#urfp_intro_device}

Inside a device, URFP code arbitrates between the device external interfaces
and internal functionality.  On both sides, the URFP API helps to structure the
development.

 - Custom interface and low level drivers
   - ... connect to URFP Transport: HTTP, UART, Tinypacket 
   - ... used by URFP API: urfp_put, urfp_get
   - ... user by URFP Standard Server: urfp_dispatch_query
     - ...which uses a custom command list (links element IDs and names to matching command handlers)
     - ...and custom command handler (interpret arguments and call device functions)
     - ...that call custom device functions (such as switching a light on or off)

The open specification of course allows to implement a compatible communication by
other means. A rudimentary but still conformant implementation could be realized with
manageable effort in FPGA logic.

### URFP clients {#urfp_intro_client}

Client software can be developed completely without using code from the URFP
project, and devices could be even accessed without any special software:

 - UART ``sv 123 456``
 - REST ``http://device.ip/urfp?sv?id=123&value=456``

But the URFP project provides some code also for the client side.

 - Custom client
   - ...uses [URFP API](#urfp_api_intro): urfp_put, urfp_get
   - ...uses URFP Transport: HTTP, UART, Tinypacket etc.
       - ...uses custom interface and low level drivers

### URFP gateways {#urfp_intro_gateway}

The URFP code enables implementation of gateways that speak URFP on one interface and pass
all communication to another interface, to another device using URFP. The proxy code is
client and server at the same time. Such gateways enable communication with devices that
actually have just one interface, e.g. CAN, via another interface like Ethernet. Inside
larger systems, the proxy code can forward some requests to sub-components.

 - URFP transport: HTTP, UART, Tinypacket etc.
 - ...used by URFP API: urfp_put, urfp_get
 - ...glued with [URFP proxy](#urfp_proxy) code together with custom control code
 - ...to URFP API: urfp_get, urfp_proxy
 - ...that uses URFP transport: HTTP, UART, Tinypacket etc.

## Communication {#urfp_comm}

Basically, the communication is based on the exchange of messages in either direction,
 - consisting of serialized basic typed data (integral and float types, strings, arrays and structures thereof).
 - with a two-letter message code identifying a general kind of message
 - and held together by a common one-letter tag (if more than one transport unit is needed for the message).

These messages often represent [Requests](#urfp_comm_request) sent by a client
to a [Server](#urfp_comm_server) and corresponding responses. However, the
protocol also allows for emission of [out-of-band messages](#urfp_comm_oob)
and [continuous data output](#urfp_comm_data) from a server without prior request.

It is possible to interleave more than one message at once, e.g. immediately respond
to a request during transmission of a large amount of measurement data.

### Requests {#urfp_comm_request}

The server (device) always waits for requests from clients.

The two-letter code of the message tells the server what kind of action the
client expects (e.g. whether to change a parameter or execute some device function).

The data in the message represents the request arguments. Typically, the first 
argument is an integer, interpreted as an element ID. Handling the following data
then is dependent on the chosen element, implemented in dedicated handler code.

After reading all arguments and acting upon, the server responds with a message
containing results and indication of success or failure. This response comes with
the same code and tag as in the request.

### Standard server {#urfp_comm_server}

Based on the rather generic specification for transporting messages, URFP defines a 
way to use it for access to typical "elements" on a "server" (e.g. embedded device):

 - Variables (e.g. configuration parameters or dynamic state values)
 - Functions (device actions, e.g. reset to factory defaults) without or with simple or complex result data
 - Blobs (arbitrary binary objects, e.g firmware update)
 - Data sources (continuous data output, e.g. measurement data)

For this purpose, some codes are allocated for access to the elements (e.g. "cf" for "call
function", for retrieving lists of elements (e.g. "lf" for "list functions") and for getting
details about the elements (e.g. "hf" for "help about function"). The aim here is to
allow operation of a device with such standard server without prior detail knowledge
about the elements it provides.

### Out of band messaging {#urfp_comm_oob}

A server with URFP can emit any information without prior request ("out of
band"), such as state and log messages, in addition or interleaved with responses
to requests. The client can differentiate this messages from the responses with
the help of the message code and tag.

### Continuous data output {#urfp_comm_data}

A concept for requesting and configuring continuous data output, e.g. measurement data,
is planned. It can utilize [OOB messaging](#urfp_comm_oob) or just control transmission
of data with other mechanisms, even over separate interfaces.

## Supported transports {#urfp_comm_transports}

There are distinct specifications for various low level transports:

 - [Tinypacket (CAN, UDP)](#urfp_comm_tpkt)
 - [HTTP (TCP/IP)](#urfp_comm_http)
 - [Serial console (UART, TELNET)](#urfp_comm_con)

The projects provide implementations of some of the specifications in C and Python
with a generalized programming interface for servers as well as clients. On the
server side, the interface is designed so that transport can be easily swapped
with another and more than one transport may be supported for accessing the same
device functionality.

It is planned to also define better generalized programming interfaces for the
client side, as well a showcase implementation of a gateway that can arbitrate
between different transport mechanisms, for example to provide access to a CAN
server for a HTTP client.

Especially the Tinypacket and HTTP transports are well suited for machine-machine
communication.

### Tinypacket (CAN, UDP) {#urfp_comm_tpkt}

[Tinypacket](#urfp_proto_tinypacket): Requests are split into into small packets (each transporting 4-32 bytes of the
request), with data in a compact, binary representation. Both server and client use the same
packet format. When used with UDP, more than one Tinypacket may be transmitted
together in one UDP frame, to minimize overhead but still use the same tiny packet format. 

### HTTP (TCP/IP) {#urfp_comm_http}

[HTTP](#urfp_proto_http): Requests are sent to a HTTP server just like REST, using GET or POST methods. The server
responds with results as JSON data. This transport can be easily used with many existing HTTP
client libraries, command line tools like ``curl`` and of course common browser software
and web client software implemented in Javascript.

### Serial console (UART, TELNET) {#urfp_comm_con}

[Console](#urfp_proto_console): Messages are represented as lines of ASCII chars, data is transmitted
as text. The requests can be entered manually, with arguments separated by
spaces, concluded with CR or LF. The server responds with lines that are
machine parseable but also very well human readable and to used interactively.
The console can be used with terminal programs such as PuTTY, TeraTerm, picocom
or TCP/IP with TELNET protocol.



