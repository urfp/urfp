# Tinypacket {#urfp_proto_tinypacket}

For lowest level details, see [C code](@ref urfp_tinypacket.c)

Transmission of values in compact binary format.

Distribution of data into "Tinypackets", small chunks consisting of only a few bytes, e.g. 32 byte in CAN including header and CRC in each packet

Optimal for calling functions with only a few numeric arguments or setting and getting integral parameter values, but transmitting larger
amounts of data is also possible.

When low level transport can transmit larger amounts of data at once, multiple Tinypackets can be combined into one frame, saving
overhead for headers and checksums with each Tinypacket.

Also, other (not URFP) data could be integrated in such larger frames beside Tinypackets.

Existing implementations transport tinypackets in CAN messages, within
small checksum-protected chunks (32 byte) of data via USART ("Ring"), or grouped in UDP packets.

