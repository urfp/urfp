#!/usr/bin/perl

while(<>)
{
    if(/(section|subsection|subsubsection)\s+(\S+)\s+(.*)[\r\n]*$/)
    {
        if ($1 eq 'section') {
            print '#';
        } elsif ($1 eq 'subsection') {
            print '##';
        } elsif ($1 eq 'subsubsection') {
            print '###';
        }
        print " $3 {#$2}\n";
    }
    else
    {
        print $_;
    }
}
        
