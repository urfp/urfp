<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1438672667528" ID="ID_161831668" MODIFIED="1438672667528" TEXT="URFP">
<node CREATED="1438672667530" ID="ID_163121689" MODIFIED="1438672667530" POSITION="right" TEXT="Frame content">
<node CREATED="1438672667530" ID="ID_460784582" MODIFIED="1438672841239" TEXT="Zwei Zeichen Kommando/Antwortcode: argv[0]"/>
<node CREATED="1438672667530" ID="ID_626218618" MODIFIED="1438672848566" TEXT="Array Argumente bzw infos: argv[1], ..."/>
<node CREATED="1438672667530" ID="ID_611278111" MODIFIED="1438672830429" TEXT="Pr&#xfc;fsumme (nicht n&#xf6;tig bei Ethernet-&#xdc;bertragungen, aber RS232 etc.)"/>
<node CREATED="1438672667530" ID="ID_1885429758" MODIFIED="1438672811667" TEXT="Sequenznummer/Windowing/Wiederholungen (wie bei APROTO, nicht bei TCP)"/>
<node CREATED="1438672667530" ID="ID_110758313" MODIFIED="1438672893347" TEXT="Kontextnummer: &#xc4;ndert sich bei &#xc4;nderungen an Verf&#xfc;gbarkeit/IDs von Variablen/Funktionen etc."/>
<node CREATED="1438672667530" ID="ID_232132872" MODIFIED="1438672798538" TEXT="Content-Kennung: Gleichbleibend z.B. f&#xfc;r Zeilen, die prinzipiell zur selben Logmeldung geh&#xf6;ren"/>
</node>
<node CREATED="1438672667530" ID="ID_109431610" MODIFIED="1484034036930" POSITION="right" TEXT="Kommando-/Antwortcodes&#xa; do(what) with(what) ...">
<node CREATED="1438672667530" MODIFIED="1438672667530" TEXT="Lg log"/>
<node CREATED="1438672667530" ID="ID_1041360497" MODIFIED="1438672667530" TEXT="Functions">
<node CREATED="1438672667531" MODIFIED="1438672667531" TEXT="Hf help function"/>
<node CREATED="1438672667531" MODIFIED="1438672667531" TEXT="Lf list functions"/>
<node CREATED="1438672667531" ID="ID_472055053" MODIFIED="1438672667531" TEXT="Cf call function"/>
<node CREATED="1438673301789" ID="ID_750810754" MODIFIED="1438673306488" TEXT="Qf quick call function (no args)"/>
</node>
<node CREATED="1438672667531" ID="ID_624434408" MODIFIED="1438672667531" TEXT="Variables">
<node CREATED="1438672667531" ID="ID_124300409" MODIFIED="1438672667531" TEXT="Hv help variable"/>
<node CREATED="1438672667531" ID="ID_1969903196" MODIFIED="1438672667531" TEXT="Lv list variables"/>
<node CREATED="1438672667531" ID="ID_773448671" MODIFIED="1438672667531" TEXT="Sv set Variable"/>
<node CREATED="1438672667531" ID="ID_41738174" MODIFIED="1438672667531" TEXT="Rv read Variable"/>
<node CREATED="1438673313793" ID="ID_173638771" MODIFIED="1438673330686" TEXT="Qv Quick read Variable"/>
</node>
<node CREATED="1438672667531" ID="ID_1779180467" MODIFIED="1438673110753" TEXT="Sources">
<node CREATED="1438672667531" ID="ID_1262302128" MODIFIED="1438672667531" TEXT="Hs help (describe) Data source"/>
<node CREATED="1438672667531" MODIFIED="1438672667531" TEXT="Ls list data Source"/>
<node CREATED="1438672667531" ID="ID_379677514" MODIFIED="1438672667531" TEXT="Rs read datasource once"/>
<node CREATED="1438672667531" MODIFIED="1438672667531" TEXT="Es enable data source"/>
<node CREATED="1438672667531" ID="ID_248845966" MODIFIED="1438672667531" TEXT="Start Index?"/>
</node>
<node CREATED="1438672667531" ID="ID_1564561050" MODIFIED="1438673115759" TEXT="Dateien/Files/blobs">
<node CREATED="1438672667531" ID="ID_1511865285" MODIFIED="1438673041716" TEXT="cd Change dir"/>
<node CREATED="1438672667531" ID="ID_1627174480" MODIFIED="1438673050471" TEXT="ld List files"/>
<node CREATED="1438672667531" ID="ID_583329792" MODIFIED="1438673091859" TEXT="sd Send File"/>
<node CREATED="1438672667531" ID="ID_928767977" MODIFIED="1438673090288" TEXT="rd Read File"/>
<node CREATED="1438672667531" ID="ID_774667164" MODIFIED="1438673137185" TEXT="hd Help (describe) file"/>
</node>
<node CREATED="1438673266097" ID="ID_307425693" MODIFIED="1438673276933" TEXT="Aliases for commands">
<node CREATED="1438673539297" ID="ID_1021673955" MODIFIED="1438673541327" TEXT="List"/>
<node CREATED="1438673543183" ID="ID_1619420672" MODIFIED="1438673543771" TEXT="Add"/>
<node CREATED="1438673544014" ID="ID_875164462" MODIFIED="1438673546374" TEXT="Remove"/>
</node>
<node CREATED="1484034041739" ID="ID_430313701" MODIFIED="1484034096305" TEXT="Options for connection">
<node CREATED="1484034059898" ID="ID_1348471075" MODIFIED="1484034065780" TEXT="Radix for numbers"/>
<node CREATED="1484034066351" ID="ID_1383279295" MODIFIED="1484034071914" TEXT="Encoding (JSON, Base64)"/>
</node>
<node CREATED="1438673250234" ID="ID_1699113205" MODIFIED="1438673374847" TEXT="Automatic Repetition of Aliases, timed">
<node CREATED="1438673521381" ID="ID_1101860431" MODIFIED="1438673524125" TEXT="LIst"/>
<node CREATED="1438673507737" ID="ID_411780939" MODIFIED="1438673520795" TEXT="Config (interval &amp; count)"/>
<node CREATED="1438673524431" ID="ID_1965407351" MODIFIED="1438673527802" TEXT="Cancel"/>
</node>
<node CREATED="1438673346676" ID="ID_1963305683" MODIFIED="1438673348070" TEXT="Exit"/>
<node CREATED="1438673169044" ID="ID_1583069114" MODIFIED="1484034040740" TEXT="Security">
<node CREATED="1438673180754" ID="ID_845386419" MODIFIED="1438673186095" TEXT="Authentication"/>
<node CREATED="1438673186625" ID="ID_808654700" MODIFIED="1438673188035" TEXT="Encryption"/>
<node CREATED="1438673198378" ID="ID_920396091" MODIFIED="1438673207093" TEXT="Exclusive Interface Access"/>
</node>
</node>
<node CREATED="1438672667529" ID="ID_1513570940" MODIFIED="1438672667529" POSITION="left" TEXT="Inhalte">
<node CREATED="1438672667530" MODIFIED="1438672667530" TEXT="Funktionen"/>
<node CREATED="1438672667530" MODIFIED="1438672667530" TEXT="Variablen">
<node CREATED="1438672667530" MODIFIED="1438672667530" TEXT="Skalare"/>
<node CREATED="1438672667530" MODIFIED="1438672667530" TEXT="Arrays"/>
<node CREATED="1438672667530" MODIFIED="1438672667530" TEXT="Strukturen"/>
</node>
<node CREATED="1438672667530" ID="ID_644485002" MODIFIED="1506509964952" TEXT="Datenquellen">
<node CREATED="1438672984554" ID="ID_491423815" MODIFIED="1438672987375" TEXT="Skalare"/>
<node CREATED="1438672988069" ID="ID_229569494" MODIFIED="1438672989720" TEXT="Strukturen"/>
<node CREATED="1438672990101" ID="ID_1524260437" MODIFIED="1438673008024" TEXT="Verweise auf Daten aus anderen Quellen (Indices)"/>
</node>
<node CREATED="1438672667530" ID="ID_412329132" MODIFIED="1438672865758" TEXT="Files/Blobs"/>
<node CREATED="1438672667530" MODIFIED="1438672667530" TEXT="Items?"/>
</node>
<node CREATED="1438672667529" ID="ID_858475113" MODIFIED="1438672667529" POSITION="left" TEXT="Transport">
<node CREATED="1438672667529" ID="ID_612557099" MODIFIED="1438672667529" TEXT="Stream">
<node CREATED="1438672667529" ID="ID_1433373736" MODIFIED="1438672667529" TEXT="TELNET"/>
<node CREATED="1438672667529" MODIFIED="1438672667529" TEXT="TCP"/>
</node>
<node CREATED="1438672667529" ID="ID_1279935576" MODIFIED="1438672667529" TEXT="Framed">
<node CREATED="1438672667529" MODIFIED="1438672667529" TEXT="UDP"/>
<node CREATED="1438672667529" MODIFIED="1438672667529" TEXT="HTTP"/>
<node CREATED="1438672667529" ID="ID_1873462706" MODIFIED="1438672667529" TEXT="CR/LF terminated lines"/>
<node CREATED="1438672667529" ID="ID_1597451109" MODIFIED="1438672667529" TEXT="PFSDP"/>
<node CREATED="1438672667529" ID="ID_1658799661" MODIFIED="1438672667529" TEXT="APROTO"/>
<node CREATED="1506509972962" ID="ID_1613978817" MODIFIED="1506509973459" TEXT="CANopen"/>
<node CREATED="1506509974496" ID="ID_731376922" MODIFIED="1506509975143" TEXT="Kalibriereinrichtung Ring"/>
</node>
<node CREATED="1438672667529" ID="ID_685835840" MODIFIED="1438672667529" TEXT="Files">
<node CREATED="1438672667529" MODIFIED="1438672667529" TEXT="Xyz Modem"/>
<node CREATED="1438672667529" MODIFIED="1438672667529" TEXT="HTTP POST"/>
<node CREATED="1438672667529" MODIFIED="1438672667529" TEXT="Referenz/URI"/>
<node CREATED="1438672667529" MODIFIED="1438672667529" TEXT="Filesystem"/>
<node CREATED="1438672667529" MODIFIED="1438672667529" TEXT="Cached"/>
</node>
</node>
<node CREATED="1438672667532" ID="ID_1822453973" MODIFIED="1438672667532" POSITION="left" TEXT="Content encoding">
<node CREATED="1438672667532" ID="ID_347373474" MODIFIED="1438672667532" TEXT="Binary"/>
<node CREATED="1438672667532" MODIFIED="1438672667532" TEXT="Base64"/>
<node CREATED="1438672667532" ID="ID_642636007" MODIFIED="1438672667532" TEXT="Human readable ASCII">
<node CREATED="1438673224808" ID="ID_1468951669" MODIFIED="1438673226469" TEXT="Hex"/>
<node CREATED="1438673227210" ID="ID_944104688" MODIFIED="1438673231312" TEXT="Dez"/>
<node CREATED="1438673231773" ID="ID_830394473" MODIFIED="1438673232366" TEXT="..."/>
</node>
</node>
</node>
</map>
