README.md
    urfp_intro
        urfp_aim
        urfp_spec
        urfp_client
        urfp_embedded
        urfp_gateway
    urfp_comm
        urfp_comm_requests
        urfp_comm_server
        urfp_comm_http
        urfp_comm_tpkt
        urfp_comm_con
    urfp_transport
        urfp_transport_con
        urfp_transport_tpkt
        urfp_transport_http



== Intro

    CAN  UART  UDP  HTTP
       \__ \   / __/
          \ \ / /
            \ /
URFP Transport: Send and receive structures made of integral types as request, response or oob data
             |
URFP Server: Defined use transport for remote access to functions, parameters, (log) data sources, blobs
             |
C API: For use in devices, proxies and clients
C++ Python and Javascript APIs: For use in clients

== Basic (Low Level) Communication Concept

    Code, Tag, Content
    Request => Response or OOB Source or Log data: Either side may start communication
    Communication may occur interleaved
    Communication may be initiated on both sides (client can act as server, eg. for OOB, logging)
    Gatewaying / Proxying

== Server (High Level) Methods and Elements

    Variables, Functions, Sources, Blobs

    Continuation
    Backgroundfunktionen
    Grouping by ID
    Log-Datenquelle
    Liste, Hilfe, Typbeschreibungen

== Protocols

    HTTP
    Console
        TELNET
        Serial
    Tinypacket
        CAN
        UDP
        Serial

== Libs and APIs

C library
    get...get_end  put...put_end

Client API Recommendations

    Language priorities
        Python
        Javascript (for Web applications)
        C++

    Functional
        urfp_var_id
        urfp_var_int_get
        urfp_var_int_set
        urfp_var_name
        urfp_var_type
        urfp_var_description_get

    Object oriented
        t = transport(...)
        u = urfp(t)

        value = u.variables.variable_name
        u.variables.variable_name = value
            -or-
        value = u.variables.variable_name.get()
        u.variables.variable_name.set(value)
            -or-
        value = u.variables.get(variable_name)
        u.variables.set(variable_name, value)


== Tools

== Recipes







