move to public SVN, e.g. sourceforge? or git: github?

urfp_http:
  - add other *output* formats, such as
    - raw binary (like Tinypacket but contiguous)
    - same but Base64 encoded
    - same but Base64 encoded and split into strings in JSON array
  - process blob commands before accepting the blob data

blobs:
  - work out blob access etc.

datasources:
  - work out datasource access and output specification

general, ...:
  - better pretest/on_hold/cancelling of output *before* trying
  - find one method to ensure complete output and implement it 
    consistently instead of fragmented, incomplete workarounds...
  - define/enforce output limits "per function call" (how much output is allowed
    "per line" or "per resume" etc.)

authentification:
  - work out in-protocol authentification flow (with options
    for utilization of higher-layer mechanisms such as the
    existing HTTP digest authentication)


  

