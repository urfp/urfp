/** @page urfp Universal Remote Function Protocol (URFP)

Copyright (C) 2007..2022 Kolja Waschk 

@section urfp_intro Intro

URFP ("Universal Remote Function Protocol") is

 - a mechanism for reliably exchanging primitive data and data sets between two
        devices (numbers, strings, array and structure boundaries)
 - conventions how to group such data for accessing parameters, trigger functions, 
        stream data and exchange blobs with remote embedded devices
 - a C library for developing servers (devices), clients, and gateways.

The protocol can be used over various connections, from RS232
to TCP/IP streams, optionally utilizing other protocols like HTTP.

URFP servers can be chained, so a server 1 might provide basic functions to
server 2, which adds some more functions, and another server 3 that adds even
more functions, and all together could be used by a client (communicating with
server 3) like a single server with a real lot of functions.

A chain might as well implement a gateway from one lower level interface to
another and for instance provide HTTP access to a CAN device.

URFP allows to send more than one command at once, before the results are
expected; and URFP allows to pass data and debug information "in parallel" to
the command requests and responses.

The protocol core implementation could make use of SWIG for the description of
the extended API. In other words, it could be applied to any software package
that exports its API via SWIG, and on the client end be useful in any
programming language for which SWIG can generate code (with some restrictions
imposed on the language features used in the API)

@section urfp_layer URFP layers

@subsection urfp_layer_content Content sets

The content sets that can be exchanged are formally defined as 

 - \c code: a pair of ASCII chars, some kind of command code/mnemonic (e.g. 'cf' might mean 'call function').
 - \c tag: an one-digit tag can be appended to the code to ease discrimination of responses in async situations
 - \c args: followed by zero or more arguments, usually strings, possibly more efficiently encoded other data

This is application- and device-independent, a core part of URFP spec.

@subsection urfp_layer_core Core functionality

At least a basic number of command codes for content sets, possible arguments
and their meaning is fixed, such as 'cf' to call functions, 'he' to request
help etc.

This is application- and device-independent, a core part of URFP spec.

@subsection urfp_layer_transport Transport

The content sets can be delivered by various means. They can be encoded

 - as lines of ASCII chars, each ending with a line delimiter (CR/LF), somewhat human readable, for interactive use (console)
 - as binary packets with framing and checksums and eventually retransmission rules (over serial streams or using UDP)
 - as HTTP requests/responses so the protocol can be utilized with browsers/bookmarks/javascript

In addition, there should be methods to transmit files. Preferably standard mechanisms should be used such as 

 - X/Y/Z-modem to transfer files over serial lines
 - HTTP POST to upload files and HTTP GET to download files
 - ... or files have to be split into several content sets.

Depending on the application and available interfaces, new methods might be developed


@subsection urfp_layer_function Application functions

Whatever a 'cf' call triggers is purely application dependent. 

@section urfp_transport Transport details

@subsection urfp_transport_sercon Serial console

Serial console can be used over UART connections or within a 
TELNET session. It is meant to be operable by humans.

@subsubsection urfp_transport_sercon_syntax Basic syntax

The basic communication cycle when using serial console transport is as follows:

 1. The server emits a prompt (an almost arbitrary string, e.g. "urfp>"; it just must not begin with a digit or asterisk "*")
 2. The client sends a command, consisting of the following components:
    - Two letters specifying the command
    - An optional digit (0..5) defining a tag (defaults to 0 if omitted) which
      will be echoed in the response
    - Whitespace (ASCII 9,32,...) followed by a parameter for the command
      (Parameters may be numbers in a format understood by strtod, ASCII words,
      or UTF-8 strings enclosed in quotes).
    - Optionally a tilda "~" to allow deferral of responses (see "Asynchronous
      operation", below)
    - Optionally a dot "." followed by checksum for the line
    - LF, CR, or CR+LF 
 3. The server responds with zero, one or more lines, each following this format specification:
    - All but the last line start with a digit 0...9, incremented for each line
      (9 followed by 0); the last line instead starts with an asterisk "*"
    - The same two letters and tag as in the command from the client
    - Zero or more times a space (ASCII 32) followed by a result of the command
    - If the client sent a checksum, the server has to append a checksum too.
    - CR+LF 

Example:
    - Command: <tt> cf0 33 argum "string" ~*3FC2 </tt>
    - Response: <tt> \*cf0\*## </tt>

It depends on the particular command and function whether there is a response
at all, but this has to be specified for the particular application and may not
change dynamically (ie. a function may respond always or never, but not just
sometimes).

@subsubsection urfp_transport_sercon_errors Responses in case of an error

In case of an error that prevented the command from producing a proper
response, the server should emit a single line prefixed with a question mark
"?" or exclamation mark "!" instead of the response. The "?" should be used in
cases where retrying the query (with better arguments) would be reasonable
(possibly followed an explanation how to improve the query), the "!" should be
used when the server can't give any hints.

@subsubsection urftp_transport_sercon_editing Line editing

By default, the server echoes all input. Emacs-style Ctrl-*char* combinations
may be sent to edit the current line. The server will use spaces, Ctrl-M (CR)
and Ctrl-H (BS) codes to rewrite the current line. Line editing may not be
available in very simple servers. Telnet-style option processing may be used
for negotiation, eventually.

Whether a line from the server is just an echo of your input or output from the
server can be distinguished by looking at the first char after CR/LF: Output
from the server always begins with a digit or an asterisk. For a prompt
however, that's not allowed.

  - Ctrl-A: Beginning of the line
  - Ctrl-B: Move cursor to the left
  - Ctrl-C: Abort - see below
  - Ctrl-D: Delete char under cursor, or Exit - see below
  - Ctrl-F: Move cursor to the right
  - Ctrl-H or Del: Move cursor to the right, delete char under cursor
  - Ctrl-K: Delete to end of line
  - Ctrl-M (CR): Enter line / send command
  - Ctrl-U: Delete whole line 

@subsubsection urfp_transport_sercon_quits Cancelling and exiting

Ctrl-D on an empty line or an appropriate command will immediately terminate
the connection. There is no response. Ctrl-C should result in cancellation of
all currently executing commands. If there are no other commands running, it
should even remove all commands scheduled for automatic repetition. In other
words, sending Ctrl-C and then sending Ctrl-C again after a prompt has been
received will stop all activity on the server.

@subsubsection urfp_transport_sercon_timeout Timeouts

The server must respond to a command within 0,5 seconds. If the server knows
that more timeout is needed to compute a response to a command, it can send a
"timeout extension request" response which is similar to the lines of the
response, but prefixed with a "~". It may only occur before the first line of
the actual response.

@subsubsection urfp_transport_sercon_checksum Checksums

Normally, CRC16 should be used (represented as exactly 4 hex digits). For memory or performance reasons, CRC8 may be used instead (represented as a hash sign "#" followed by two hex digits), or, even if requested, no CRC at all (represented as two hash signs "##"). If either end knows that a given communication media is "error-free" (e.g. a TCP/IP connection), it is also wise to omit checksumming. Checksums are computed over all bytes in a line, including the leading digit or asterisk (in responses) and trailing asterisk before the checksum or even the trailing hash sign before an 8-bit checksum.

@subsubsection urfp_transport_sercon_files File transfer

Additionally, there are two mechanisms for transfer of large amounts of data
(such as FPGA configuration or firmware binaries): Data can either be split
into smaller chunks, so that every chunk fits into a command line on its own
(see "DA" command). Second, optionally, servers that have the memory and CPU
power should provide functions for transferring "files" using X/Y/ZModem
protocol. 

@subsection urfp_transport_http HTTP

 - TBD: 
   - Commands passed as Querystring, URI-encoded, separated by \c &
     - Querystring consists of code+tag=args pairs
     - Arguments separated by space
   - *or* only *one* command per request,
     - querystring contains arguments
     - may contain name=value pairs, not just strings
     - multiple commands could be done at once after function aliasing, as in serial console
 - Replies to multiple commands within one response body possible
 - Needs HTTP/1.1 support for Transfer-encoding: chunked for transmission of
   data without knowing its size in advance
 - Authentication as part of the transport
 - How to get OOB data over HTTP? "Attached" to responses to other requests?
   Requires polling. Requires identification of a connection so that any
   interested client gets all the OOB data. Or one client only and no one else.
 - Same client may open several connections at once, raising questions about
   order of processing the requests and/or emitting OOB data on the various
   connections
 - Response formatting/encoding may be determined by URL (path)

Examples for uploading a blob:

@verbatim
wget --post-file=$(APP_DATA_TMPFILE) "http://$(TARGET)/cmd/set_app_data" -O-
curl -vv --data "@$(APP_DATA_TMPFILE)" --header "Content-Type: application/binary" "http://$(TARGET)/cmd/set_app_data"
@endverbatim


@subsection urfp_transport_pfsdp PFSDP (Pepperl+Fuchs Simple Device Protocol)

 - Framing with basic header fields as in R2000 data output
 - Can also encapsulate commands and responses, see R2000 DSP2 UART communication







@section urfp_api Application Programming Interface

@subsection urfp_api_http_parser HTTP request and response parser

The HTTP request and response parser code in urfp_http.c can decode a stream of
contiguous requests (or responses, on the other side) and execute callback
functions right at the beginning (e.g. request line or status reponse
headline), for each header line, while receiving the body and when the request
or response has been received completely.

Its use is documented in \c urfp_http.h 

@subsection urfp_api_transport Transport API

(work in progress)

Server side (ie. device / sensor):
 - After a command line has been received, call <tt>cmd_handler(argc, argv[], arglen)</tt>
 - The handler can call functions to start / commence / finish response, then exit
 - The handler could as well exit and hand over control back to the server code by
   telling him a callback to call under certain circumstances - when there
   is more memory, more data, etc... until something happens

Client side (ie. PC / smartphone):
 - Send command
 - Receiving responses should be quite similar to command handling on the server side
  

TBD:
 - Files
   - Pass read() function pointer to transport, e.g. Special type of argument? E.g. arglen == -filesize ??
   - Send \b after exiting the cmd_handler function?
   - Cache for files?
   - Access to some kind of filesystem (e.g. with static HTML pages)?
 - Out of band / extra channels?
   - Late / asynchronous function results
   - Measurement results, data streams
   - Logging
 



@subsection urfp_api_types Data types

(work in progress, currently supported especially by URFP tinypacket)

== Type byte ==

An 8 bit type specifier tells about data type. Some values are
predefined and represent basic boolean, integer and float types.

@ 0100 0000 = struct start
A 0100 0001 = array start + length
B 0100 0010 = array end
C 0100 0011 = struct end

D 0100 0100 = bool     encoding is transport-specific (bit, int, string, ...)
E 0100 0100 = string   x byte c-string (UTF8, terminated by null-byte)
F 0100 0110 = float    4 byte
G 0100 0111 = double   8 byte

H 0100 1000 = 2^0 = 1 byte int
I 0100 1001 = 2^1 = 2 byte int
J 0100 1010 = 2^2 = 4 byte int
K 0100 1011 = 2^3 = 8 byte int

L 0100 1100 = 2^0 = 1 byte unsigned
M 0100 1101 = 2^1 = 2 byte unsigned
N 0100 1110 = 2^2 = 4 byte unsigned
O 0100 1111 = 2^3 = 8 byte unsigned
      
Others can be allocated on demand for device-specific integers
with scaling factors, units etc., or strings with specific encoding
or extra length

TBD:
- enum (with named values, either to be transported as int, or string)
- combined data (e.g. 20 bit + 12 bit within one 32 bit date)

== Information about element types/structures ==

When telling an other end about data type of output
or expected arguments, historically three schemes exist.

The newest scheme consists of a prefix byte followed
by information as defined above. The prefix byte tells

 - input (argument) or output (result) element
 - comes with or without element name
 - ...  TBD

The older scheme consists of a null-byte-terminated string of chars as follows,
which are no valid prefix bytes, and describe output elements:

 {,} struct start, end
 [   array start followed by 4 bytes int32 (array length -1 or > 0)
 ]   array end
 s   c-string
 o   bool (one byte zero or nonzero)
 I,H,B uint32, uint16, uint8
 i,h,b int32, int16, int8

The oldest scheme consists of only one single char, not even null-terminated,
as above, describing a scalar type soIHBihb.

*/
