# URFP Standard Server {#urfp_server_basics}

This document describes the "URFP standard server" from user (client) view.

For details about implementing the server side functionality see [URFP API](#urfp_api_intro),
for low level details about transmission of data have a look at the [URFP communication protocol](#urfp_proto).

For lowest level details, see [C code](@ref urfp_server.c)

## Elements {#urfp_server_basics_elem}

[Blobs](#urfp_server_blobs): for transmitting large amounts of (binary) data without knowledge abouts its contents (e.g. firmware updates or memory dumps)

[Channels](#urfp_server_channels): for control of contexts for data source output (to one or more distinct connections, if not inline to requester)

[Functions](#urfp_server_funcs): take arguments, can produce arbitrarily complex output containing data of various types, structure and amount of output may depend on state, may take some time to execute.

[Sources](#urfp_server_sources): functions for control of continous output of recurring data structures and access to selected data from it.

[Variables](#urfp_server_vars): restricted to one defined type of data. scalar or fixed size array.  Reading and writing variables should take no (noticeable) time and
normally should not have side effects (such as triggering some process)


## Methods (Codes) {#urfp_server_basics_methods}

A Code is built from two letters in `lower` case (e.g. ``gv`` to get a variable). The
following list uses upper case just for better readability.

The _second_ letter of the code typically selects the element type:

  - ``B`` [Blobs](#urfp_server_blobs)
  - ``C`` [Channels](#channels)
  - ``F`` [Functions](#functions)
  - ``S`` [Sources](#sources)
  - ``V`` [Variables](#variables)

The _first_ letter of the code describes the desired action.

  - ``L`` - [List](#urfp_server_basics_list): For listing and finding elements: Result are lists of element IDs and single word names, evtl filtered by a string pattern.
  - ``H`` - [Help](#urfp_server_basics_help): For getting more info about a particular element, e.g. short textual description, data type and structure of variables, function arguments and results.
  - Only with blobs:
    -  ``W`` - Write blob (upload)
    -  ``R`` - Read blob (download)
  - Only with functions:
    -  ``C`` - Call function
  - Only with sources:
    -  ``D`` - Disable source(s): disable output from this source(s)
    -  ``E`` - Enable source(s): enable continuous output from this source(s)
    -  ``R`` - Read source once: return one set of recent data from the source
  - Only with variables:
    - ``G`` - Get variable value
    - ``S`` - Set variable value
    - ``U`` - Update variable value (with offset to update arrays partially)

An upper case second element letter changes the command into a "channel" command
which expects a channel ID before any other arguments and targets the variables,
functions or sources of some channel other than the current one.

## List results {#urfp_server_basics_list}

The server returns a list of pairs of IDs and names of elements of the given type.

A primitive pattern can be passed as a string argument, either
 - just some string: list only elements whose names start with the string
 - an asterisk ``*`` followed by a string: list only elements whose names contain the string

## Help results {#urfp_server_basics_help}

A more detailed information about a single element, identified by its numeric ID as the
argument, is returned. The information consists of the following parts: 
 - A short description string (help text)
 - The data type(s) to be used with the element

For variables, the data type typically is just a single value (ie. string
"u32" on URFP console indicates an unsigned 32-bit integer variable) or array
with one member.

For sources, the data type specification often contains more than one element.
Sources can output structures with several values (even with different data types per value).

Help for functions additionally contains the definition of arguments that can be passed
to the function. 

The method for specification of the data type depends on the underlying transport.

# Functions {#urfp_server_funcs}

Functions: take arguments, can produce arbitrarily complex output containing data of various types, structure and amount of output may depend on state, may take some time to execute.

## Execution {#urfp_server_funcs_exec}

### Arguments {#urfp_server_funcs_exec_args}

The number and type of arguments expected by a function can be queried beforehand using the help method.

Order and completeness of arguments is important even though some transports allow to specify them with name.

Only trailing arguments might be optional, not first or middle arguments.

### Result {#urfp_server_funcs_exec_result}

When using transports where data does not implicitly contain the type information (e.g. tinypacket), the
results from functions must be parsed according to the type information from the help method.

# Variables {#urfp_server_vars}

Variables: restricted to one defined type of data. scalar or fixed size array.
Reading and writing variables should take no (noticeable) time and normally
should not have side effects (such as triggering some process)


## Read {#urfp_server_vars_read}

Reading a variable always returns its whole content. It is not yet possible to read a part
of array only.

## Write {#urfp_server_vars_write}

Writing to a variable always starts changing the content at the beginning (of arrays).
If only a few elements are written, a remaining array (typically) stays
unchanged, but there may be implementations where the tail is reset to some
default value.
To start somewhere in the middle of an array, use the [update](#urfp_server_vars_update) instead.

## Update {#urfp_server_vars_update}

Updates to a variable can be made at an arbitrary offset into an array, which has to specified as an
argument before all actual data. 
If only a few elements are written, a remaining array (typically) stays
unchanged, but there may be implementations where the tail is reset to some
default value.

# Data sources {#urfp_server_sources}

Data sources: functions for control of continous output of recurring data structures and access to selected data from it.

When enabled, a data source outputs an incrementing numeric index followed by
one or more data sets to some separate output channel, not inline (please have
a look at [Channels](#urfp_server_channels) for more info).

The index is incremented by one for each data set, which may be a single value
or a complex structure by itself, e.g.

 - measurement data (raw and processed values)
 - logging (timestamp, log message)
 - status information (ethernet link change etc.)

The output occurs without further request, just driven by the source of the data. 

Data from one source can be referenced by index in other sources. For example,
a data source presenting metadata about a group of measurements (samples) might
itself contain an item ``#sample`` that refers to the index of the first
measurement and another item ``@sample`` that tells how many measurements are
contained in the group. That does not imply some specific order: The output of the
referenced samples may occur before or after the output of the metadata.

## Enable {#urfp_server_sources_enable}

The enable method allows to enable one or more data sources by numeric ID. It will
return a list of IDs of all currently enabled source IDs. To just get a list, call
it without any arguments.

## Disable {#urfp_server_sources_disable}

The disable method allows to disable one or more data sources by numeric ID. It will
return a list of IDs of all currently enabled source IDs. To disable all at once, call
it without any arguments.

## Read {#urfp_server_sources_read}

The read method returns the most recent data set published from the specified data source
to the caller. This is a convenient method to get data directly, but it is not meant for
continuous polling of data.


# Blobs {#urfp_server_blobs}

Blobs: for transmitting large amounts of (binary) data without knowledge abouts its contents (e.g. firmware updates or memory dumps)


## Read {#urfp_server_blobs_read}

## Write {#urfp_server_blobs_write}

# Channels {#urfp_server_channels}

Channels: for control of contexts for data source output (to one or more distinct connections, if not inline to requester)


# Future development {#urfp_server_future}

Channels and Data source handling are undergoing the most development progress
and changes. Currently, only the tinypacket transport is well supported, and
access to channels other than the one in which a request is made is not well
implemented. So, in order to get data from data sources, a request to enable a
source has to me made using tinypacket and will enable data output to the
requester (asynchronously) using tinypacket only.

The next steps are
 - possibility to control output via tinypacket channel using HTTP requests
 - more effective (and gapless) polling from data sources with HTTP clients

