# URFP communication protocol {#urfp_proto}

This protocol is the *base* for communication with an URFP server.

For *practical* application and definition of semantics with elements like
variables please refer to the [URFP Standard Server](#urfp_server_basics)
documentation.

## Parts of a request {#urfp_proto_parts}

Code (two letters): identifies the type of request (e.g. set variable "sv" or call function "cf")

Tag (one char): identifies one request (to differentiate from others at the same time)

Arguments: numbers, strings and evtl. boundary identifiers for arrays and structures, represented in human readable form

Results: like arguments, just for the other end.

The server duplicates the tag and code received in a request in its response, to enable the client to
relate the response to its request. This allows to split a response e.g. into small packets and send them
interleaved with other data (out of band log messages, measurement data) to avoid having to delay a
response. 

The tag is the only way to relate further packets to the same transaction (if a
transaction is split into more than one packet, only the first also mentions
the code in addition to the tag). To avoid collisions, server and client should
pick tags (chars) from different ranges:

    - Server when responding to a request without tag: 0..9
    - Client when making a request: 0..9 or a..z (lower case)
    - Server when making OOB output: A..Z (upper case)

