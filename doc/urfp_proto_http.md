# HTTP {#urfp_proto_http}

## General

Usable with any HTTP client, e.g. browser or curl or requests libraries.

Responses come as JSON or, if requested so, as compact binary or binary encoded in several
lines of Base64 etc pp

GET for basic requests with only few arguments: URI = Code + Argumente in Querystring

POST for upload of blobs and requests with larger arguments such as CSV

Processing of multiple requests within persistent connection (HTTP 1.1 Connection: keep-alive),
output without Content-Length but Transfer-Encoding: chunked

The URFP C library contains quite a lot of code that assists server and client implementations
to parse HTTP requests and responses, including multipart decoders.

For lowest level details, see [C code](@ref urfp_http.c)

URFP C library does not provide a complete web server. The examples contain a simple one that
uses sockets. For use with lwIP, please contact author

## Examples

### Request

A request can be as short as this:

    GET /urfp/gv?id=1 HTTP/1.0

To set a variable, use

    GET /urfp/sv?id=1&variable_name=value HTTP/1.0

To set an array, construct one comma or semicolon or space (`%20`) separated list as the value:

    GET /urfp/sv?id=4&array_name=5,6,7,8 HTTP/1.0

Note: The name `variable_name` is not literally evaluated by current server
implementations, instead it is important to pass the elements in correct order
as with URFP console with arbitrary names before the equal sign `=`. However,
future implementations might also parse the arguments by name.

To call a function:

    GET /urfp/cf?id=1&arg1=1&arg2=test&arg3=more HTTP/1.0

Make sure to URI-encode special chars in the values. Strings should conform to
UTF-8, numbers should be made entirely of digits, an optional sign, and maybe a
decimal dot (if there is floating point support). Boolean values may be specified
as an unsigned number (only 0 is interpreted as false) or one of the strings

    on, yes, true, off, no, false

The server implementation might impose a limit on the length of the GET line,
typically 256 bytes.


### Response

Responses come with all information in body, formatted as JSON, and
contain one field named after the command code, e.g. "gv0", containing
at least an integer `error_code` (zero if all went well) and string
`error_text` (for humans, not to be evaluated by machines).

Note: In some future it might additionally contain more than just one command
result field, e.g. out-of-band and source data.

A minimal response without further result data might look like:

    HTTP/1.1 200 OK
    Content-Type: text/plain
    Content-Length: 50
    Connection: keep-alive

    {
    "sv0":{
    "error_code":0,
    "error_text":"" } }

A request for an array variable value could yield

    {
    "gv0":{
    "array_name":[
    201083,
    150930,
    100797,
    100826,
    100808
    ],
    "error_code":0,
    "error_text":"success" } }

Results from function calls might be more complex and even contain nested structures.

### POST with cURL

Set a large array (variable):

    curl -F array="201083 150930 100797 100826 100808 100744 100728 100456 100441" \
        "http://ADDR/urfp/sv?id=49"

Upload a binary file (blob):

    curl -F "image=@update.bin" \
        "http://ADDR/urfp/wb?id=289"

