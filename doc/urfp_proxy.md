# URFP Proxy {#urfp_proxy}

## Basics {#urfp_proxy_basics}

An URFP proxy provices access to variables and other elements of a separate
device (or a separate internal component inside one device).  The elements
appear within the URFP server lists of the device running the proxy.

During initialization, the proxy acquires the lists and further details such
as data types, function arguments and result structures for each element provided
by the separate devices and extends its own element list with proxy handlers for
them.

The access to the other device may use another transport then the access to the
proxy. This way, a device that can be reached via HTTP can allow access to 
elements of a component inside connected via Tinypacket.

For an example (not fully platform-independent) implementation, see [C code](@ref urfp_proxy.c)

## Limits {#urfp_proxy_limits}

The proxy may limit the size and complexity of requests so that not functionality
of the separate device can be actually used. For example, the maximum length of
strings may be limited to a shorter length than supported by the primary server.
Calling functions that take some time may lead to timeout errors in the proxy.

