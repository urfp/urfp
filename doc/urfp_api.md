# URFP API and C library {#urfp_api_intro}

## Specification aim {#urfp_api_aim}


The API specification and C library are meant to help to structure and 
implement device functionality and access to it via communication interfaces.

One one side, it provides a programming interface for outputting information
and execution of device functions with clearly defined modalities via standard
server ("elements").

On the other side, the transport side, it connects to the low level transports
for actual physical transmission of data.


## Application Interface {#urfp_api_application}

### Get and put methods {#urfp_api_get_put}

Principle: Provide functions for sending basic data (put) and receiving input (get), directed by
a "code" and glued with a "tag" in a so-called [transaction environment](urfp_api_environment).

Both server and client use the same basic functions for putting and getting data: 
``urfp_put`` for output, ``urfp_get`` for input. A proxy (server and client at the same time)
receives data from one end with ``urfp_get`` and forwards it to the other end with ``urfp_put``.

URFP client or server implementations that do not make use of this API of course can provide
other methods to hand over the data.

### Transaction Environment {#urfp_api_environemnt}

An environment provides context for one group of ``urfp_put`` and ``urfp_get``, ie. bidirectional
communication with one single peer. At the beginning of a request, the initiator allocates a
new environment with the desired "code". This environment will be used to put
all arguments to the server and then get all results back from the server. 

On the server side, an environment is allocated when the server starts receiving the request. It then
branches depending on the code to custom handler code which uses ``urfp_get`` methods to get its
arguments and ``urfp_put`` to output results.

## Examples {#urfp_api_examples}

@todo Examples for implementing a "variable" or "function" with ``get*arg/end_get/put/end_put`` and with continuation

@todo Example for gatewaying

## Transport Interface {#urfp_api_transport}

 - Low Level Input (specific to each transport)
    - Packets => urfp_tinypacket_input() =>  Standard Server
    - Char by char => urfp_console_input_char(c) => Standard Server
    - HTTP requests => prepare_urfp_reply()/process_urfp_reply() => Standard Server
 - URFP Standard Server (independent of transport)
    - Allocates environment (context)
    - urfp_dispatch_query() calls handlers that implement device functions
    - urfp_resume_get(), urfp_finish_get(), urfp_resume_put() to handle requests that come in fragments
 - Device functions (independent of transport)
    - use [get and put methods](urfp_api_get_put) to read arguments and output results
 - URFP API functions (specific to each transport)
    - Packets => created by urfp_append_output()
    - Char by char => lines created by urfp_output_char() and urfp_output_string()
    - HTTP => response headers, JSON text, response end

# Basic functions for application side {#urfp_api_methods}

## Basic functions in C library {#urfp_api_methods_basic}

 - get
 - get_end
 - put
 - put_end

## Concepts for implementing server elements {#urfp_api_elem}

@todo Implementation of elements in C using the functions provided by urfp_api.c

Concepts

 - Transition between get <=> put
 - dry_run (help)

 - set var
 - get var
 - call func
 - read blob
 - write blob
 - data??

Client: Send code, put arguments, get results

Server: Dependent on code, read first argument as element ID, branch to handlers

Handler: Get further arguments (evtl. in "dry run"), then act upon (unless in "dry run"), output results


## Advanced methods {#urfp_api_methods_advanced}

## Getting help about elements {#urfp_api_methods_help}

The Standard Server calls device function handlers with "dry_run" flag for just asking The m about
their expected arguments (with names and data type) and what results will be output when actually called.

The functions may execute just the same code as without "dry_run", using ``urfp_get`` and ``urfp_put`` to
retrieve arguments or output results. However, these methons during "dry_run" will output information about
name, data type and size instead of actual data.

In such context, the handler shall avoid to make any delays or actual activity with real effects. For
example a "reboot" handler should not actually perform a reboot.

## Formatted Output {#urfp_api_methods_format}

A function for formatted output takes multiple values (as varargs) and calls ``urfp_put`` for each of
them, according to a specification in a format string just like printf() does.

## Continuation {#urfp_api_methods_cont}

A device function handler may be designed in a way so that Standard Server has to call it several times
until it is completely done. This can be useful if not all arguments (input) are available at once or
if not all result data can be output at once, e.g. because it is a large amount of data that doesn't fit
into a contiguous buffer in memory or because some data is only available after a delay.

Such handler doesn't end with urfp_put_end() but instead returns another handler for the additional
work to do as a callback function to the server, to be called later:

 - urfp_get_backgrounded()
 - urfp_put_backgrounded()

On transport side this is supported by the "resume" function, to be called by the low level transport
driver when new arguments have been received resp when there is more space available for further output.

## Binary Large Objects (BLOBs) {#urfp_api_methods_blob}

For exchange of large amounts of binary data, such as firmware updates, there is a similar concept with 
callbacks for further data as with [continuation](#urfp_api_methods_cont)


 - write blob from outside
 - for receiving data chunks in the device:
   - urfp_get_blob_access()
   - urfp_get_blob_pause()
   - urfp_get_blob_end()

# Miscellaneous helper functions {#urfp_api_misc}

 - UTF-8 syntax checks
 - HTTP request and response parser
 - HTTP multipart decoder
 - Base64 encoder and decoder

# Basic functions for transport side {#urfp_api_transport}

 - C Library Funktionen for various transports
   - HTTP
   - HTTP server
   - HTTP parser
   - Tinypacket
   - Console
   - Proxy
 - urfp_dispatch_query
 - resume_get
 - finish_get
 - resume_put
 - finish_put

@defgroup urfp_api_c URFP API
@defgroup urfp_server URFP Standard Server

# Proxy functions {#urfp_api_proxy}

# Other programming languages {#urfp_api_otherlang}

 - Python Module
 - JavaScript URFP Web-Application
 - C++ ScanAPI

