
# Console {#urfp_proto_console}

For lowest level details, see [C code](@ref urfp_console.c)

Well suited for serial transmission with UART or in TELNET or SSH sessions

Formatted as lines with ASCII encoding. Numbers are transmitted in decimal.

Responses can span over multiple lines (input is limited to a single line).

Passing an unique tag and checksum is optional for requests (so it can be made
by humans in an interactive session without a calculator at hand).

Output of checksum after results is also optional (and not yet implemented anywhere,
but planned).

Comfort for human interaction: Line editing, reshowing of half-entered lines when
the input was interrupted by OOB output from server side, input history, tab completion?


