# URFP Client API {#urfp_client}

## Recommendations {#urfp_client_recommended}

### Languages
 - Python, e.g. http://gitlab.com/urfp/urfp-python
 - Javascript (for Web applications, unfortunately without support for UDP data reception)
 - C#
 - C++

In C or C++, the [URFP API and C library](#urfp_api_intro) could be used not only
for server side but also for client side application.

### Transport independence

On the upper layer, the API should look the same, independent of the transport
(HTTP, Tinypacket etc) used below. Wherever possible, the code should be designed
so that transport can be exchanged with another one.


### Element specification

It should be avoided to fetch all elements with all their details from the server
unless really needed, as it may trigger unwanted side effects and takes time.

Typically, only a few elements are really accessed and only these should be
fetched en detail, and failures triggered by access to unneeded elements should
not hinder access to the wanted elements.

Another method to offload such initial burden from a server and a way to cope
with servers that do not even implement the help method would be an option to
store and load element specification (name, IDs, type information) from a local
config file.


### Functional


```
u = new_urfp_client_instance(...)
error = urfp_var_get(u, type, "name", &value))
error = urfp_var_get_array(u, type, "name", &values, &length))
urfp_var_set_int
urfp_var_id
urfp_var_name
urfp_var_type
urfp_var_description_get
...
```

### Object oriented

Error conditions should preferably be communicated as Exceptions, not return values

```
t = transport(...)
u = urfp(t)

value = u.get_variable(name)
u.set_variable(name, value)

    -or-
value = u.variables.name
u.variables.name = value

    -or-
value = u.variables.name.get()
u.variables.name.set(value)
```

## Client-side tools

 - generic variable access
 - backup and restore of elements
 - universal data source reader / graph

## How-to

